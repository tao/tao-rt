# Check for Andor SDK availability.
AC_DEFUN([TAO_CHECK_ANDOR], [
    AC_ARG_ENABLE([andor],
        AS_HELP_STRING([--enable-andor[[=yes|no|check|DIR]]],
            [support for Andor cameras @<:@default=check@:>@]),
        [enable_andor=$enableval], [enable_andor=check])
    if test "x$enable_andor" = x; then
        AC_MSG_ERROR([empty argument for --enable-andor is not allowed])
    fi
    AC_MSG_CHECKING([for Andor SDK])
    ANDOR_INCDIR=
    ANDOR_LIBDIR=
    use_andor=no
    # usage: search_andor dir
    search_andor() {
        if test $use_andor = no \
                -a -f "[$]1/include/atcore.h" \
                -a -r "[$]1/lib/libatcore.so"; then
            ANDOR_INCDIR="[$]1/include"
            ANDOR_LIBDIR="[$]1/lib"
            AC_MSG_RESULT([found in $ANDOR_INCDIR and $ANDOR_LIBDIR])
            use_andor=yes
        fi
    }
    case "${enable_andor}" in
        no )
            AC_MSG_RESULT([no])
            ;;
        yes | check )
            # First search in EXEC_PREFIX, then in PREFIX, then in a list of
            # predefined directories.
            if test $use_andor = no -a x"$exec_prefix" != xNONE; then
                search_andor "$exec_prefix"
                search_andor "$exec_prefix/andor"
                search_andor "$exec_prefix/libexec/andor"
            fi
            if test $use_andor = no -a x"$prefix" != xNONE; then
                search_andor "$prefix"
                search_andor "$prefix/andor"
                search_andor "$prefix/libexec/andor"
            fi
            if test $use_andor = no; then
                search_andor /opt/andor
                search_andor /usr/local
                search_andor /usr/local/andor
                search_andor /usr/local/libexec/andor
                search_andor /apps
                search_andor /apps/andor
                search_andor /apps/libexec/andor
            fi
            if test $use_andor = no; then
                AC_MSG_RESULT([not found])
                if test $enable_andor = yes; then
                    AC_MSG_ERROR([Andor SDK not found])
                fi
            fi
            ;;
        * )
            dir="${enable_andor}"
            search_andor "$dir"
            if test $use_andor = no; then
                AC_MSG_RESULT([not found in $dir])
                AC_MSG_ERROR([Andor SDK not found, --enable-andor argument should be "yes", "check", "no" or the directory containing "include/atcore.h" and "lib/libatcore.so"])
            fi
            ;;
    esac
    if test "$use_andor" = yes; then
        ANDOR_DEFS="-I'\$(ANDOR_INCDIR)'"
        ANDOR_LIBS="-L'\$(ANDOR_LIBDIR)' -Wl,-rpath,'\$(ANDOR_LIBDIR)' -latcore"
    else
        ANDOR_DEFS=
        ANDOR_LIBS=
    fi
    AM_CONDITIONAL([USE_ANDOR], [test "$use_andor" = yes])
    AC_SUBST([ANDOR_INCDIR], ["${ANDOR_INCDIR}"])
    AC_SUBST([ANDOR_LIBDIR], ["${ANDOR_LIBDIR}"])
    AC_SUBST([ANDOR_DEFS], ["${ANDOR_DEFS}"])
    AC_SUBST([ANDOR_LIBS], ["${ANDOR_LIBS}"])
])
