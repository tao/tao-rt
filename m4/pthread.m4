# Check for functions in POSIX threads library.
AC_DEFUN([TAO_CHECK_PTHREAD_FUNCS],[
    AC_CACHE_CHECK(
        [whether pthread_mutex_timedlock is provided by POSIX Threads Library],
        [tao_cv_have_pthread_mutex_timedlock],
        [_TAO_CHECK_PTHREAD_MUTEX_TIMEDLOCK(
            [tao_cv_have_pthread_mutex_timedlock=yes],
            [tao_cv_have_pthread_mutex_timedlock=no])])
    if test x"$tao_cv_have_pthread_mutex_timedlock" = xyes; then
        AC_DEFINE([HAVE_PTHREAD_MUTEX_TIMEDLOCK], 1,
            [Define if pthread_mutex_timedlock is provided by POSIX Threads Library.])
    fi
    AC_CACHE_CHECK(
        [whether pthread_rwlock_timedrdlock is provided by POSIX Threads Library],
        [tao_cv_have_pthread_rwlock_timedrdlock],
        [_TAO_CHECK_PTHREAD_RWLOCK_TIMEDRDLOCK(
            [tao_cv_have_pthread_rwlock_timedrdlock=yes],
            [tao_cv_have_pthread_rwlock_timedrdlock=no])])
    if test x"$tao_cv_have_pthread_rwlock_timedrdlock" = xyes; then
        AC_DEFINE([HAVE_PTHREAD_RWLOCK_TIMEDRDLOCK], 1,
            [Define if pthread_rwlock_timedrdlock is provided by POSIX Threads Library.])
    fi
    AC_CACHE_CHECK(
        [whether pthread_rwlock_timedwrlock is provided by POSIX Threads Library],
        [tao_cv_have_pthread_rwlock_timedwrlock],
        [_TAO_CHECK_PTHREAD_RWLOCK_TIMEDWRLOCK(
            [tao_cv_have_pthread_rwlock_timedwrlock=yes],
            [tao_cv_have_pthread_rwlock_timedwrlock=no])])
    if test x"$tao_cv_have_pthread_rwlock_timedwrlock" = xyes; then
        AC_DEFINE([HAVE_PTHREAD_RWLOCK_TIMEDWRLOCK], 1,
            [Define if pthread_rwlock_timedwrlock is provided by POSIX Threads Library.])
    fi
    ]) # TAO_CHECK_PTHREAD_FUNCS

AC_DEFUN([_TAO_CHECK_PTHREAD_MUTEX_TIMEDLOCK], [
    tao_save_CFLAGS="$CFLAGS"
    tao_save_CPPFLAGS="$CPPFLAGS"
    tao_save_LIBS="$LIBS"
    CFLAGS="$CFLAGS"
    LIBS="$PTHREADLIB"
    AC_LANG_PUSH([C])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([[
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
        ]],[[
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    struct timespec abstime;
    if (clock_gettime(CLOCK_REALTIME, &abstime) == 0) {
        abstime.tv_sec += 1;
        if (pthread_mutex_timedlock(&mutex, &abstime) == 0) {
            if (pthread_mutex_unlock(&mutex) == 0) {
                return EXIT_SUCCESS;
            }
        }
    }
    return EXIT_SUCCESS;
    ]])],[$1],[$2])
    AC_LANG_POP([C])
    CFLAGS="$tao_save_CFLAGS"
    CPPFLAGS="$tao_save_CPPFLAGS"
    LIBS="$tao_save_LIBS"])

AC_DEFUN([_TAO_CHECK_PTHREAD_RWLOCK_TIMEDRDLOCK], [
    tao_save_CFLAGS="$CFLAGS"
    tao_save_CPPFLAGS="$CPPFLAGS"
    tao_save_LIBS="$LIBS"
    CFLAGS="$CFLAGS"
    LIBS="$PTHREADLIB"
    AC_LANG_PUSH([C])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([[
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
        ]],[[
    pthread_rwlock_t rwlock = PTHREAD_RWLOCK_INITIALIZER;
    struct timespec abstime;
    if (clock_gettime(CLOCK_REALTIME, &abstime) == 0) {
        abstime.tv_sec += 1;
        if (pthread_rwlock_timedrdlock(&rwlock, &abstime) == 0) {
            if (pthread_rwlock_unlock(&rwlock) == 0) {
                return EXIT_SUCCESS;
            }
        }
    }
    return EXIT_SUCCESS;
    ]])],[$1],[$2])
    AC_LANG_POP([C])
    CFLAGS="$tao_save_CFLAGS"
    CPPFLAGS="$tao_save_CPPFLAGS"
    LIBS="$tao_save_LIBS"])

AC_DEFUN([_TAO_CHECK_PTHREAD_RWLOCK_TIMEDWRLOCK], [
    tao_save_CFLAGS="$CFLAGS"
    tao_save_CPPFLAGS="$CPPFLAGS"
    tao_save_LIBS="$LIBS"
    CFLAGS="$CFLAGS"
    LIBS="$PTHREADLIB"
    AC_LANG_PUSH([C])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([[
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
        ]],[[
    pthread_rwlock_t rwlock = PTHREAD_RWLOCK_INITIALIZER;
    struct timespec abstime;
    if (clock_gettime(CLOCK_REALTIME, &abstime) == 0) {
        abstime.tv_sec += 1;
        if (pthread_rwlock_timedwrlock(&rwlock, &abstime) == 0) {
            if (pthread_rwlock_unlock(&rwlock) == 0) {
                return EXIT_SUCCESS;
            }
        }
    }
    return EXIT_SUCCESS;
    ]])],[$1],[$2])
    AC_LANG_POP([C])
    CFLAGS="$tao_save_CFLAGS"
    CPPFLAGS="$tao_save_CPPFLAGS"
    LIBS="$tao_save_LIBS"])
