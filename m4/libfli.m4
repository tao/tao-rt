# Check for Finger Lakes Instrumentation (LIBFLI) SDK availability.
AC_DEFUN([TAO_CHECK_LIBFLI], [
    AC_ARG_ENABLE([libfli],
        AS_HELP_STRING([--enable-libfli[[=yes|no|check|DIR]]],
            [support for Finger Lakes Instrumentation cameras via LIBFLI SDK @<:@default=check@:>@]),
        [enable_libfli=$enableval], [enable_libfli=check])
    if test "x$enable_libfli" = x; then
        AC_MSG_ERROR([empty argument for --enable-libfli is not allowed])
    fi
    AC_MSG_CHECKING([for LIBFLI SDK])
    LIBFLI_INCDIR=
    LIBFLI_LIBDIR=
    use_libfli=no
    # usage: search_libfli dir
    search_libfli() {
        if test $use_libfli = no; then
            if test -f "[$]1/include/libfli.h" \
                 -a -r "[$]1/lib/libfli.so"; then
                LIBFLI_INCDIR="[$]1/include"
                LIBFLI_LIBDIR="[$]1/lib"
                use_libfli=yes
            elif test -f "[$]1/libfli.h" \
                   -a -r "[$]1/libfli.so"; then
                LIBFLI_INCDIR="[$]1"
                LIBFLI_LIBDIR="[$]1"
                use_libfli=yes
            fi
            if test $use_libfli = yes; then
                AC_MSG_RESULT([found in $LIBFLI_INCDIR and $LIBFLI_LIBDIR])
            fi
        fi
    }
    case "${enable_libfli}" in
        no )
            AC_MSG_RESULT([no])
            ;;
        yes | check )
            # First search in EXEC_PREFIX, then in PREFIX, then in a list of
            # predefined directories.
            if test $use_libfli = no -a x"$exec_prefix" != xNONE; then
                search_libfli "$exec_prefix"
                search_libfli "$exec_prefix/libfli"
                search_libfli "$exec_prefix/libexec/libfli"
            fi
            if test $use_libfli = no -a x"$prefix" != xNONE; then
                search_libfli "$prefix"
                search_libfli "$prefix/libfli"
                search_libfli "$prefix/libexec/libfli"
            fi
            if test $use_libfli = no; then
                search_libfli /opt/libfli
                search_libfli /usr/local
                search_libfli /usr/local/libfli
                search_libfli /usr/local/libexec/libfli
                search_libfli /apps
                search_libfli /apps/libfli
                search_libfli /apps/libexec/libfli
            fi
            if test $use_libfli = no; then
                AC_MSG_RESULT([not found])
                if test $enable_libfli = yes; then
                    AC_MSG_ERROR([Finger Lakes Instrumentation LIBFLI SDK not found])
                fi
            fi
            ;;
        * )
            dir="${enable_libfli}"
            search_libfli "$dir"
            if test $use_libfli = no; then
                AC_MSG_RESULT([not found in $dir])
                AC_MSG_ERROR([Finger Lakes Instrumentation not found, --enable-libfli argument should be "yes", "check", "no" or the directory containing "libfli.h" and "libfli.so"])
            fi
            ;;
    esac
    if test "$use_libfli" = yes; then
        LIBFLI_DEFS="-I'\$(LIBFLI_INCDIR)'"
        LIBFLI_LIBS="-L'\$(LIBFLI_LIBDIR)' -Wl,-rpath,'\$(LIBFLI_LIBDIR)' -lfli"
    else
        LIBFLI_DEFS=
        LIBFLI_LIBS=
    fi
    AM_CONDITIONAL([USE_LIBFLI], [test "$use_libfli" = yes])
    AC_SUBST([LIBFLI_INCDIR], ["${LIBFLI_INCDIR}"])
    AC_SUBST([LIBFLI_LIBDIR], ["${LIBFLI_LIBDIR}"])
    AC_SUBST([LIBFLI_DEFS], ["${LIBFLI_DEFS}"])
    AC_SUBST([LIBFLI_LIBS], ["${LIBFLI_LIBS}"])
])
