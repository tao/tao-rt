# Check for Finger Lakes Instrumentation (LIBFLIPRO) SDK availability.
AC_DEFUN([TAO_CHECK_LIBFLIPRO], [
    AC_ARG_ENABLE([libflipro],
        AS_HELP_STRING([--enable-libflipro[[=yes|no|check|DIR]]],
            [support for Finger Lakes Instrumentation cameras via LIBFLIPRO SDK @<:@default=check@:>@]),
        [enable_libflipro=$enableval], [enable_libflipro=check])
    if test "x$enable_libflipro" = x; then
        AC_MSG_ERROR([empty argument for --enable-libflipro is not allowed])
    fi
    AC_MSG_CHECKING([for LIBFLIPRO SDK])
    LIBFLIPRO_INCDIR=
    LIBFLIPRO_LIBDIR=
    use_libflipro=no
    # usage: search_libflipro dir
    search_libflipro() {
        if test $use_libflipro = no; then
            if test -f "[$]1/include/libflipro.h" \
                 -a -r "[$]1/lib/liblibflipro.so"; then
                LIBFLIPRO_INCDIR="[$]1/include"
                LIBFLIPRO_LIBDIR="[$]1/lib"
                use_libflipro=yes
            elif test -f "[$]1/libflipro.h" \
                   -a -r "[$]1/liblibflipro.so" \
                   -a -r "[$]1/liblibflialgo.so"; then
                LIBFLIPRO_INCDIR="[$]1"
                LIBFLIPRO_LIBDIR="[$]1"
                use_libflipro=yes
            fi
            if test $use_libflipro = yes; then
                AC_MSG_RESULT([found in $LIBFLIPRO_INCDIR and $LIBFLIPRO_LIBDIR])
            fi
        fi
    }
    case "${enable_libflipro}" in
        no )
            AC_MSG_RESULT([no])
            ;;
        yes | check )
            # First search in EXEC_PREFIX, then in PREFIX, then in a list of
            # predefined directories.
            if test $use_libflipro = no -a x"$exec_prefix" != xNONE; then
                search_libflipro "$exec_prefix"
                search_libflipro "$exec_prefix/libflipro"
                search_libflipro "$exec_prefix/libexec/libflipro"
            fi
            if test $use_libflipro = no -a x"$prefix" != xNONE; then
                search_libflipro "$prefix"
                search_libflipro "$prefix/libflipro"
                search_libflipro "$prefix/libexec/libflipro"
            fi
            if test $use_libflipro = no; then
                search_libflipro /opt/libflipro
                search_libflipro /usr/local
                search_libflipro /usr/local/libflipro
                search_libflipro /usr/local/libexec/libflipro
                search_libflipro /apps
                search_libflipro /apps/libflipro
                search_libflipro /apps/libexec/libflipro
            fi
            if test $use_libflipro = no; then
                AC_MSG_RESULT([not found])
                if test $enable_libflipro = yes; then
                    AC_MSG_ERROR([Finger Lakes Instrumentation LIBFLIPRO SDK not found])
                fi
            fi
            ;;
        * )
            dir="${enable_libflipro}"
            search_libflipro "$dir"
            if test $use_libflipro = no; then
                AC_MSG_RESULT([not found in $dir])
                AC_MSG_ERROR([Finger Lakes Instrumentation not found, --enable-libflipro argument should be "yes", "check", "no" or the directory containing "libflipro.h" and "liblibflipro.so"])
            fi
            ;;
    esac
    if test "$use_libflipro" = yes; then
        LIBFLIPRO_DEFS="-I'\$(LIBFLIPRO_INCDIR)'"
        LIBFLIPRO_LIBS="-L'\$(LIBFLIPRO_LIBDIR)' -Wl,-rpath,'\$(LIBFLIPRO_LIBDIR)' -llibflipro -llibflialgo"
    else
        LIBFLIPRO_DEFS=
        LIBFLIPRO_LIBS=
    fi
    AM_CONDITIONAL([USE_LIBFLIPRO], [test "$use_libflipro" = yes])
    AC_SUBST([LIBFLIPRO_INCDIR], ["${LIBFLIPRO_INCDIR}"])
    AC_SUBST([LIBFLIPRO_LIBDIR], ["${LIBFLIPRO_LIBDIR}"])
    AC_SUBST([LIBFLIPRO_DEFS], ["${LIBFLIPRO_DEFS}"])
    AC_SUBST([LIBFLIPRO_LIBS], ["${LIBFLIPRO_LIBS}"])
])
