# Check for Boston Micromachines SDK availability.
AC_DEFUN([TAO_CHECK_BMC], [
    AC_ARG_ENABLE([bmc],
        AS_HELP_STRING([--enable-bmc[[=yes|no|check|DIR]]],
            [support for Boston Micromachines SDK @<:@default=check@:>@]),
        [enable_bmc=$enableval], [enable_bmc=check])
    if test "x$enable_bmc" = x; then
        AC_MSG_ERROR([empty argument for --enable-bmc is not allowed])
    fi
    AC_MSG_CHECKING([for Boston Micromachines SDK])
    BMC_INCDIR=
    BMC_LIBDIR=
    use_bmc=no
    # usage: search_bmc INCDIR LIBDIR
    search_bmc() {
        if test $use_bmc = no \
                -a -f "[$]1/BMCApi.h" \
                -a -f "[$]1/BMCDefs.h" \
                -a -r "[$]2/libBMC.so"; then
            BMC_INCDIR="[$]1"
            BMC_LIBDIR="[$]2"
            AC_MSG_RESULT([found in $BMC_INCDIR and $BMC_LIBDIR])
            use_bmc=yes
        fi
    }
    case "${enable_bmc}" in
        no )
            AC_MSG_RESULT([no])
            ;;
        yes | check )
            # First search in EXEC_PREFIX, then in PREFIX, then in a list of
            # predefined directories.
            if test $use_bmc = no; then
                bmc_dir_list="/opt /local /apps/libexec"
                if test "x$exec_prefix" != xNONE; then
                    bmc_dir_list="$exec_prefix $bmc_dir_list"
                fi
                if test "x$prefix" != xNONE; then
                    bmc_dir_list="$prefix $prefix/libexec $bmc_dir_list"
                fi
                for bmc_dir1 in $bmc_dir_list; do
                    for bmc_dir2 in "Boston Micromachines" \
                        "Boston_Micromachines" "bmc"; do
                        bmc_dir="$bmc_dir1/$bmc_dir2"
                        search_bmc "$bmc_dir/include" "$bmc_dir/lib"
                    done
                done
            fi
            if test $use_bmc = no; then
                AC_MSG_RESULT([not found])
                if test $enable_bmc = yes; then
                    AC_MSG_ERROR([Boston Micromachines SDK not found])
                fi
            fi
            ;;
        * )
            dir="${enable_bmc}"
            search_bmc "$dir/include" "$dir/lib"
            if test $use_bmc = no; then
                AC_MSG_RESULT([not found in $dir])
                AC_MSG_ERROR([Boston Micromachines SDK not found, --enable-bmc argument should be "yes", "check", "no" or the directory containing "include/BMCApi.h" and "lib/libBMC.so"])
            fi
            ;;
    esac
    if test "$use_bmc" = yes; then
        BMC_DEFS="-I'\$(BMC_INCDIR)'"
        BMC_LIBS="-L'\$(BMC_LIBDIR)' -Wl,-rpath,'\$(BMC_LIBDIR)' -lBMC -lBMC_PCIeAPI -lBMC_USBAPI -llog4cxx"
    else
        BMC_DEFS=
        BMC_LIBS=
    fi
    AM_CONDITIONAL([USE_BMC], [test "$use_bmc" = yes])
    AC_SUBST([BMC_INCDIR], ["${BMC_INCDIR}"])
    AC_SUBST([BMC_LIBDIR], ["${BMC_LIBDIR}"])
    AC_SUBST([BMC_DEFS], ["${BMC_DEFS}"])
    AC_SUBST([BMC_LIBS], ["${BMC_LIBS}"])
])
