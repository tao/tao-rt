# Check for NüVü SDK.
AC_DEFUN([TAO_CHECK_NUVU], [
    AC_ARG_ENABLE([nuvu],
        AS_HELP_STRING([--enable-nuvu[[=yes|no|check|DIR]]],
            [support for NüVü SDK @<:@default=check@:>@]),
        [enable_nuvu=$enableval], [enable_nuvu=check])
    if test "x$enable_nuvu" = x; then
        AC_MSG_ERROR([empty argument for --enable-nuvu is not allowed])
    fi
    AC_MSG_CHECKING([for NüVü SDK])
    NUVU_INCDIR=
    NUVU_LIBDIR=
    use_nuvu=no
    # usage: search_nuvu INCDIR LIBDIR
    search_nuvu() {
        if test $use_nuvu = no \
                -a -f "[$]1/nc_driver.h" \
                -a -r "[$]2/libnuvu.so"; then
            NUVU_INCDIR="[$]1"
            NUVU_LIBDIR="[$]2"
            AC_MSG_RESULT([found in $NUVU_INCDIR and $NUVU_LIBDIR])
            use_nuvu=yes
        fi
    }
    case "${enable_nuvu}" in
        no )
            AC_MSG_RESULT([no])
            ;;
        yes | check )
            # First search in EXEC_PREFIX, then in PREFIX, then in a list of
            # predefined directories.
            if test $use_nuvu = no; then
                nuvu_dir_list="/opt /local /apps/libexec"
                if test "x$exec_prefix" != xNONE; then
                    nuvu_dir_list="$exec_prefix $nuvu_dir_list"
                fi
                if test "x$prefix" != xNONE; then
                    nuvu_dir_list="$prefix $prefix/libexec $nuvu_dir_list"
                fi
                for nuvu_dir1 in $nuvu_dir_list; do
                    for nuvu_dir2 in "NuvuCameras"; do
                        nuvu_dir="$nuvu_dir1/$nuvu_dir2"
                        search_nuvu "$nuvu_dir/include" "$nuvu_dir/lib"
                    done
                done
            fi
            if test $use_nuvu = no; then
                AC_MSG_RESULT([not found])
                if test $enable_nuvu = yes; then
                    AC_MSG_ERROR([NüVü SDK not found])
                fi
            fi
            ;;
        * )
            dir="${enable_nuvu}"
            search_nuvu "$dir/include" "$dir/lib"
            if test $use_nuvu = no; then
                AC_MSG_RESULT([not found in $dir])
                AC_MSG_ERROR([NüVü SDK not found, --enable-nuvu argument should be "yes", "check", "no" or the directory containing "include/nc_driver.h" and "lib/libnuvu.so"])
            fi
            ;;
    esac
    if test "$use_nuvu" = yes; then
        NUVU_DEFS="-I'\$(NUVU_INCDIR)'"
        NUVU_LIBS="-L'\$(NUVU_LIBDIR)' -Wl,-rpath,'\$(NUVU_LIBDIR)' -lnuvu"
    else
        NUVU_DEFS=
        NUVU_LIBS=
    fi
    AM_CONDITIONAL([USE_NUVU], [test "$use_nuvu" = yes])
    AC_SUBST([NUVU_INCDIR], ["${NUVU_INCDIR}"])
    AC_SUBST([NUVU_LIBDIR], ["${NUVU_LIBDIR}"])
    AC_SUBST([NUVU_DEFS], ["${NUVU_DEFS}"])
    AC_SUBST([NUVU_LIBS], ["${NUVU_LIBS}"])
])
