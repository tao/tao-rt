# Check for Yorick.
AC_DEFUN([TAO_CHECK_YORICK], [
    AC_ARG_ENABLE([yorick],
        AS_HELP_STRING([--enable-yorick[[=yes|no|check|PATH]]],
            [support for Yorick @<:@default=check@:>@]),
        [enable_yorick=$enableval], [enable_yorick=check])
    if test "x$enable_yorick" = x; then
        AC_MSG_ERROR([empty argument for --enable-yorick is not allowed])
    fi
    yorick_exe=yorick
    case "${enable_yorick}" in
        no )
            use_yorick=disabled
            ;;
        yes | check )
            use_yorick=try
            ;;
        * )
            use_yorick=try
            yorick_exe="${enable_yorick}"
            ;;
    esac
    AC_MSG_CHECKING([for Yorick executable])
    if test ${use_yorick} = try; then
        use_yorick=no
        echo >conftest.i "nl = strchar(char(0x0a)); write, format=\"Y_HOME=%s\"+nl+\"Y_SITE=%s\"+nl+\"Y_EXE=%s\"+nl, Y_HOME, Y_SITE, get_argv()(1); quit;"
        if "$yorick_exe" -batch conftest.i >conftest.out 2>/dev/null; then
            YORICK_HOME=$(sed <conftest.out -e '/^Y_HOME=/!d;s/^Y_HOME=//')
            YORICK_SITE=$(sed <conftest.out -e '/^Y_SITE=/!d;s/^Y_SITE=//')
            YORICK_EXE=$(sed <conftest.out -e '/^Y_EXE=/!d;s/^Y_EXE=//')
            if test -d "$YORICK_HOME" -a -d "$YORICK_SITE" \
                    -a -x "$YORICK_EXE"; then
                use_yorick=yes
            fi
        fi
        rm -f conftest.i conftest.out
    fi
    AC_MSG_RESULT([$use_yorick])
    if test $use_yorick != yes \
            -a $enable_yorick != no \
            -a $enable_yorick != check; then
       AC_MSG_FAILURE([failed to execute simple Yorick script with "$yorick_exe"])
    fi
    AM_CONDITIONAL([USE_YORICK], [test "$use_yorick" = yes])
    AC_SUBST([YORICK_EXE], [$YORICK_EXE])
    AC_SUBST([YORICK_MAKEDIR], [$YORICK_HOME])
    AC_SUBST([YORICK_HOME], [$YORICK_HOME])
    AC_SUBST([YORICK_SITE], [$YORICK_SITE])
])
