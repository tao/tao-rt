# Enable FITS support via CFITSIO library.
AC_DEFUN([TAO_CHECK_CFITSIO], [
    AC_ARG_WITH([cfitsio-defs],
        AS_HELP_STRING([--with-cfitsio-defs=FLAGS],
            [Pre-processor flags to compile with CFITSIO headers @<:@default=@:>@]),
            [with_cfitsio_defs=$withval], [with_cfitsio_defs=])
    AC_ARG_WITH([cfitsio-libs],
        AS_HELP_STRING([--with-cfitsio-libs=FLAGS],
            [Linker flags to link with CFITSIO library @<:@default=-lcfitsio@:>@]),
            [with_cfitsio_libs=$withval], [with_cfitsio_libs=])
    AC_ARG_ENABLE([cfitsio],
        AS_HELP_STRING([--enable-cfitsio[[=yes|no|check|force|DIR]]],
            [support for FITS files via CFITSIO library @<:@default=check@:>@]),
        [enable_cfitsio=$enableval], [enable_cfitsio=check])
    # Make sure options are coherent.
    if test "x$enable_cfitsio" = x; then
        AC_MSG_ERROR([empty argument for --enable-cfitsio is not allowed])
    fi
    CFITSIO_DEFS="$with_cfitsio_defs"
    CFITSIO_LIBS="$with_cfitsio_libs"
    case "${enable_cfitsio}" in
        no )
            use_cfitsio=disabled
            ;;
        yes | check )
            use_cfitsio=try
            ;;
        force )
            use_cfitsio=yes
            ;;
        * )
            use_cfitsio=try
            if test x"$with_cfitsio_defs" = x -a x"$with_cfitsio_libs" = x; then
                CFITSIO_DEFS="-I${enable_cfitsio}/include"
                CFITSIO_LIBS="-L${enable_cfitsio}/lib -lcfitsio"
            else
                AC_MSG_WARN([argument "$enable_cfitsio" of option --enable-cfitsio is ignored when options --with-cfitsio-defs and/or --with-cfitsio-libs are specified])
            fi
            ;;
    esac
    AC_MSG_CHECKING([for CFITSIO library])
    if test x"$CFITSIO_LIBS" = x; then
        CFITSIO_LIBS="-lcfitsio"
    fi
    if test $use_cfitsio = try; then
        ac_save_CFLAGS="$CFLAGS"
        ac_save_CPPFLAGS="$CPPFLAGS"
        ac_save_LIBS="$LIBS"
        CFLAGS="$CFLAGS $CFITSIO_DEFS"
        LIBS="$CFITSIO_LIBS"
        AC_LINK_IFELSE([AC_LANG_PROGRAM([[
#include <unistd.h>
#include <fitsio2.h>
        ]],[[
    int status = 0;
    char* filename = "conftest.fits";
    fitsfile* fptr;

    if (fits_create_file(&fptr, filename, &status) != 0) {
        return 1;
    }
    if (fits_close_file(fptr, &status) != 0) {
        return 1;
    }
    unlink(filename);
        ]])], use_cfitsio=yes, use_cfitsio=no)
        CFLAGS="$ac_save_CFLAGS"
        CPPFLAGS="$ac_save_CPPFLAGS"
        LIBS="$ac_save_LIBS"
    fi
    AC_MSG_RESULT([$use_cfitsio])
    if test $use_cfitsio = yes; then
        AC_DEFINE([TAO_USE_CFITSIO], [1],
            [Indicate whether TAO implements FITS support via the CFITSIO library.])
    else
        if test $enable_cfitsio != no \
                -a $enable_cfitsio != check; then
            AC_MSG_FAILURE([failed to link with CFITSIO library])
        fi
        CFITSIO_DEFS=
        CFITSIO_LIBS=
    fi

    AM_CONDITIONAL([USE_CFITSIO], [test "$use_cfitsio" = yes])
    AC_SUBST([CFITSIO_DEFS], ["${CFITSIO_DEFS}"])
    AC_SUBST([CFITSIO_LIBS], ["${CFITSIO_LIBS}"])
])
