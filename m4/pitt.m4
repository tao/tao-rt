# Check for Physical Instruments Tip-Tilt device.
AC_DEFUN([TAO_CHECK_PITT], [
    AC_ARG_ENABLE([pitt],
        AS_HELP_STRING([--enable-pitt[[=yes|no]]],
            [support for Physical Instruments Tip-Tilt device @<:@default=yes@:>@]),
        [enable_pitt=$enableval], [enable_pitt=yes])
    if test "x$enable_pitt" = x; then
        AC_MSG_ERROR([empty argument for --enable-pitt is not allowed])
    fi
    AC_MSG_CHECKING([for Physical Instruments Tip-Tilt support])
    PITT_DEFS=""
    PITT_LIBS=""
    use_pitt=no
    case "${enable_pitt}" in
        yes | check )
            # Nothing to do since no SDK is needed.
            use_pitt=yes
            ;;
        * )
    esac
    AC_MSG_RESULT([$use_pitt])

    AM_CONDITIONAL([USE_PITT], [test "$use_pitt" = yes])
    AC_SUBST([PITT_DEFS], ["${PITT_DEFS}"])
    AC_SUBST([PITT_LIBS], ["${PITT_LIBS}"])
])
