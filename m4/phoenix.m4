# Check for the availability of ActiveSilicon Phoenix libraries.
AC_DEFUN([TAO_CHECK_PHOENIX], [
    AC_ARG_ENABLE([phoenix],
        AS_HELP_STRING([--enable-phoenix[[=yes|no|check|DIR]]],
            [support for Phoenix cameras @<:@default=check@:>@]),
        [enable_phoenix=$enableval], [enable_phoenix=check])
    if test "x$enable_phoenix" = x; then
        AC_MSG_ERROR([empty argument for --enable-phoenix is not allowed])
    fi
    AC_MSG_CHECKING([for ActiveSilicon Phoenix libraries])
    PHOENIX_INCDIR=
    PHOENIX_LIBDIR=
    phoenix_dll=libphxapi-x86_64.so
    use_phoenix=no
    # usage: search_phoenix dir
    search_phoenix() {
        if test $use_phoenix = no -a -f "[$]1/include/phx_api.h"; then
            if test -r "[$]1/lib64/${phoenix_dll}"; then
                PHOENIX_LIBDIR="[$]1/lib64"
            elif test -r "[$]1/lib/${phoenix_dll}"; then
                PHOENIX_LIBDIR="[$]1/lib"
            else
                PHOENIX_LIBDIR=
            fi
            if test x"$PHOENIX_LIBDIR" != x; then
                PHOENIX_INCDIR="[$]1/include"
                AC_MSG_RESULT([found in $PHOENIX_INCDIR and $PHOENIX_LIBDIR])
                use_phoenix=yes
            fi
        fi
    }
    case "${enable_phoenix}" in
        no )
            AC_MSG_RESULT([no])
            ;;
        yes | check )
            # First search in EXEC_PREFIX, then in PREFIX, then in a list of
            # predefined directories.
            if test $use_phoenix = no -a x"$exec_prefix" != xNONE; then
                search_phoenix "$exec_prefix"
                search_phoenix "$exec_prefix/activesilicon"
                search_phoenix "$exec_prefix/libexec/activesilicon"
            fi
            if test $use_phoenix = no -a x"$prefix" != xNONE; then
                search_phoenix "$prefix"
                search_phoenix "$prefix/activesilicon"
                search_phoenix "$prefix/libexec/activesilicon"
            fi
            if test $use_phoenix = no; then
                search_phoenix /opt/activesilicon
                search_phoenix /usr/local
                search_phoenix /usr/local/activesilicon
                search_phoenix /usr/local/libexec/activesilicon
                search_phoenix /apps
                search_phoenix /apps/activesilicon
                search_phoenix /apps/libexec/activesilicon
            fi
            if test $use_phoenix = no; then
                AC_MSG_RESULT([not found])
                if test $enable_phoenix = yes; then
                    AC_MSG_ERROR([ActiveSilicon Phoenix libraries not found])
                fi
            fi
            ;;
        * )
            dir="${enable_phoenix}"
            search_phoenix "$dir"
            if test $use_phoenix = no; then
                AC_MSG_RESULT([not found in $dir])
                AC_MSG_ERROR([ActiveSilicon Phoenix libraries not found, --enable-phoenix argument should be "yes", "check", "no" or the directory containing "include/phx_api.h" and "lib64/${phoenix_dll}"])
            fi
            ;;
    esac
    if test "$use_phoenix" = yes; then
        PHOENIX_DEFS="-I'\$(PHOENIX_INCDIR)' -D_PHX_LINUX"
        PHOENIX_LIBS="-L'\$(PHOENIX_LIBDIR)'  -Wl,-rpath,'\$(PHOENIX_LIBDIR)' -lphxfb02-x86_64 -lphxapi-x86_64 -lphxbl-x86_64 -lphxil-x86_64 -lphxdl-x86_64"
        #PHOENIX_DLL="'\$(PHOENIX_LIBDIR)'/${phoenix_dll}"
        PHOENIX_DLL="${PHOENIX_LIBDIR}/${phoenix_dll}"
    else
        PHOENIX_DEFS=
        PHOENIX_LIBS=
        PHOENIX_DLL=
    fi
    AM_CONDITIONAL([USE_PHOENIX], [test "$use_phoenix" = yes])
    AC_SUBST([PHOENIX_INCDIR], ["${PHOENIX_INCDIR}"])
    AC_SUBST([PHOENIX_LIBDIR], ["${PHOENIX_LIBDIR}"])
    AC_SUBST([PHOENIX_DEFS], ["${PHOENIX_DEFS}"])
    AC_SUBST([PHOENIX_LIBS], ["${PHOENIX_LIBS}"])
    AC_SUBST([PHOENIX_DLL], ["${PHOENIX_DLL}"])
])
