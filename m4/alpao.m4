# Check for Alpao SDK availability.
AC_DEFUN([TAO_CHECK_ALPAO], [
    AC_ARG_ENABLE([alpao],
        AS_HELP_STRING([--enable-alpao[[=yes|no|check|DIR]]],
            [support for Alpao deformable mirrors @<:@default=check@:>@]),
        [enable_alpao=$enableval], [enable_alpao=check])
    if test "x$enable_alpao" = x; then
        AC_MSG_ERROR([empty argument for --enable-alpao is not allowed])
    fi
    AC_MSG_CHECKING([for Alpao SDK])
    ALPAO_INCDIR=
    ALPAO_LIBDIR=
    use_alpao=no
    # usage: search_alpao dir
    search_alpao() {
        if test $use_alpao = no -a -f "[$]1/include/asdkWrapper.h"; then \
            if test -f "[$]1/lib/x86_64-linux-gnu/libASDK.so" \
                 -o -f "[$]1/lib/x86_64-linux-gnu/libasdk.so"; then
                ALPAO_LIBDIR="[$]1/lib/x86_64-linux-gnu"
            elif test -f "[$]1/lib64/libASDK.so" \
                   -o -f "[$]1/lib64/libasdk.so"; then
                ALPAO_LIBDIR="[$]1/lib64"
            elif test -f "[$]1/lib/libASDK.so" \
                   -o -f "[$]1/lib/libasdk.so"; then
                ALPAO_LIBDIR="[$]1/lib"
            fi
            if test "x$ALPAO_LIBDIR" != x; then
               ALPAO_INCDIR="[$]1/include"
               AC_MSG_RESULT([found in $ALPAO_INCDIR and $ALPAO_LIBDIR])
               use_alpao=yes
            fi
        fi
    }
    case "${enable_alpao}" in
        no )
            AC_MSG_RESULT([no])
            ;;
        yes | check )
            # First search in EXEC_PREFIX, then in PREFIX, then in a list of
            # predefined directories.
            if test $use_alpao = no -a x"$exec_prefix" != xNONE; then
                search_alpao "$exec_prefix"
                search_alpao "$exec_prefix/alpao"
                search_alpao "$exec_prefix/libexec/alpao"
            fi
            if test $use_alpao = no -a x"$prefix" != xNONE; then
                search_alpao "$prefix"
                search_alpao "$prefix/alpao"
                search_alpao "$prefix/libexec/alpao"
            fi
            if test $use_alpao = no; then
                search_alpao /opt/alpao
                search_alpao /usr
                search_alpao /usr/local
                search_alpao /usr/local/alpao
                search_alpao /usr/local/libexec/alpao
                search_alpao /apps
                search_alpao /apps/alpao
                search_alpao /apps/libexec/alpao
            fi
            if test $use_alpao = no; then
                AC_MSG_RESULT([not found])
                if test $enable_alpao = yes; then
                    AC_MSG_ERROR([Alpao SDK not found])
                fi
            fi
            ;;
        * )
            dir="${enable_alpao}"
            search_alpao "$dir"
            if test $use_alpao = no; then
                AC_MSG_RESULT([not found in $dir])
                AC_MSG_ERROR([Alpao SDK not found, --enable-alpao argument should be "yes", "check", "no" or the directory containing "include/asdkWrapper.h" and "lib64/libasdk.so" or "lib/libasdk.so"])
            fi
            ;;
    esac
    if test "$use_alpao" = yes; then
        ALPAO_DEFS="-I'\$(ALPAO_INCDIR)'"
        if test -r "$ALPAO_LIBDIR/libASDK.so"; then
            ALPAO_LIBS="-L'\$(ALPAO_LIBDIR)' -Wl,-rpath,'\$(ALPAO_LIBDIR)' -lASDK"
        else
            ALPAO_LIBS="-L'\$(ALPAO_LIBDIR)' -Wl,-rpath,'\$(ALPAO_LIBDIR)' -lasdk"
        fi
    else
        ALPAO_DEFS=
        ALPAO_LIBS=
    fi
    AM_CONDITIONAL([USE_ALPAO], [test "$use_alpao" = yes])
    AC_SUBST([ALPAO_INCDIR], ["${ALPAO_INCDIR}"])
    AC_SUBST([ALPAO_LIBDIR], ["${ALPAO_LIBDIR}"])
    AC_SUBST([ALPAO_DEFS], ["${ALPAO_DEFS}"])
    AC_SUBST([ALPAO_LIBS], ["${ALPAO_LIBS}"])
])
