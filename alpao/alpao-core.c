// alpao-core.c -
//
// Implement a simple wrapper to handle Alpao deformable mirrors in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include "tao-alpao.h"
#include "tao-errors.h"
#include "tao-layouts.h"
#include "tao-generic.h"

#include <asdkWrapper.h>

#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

// MANAGEMENT OF ERRORS -------------------------------------------------------

// Print formatted warning message.
static void warn(const char* format, ...) TAO_FORMAT_PRINTF(1,2);

// The following list has been extracted (by hand) from <sdkErrNo.h>
#define _ERRLIST \
  _ERR(NoASDKError,       0x0000, "No error or warning in the stack") \
  _ERR(NoMoreAlloc,       0x0001, "Out of memory") \
  _ERR(TooManyDM,         0x0002, "Cannot handle more DM") \
  _ERR(NActMismatch,      0x0003, "All DM handled using MultiDM should have the same number of actuators") \
  _ERR(AlreadyLoad,       0x0004, "The configuration file is already loaded") \
  /* Send / Get / Set */ \
  _ERR(CommandNotFound,   0x0010, "Command not found (check spelling and access)") \
  _ERR(InvalidRange,      0x0011, "Parameter value not in range") \
  /* Initialisation */ \
  _ERR(CannotLoadDll,     0x0020, "Interface DLL cannot be loaded")   \
  _ERR(InvalidItfDll,     0x0021, "Interface DLL is not part of ADSK or corrupted") \
  _ERR(NoInterface,       0x0022, "No hardware interface is connected") \
  _ERR(NoCfgReader,       0x0023, "Default configuration reader cannot be found") \
  _ERR(CannotOpenCfg,     0x0024, "Cannot open configuration file") \
  _ERR(CannotReadCfg,     0x0025, "Cannot read configuration file") \
  _ERR(UnknowCmdCfg,      0x0026, "Invalid command in configuration file") \
  _ERR(CannotOpenACfg,    0x0027, "Cannot open the ASCII configuration file") \
  _ERR(UnknowCmdACfg,     0x0028, "Invalid command in ASCII configuration file") \
  _ERR(NoValueForCmdACfg, 0x0029, "ASCII command without parameter") \
  /* Legacy C */ \
  _ERR(InvalidNumDM,      0x0030, "Invalid number of DM (negative value)") \
  _ERR(InvalidIndex,      0x0031, "Invalid index") \
  _ERR(NotYetSupported,   0x0032, "Command is not yet supported") \
  /* Hardware interface */ \
  _ERR(MissingParameter,  0x0100, "N/A")                           \
  _ERR(ItfNotConnected,   0x0101, "Interface is in offline state") \
  _ERR(DOTimeOut,         0x0102, "Output data time-out (previous transfer not finished)") \
  _ERR(DITimeOut,         0x0103, "Input data time-out") \
  _ERR(DOGeneric,         0x0104, "Generic digital output error (from interface)") \
  _ERR(DIGeneric,         0x0105, "Generic digital input error (from interface)") \
  _ERR(DOAsyncCheck,      0x0106, "Cannot check digital write status") \
  _ERR(DIAsyncCheck,      0x0107, "Cannot check digital read status") \
  _ERR(DOBufferClear,     0x0108, "N/A") \
  _ERR(DIBufferClear,     0x0109, "N/A") \
  _ERR(NotSupported,      0x010A, "Function not supported by the current interface") \
  _ERR(DriverApi,         0x010B, "Driver error on interface initialisation") \
  _ERR(OutBufferSize,     0x010C, "Size of listened data is unknown") \
  _ERR(AckTimeOut,        0x010D, "Acknowledge time-out (Ethernet)") \
  _ERR(TrigInTimeOut,     0x010E, "Trigger input time-out (Ethernet)")

static struct {
    const char* name;
    const char* mesg;
    int         code;
} errors[] = {
#define _ERR(a,b,c) {#a, c, b},
    _ERRLIST
    {NULL, NULL, -1},
#undef _ERR
};

// Callback for TAO error management system.
static void get_error_details(
    int code,
    const char** mesg,
    const char** name)
{
    if (mesg != NULL) {
        *mesg = "Unknown Alpao error";
    }
    if (name != NULL) {
        *name = "UnknownError";
    }
    if (mesg != NULL || name != NULL) {
        for (int i = 0; errors[i].name != NULL; ++i) {
            if (errors[i].code == code) {
                if (mesg != NULL) {
                    *mesg = errors[i].mesg;
                }
                if (name != NULL) {
                    *name = errors[i].name;
                }
                break;
            }
        }
    }
}

static void push_last_error(
    const char* func)
{
    const int len = 255;
    int status;
    UInt code;
    char mesg[len+1];
    status = asdkGetLastError(&code, mesg, len);
    if (status != SUCCESS) {
        warn("Failed to retrieve last Alpao error message");
        code = -1;
    } else {
        mesg[len] = '\0';
        for (int i = 0; errors[i].name != NULL; ++i) {
            if (errors[i].code == code) {
                // The error message is different than expected, print a
                // warning and clone the error message to avoid another
                // warning and yet provide the correct error message and
                // number.  This memory will never be returned...
                if (strcmp(mesg, errors[i].mesg) != 0) {
                    warn("Alpao error message is now \"%s\", "
                         "it was \"%s\" (code = 0x%04d)",
                         mesg, errors[i].mesg, (unsigned int)code);
                    char* buf = malloc(strlen(mesg) + 21);
                    if (buf != NULL) {
                        strcat(buf + 20, mesg);
                        sprintf(buf, "0x%04d", code);
                        errors[i].mesg = buf + 20;
                        errors[i].name = buf;
                    }
                }
                break;
            }
        }
    }
    tao_store_other_error(func, code, get_error_details);
}

static void warn(
    const char* format,
    ...)
{
    va_list args;
    fputs("WARNING: ", stderr);
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
    fputs("\n", stderr);
    fflush(stderr);
}

// KNOWN MIRROR MODELS --------------------------------------------------------

static struct {
    const char* name;
    int nacts;
    int diam;
} models[] = {
    {"DM69",     69,  9},
    {"DM97",     97, 11},
    {"DM192",   192, 16},
    {"DM241",   241, 17},
    {"DM292",   292, 20},
    {"DM468",   468, 24},
    {"DM820",   820, 32},
    {"DM3228", 3228, 64},
    {"DMX37",    37,  7},
    {"DMX61",    61,  9},
    {"DMX85",    85, 11},
    {"DMX121",  121, 13},
    {"DMX163",  163, 15},
    {NULL,        0,  0}};

uint8_t* alpao_mirror_mask_create_by_name(
    const char* name,
    long        dims[2])
{
    if (name != NULL) {
        for (int i = 0; models[i].name != NULL; ++i) {
            if (strcasecmp(models[i].name, name) == 0) {
                long dim = models[i].diam;
                long nacts = models[i].nacts;
                if (dims != NULL) {
                    dims[0] = dim;
                    dims[1] = dim;
                }
                return tao_layout_mask_create(dim, dim, nacts);
            }
        }
    }
    tao_store_error(__func__, TAO_BAD_NAME);
    return NULL;
}

uint8_t* alpao_mirror_mask_create_by_nacts(
    long nacts,
    long dims[2])
{
    int idx = -1;
    for (int i = 0; models[i].name != NULL; ++i) {
        if (models[i].nacts == nacts) {
            if (idx >= 0) {
                tao_store_error(__func__,
                                TAO_BAD_VALUE); // FIXME: TAO_DUPLICATE
                return NULL;
            }
            idx = i;
        }
    }
    if (idx < 0) {
        tao_store_error(__func__, TAO_BAD_VALUE);
        return NULL;
    }
    long dim = models[idx].diam;
    if (dims != NULL) {
        dims[0] = dim;
        dims[1] = dim;
    }
    return tao_layout_mask_create(dim, dim, nacts);
}

// OPERATIONS ON MIRROR INSTANCES ---------------------------------------------

// For correct handling of errors, it is easier to split the code for
// alpao_open_mirror() in two functions using pointers to store other returned
// values than the device handle.
static asdkDM* open_mirror(
    const char* func,
    const char* filename,
    long* nacts_ptr,
    char** serial_ptr)
{
    char* serial; // base name part of filename
    char* dirname; // directory part of filename
    char* ptr; // for searching characters
    char workdir[PATH_MAX+1]; // to store the initial working directory
    char path[PATH_MAX+1]; // to store a writable copy of filename

    // In case of errors.
    *nacts_ptr = 0;
    *serial_ptr = NULL;
    asdkDM* handle = NULL;

    // Determine the location of the mirror configuration file.
    if (filename == NULL || filename[0] == '\0' ||
        strlen(filename) >= sizeof(path)) {
        tao_store_error(func, TAO_BAD_SERIAL);
        goto error;
    }
    strcpy(path, filename);
    ptr = strrchr(path, '/');
    if (ptr == NULL) {
        // No directory separator found.
        serial = path;
        dirname = NULL;
    } else {
        // Split the path in 2 parts (directory and base name).
        *ptr = '\0';
        serial = ptr + 1;
        dirname = path;
    }

    // Strip file extension and duplicate result.
    ptr = strrchr(serial, '.');
    if (ptr != NULL && strcmp(ptr, ".acfg") == 0) {
        *ptr = '\0';
    }
    *serial_ptr = strdup(serial);
    if (*serial_ptr == NULL) {
        tao_store_system_error("strdup");
        goto error;
    }

    // If specified, change to file directory, open the mirror and go back to
    // initial working directory before checking for errors.
    if (dirname != NULL) {
        if (getcwd(workdir, sizeof(workdir)) == NULL) {
            tao_store_system_error("getcwd");
            goto error;
        }
        if (chdir(path) != 0) {
            tao_store_system_error("chdir");
            goto error;
        }
    }
    handle = asdkInit(serial);
    if (dirname != NULL && chdir(workdir) != 0) {
        tao_store_system_error("chdir");
        goto error;
    }
    if (handle == NULL) {
        push_last_error(func);
        goto error;
    }

    // Retrieve the number of actuators.
    double val;
    if (asdkGet((asdkDM*)handle, "NbOfActuator", &val) != SUCCESS) {
        push_last_error(func);
        goto error;
    }
    long nacts = lround(val);
    if (nacts <= 0 || nacts != val) {
        tao_store_error(func, TAO_BAD_SIZE);
        goto error;
    }
    *nacts_ptr = nacts;

    // Disables the throwing of an exception on error.
    if (asdkSet(handle, "UseException", 0) != SUCCESS) {
        push_last_error(func);
        goto error;
    }
    return handle;

 error:
    *nacts_ptr = 0;
    if (*serial_ptr != NULL) {
        free(*serial_ptr);
        *serial_ptr = NULL;
    }
    if (handle != NULL) {
        asdkRelease(handle);
    }
    return NULL;
}

#define FORCE_SET(T, lval, rval) do { *(T*)&(lval) = (rval); } while (0)

alpao_mirror* alpao_open_mirror(
    const char* filename)
{
    // Open device to mirror.
    long nacts;
    char* serial;
    asdkDM* handle = open_mirror(__func__, filename, &nacts, &serial);
    if (handle == NULL) {
        return NULL;
    }

    // Allocate zero-filled memory for the mirror structure plus enough aligned
    // space for temporary commands.
    size_t size, offset;
    if (sizeof(Scalar) != sizeof(double)) {
        // A temporary buffer is needed for the commands.
        offset = TAO_ROUND_UP(sizeof(alpao_mirror), TAO_ALIGNMENT);
        size = offset + nacts*sizeof(Scalar);
    } else {
        // No temporary buffer needed for the commands.
        size = sizeof(alpao_mirror);
        offset = 0;
    }
    alpao_mirror* dev = tao_malloc(size);
    if (dev == NULL) {
        free(serial);
        asdkRelease(handle);
        return NULL;
    }
    memset(dev, 0, size);
    tao_forced_store(&dev->handle, handle);
    tao_forced_store(&dev->nacts,  nacts);
    tao_forced_store(&dev->serial, serial);
    if (sizeof(Scalar) != sizeof(double)) {
        tao_forced_store(&dev->cmds, (char*)dev + offset);
    } else {
        tao_forced_store(&dev->cmds, NULL);
    }

    return dev;
}

void alpao_close_mirror(
    alpao_mirror* dev)
{
    if (dev != NULL) {
        tao_error* err = tao_get_last_error();
        tao_clear_error(err);
        if (dev->handle != NULL) {
            asdkRelease((asdkDM*)dev->handle);
        }
        if (dev->serial != NULL) {
            free((void*)dev->serial);
        }
        free(dev);
        if (tao_any_errors(err)) {
            tao_report_error();
        }
    }
}

tao_status alpao_send_commands(
    alpao_mirror* dev,
    double *cmds,
    long n)
{
    const double cmin = -1;
    const double cmax = +1;
    const double cavg =  0;
    if (dev == NULL || cmds == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (n != dev->nacts) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return TAO_ERROR;
    }

    // Check whether commands are all in range.
    bool in_range = true;
    for (long i = 0; i < n; ++i) {
        double cval = cmds[i];
        in_range &= ((cmin <= cval)&(cval <= cmax));
    }

    // Copy/fix commands.
    Scalar* dev_cmds;
    if (dev->cmds != NULL) {
        // Scalar is not `double`.
        dev_cmds = dev->cmds;
        if (in_range) {
            for (long i = 0; i < n; ++i) {
                Scalar cval = cmds[i];
                dev_cmds[i] = cval;
                cmds[i] = cval;
            }
        } else {
            for (long i = 0; i < n; ++i) {
                Scalar cval = tao_safe_clamp(cmds[i], cmin, cmax, cavg);
                dev_cmds[i] = cval;
                cmds[i] = cval;
            }
        }
    } else {
        // Scalar is `double`.
        if (! in_range) {
            for (long i = 0; i < n; ++i) {
                double cval = tao_safe_clamp(cmds[i], cmin, cmax, cavg);
                cmds[i] = (isfinite(cval) ? cval : cavg);
            }
        }
        dev_cmds = cmds;
    }
    if (asdkSend((asdkDM*)dev->handle, dev_cmds) != SUCCESS) {
        push_last_error(__func__);
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status alpao_reset_mirror(
    alpao_mirror* dev)
{
    if (asdkReset((asdkDM*)dev->handle) != SUCCESS) {
        push_last_error(__func__);
        return TAO_ERROR;
    }
    memset(dev->cmds, 0, dev->nacts*sizeof(Scalar));
    return TAO_OK;
}

tao_status alpao_stop_mirror(
    alpao_mirror* dev)
{
    if (asdkStop((asdkDM*)dev->handle) != SUCCESS) {
        push_last_error(__func__);
        return TAO_ERROR;
    }
    return TAO_OK;
}

// MANAGEMENT OF ATTRIBUTES ---------------------------------------------------

// Attribute flags.
#define BOOLEAN     1
#define INTEGER     2
#define FLOAT       3
#define STRING      4
#define READABLE   (1 << 4)
#define WRITABLE   (READABLE << 1)
#define TYPE_MASK  (READABLE - 1)

static struct attribute {
    const char*   name;
    unsigned int flags;
    const char*  descr;
} attributes[] = {
    {
        "AckTimeout", FLOAT|READABLE|WRITABLE,
        "For Ethernet / USB interface only, set the time-out (ms); "
        "can be set in synchronous mode only (see SyncMode)."
    }, {
        "DacReset", BOOLEAN|WRITABLE,
        "Reset all digital to analog converters of drive electronics."
    }, {
        "ItfState", INTEGER|READABLE,
        "Return 1 if PCI interface is busy or 0 otherwise."
    }, {
        "LogDump", INTEGER|WRITABLE,
        "Dump the log stack on the standard output."
    }, {
        "LogPrintLevel", INTEGER|READABLE|WRITABLE,
        "Changes the output level of the logger to the standard output."
    }, {
        "NbOfActuator", INTEGER|READABLE,
        "Get the numbers of actuator for that mirror."
    }, {
        "SyncMode", BOOLEAN|WRITABLE,
        "0: Synchronous mode, will return when send is done."
        "1: Asynchronous mode, return immediately after safety checks."
    }, {
        "TriggerMode", BOOLEAN|WRITABLE,
        "Set mode of the (optional) electronics trigger output."
        "0: long pulse width or 1: short pulse width on each command."
    }, {
        "TriggerIn", INTEGER|WRITABLE,
        "Set mode of the (optional) input trigger."
        "0: disabled, 1: trig on rising edge or 2: trig on falling edge."
    }, {
        "UseException", BOOLEAN|READABLE|WRITABLE,
        "Enables or disables the throwing of an exception on error."
     }, {
        "VersionInfo", INTEGER|READABLE,
        "Alpao SDK core version, e.g. 3040500612 is SDK v3.04.05.0612 "
        "where 0612 is build number."
    }, {
        NULL, 0, NULL
    }
};

static int check_attribute(
    const char *func,
    const char* key,
    unsigned mode,
    unsigned type)
{
    int j = -1;
    if (key != NULL && key[0] != '\0') {
        for (int i = 0; attributes[i].name != NULL; ++i) {
            if (strcasecmp(attributes[i].name, key) == 0) {
                j = i;
                break;
            }
        }
    }
    if (j < 0) {
        tao_store_error(func, TAO_BAD_NAME);
        return -1;
    }
    if ((attributes[j].flags & mode) != mode) {
        tao_store_error(
            func, (mode == READABLE ? TAO_UNREADABLE : TAO_UNWRITABLE));
        return -1;
    }
    if ((attributes[j].flags & TYPE_MASK) != type) {
        tao_store_error(func, TAO_BAD_TYPE);
        return -1;
    }
    return j;
}

tao_status alpao_get_boolean_attribute(
    alpao_mirror* dev,
    const char* key,
    bool* ptr)
{
    if (check_attribute(__func__, key, READABLE, BOOLEAN) < 0) {
        return TAO_ERROR;
    }
    Scalar val;
    if (asdkGet((asdkDM*)dev->handle, key, &val) != SUCCESS) {
        push_last_error(__func__);
        return TAO_ERROR;
    }
    *ptr = (val ? true : false);
    return TAO_OK;
}

tao_status alpao_get_integer_attribute(
    alpao_mirror* dev,
    const char* key,
    long* ptr)
{
    if (check_attribute(__func__, key, READABLE, INTEGER) < 0) {
        return TAO_ERROR;
    }
    Scalar val;
    if (asdkGet((asdkDM*)dev->handle, key, &val) != SUCCESS) {
        push_last_error(__func__);
        return TAO_ERROR;
    }
    *ptr = lround(val);
    return TAO_OK;
}

tao_status alpao_get_real_attribute(
    alpao_mirror* dev,
    const char* key,
    double* ptr)
{
    if (check_attribute(__func__, key, READABLE, FLOAT) < 0) {
        return TAO_ERROR;
    }
    Scalar val;
    if (asdkGet((asdkDM*)dev->handle, key, &val) != SUCCESS) {
        push_last_error(__func__);
        return TAO_ERROR;
    }
    *ptr = val;
    return TAO_OK;
}

tao_status alpao_set_boolean_attribute(
    alpao_mirror* dev,
    const char* key,
    bool val)
{
    if (check_attribute(__func__, key, WRITABLE, BOOLEAN) < 0) {
        return TAO_ERROR;
    }
    if (asdkSet((asdkDM*)dev->handle, key, (val ? 1 : 0)) != SUCCESS) {
        push_last_error(__func__);
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status alpao_set_integer_attribute(
    alpao_mirror* dev,
    const char* key,
    long val)
{
    if (check_attribute(__func__, key, WRITABLE, INTEGER) < 0) {
        return TAO_ERROR;
    }
    if (asdkSet((asdkDM*)dev->handle, key, val) != SUCCESS) {
        push_last_error(__func__);
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status alpao_set_real_attribute(
    alpao_mirror* dev,
    const char* key,
    double val)
{
    if (check_attribute(__func__, key, WRITABLE, FLOAT) < 0) {
        return TAO_ERROR;
    }
    if (asdkSet((asdkDM*)dev->handle, key, val) != SUCCESS) {
        push_last_error(__func__);
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status alpao_set_string_attribute(
    alpao_mirror* dev,
    const char* key,
    const char* val)
{
    if (check_attribute(__func__, key, WRITABLE, STRING) < 0) {
        return TAO_ERROR;
    }
    if (asdkSetString((asdkDM*)dev->handle, key,
                      (val == NULL ? "" : val)) != SUCCESS) {
        push_last_error(__func__);
        return TAO_ERROR;
    }
    return TAO_OK;
}
