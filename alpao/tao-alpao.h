// tao-alpao.h -
//
// Definitions for Alpao deformable mirrors in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#ifndef TAO_ALPAO_H_
#define TAO_ALPAO_H_ 1

#include <tao-remote-mirrors.h>

#include <stdbool.h>

TAO_BEGIN_DECLS

/**
 * @ingroup AlpaoMirrors
 *
 * @{
 */

/**
 * Structure for an Alpao deformable mirror.
 *
 * This structure is created by the owner of the mirror by calling
 * alpao_open_mirror() and destroyed by calling alpao_close_mirror().
 *
 * The buffer `cmds` is private to the owner of the mirror and is used to
 * prepare the commands to apply.
 */
typedef struct alpao_mirror_ {
    void* const       handle;///< Handle to device.
    long const         nacts;///< Number of actuators.
    char const* const serial;///< Serial number of device.
    void* const         cmds;///< Buffer to temporarily store commands to send,
                             ///  `NULL` if not needed.
} alpao_mirror;

/**
 * Open Alpao deformable mirror.
 *
 * @param filename   Name of Alpao deformable mirror configuration file.
 *
 * @return The address of an allocated Alpao mirror instance; or `NULL` on
 *         error.
 */
extern alpao_mirror* alpao_open_mirror(
    const char* filename);

/**
 * Close Alpao deformable mirror.
 *
 * This function closes the device and release associated resources.
 *
 * @param dev   Address of Alpao mirror instance (can be `NULL`).
 */
extern void alpao_close_mirror(
    alpao_mirror* dev);

/**
 * Apply actuator commands of an Alpao deformable mirror.
 *
 * This function applies given commands to the Alpao deformable mirror `dev`.
 * The requested command for the `i`-th actuator is `cmds[i]`.  On return,
 * `cmds[i]` is set with the commands effectively applied to the deformable
 * mirror (i.e., after accounting for clipping and filtering of bad values).
 *
 * @param dev   Address of Alpao mirror instance.
 *
 * @param cmds  Vector of commands.
 *
 * @param refs  Vector of reference commands (may be `NULL`).
 *
 * @param n     Number of elements in `cmds` and `refs` (if not `NULL`).
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status alpao_send_commands(
    alpao_mirror* dev,
    double *cmds,
    long n);

/**
 * Stop Alpao deformable mirror.
 *
 * This function stops Alpao deformable mirror `dev`.
 *
 * @param dev   Address of Alpao mirror instance.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status alpao_stop_mirror(
    alpao_mirror* dev);

/**
 * Reset Alpao deformable mirror.
 *
 * This function resets Alpao deformable mirror `dev` and zero-fills the
 * internal buffer of commands.
 *
 * @param dev   Address of Alpao mirror instance.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status alpao_reset_mirror(
    alpao_mirror* dev);

/**
 * Get boolean attribute of Alpao mirror.
 *
 * This function retrieves boolean attribute `key` from Alpao deformable mirror
 * `dev` and store its value in `ptr`.
 *
 * @param dev   Address of Alpao mirror instance.
 *
 * @param key   Attribute name.
 *
 * @param ptr   Address to store attribute value.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status alpao_get_boolean_attribute(
    alpao_mirror* dev,
    const char* key,
    bool* ptr);

/**
 * Get integer attribute of Alpao mirror.
 *
 * This function retrieves integer attribute `key` from Alpao deformable mirror
 * `dev` and store its value in `ptr`.
 *
 * @param dev   Address of Alpao mirror instance.
 *
 * @param key   Attribute name.
 *
 * @param ptr   Address to store attribute value.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status alpao_get_integer_attribute(
    alpao_mirror* dev,
    const char* key,
    long* ptr);

/**
 * Get floating-point attribute of Alpao mirror.
 *
 * This function retrieves floating-point attribute `key` from Alpao deformable
 * mirror `dev` and store its value in `ptr`.
 *
 * @param dev   Address of Alpao mirror instance.
 *
 * @param key   Attribute name.
 *
 * @param ptr   Address to store attribute value.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status alpao_get_real_attribute(
    alpao_mirror* dev,
    const char* key,
    double* ptr);

/**
 * Set boolean attribute of Alpao mirror.
 *
 * This function sets boolean attribute `key` from Alpao deformable mirror
 * `dev` to value `val`.
 *
 * @param dev   Address of Alpao mirror instance.
 *
 * @param key   Attribute name.
 *
 * @param val   Attribute value.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status alpao_set_boolean_attribute(
    alpao_mirror* dev,
    const char* key,
    bool val);

/**
 * Set integer attribute of Alpao mirror.
 *
 * This function sets integer attribute `key` from Alpao deformable mirror
 * `dev` to value `val`.
 *
 * @param dev   Address of Alpao mirror instance.
 *
 * @param key   Attribute name.
 *
 * @param val   Attribute value.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status alpao_set_integer_attribute(
    alpao_mirror* dev,
    const char* key,
    long val);

/**
 * Set floating-point attribute of Alpao mirror.
 *
 * This function sets floating-point attribute `key` from Alpao deformable
 * mirror `dev` to value `val`.
 *
 * @param dev   Address of Alpao mirror instance.
 *
 * @param key   Attribute name.
 *
 * @param val   Attribute value.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status alpao_set_real_attribute(
    alpao_mirror* dev,
    const char* key,
    double val);

/**
 * Set textual attribute of Alpao mirror.
 *
 * This function sets textual attribute `key` from Alpao deformable mirror
 * `dev` to value `val`.
 *
 * @param dev   Address of Alpao mirror instance.
 *
 * @param key   Attribute name.
 *
 * @param val   Attribute value.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status alpao_set_string_attribute(
    alpao_mirror* dev,
    const char* key,
    const char* val);

/**
 * Create a mask of an Alpao deformable mirror given its name.
 *
 * @param name   The name of the mirror model.
 *
 * @param dims   An optional array of 2 integers to store the dimensons of the
 *               result.
 *
 * @return The address of the mask, `NULL` in case of failure.
 */
extern uint8_t* alpao_mirror_mask_create_by_name(
    const char* name,
    long dims[2]);

/**
 * Create a mask of an Alpao deformable mirror given its number of actuators.
 *
 * @param name   The name of the mirror model.
 *
 * @param dims   An optional array of 2 integers to store the dimensons of the
 *               result.
 *
 * @return The address of the mask, `NULL` in case of failure.
 */
extern uint8_t* alpao_mirror_mask_create_by_nacts(
    long nacts,
    long dims[2]);


/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_ALPAO_H_
