// remote-cameras.c -
//
// Management of remote cameras.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2019-2024, Éric Thiébaut.

#include <assert.h>
#include <string.h>
#include <stdio.h> // *before* <tao-errors.h>
#include <stdlib.h>

#include "tao-remote-cameras-private.h"
#include "tao-config.h"
#include "tao-errors.h"
#include "tao-generic.h"

#define BUFFERS_OFFSET TAO_ROUND_UP(sizeof(tao_remote_camera), sizeof(tao_shmid))

static inline tao_shmid* get_bufs(
    tao_remote_camera* cam)
{
    return (tao_shmid*)TAO_COMPUTED_ADDRESS(cam, BUFFERS_OFFSET);
}

tao_remote_camera* tao_remote_camera_create(
    const char* owner,
    long        nbufs,
    unsigned    flags)
{
    // Check arguments.
    if (nbufs < 2) {
        tao_store_error(__func__, TAO_BAD_BUFFERS);
        return NULL;
    }

    // Compute stride, offest, and size to store `nbufs` shared memory
    // identifiers.
    size_t stride = sizeof(tao_shmid);
    size_t offset = TAO_ROUND_UP(sizeof(tao_remote_camera), stride);
    size_t size = offset + nbufs*stride;
    if (offset != BUFFERS_OFFSET) {
        tao_store_error(__func__, TAO_ASSERTION_FAILED);
        return NULL;
    }

    // Allocate remote shared object.
    tao_remote_camera* cam = (tao_remote_camera*)tao_remote_object_create(
        owner, TAO_REMOTE_CAMERA, nbufs, offset, stride, size, flags);
    if (cam == NULL) {
        return NULL;
    }

    // Instantiate object.
    tao_camera_config_initialize(&cam->config);
    tao_shmid* bufs = get_bufs(cam);
    for (long i = 0; i < nbufs; ++i) {
        bufs[i] = TAO_BAD_SHMID;
    }
    for (long i = 0; i < 4; ++i) {
        cam->preproc[i] = TAO_BAD_SHMID;
    }
    return cam;
}

tao_shmid tao_remote_camera_get_preprocessing_shmid(
    const tao_remote_camera* cam,
    int idx)
{
    // Check arguments. This function leaves the caller's last error unchanged.
    if (cam == NULL || idx < 0 || idx > 3) {
        return TAO_BAD_SHMID;
    }
    return cam->preproc[idx];
}

tao_shmid tao_remote_camera_get_image_shmid(
    tao_remote_camera* cam,
    tao_serial serial)
{
    // Check arguments.
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_BAD_SHMID;
    }
    if (serial < 1) {
        tao_store_error(__func__, TAO_BAD_SERIAL);
        return TAO_BAD_SHMID;
    }

    // Retrieve result.
    const tao_shmid* bufs = get_bufs(cam);
    tao_shmid shmid = bufs[(serial - 1)%cam->base.nbufs];
    if (shmid == TAO_BAD_SHMID) {
        tao_store_error(__func__, TAO_NO_DATA);
    }
    return shmid;
}


// Encode other accessors (not for named attributes).
#define ACCESSOR(type, prefix, member, def)                     \
    type tao_remote_camera_get_##member(                        \
        const tao_remote_camera* cam)                           \
    {                                                           \
        return (cam != NULL ? cam->prefix member : (def));      \
    }
ACCESSOR(long,              config.    , sensorwidth,   0)
ACCESSOR(long,              config.    , sensorheight,  0)
ACCESSOR(tao_time,          config.    , origin,        TAO_UNKNOWN_TIME)
ACCESSOR(tao_serial,        config.    , frames,        0)
ACCESSOR(tao_serial,        config.    , droppedframes, 0)
ACCESSOR(tao_serial,        config.    , overruns,      0)
ACCESSOR(tao_serial,        config.    , lostframes,    0)
ACCESSOR(tao_serial,        config.    , overflows,     0)
ACCESSOR(tao_serial,        config.    , lostsyncs,     0)
ACCESSOR(tao_serial,        config.    , timeouts,      0)
ACCESSOR(long,              config.roi., xbin,          0)
ACCESSOR(long,              config.roi., ybin,          0)
ACCESSOR(long,              config.roi., xoff,          0)
ACCESSOR(long,              config.roi., yoff,          0)
ACCESSOR(long,              config.roi., width,         0)
ACCESSOR(long,              config.roi., height,        0)
ACCESSOR(double,            config.    , framerate,     0.0)
ACCESSOR(double,            config.    , exposuretime,  0.0)
ACCESSOR(tao_eltype,        config.    , pixeltype,     TAO_UINT8)
ACCESSOR(tao_encoding,      config.    , rawencoding,   TAO_ENCODING_UNKNOWN)
ACCESSOR(tao_preprocessing, config.    , preprocessing, TAO_PREPROCESSING_NONE)
#undef ACCESSOR

//-----------------------------------------------------------------------------
// NAMED ATTRIBUTES
//
// NOTE: The idea is to not expose the attribute table, so we only provide API
//       to retrieve the number of attributes, to retrieve an attribute by
//       index, and to find an attribute by key.
//
// NOTE: Functions only return a read-only attribute pointer. By convention,
//       the owner of the resource is allowed to cast it into a non-`const`
//       pointer to change the attribute value.

long tao_remote_camera_get_attr_number(
    const tao_remote_camera* cam)
{
    return cam == NULL ? 0 : cam->config.attr_table.length;
}

const tao_attr* tao_remote_camera_get_attr(
    const tao_remote_camera* cam,
    long index)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    return tao_attr_table_get_entry(&cam->config.attr_table, index);
}

const tao_attr* tao_remote_camera_unsafe_get_attr(
    const tao_remote_camera* cam,
    long index)
{
    return tao_attr_table_unsafe_get_entry(&cam->config.attr_table, index);
}

long tao_remote_camera_try_find_attr(
    const tao_remote_camera* cam,
    const char* key)
{
    return cam == NULL ? -2 : tao_attr_table_try_find_entry(&cam->config.attr_table, key);
}

const tao_attr* tao_remote_camera_find_attr(
    const tao_remote_camera* cam,
    const char* key)
{
    if (cam == NULL || key == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    return tao_attr_table_find_entry(&cam->config.attr_table, key);
}

tao_status tao_remote_camera_get_configuration(
    const tao_remote_camera* src,
    tao_camera_config* dst)
{
    if (src == NULL || dst == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_camera_config_copy(dst, &src->config);
    return TAO_OK;
}

// Arguments for task to execute on confguration command by a client.
typedef struct {
    const tao_camera_config* cfg;
} configure_args;

// Task to execute on confguration command by a client.
static tao_status configure_task(
    tao_remote_camera* cam,
    void* data)
{
    configure_args* args = data;
    memcpy(&cam->arg.config, args->cfg, sizeof(*args->cfg));
    return TAO_OK;
}

tao_serial tao_remote_camera_configure(
    tao_remote_camera* cam,
    const tao_camera_config* cfg,
    double secs)
{
    // Check arguments.
    if (cam == NULL || cfg == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return -1;
    }

    // Execute the command.
    configure_args args = {
        .cfg = cfg
    };
    return tao_remote_camera_send_command(
        cam, TAO_COMMAND_CONFIG, configure_task, &args, secs);
}

tao_serial tao_remote_camera_start(
    tao_remote_camera* cam,
    double secs)
{
    return tao_remote_camera_send_command(cam, TAO_COMMAND_START, NULL, NULL, secs);
}

tao_serial tao_remote_camera_stop(
    tao_remote_camera* cam,
    double secs)
{
    return tao_remote_camera_send_command(cam, TAO_COMMAND_STOP, NULL, NULL, secs);
}

tao_serial tao_remote_camera_abort(
    tao_remote_camera* cam,
    double secs)
{
    return tao_remote_camera_send_command(cam, TAO_COMMAND_ABORT, NULL, NULL, secs);
}

tao_serial tao_remote_camera_reset(
    tao_remote_camera* cam,
    double secs)
{
    return tao_remote_camera_send_command(cam, TAO_COMMAND_RESET, NULL, NULL, secs);
}

// Generic shared object methods.
#define TYPE  remote_camera
#define MAGIC TAO_REMOTE_CAMERA
#define REMOTE_OBJECT 2 // sub-type of remote object type
#include "./shared-methods.c"
