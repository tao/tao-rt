// shared-objects.c -
//
// Management of shared memory and implementation of basic objects whose contents can be
// shared between processes.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2018-2022, Éric Thiébaut.

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdio.h> // *before* <tao-errors.h>
#include <string.h>
#include <time.h>

#include "tao-basics.h"
#include "tao-errors.h"
#include "tao-utils.h"
#include "tao-threads.h"
#include "tao-macros.h"
#include "tao-generic.h"
#include "tao-shared-memory.h"
#include "tao-shared-objects-private.h"
#include "tao-remote-objects-private.h"
#include "tao-rwlocked-objects-private.h"

//-----------------------------------------------------------------------------
// SHARED MEMORY

// The least significant 9 bits of flags specify the permissions granted to the
// owner, group, and others. The following macro is to select allowed
// permissions.
#define PERMS_MASK (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH)

// Bad address for a shared memory segment (as returned by `shmat`).
#define BAD_SHM_ADDR ((void*)-1)

// Call `shmdt`, update caller's last error in case of failure.
static inline tao_status shared_memory_detach(
    void* addr)
{
    if (shmdt(addr) != 0) {
        tao_store_system_error("shmdt");
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Call `shmctl` with `IPC_RMID`, updating caller's last error in case of
// failure.
static inline tao_status shared_memory_destroy(
    tao_shmid shmid)
{
    if (shmctl(shmid, IPC_RMID, NULL) != 0) {
        tao_store_system_error("shmctl");
        return TAO_ERROR;
    }
    return TAO_OK;
}

void* tao_shared_memory_create(
    tao_shmid* shmid_ptr,
    size_t     size,
    unsigned   perms)
{
    void* addr = BAD_SHM_ADDR;

    // Create a new segment of shared memory and get its identifier.  Note that
    // `shmget` guarantees that the contents of the shared memory segment is
    // zero-filled when `IPC_CREAT` and `IPC_EXCL` are both set.
    int shmid = shmget(
        IPC_PRIVATE, size, (perms & PERMS_MASK) | (IPC_CREAT | IPC_EXCL));
    if (shmid == -1) {
        tao_store_system_error("shmget");
        goto error;
    }

    // Attach the shared memory segment to the address space of the caller.
    addr = shmat(shmid, NULL, 0);
    if (addr == BAD_SHM_ADDR) {
        tao_store_system_error("shmat");
        goto error;
    }

    // Store shared memory identifier and return attachment address.
    if (shmid_ptr != NULL) {
        *shmid_ptr = shmid;
    }
    return addr;

    // In case of error, detach shared memory form caller's address space.
 error:
    if (addr != BAD_SHM_ADDR) {
        shared_memory_detach(addr);
    }
    if (shmid_ptr != NULL) {
        *shmid_ptr = TAO_BAD_SHMID;
    }
    return NULL;
}

tao_status tao_shared_memory_stat(
    tao_shmid shmid,
    size_t*   segsz,
    int64_t*  nattch)
{
    tao_status status = TAO_OK;
    struct shmid_ds ds;
    if (shmctl(shmid, IPC_STAT, &ds) != 0) {
        ds.shm_segsz  = 0;
        ds.shm_nattch = 0;
        status = TAO_ERROR;
    }
    if (segsz != NULL) {
        *segsz = ds.shm_segsz;
    }
    if (nattch != NULL) {
        *nattch = ds.shm_nattch;
    }
    return status;
}

void* tao_shared_memory_attach(
    tao_shmid shmid,
    size_t*   sizeptr)
{
    void* addr = shmat(shmid, NULL, 0);
    if (addr == BAD_SHM_ADDR) {
        tao_store_system_error("shmat");
        return NULL;
    }
    if (sizeptr != NULL) {
        // The caller is interested in getting the size of the shared memory
        // segment.
        struct shmid_ds ds;
        if (shmctl(shmid, IPC_STAT, &ds) != 0) {
            tao_store_system_error("shmctl");
            tao_shared_memory_detach(addr);
            return NULL;
        }
        *sizeptr = ds.shm_segsz;
    }
    return addr;
}

tao_status tao_shared_memory_detach(
    void* addr)
{
    // Just ignore if address is null.
    return (addr == NULL) ? TAO_OK : shared_memory_detach(addr);
}

tao_status tao_shared_memory_destroy(
    tao_shmid shmid)
{
    return shared_memory_destroy(shmid);
}

//-----------------------------------------------------------------------------
// SHARED OBJECTS

tao_shared_object* tao_shared_object_create(
    uint32_t type,
    size_t   size,
    unsigned flags)
{
    // Check assertions made in shared ressources for atomic values.
    TAO_ASSERT(sizeof(atomic_bool) == sizeof(bool), return NULL);
    TAO_ASSERT(sizeof(volatile _Atomic tao_serial) == sizeof(tao_serial), return NULL);
    TAO_ASSERT(sizeof(volatile _Atomic tao_state) == sizeof(tao_state), return NULL);

    // Check arguments.
    if ((type & TAO_SHARED_MASK) != TAO_SHARED_MAGIC) {
        tao_store_error(__func__, TAO_BAD_MAGIC);
        return NULL;
    }
    if (size < sizeof(tao_shared_object)) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return NULL;
    }

    // Create shared memory with at least read and write access for the caller.
    unsigned perms = (flags & PERMS_MASK) | (S_IRUSR | S_IWUSR);
    tao_shmid shmid;
    tao_shared_object* obj = tao_shared_memory_create(&shmid, size, perms);
    if (obj == NULL) {
        return NULL;
    }

    // Initialize mutex and condition variable so that they can be shared
    // with other processes.
    int initlevel = 0;
    if (tao_mutex_initialize(&obj->mutex, TAO_PROCESS_SHARED) != TAO_OK) {
        goto error;
    }
    ++initlevel;
    if (tao_condition_initialize(&obj->cond, TAO_PROCESS_SHARED) != TAO_OK) {
        goto error;
    }
    ++initlevel;

    // Instantiate object members.
    obj->nrefs = 1;
    tao_forced_store(&obj->size,  size);
    tao_forced_store(&obj->shmid, shmid);
    tao_forced_store(&obj->flags, flags);
    tao_forced_store(&obj->type,  type);
    return obj;

 error:
    if (initlevel >= 2) {
        pthread_cond_destroy(&obj->cond);
    }
    if (initlevel >= 1) {
        pthread_mutex_destroy(&obj->mutex);
    }
    tao_shared_memory_detach((void*)obj);
    return NULL;
}

// Effectively destroy shared object: destroy the mutex and the condition
// variable of the shared object, detach the object, and destroy the shared
// memory.
static tao_status shared_object_destroy(
    tao_shared_object* obj)
{
    // In case of race, another process attempting to use (e.g. lock) an
    // uninitialized mutex would get error code 22 (EINVAL), while attempting
    // to destroy a locked mutex yields error code 16 (EBUSY). In this latter
    // case, we skip remaining destruction stages other than detaching the
    // shared memory.
    tao_status status = TAO_OK;
    bool destroy = true; // shall we destroy shared resources?
    int code = pthread_mutex_destroy(&obj->mutex);
    if (code != 0) {
        if (code == EBUSY) {
            // Race condition detected.
            destroy = false;
        } else {
            // Something unexpected occurred.
            tao_store_error("pthread_mutex_destroy", code);
            status = TAO_ERROR;
        }
    }

    // Possibly destroy the condition variable.
    if (destroy) {
        code = pthread_cond_destroy(&obj->cond);
        if (code != 0) {
            tao_store_error("pthread_cond_destroy", code);
            status = TAO_ERROR;
        }
    }

    // Detach shared memory.
    tao_shmid shmid = obj->shmid;
    if (shared_memory_detach((void*)obj) != TAO_OK) {
        status = TAO_ERROR;
    }

    // Possibly destroy the shared memory segment.
    if (destroy && shared_memory_destroy(shmid) != TAO_OK) {
        status = TAO_ERROR;
    }
    return status;
}

tao_shared_object* tao_shared_object_attach(
    tao_shmid shmid)
{
    // Attach shared memory segment to the address space of the caller and
    // check that the size and the type are consistent.
    size_t size;
    tao_shared_object* obj =
        (tao_shared_object*)tao_shared_memory_attach(shmid, &size);
    if (obj == NULL) {
        return NULL;
    }
    tao_error_code code = TAO_SUCCESS;
    if (size < sizeof(*obj) || size < obj->size) {
        code = TAO_BAD_SIZE;
    } else if ((obj->type & TAO_SHARED_MASK) != TAO_SHARED_MAGIC) {
        code = TAO_BAD_MAGIC;
    }
    if (code != TAO_SUCCESS) {
        tao_shared_memory_detach((void*)obj);
        tao_store_error(__func__, code);
        return NULL;
    }

    // Check whether object must be destroyed and, if not, increment the number
    // of attachments `nrefs`. This is done using `ctrl` as a spin lock.
    while (atomic_flag_test_and_set(&obj->ctrl)) {
        ; // just wait until we acquire exclusive access on `nrefs`
    }
    bool destroy = (obj->nrefs < 0); // object is being destroyed or must be destroyed?
    if (! destroy) {
        ++obj->nrefs;
    }
    atomic_flag_clear(&obj->ctrl); // release `nrefs`
    if (destroy) {
        shared_object_destroy(obj);
        tao_store_error(__func__, TAO_DESTROYED);
        return NULL;
    }
    return obj;
}

tao_status tao_shared_object_detach(
    tao_shared_object* obj)
{
    // Minimal check.
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }

    // Decrement the number of attachments `nrefs` and check whether object
    // must be destroyed using `ctrl` as a spin lock.
    while (atomic_flag_test_and_set(&obj->ctrl)) {
        ; // just wait until we acquire exclusive access on `nrefs`
    }
    if (obj->nrefs > 0) {
        --obj->nrefs;
    }
    if (obj->nrefs == 0 && (obj->flags & TAO_PERSISTENT) == 0) {
        // Non-persitent object shall be destroyed when no longer attached to
        // any processes.
        obj->nrefs = -1; // indicate that destroy is pending for others
    }
    bool destroy = obj->nrefs < 0; // destroy object?
    atomic_flag_clear(&obj->ctrl); // release `nrefs`
    if (destroy) {
        // Effectively destroy object *after* releasing the spin lock.
        return shared_object_destroy(obj);
    } else {
        // Otherwise, just detach shared memory.
        return shared_memory_detach(obj);
    }
}

const char* tao_shared_object_get_typename(
    const tao_shared_object* obj)
{
    if (obj == NULL) {
        return "NULL object";
    } else if (obj->type == TAO_SHARED_OBJECT) {
        return "shared object";
    } else if (obj->type == TAO_RWLOCKED_OBJECT) {
        return "r/w locked object";
    } else if (obj->type == TAO_REMOTE_OBJECT) {
        return "remote object";
    } else if (obj->type == TAO_SHARED_ARRAY) {
        return "shared array";
    } else if (obj->type == TAO_REMOTE_CAMERA) {
        return "remote camera";
    } else if (obj->type == TAO_REMOTE_MIRROR) {
        return "remote mirror";
    } else if (obj->type == TAO_REMOTE_SENSOR) {
        return "remote sensor";
    } else {
        return "unknown object";
    }
}

#define TYPE shared_object
#define SHARED_OBJECT 1 // basic shared object type
#include "./shared-methods.c"
