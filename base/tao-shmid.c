// tao-shmid.c -
//
// Retrieve the ShmId of a TAO server.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2024, Éric Thiébaut.

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "tao-config.h"
#include "tao-utils.h"
#include "tao-macros.h"

int main(
    int argc,
    char* argv[])
{
    // Determine program name.
    const char* progname = tao_basename(argv[0]);

    // Parse arguments.
    const char* usage = "Usage: %s [OPTIONS] [--] SERVER\n";
    int iarg = 1;
    while (iarg < argc) {
        if (argv[iarg][0] != '-') {
            break;
        }
        if (argv[iarg][1] == '-' && argv[iarg][2] == '\0') {
            ++iarg;
            break;
        }
        if (strcmp(argv[iarg], "-h") == 0
            || strcmp(argv[iarg], "-help") == 0
            || strcmp(argv[iarg], "--help") == 0) {
            printf(usage, progname);
            puts("\n"
                 "Retrieve the shared memory identifier of a TAO server, or -1 if non-existing.\n"
                 "\n"
                 "Arguments:\n"
                 "  SERVER             Server name.\n"
                 "\n"
                 "Options:\n"
                 "  -h, -help, --help  Print this help.\n");
            return EXIT_SUCCESS;
        }
        fprintf(stderr, "%s: Unknown option %s\n", progname, argv[iarg]);
        return EXIT_FAILURE;
    }
    if (argc - iarg != 1) {
        fprintf(stderr, "%s: Invalid number of arguments.\n", progname);
        fprintf(stderr, usage, progname);
        return EXIT_FAILURE;
    }
    const char* server = argv[iarg];
    tao_shmid shmid = tao_config_read_shmid(server);
    fprintf(stdout, TAO_INT64_FORMAT(,d) "\n", (int64_t)shmid);
    return shmid == TAO_BAD_SHMID ? EXIT_FAILURE : EXIT_SUCCESS;
}
