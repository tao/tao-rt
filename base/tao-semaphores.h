// tao-semaphores.h -
//
// Definitions for semaphores in TAO library.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2019-2024, Éric Thiébaut.

#ifndef TAO_SEMAPHORES_H_
#define TAO_SEMAPHORES_H_ 1

#include <stdbool.h>
#include <semaphore.h>

#include <tao-basics.h>
#include <tao-time.h>

TAO_BEGIN_DECLS

/**
 * @defgroup Semaphores  Semaphores
 *
 * @ingroup ParallelProgramming
 *
 * @brief Named and anonymous semaphores.
 *
 * Semaphores come in two flavors: named semaphores and anonymous semaphores.
 *
 * @{
 */

/**
 * @brief Initialize an anonymous semaphore.
 *
 * This function must be called to initialize an anonymous semaphore (stored in shared
 * memory). The caller is responsible of eventually calling tao_semaphore_destroy() when
 * the semaphore is no longer needed.
 *
 * @param sem    Address of an anonymous semaphore to initialize.
 *
 * @param share  If set to @ref TAO_PROCESS_SHARED, require that the semaphore be
 *               accessible between processes; otherwise, must be @ref TAO_PROCESS_PRIVATE
 *               and the semaphore will be *private* (that is, only accessible by threads
 *               in the same process as the caller).
 *
 * @param value  Initial value of the anonymous semaphore.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_semaphore_initialize(
    sem_t* sem,
    tao_process_sharing share,
    unsigned int value);

/**
 * Destroy an anonymous semaphore.
 *
 * This function must be called to release the resources associated with an
 * anonymous semaphore initialized by tao_semaphore_initialize().
 *
 * @param sem    Address of anonymous semaphore to destroy.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_semaphore_destroy(
    sem_t* sem);

/**
 * Create a named semaphore.
 *
 * This function is called by the owner of a named semaphore to create it.  A
 * named semaphore with the same name must not already exists.  Call
 * tao_semaphore_open() to open an existing named semaphore.  Call
 * tao_semaphore_close() to close the access to the named semaphore for the
 * caller.  Call tao_semaphore_unlink() to remove the named semaphore.
 *
 * @param name   The name of the named semaphore.
 *
 * @param perms  Access permissions.
 *
 * @param value  Initial value of the named semaphore.
 *
 * @return The address of a new named semaphore; `NULL` in case of failure.
 */
extern sem_t* tao_semaphore_create(
    const char* name,
    int perms,
    unsigned int value);

/**
 * Open an existing named semaphore.
 *
 * This function is open an existing named semaphore. The caller must call
 * tao_semaphore_close() to close the access to the named semaphore.
 *
 * @param name   The name of the named semaphore.
 *
 * @return The address of a new named semaphore; `NULL` in case of failure.
 */
extern sem_t* tao_semaphore_open(
    const char* name);

/**
 * Close a named semaphore.
 *
 * @param sem    Address of named semaphore to close.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_semaphore_close(
    sem_t* sem);

/**
 * Remove a named semaphore.
 *
 * @param name   Name of named semaphore to remove.
 *
 * @param force  Indicate whether the named semaphore may not exist.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_semaphore_unlink(
    const char* name,
    bool force);

/**
 * Get the current value of a semaphore.
 *
 * @param sem    Address of semaphore.
 *
 * @param val    Address of variable to store the semaphore value.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_semaphore_get_value(
    sem_t* sem,
    int* val);

/**
 * Increment the value of a semaphore.
 *
 * This function increments the value of a semaphore by one, thus unblocking one of the
 * processes or threads waiting to be able to decrement the semaphore.
 *
 * @param sem    Address of semaphore.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_semaphore_post(
    sem_t* sem);

/**
 * Decrement the value of a semaphore.
 *
 * This function blocks until the value of the semaphore becomes strictly positive and is
 * decremented by one.
 *
 * @param sem    Address of semaphore.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_semaphore_wait(
    sem_t* sem);

/**
 * Attempt to decrement the value of a semaphore immediately.
 *
 * This function is similar to tao_semaphore_wait() but it returns immediately and reports
 * whether the value of the semaphore was decremented.
 *
 * @param sem    Address of semaphore.
 *
 * @return @ref TAO_OK if the value of the semaphore was successfully decremented; @ref
 *         TAO_TIMEOUT if the value of a semaphore was zero; @ref TAO_ERROR in case of
 *         another failure.
 */
extern tao_status tao_semaphore_try_wait(
    sem_t* sem);

/**
 * Attempt to decrement the value of a semaphore with a time limit.
 *
 * This function is similar to tao_semaphore_wait() but it waits no longer than a given
 * absolute time limit.
 *
 * @param sem      Address of semaphore.
 *
 * @param abstime  Absolute time limit for waiting.
 *
 * @return @ref TAO_OK if the was successfully decremented before the specified number of
 *         seconds; @ref TAO_TIMEOUT if timeout occurred before; @ref TAO_ERROR in case of
 *         another failure.
 */
extern tao_status tao_semaphore_abstimed_wait(
    sem_t* sem,
    const tao_time* abstime);

/**
 * Attempt to decrement the value of a semaphore with a time limit.
 *
 * This function is similar to tao_semaphore_wait() but it waits no longer than
 * a given number of seconds from now.
 *
 * @param sem    Address of semaphore.
 *
 * @param secs   Maximum time to wait (in seconds). If this amount of time is very large,
 *               e.g. more than @ref TAO_MAX_TIME_SECONDS, the effect is the same as
 *               calling tao_semaphore_wait(). If this amount of time is very small, the
 *               effect is the same as calling tao_semaphore_try_wait().
 *
 * @return @ref TAO_OK if the was successfully decremented before the specified number of
 *         seconds; @ref TAO_TIMEOUT if timeout occurred before; @ref TAO_ERROR in case of
 *         another failure.
 */
extern tao_status tao_semaphore_timed_wait(
    sem_t* sem,
    double secs);

/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_SEMAPHORES_H_
