// threadpools.c -
//
// Implement minimalist thread-pools in TAO library.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2020-2024, Éric Thiébaut.

#include <errno.h>
#include <stdbool.h>
#include <stdio.h> // *before* <tao-errors.h> and <tao-time.h>
#include <stdlib.h>

#include "tao-threadpools.h"
#include "tao-threads.h"

// Private structure to represent a job to be executed.
typedef struct tao_job_ tao_job;
struct tao_job_ {
    void (*func)(void* arg);
    void* arg;
    tao_job* next;
};

struct tao_threadpool_ {
    tao_mutex mutex; ///< Lock to control access to resources.
    tao_cond   task; ///< Condition variable to notify idle workers that a new
                     ///  task has been assigned (a job has been pushed or
                     ///  quitting has been requested).
    tao_cond   done; ///< Condition variable to notify the "scheduler" that
                     ///  some task has been executed by a worker (a worker
                     ///  thread has been created, has exited or a job has been
                     ///  completed).
    int     workers; ///< Number of existing worker threads.
    int        jobs; ///< Number of jobs being executed.
    bool   quitting; ///< Thread-pool is about to be destroyed.

    // Queue of jobs.  To have jobs executed in order, the two end-points of
    // the queue have to be memorized.
    tao_job*  first; ///< First pending job, NULL if none.  Used to pull a job
                     ///  out of the queue.
    tao_job*   last; ///< Last pending job, NULL if none.  Used to push a new
                     ///  job at the end of the queue.
    tao_job* unused; ///< Chained list of re-usable job structures.
};

#define CALLOC(N,T)            ((T*)calloc((N), sizeof(T)))
#define LOCK(P)                pthread_mutex_lock(&(P)->mutex)
#define UNLOCK(P)              pthread_mutex_unlock(&(P)->mutex)
#define WAIT_TASK(P)           pthread_cond_wait(&(P)->task, &(P)->mutex)
#define WAIT_DONE(P)           pthread_cond_wait(&(P)->done, &(P)->mutex)
#define NOTIFY_ANY_WORKER(P)   pthread_cond_signal(&(P)->task)
#define NOTIFY_MASTER(P)       pthread_cond_signal(&(P)->done)
#define NOTIFY_ALL_WORKERS(P)  pthread_cond_broadcast(&(P)->task)

// The routine executed by any worker.  The passed argument is the
// thread-pool.
static void* run_worker(
    void* arg)
{
    tao_threadpool* pool = arg;

    // Inform others that worker has been created.
    LOCK(pool);
    ++pool->workers;
    NOTIFY_MASTER(pool);

    while (true) {
        // Wait for new jobs or for being requested to quit.
        while (!pool->quitting && pool->first == NULL) {
            WAIT_TASK(pool);
        }
        if (pool->quitting) {
            break;
        }
        if (pool->first != NULL) {
            // Remove first pending job from the queue.
            tao_job* job = pool->first;
            pool->first = job->next;
            if (pool->last == job) {
                // This job was also the last pending one.
                pool->last = NULL;
            }
            if (job->func != NULL) {
                // Execute the job.
                ++pool->jobs;
                UNLOCK(pool);
                job->func(job->arg);
                LOCK(pool);
                --pool->jobs;
            }
            // Insert completed job in the list of unused jobs and notify
            // scheduler that a job has been completed.
            job->next = pool->unused;
            pool->unused = job;
            NOTIFY_MASTER(pool);
        }
    }

    // Worker is about to quit.
    --pool->workers;
    NOTIFY_MASTER(pool);
    UNLOCK(pool);
    return NULL;
}

// The routine to release resources associated with a thread-pool.  It is
// assumed that the structure has been allocated (non-NULL) although it may
// not have been fully initialized (as indicated by the level).
static void finalize(
    tao_threadpool* pool,
    int level)
{
    // If thread-pool was correctly initialized, there may be pending
    // and/or running jobs.
    if (level >= 3) {
        // Lock resources.
        LOCK(pool);

        // Remove all unstarted jobs from the queue of pending jobs.
        pool->last = NULL;
        while (pool->first != NULL) {
            tao_job* job = pool->first;
            pool->first = job->next;
            free((void*)job);
        }

        // Set the flag to require workers to quit and wait for all running
        // jobs to finish and all workers to exit.
        pool->quitting = true;
        while (pool->jobs > 0 || pool->workers > 0) {
            NOTIFY_ALL_WORKERS(pool);
            WAIT_DONE(pool);
        }

        // Free all unused job structures.
        while (pool->unused != NULL) {
            tao_job* job = pool->unused;
            pool->unused = job->next;
            free((void*)job);
        }

        // Unlock resources (the mutex must be unlocked before being
        // destroyed).
        UNLOCK(pool);
    }

    // Destroy other resources.
    if (level >= 3) {
        pthread_cond_destroy(&pool->done);
    }
    if (level >= 2) {
        pthread_cond_destroy(&pool->task);
    }
    if (level >= 1) {
        pthread_mutex_destroy(&pool->mutex);
    }
    free((void*)pool);
}

tao_threadpool* tao_threadpool_create(
    int workers)
{
    // Allocate and instantiate thread-pool structure.
    int status, level, i;
    tao_threadpool* pool = CALLOC(1, tao_threadpool);
    if (pool == NULL) {
        return NULL;
    }
    if (workers < 2) {
        workers = 2;
    }
    pool->workers = 0;
    pool->jobs = 0;
    pool->quitting = false;
    pool->first = NULL;
    pool->last = NULL;
    pool->unused = NULL;
    level = 0;
    status = pthread_mutex_init(&pool->mutex, NULL);
    if (status != 0) {
        goto error;
    }
    level = 1;
    status = pthread_cond_init(&pool->task, NULL);
    if (status != 0) {
        goto error;
    }
    level = 2;
    status = pthread_cond_init(&pool->done, NULL);
    if (status != 0) {
        goto error;
    }
    level = 3;

    // Create detached worker threads.
    for (i = 0; i < workers; ++i) {
        pthread_t thread;
        status = pthread_create(&thread, NULL, run_worker, pool);
        if (status != 0) {
            goto error;
        }
        status = pthread_detach(thread);
        if (status != 0) {
            goto error;
        }
    }

    // Wait for all workers to start.
    LOCK(pool);
    while (pool->workers < workers) {
        WAIT_DONE(pool);
    }
    UNLOCK(pool);
    return pool;

 error:
    errno = status;
    finalize(pool, level);
    return NULL;
}

void tao_threadpool_destroy(
    tao_threadpool* pool)
{
    if (pool != NULL) {
        finalize(pool, 3);
    }
}

tao_status tao_threadpool_wait(
    tao_threadpool* pool)
{
    tao_status status = TAO_OK;
    if (pool == NULL) {
        errno = EFAULT;
        status = TAO_ERROR;
    } else {
        // Wait for all running jobs to finish.
        LOCK(pool);
        while (!pool->quitting && pool->workers > 0 &&
               (pool->jobs > 0 || pool->first != NULL)) {
            NOTIFY_ANY_WORKER(pool); // FIXME: should not be needed although it
                                     // does not hurt.
            WAIT_DONE(pool);
        }
        if (pool->jobs > 0 || pool->first != NULL) {
            errno = ECANCELED;
            status = TAO_ERROR;
        }
        UNLOCK(pool);
    }
    return status;
}

tao_status tao_threadpool_push_job(
    tao_threadpool* pool,
    void (*func)(void*),
    void* arg)
{
    // No errors so far.
    tao_status status = TAO_OK;

    // Minimal check.
    if (pool == NULL || func == NULL) {
        errno = EFAULT;
        status = TAO_ERROR;
    } else {
        // Lock resources.
        LOCK(pool);

        if (pool->quitting) {
            // All worker threads have been requested to quit.
            errno = ECANCELED;
            status = TAO_ERROR;
        } else {
            // Peak an unused job structure or allocate a new one.
            tao_job *job;
            if (pool->unused == NULL) {
                job = CALLOC(1, tao_job);
                if (job == NULL) {
                    status = TAO_ERROR;
                }
            } else {
                job = pool->unused;
                pool->unused = job->next;
            }
            if (job != NULL) {
                // Instantiate the job structure.
                job->func = func;
                job->arg  = arg;
                job->next = NULL;

                // Append the new job to the end of the queue of pending jobs
                // taking care that it may be the first (and only) one.
                if (pool->first == NULL) {
                    pool->first = job;
                } else {
                    pool->last->next = job;
                }
                pool->last = job;

                // Notify workers that a new job is pending.
                NOTIFY_ANY_WORKER(pool);
            }
        }

        // Unlock resources.
        UNLOCK(pool);
    }

    // Report status.
    return status;
}
