// tao-utils.h -
//
// Definitions for utility functions in TAO (dynamic memory, strings, time).
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2019-2024, Éric Thiébaut.

#ifndef TAO_UTILS_H_
#define TAO_UTILS_H_ 1

#include <tao-basics.h>
#include <tao-buffers.h>

TAO_BEGIN_DECLS

/**
 * @defgroup DynamicMemory  Dynamic memory
 *
 * @ingroup Utilities
 *
 * @brief Management of conventional dynamic memory.
 *
 * These functions are provided to report errors like other functions in TAO
 * Library.
 *
 * @see SharedMemory
 *
 * @{
 */

/**
 * Allocate dynamic memory.
 *
 * This function behaves as malloc() except that, in case of failure, the last
 * error of the calling thread is updated.  The caller is responsible for
 * calling free() or tao_free() to free the allocated memory when no longer
 * needed.
 *
 * @param size   Number of bytes to allocate.
 *
 * @return The address of allocated dynamic memory; `NULL` in case of failure.
 *
 * @see tao_free, tao_calloc.
 */
extern void* tao_malloc(
    size_t size);

/**
 * Reallocate dynamic memory.
 *
 * This function behaves as realloc() except that, in case of failure, the last
 * error of the calling thread is updated.  The caller is responsible for
 * calling free() or tao_free() to free the allocated memory when no longer
 * needed.
 *
 * @param ptr    Address of dynamic memory to reallocate.
 * @param size   Number of bytes to allocate.
 *
 * @return The address of allocated dynamic memory; `NULL` in case of failure.
 *
 * @see tao_free, tao_malloc, tao_calloc.
 */
extern void* tao_realloc(
    void* ptr,
    size_t size);

/**
 * Allocate dynamic memory.
 *
 * This function behaves as calloc() except that, in case of failure, the last
 * error of the calling thread is updated.  The caller is responsible for
 * calling free() or tao_free() to free the allocated memory when no longer
 * needed.
 *
 * @param nelem  Number of elements to allocate.
 * @param elsize Number of bytes per element.
 *
 * @return The address of allocated dynamic memory; `NULL` in case of failure.
 *
 * @see tao_free, tao_malloc.
 */
extern void* tao_calloc(
    size_t nelem,
    size_t elsize);

/**
 * @def TAO_NEW(numb, type)
 *
 * @brief Allocate zero-filled memory for a given number of items of the same
 *        type.
 *
 * Each argument is evaluated once.
 *
 * @param numb   Number of items.
 * @param type   Type of items.
 *
 * @return A `type*` pointer, `NULL` in case of failure.
 *
 * @see tao_calloc().
 */
#define TAO_NEW(numb, type) ((type*)tao_calloc((numb), sizeof(type)))

/**
 * Free dynamic memory.
 *
 * This function behaves as free() except that it accepts a `NULL` pointer.
 *
 * @param ptr    Address of dynamic memory (can be `NULL`).
 *
 * @see tao_malloc, tao_calloc.
 */
extern void tao_free(
    void* ptr);

/**
 * @}
 */

/**
 * @defgroup StringTools  String tools
 *
 * @ingroup Utilities
 *
 * @brief Useful functions for C-strings.
 *
 * @{
 */

/**
 * Get the length of a string.
 *
 * This function behaves as strlen() except that a `NULL` argument yield 0.
 *
 * @param str   String.
 *
 * @return The length of the string; 0 if @b str is `NULL`.
 *
 * @see strlen.
 */
extern size_t tao_strlen(
    const char* str);

/**
 * Strip directory part of a path.
 *
 * @param path   String.
 *
 * @return The path after the last `'/'` if any; `NULL` if @b path is `NULL`.
 */
extern const char* tao_basename(
    const char* path);

/**
 * @brief Check the validity of a C string.
 *
 * @param str    The string to check.
 * @param siz    The maximum number of characters including the final NUL.
 *
 * @return The length of the string (0 is @b str is `NULL`), or `-1` if the string is not NUL terminated.
 */
extern long tao_check_string(const char* str, long siz);

/**
 * @}
 */

/**
 * @defgroup Messages  Messages
 *
 * @ingroup Utilities
 *
 * @brief Management of messages.
 *
 * @{
 */

/**
 * @brief Level of message for logging.
 *
 * These enumeration values are in increasing order of seriousness so that it
 * is possible to use them to set a threshold for filtering messages.
 */
typedef enum tao_message_level_ {
    // Values must be in ascending order.
    TAO_MESG_DEBUG  = 0, ///< Debug message.
    TAO_MESG_INFO   = 1, ///< Information message.
    TAO_MESG_WARN   = 2, ///< Warning message.
    TAO_MESG_ERROR  = 3, ///< Runtime error.
    TAO_MESG_ASSERT = 4, ///< Assertion error or bug.
    TAO_MESG_FATAL  = 5, ///< Fatal error causing the process to exit.
    TAO_MESG_QUIET  = 6  ///< Suppress all messages.
} tao_message_level;

/**
 * Print a formatted message.
 *
 * Depending on the current threshold for printing messages, this function
 * either does nothing, or print a formatted message.
 *
 * This function is thread-safe.  Errors are ignored.
 *
 * @param output  File stream to print the message.  `stdout` is assumed if
 *                `NULL`.
 *
 * @param level   Seriousness of the message.  The message is printed if this
 *                value is greater of equal the current threshold set by
 *                tao_message_threshold_set().
 *
 * @param format  Format string (as for `printf`).
 *
 * @param ...     Other arguments.
 *
 * @warning Include `<stdio.h>` before `<tao-utils.h>` to have this function declared.
 */
#if defined(TAO_DOXYGEN_) || defined(BUFSIZ) // <stdio.h> included?
extern void tao_inform(
    FILE* output,
    tao_message_level level,
    const char* format,
    ...) TAO_FORMAT_PRINTF(3,4);
#endif

/**
 * Get the minimum level of printed messages.
 *
 * This function is thread-safe.
 *
 * @return The current minimum level for messages printed by tao_inform().
 */
extern tao_message_level tao_message_threshold_get(
    void);

/**
 * Set the minimum level of printed messages.
 *
 * This function is thread-safe.
 *
 * @param level   The minimum level of messages printed by tao_inform().  For
 *                instance, `TAO_MESG_QUIET` to suppress printing of messages
 *                or `TAO_MESG_DEBUG` to print everything.
 */
extern void tao_message_threshold_set(
    tao_message_level level);

/**
 * @}
 */

//-----------------------------------------------------------------------------

/**
 * @defgroup Commands  Command parsing
 *
 * @ingroup Utilities
 *
 * @brief Parsing of textual commands.
 *
 * These functions are provided to split lines of commands into arguments and
 * conversely to pack arguments into a command-line.
 *
 * Just like the `argv` array in C `main` function, a command is a list of
 * "words" which are ordinary C-strings.  All characters are allowed in a word
 * except the null character `\0` as it serves as a marker of the end of the
 * string.
 *
 * In order to communicate, processes may exchange commands packed into a
 * single string and sent through communication channels.  In order to allow
 * for sendind/receiving several successive commands and for coping with
 * partially transmitted commands, their size must be part of the sent data or
 * they must be terminated by some given marker.  In order to make things
 * simple, it has been chosen that successive commands be separated by a single
 * line-feed character (`'\n'`, ASCII code `0x0A`).  This also simplify the
 * writing of commands into scripts.
 *
 * String commands have to be parsed into words before being used.  Since any
 * character (but the null) is allowed in such words there must be means to
 * separate words in command strings and to allow for having a line-feed
 * character in a word (not a true one because it is used to indicate end of
 * the command string).
 *
 * The following rules are applied to parse a command string into words:
 *
 * 1- An end-of-line (EOL) sequence at the end of the command string is allowed
 *    and ignored.  To cope with different styles, an EOL can be any of the
 *    following sequences: a single carriage-return (CR, ASCII code `0x0D`)
 *    character, a single line-feed (LF, ASCII code `0x0A`) character or a
 *    sequence of the two characters CR-LF.
 *
 * 2- Leading and trailing spaces in a command string are ignored (trailing
 *    spaces may occur before the EOL sequence if any but not after).  Space
 *    characters are ordinary spaces `' '` (ASCII code `0x20`) or tabulations
 *    `'\t'` (ASCII code `0x09`).
 *
 * 3- Words are separated by one or more space characters.  A word is either a
 *    sequence of contiguous ordinary characters (non-space, non-quote,
 *    non-escape, non-forbidden) or a quoted string (see next).
 *
 * 6- Strings start with a quoting character (either a single or a double
 *    quote) and end with the same quoting character.  The opening and closing
 *    quotes are not part of the resulting word.  There must be at least one
 *    space character after (respectively before) the openning (respectively
 *    closing) quote if the string is not the first (respectively last) word of
 *    the sequence.  That is, quotes cannot be part of non-quoted words and are
 *    therefore not considered as ordinary characters.  There are 2 forms of
 *    quoted strings: strings enclosed in single quotes are extracted literaly
 *    (there may be double quotes inside but no single quotes and the escape
 *    character is considered as an ordinary character in literal strings) and
 *    strings enclosed in double quotes which may contain escape sequence to
 *    represent some special characters.  The following escape sequences are
 *    allowed and recognized in double quoted strings (any other occurrences of
 *    the escape character is considered as an error):
 *
 *   - `\t` yields an horizontal tabulation character;
 *   - `\n` yields a line-feed character;
 *   - `\r` yields a carriage-return character;
 *   - `\"` yields a double-quote character;
 *   - `\\` yields a backslash character.
 *
 * Thus quoted strings can have embedded spaces.  To have a zero-length word, a
 * quoted string like `''` or `""` must be used.
 *
 * The following errors may occur:
 *
 *  - Illegal characters: Anywhere but at the end of the command string, CR and
 *    LF characters are considered as an error.  This is because LF are used to
 *    separate successive commands in communication channels.  The null
 *    character must not appear in the command string (as it serves as end of
 *    string marker).
 *
 *  - Successive quoted strings not separated by a space.  Quotes appearing in
 *    a non-quoted word.  Unclosed quoted strings.  Unterminated escape
 *    sequences.
 *
 * @{
 */

/**
 * Split command in individual words.
 *
 * @param list   Address of a variable to store the list.  The list and its
 *               constitutive words are stored in a single block of memory.
 * @param cmd    Command line string to split.
 * @param len    Optional length of @b cmd.  If @b len is nonnegative, it is
 *               assumed to give the number of characters of the command line
 *               (which must not then be null-terminated).  Otherwise, @b len
 *               may be equal to `-1` and the command line must be
 *               null-terminated.
 *
 * The caller is responsible for properly initializing the list pointer to
 * `NULL` and calling free() when the list is no longer needed.  The list
 * argument can be re-used several times to parse different command lines.
 *
 * @fixme What are the advantages of this kind of allocation?  This is only
 * useful if we can avoid reallocating such lists.  There is no portable way to
 * query the size of a malloc'ed block of memory given its address.  The only
 * possibility is to make a hack an allocate more bytes to store the size
 * (however beware of alignment).  On my laptop, malloc() and free() of blocks
 * or random sizes (between 1 and 10,000 bytes) takes less than 100ns on
 * average.
 *
 * @return The number of words in the list; `-1` in case of failure.
 */
extern int tao_split_command(
    const char*** list,
    const char* cmd,
    long len);

/**
 * Pack words into a command-line.
 *
 * This function assembles given words into a single command-line.  This
 * function does the opposite of tao_unpack_words().
 *
 * @param dest   Address of an i/o buffer to store the result.  The resulting
 *               command-line is appended to any existing contents of @b dest.
 * @param argv   List of words.  The elements of the list must be non-null
 *               pointers to ordinary C-strings terminated by a null character.
 *               If @b argc is equal to `-1`, the list must have one more
 *               pointer then the number of words, this last pointer being set
 *               to `NULL` to mark the end of the list.
 * @param argc   Optional number of words in @b argv.  If @b argc is
 *               nonnegative, it is assumed to give the number of words in the
 *               list.  Otherwise, @b argc can be equal to `-1` to indicate
 *               that the first null-pointer in @b argv marks the end of the
 *               list.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 *
 * @note In case of failure, the contents of @b dest existing prior to the call
 * is untouched but its location may have change.
 *
 * @see tao_unpack_words.
 */
extern tao_status tao_pack_words(
    tao_buffer* dest,
    const char* argv[],
    int argc);

/**
 * Read an `int` value in a word.
 *
 * This function is intended to parse an integer value from a word as obtained
 * by splitting commands with tao_split_command().  To be valid, the word must
 * contains single integer in a human redable form and which fits in an `int`.
 *
 * @param str    Input word to parse.
 * @param ptr    Address to write parsed value.  This value is left unchanged
 *               in case of failure.
 * @param base   Base to use for conversion, `0` to apply C-style conventions.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.  Whatever
 *         the result, the caller's last error is left unchanged.
 */
extern tao_status tao_parse_int(
    const char* str,
    int* ptr,
    int base);

/**
 * Read a `long` value in a word.
 *
 * This function is intended to parse an integer value from a word as obtained
 * by splitting commands with tao_split_command().  To be valid, the word must
 * contains single integer in a human redable form and which fits in a `long`.
 *
 * @param str    Input word to parse.
 * @param ptr    Address to write parsed value.  This value is left unchanged
 *               in case of failure.
 * @param base   Base to use for conversion, `0` to apply C-style conventions.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.  Whatever
 *         the result, the caller's last error is left unchanged.
 */
extern tao_status tao_parse_long(
    const char* str,
    long* ptr,
    int base);

/**
 * Read a `double` value in a word.
 *
 * This function is intended to parse a floating point value from a word as
 * obtained by splitting commands with tao_split_command().  To be valid, the
 * word must contains single floating value in a human redable form.
 *
 * @param str    Input word to parse.
 * @param ptr    Address to write parsed value.  This value is left unchanged
 *               in case of failure.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.  Whatever
 *         the result, the caller's last error is left unchanged.
 */
extern tao_status tao_parse_double(
    const char* str,
    double* ptr);

/**
 * Transcode an ASCII string into a Unicode string.
 *
 * @param src   Input string. The input string must be a valid sequence of ASCII
 *              encoded characters stored in a null-terminated C-string. As a
 *              consequence, the input string cannot contains embedded nulls
 *              other than the mandatory final null.
 *
 * @param dst   Output string. On success, the output string is a valid
 *              null-terminated Unicode string.
 *
 * @param len   Number of characters available in output string. No more
 *              than this number of characters is written to the output string,
 *              including the final null.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure. It is
 *         considered as an error (@ref TAO_BAD_CHARACTER) if the source string
 *         contains non-ASCII character(s). The other possible error is that
 *         the output string is too short (@ref TAO_BAD_SIZE).
 */
extern tao_status tao_transcode_utf8_string_to_unicode(
    const char* src,
    wchar_t *dst,
    long len);

/**
 * Transcode a Unicode string into an ASCII string.
 *
 * @param src   Input string. The input string must be null-terminated
 *              sequence of valid Unicode characters. As a consequence, the
 *              input string cannot contains embedded nulls other than the
 *              mandatory final null.
 *
 * @param dst   Output string. On success, the output string is a valid
 *              null-terminated ASCII string.
 *
 * @param len   Number of characters available in output string. No more
 *              than this number of characters is written to the output string,
 *              including the final null.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure. It is
 *         considered as an error (@ref TAO_BAD_CHARACTER) if the source string
 *         contains non-ASCII character(s). The other possible error is that
 *         the output string is too short (@ref TAO_BAD_SIZE).
 */
tao_status tao_transcode_unicode_string_to_ascii(
    const wchar_t* src,
    char *dst,
    long len);

/**
 * Transcode an UTF-8 string into a Unicode string.
 *
 * @param src   Input string. The input string must be a valid sequence of UTF-8
 *              encoded characters stored in a null-terminated C-string. As a
 *              consequence, the input string cannot contains embedded nulls
 *              other than the mandatory final null.
 *
 * @param dst   Output string. On success, the output string is a valid
 *              null-terminated Unicode string.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure. It is
 *         considered as an error (@ref TAO_BAD_CHARACTER) if the source string
 *         contains invalid UTF-8 sequence(s). The other possible error is that
 *         the output string is too short (@ref TAO_BAD_SIZE).
 */
extern tao_status tao_transcode_utf8_string_to_unicode(
    const char* src,
    wchar_t *dst,
    long len);

/**
 * Transcode a Unicode string into an UTF-8 string.
 *
 * @param src   Input string. The input string must be null-terminated
 *              sequence of valid Unicode characters. As a consequence, the
 *              input string cannot contains embedded nulls other than the
 *              mandatory final null.
 *
 * @param dst   Output string. On success, the output string is a valid
 *              null-terminated UTF-8 string.
 *
 * @param len   Number of characters available in output string. No more
 *              than this number of characters is written to the output string,
 *              including the final null.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure. It is
 *         considered as an error (@ref TAO_BAD_CHARACTER) if the source string
 *         contains invalid Unicode character(s). The other possible error is
 *         that the output string is too short (@ref TAO_BAD_SIZE).
 */
tao_status tao_transcode_unicode_string_to_utf8(
    const wchar_t* src,
    char *dst,
    long len);

/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_UTILS_H_
