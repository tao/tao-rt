// test-basics.c -
//
// Test basic macros and functions in TAO library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include <stdbool.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <math.h>

#include <tao-basics.h>
#include <tao-macros.h>
#include <tao-utils.h>
#include <tao-threads.h>
#include <tao-generic.h>
#include <tao-shared-objects-private.h>
#include <tao-remote-objects-private.h>
#include <tao-rwlocked-objects-private.h>
#include <tao-cameras.h>

#define IS_SIGNED(T) ((T)(-1) < (T)(0))

#define show(out, fmt, expr) fprintf((out), "%s -> " fmt "\n", #expr, (expr))

#define test(expr)                                              \
    do {                                                        \
        if (! (expr)) {                                         \
            fprintf(stderr, "assertion failed: %s\n", #expr);   \
            ++nerrors;                                          \
        }                                                       \
        ++ntests;                                               \
    } while (false)

int main(
    int argc,
    char* argv[])
{
    long nerrors = 0, ntests = 0;
    const char* str_null = NULL;
    const char* str_empty = "";
    const char* str_hello = "hello";

    // Informations about the boolean type.
    printf("sizeof(bool) = %zu byte(s)\n", sizeof(bool));
    printf("true  = %d\n", (int)true);
    printf("false = %d\n", (int)false);

    // Print size of basic objects.
    printf("sizeof(tao_mutex) = %zd bytes\n",
           sizeof(tao_mutex));
    printf("sizeof(tao_cond) = %zd bytes\n",
           sizeof(tao_cond));
    printf("sizeof(tao_shared_object) = %zd bytes\n",
           sizeof(tao_shared_object));
    printf("sizeof(tao_remote_object) = %zd bytes\n",
           sizeof(tao_remote_object));
    printf("sizeof(tao_rwlocked_object) = %zd bytes\n",
           sizeof(tao_rwlocked_object));
    printf("sizeof(tao_camera_config) = %zd bytes\n",
           sizeof(tao_camera_config));

    // Check signedness of time_t.
    test(IS_SIGNED(time_t));

    // Check integer min/max macros.
#ifdef UCHAR_MIN
    test(UCHAR_MIN == TAO_MIN_UNSIGNED_INT(unsigned char));
#endif
    test(UCHAR_MAX == TAO_MAX_UNSIGNED_INT(unsigned char));
    test(SCHAR_MIN == TAO_MIN_SIGNED_INT(signed char));
    test(SCHAR_MAX == TAO_MAX_SIGNED_INT(signed char));

#ifdef USHRT_MIN
    test(USHRT_MIN == TAO_MIN_UNSIGNED_INT(unsigned short));
#endif
    test(USHRT_MAX == TAO_MAX_UNSIGNED_INT(unsigned short));
    test(SHRT_MIN == TAO_MIN_SIGNED_INT(short));
    test(SHRT_MAX == TAO_MAX_SIGNED_INT(short));

#ifdef UINT_MIN
    test(UINT_MIN == TAO_MIN_UNSIGNED_INT(unsigned int));
#endif
    test(UINT_MAX == TAO_MAX_UNSIGNED_INT(unsigned int));
    test(INT_MIN == TAO_MIN_SIGNED_INT(int));
    test(INT_MAX == TAO_MAX_SIGNED_INT(int));

#ifdef ULONG_MIN
    test(ULONG_MIN == TAO_MIN_UNSIGNED_INT(unsigned long));
#endif
    test(ULONG_MAX == TAO_MAX_UNSIGNED_INT(unsigned long));
    test(LONG_MIN == TAO_MIN_SIGNED_INT(long));
    test(LONG_MAX == TAO_MAX_SIGNED_INT(long));

#ifdef ULLONG_MIN
    test(ULLONG_MIN == TAO_MIN_UNSIGNED_INT(unsigned long long));
#endif
    test(ULLONG_MAX == TAO_MAX_UNSIGNED_INT(unsigned long long));
    test(LLONG_MIN == TAO_MIN_SIGNED_INT(long long));
    test(LLONG_MAX == TAO_MAX_SIGNED_INT(long long));

    test(TAO_UINT8_MAX == TAO_MAX_UNSIGNED_INT(uint8_t));
    test(TAO_INT8_MAX == TAO_MAX_SIGNED_INT(int8_t));
    test(TAO_INT8_MIN == TAO_MIN_SIGNED_INT(int8_t));
    test(TAO_INT8_MIN == (-TAO_INT8_MAX - 1));

    test(TAO_UINT16_MAX == TAO_MAX_UNSIGNED_INT(uint16_t));
    test(TAO_INT16_MAX == TAO_MAX_SIGNED_INT(int16_t));
    test(TAO_INT16_MIN == TAO_MIN_SIGNED_INT(int16_t));
    test(TAO_INT16_MIN == (-TAO_INT16_MAX - 1));

    test(TAO_UINT32_MAX == TAO_MAX_UNSIGNED_INT(uint32_t));
    test(TAO_INT32_MAX == TAO_MAX_SIGNED_INT(int32_t));
    test(TAO_INT32_MIN == TAO_MIN_SIGNED_INT(int32_t));
    test(TAO_INT32_MIN == (-TAO_INT32_MAX - 1));

    test(TAO_UINT64_MAX == TAO_MAX_UNSIGNED_INT(uint64_t));
    test(TAO_INT64_MAX == TAO_MAX_SIGNED_INT(int64_t));
    test(TAO_INT64_MIN == TAO_MIN_SIGNED_INT(int64_t));
    test(TAO_INT64_MIN == (-TAO_INT64_MAX - 1));

    test(TAO_CHAR_BITS == 8*sizeof(char));
    test(TAO_SHORT_BITS == 8*sizeof(short));
    test(TAO_INT_BITS == 8*sizeof(int));
    test(TAO_LONG_BITS == 8*sizeof(long));
    test(TAO_LLONG_BITS == 8*sizeof(long long));

    // Check formatting of integers.
    {
        char buffer[256];
        int16_t i16 = 1616;
        sprintf(buffer, TAO_INT16_FORMAT( ,d), i16);
        test(strcmp(buffer, "1616") == 0);
        sprintf(buffer, TAO_INT16_FORMAT(8,d), i16);
        test(strcmp(buffer, "    1616") == 0);
        int32_t i32 = 32;
        sprintf(buffer, TAO_INT32_FORMAT( ,d), i32);
        test(strcmp(buffer, "32") == 0);
        sprintf(buffer, TAO_INT32_FORMAT(-8,d), i32);
        test(strcmp(buffer, "32      ") == 0);
        int64_t i64 = -32915;
        int64_t u64 =  32789;
        sprintf(buffer, TAO_INT64_FORMAT( ,d), i64);
        test(strcmp(buffer, "-32915") == 0);
        sprintf(buffer, TAO_INT64_FORMAT(8,d), i64);
        test(strcmp(buffer, "  -32915") == 0);
        sprintf(buffer, TAO_INT64_FORMAT( ,u), u64);
        test(strcmp(buffer, "32789") == 0);
        sprintf(buffer, TAO_INT64_FORMAT(-8,u), u64);
        test(strcmp(buffer, "32789   ") == 0);
    }

    // Check TAO_STRLEN.
    test(TAO_STRLEN(str_null) == 0);
    test(TAO_STRLEN(str_empty) == 0);
    test(TAO_STRLEN(str_hello) == 5);
    test(TAO_STRLEN("") == 0);
    test(TAO_STRLEN("hello") == 5);

    // Test time functions/macros.
    test(TAO_MILLISECONDS_PER_SECOND == 1000);
    test(TAO_MICROSECONDS_PER_SECOND == 1000*TAO_MILLISECONDS_PER_SECOND);
    test(TAO_NANOSECONDS_PER_SECOND  == 1000*TAO_MICROSECONDS_PER_SECOND);
    test(fabs(TAO_MILLISECOND*TAO_MILLISECONDS_PER_SECOND - 1) < 1e-16);
    test(fabs(TAO_MICROSECOND*TAO_MICROSECONDS_PER_SECOND - 1) < 1e-16);
    test(fabs(TAO_NANOSECOND*TAO_NANOSECONDS_PER_SECOND - 1) < 1e-16);
    test(fabs(TAO_MINUTE/(60*TAO_SECOND) - 1) < 1e-16);
    test(fabs(TAO_HOUR/(60*TAO_MINUTE) - 1) < 1e-16);
    test(fabs(TAO_DAY/(24*TAO_HOUR) - 1) < 1e-16);
    test(fabs(TAO_YEAR/(365.25*TAO_DAY) - 1) < 1e-16);

    // Test printing of time-stamps.
    {
        tao_time t;
        tao_get_current_time(&t);
        fprintf(stdout, "  Current time (1): ");
        tao_time_fprintf(stdout, TAO_TIME_FORMAT_FRACTIONAL_SECONDS, &t);
        fprintf(stdout, "\n");
        fprintf(stdout, "  Current time (1): ");
        tao_time_fprintf(stdout, TAO_TIME_FORMAT_DATE_WITH_NANOSECONDS, &t);
        fprintf(stdout, "\n");
        fprintf(stdout, "  Current time (3): ");
        tao_time_fprintf(stdout, TAO_TIME_FORMAT_DATE_WITH_NANOSECONDS, NULL);
        fprintf(stdout, "\n");
        fprintf(stdout, "  Current time (4): ");
        tao_time_fprintf(stdout, TAO_TIME_FORMAT_DATE_WITH_NANOSECONDS, NULL);
        fprintf(stdout, "\n");
    }

    // Test tao_min generic macros.
    test(tao_min( 3.0,  3.0) ==  3.0);
    test(tao_min( 1.0, -2.0) == -2.0);
    test(tao_min(-1.0,  0.0) == -1.0);
    test(tao_min( (float)3.0,  (float)3.0) ==  (float)3.0);
    test(tao_min( (float)1.0, -(float)2.0) == -(float)2.0);
    test(tao_min(-(float)1.0,  (float)0.0) == -(float)1.0);

    // Test tao_max generic macros.
    test(tao_max( 3.0,  3.0) ==  3.0);
    test(tao_max( 1.0, -2.0) ==  1.0);
    test(tao_max(-1.0,  0.0) ==  0.0);
    test(tao_max( (float)3.0,  (float)3.0) ==  (float)3.0);
    test(tao_max( (float)1.0, -(float)2.0) ==  (float)1.0);
    test(tao_max(-(float)1.0,  (float)0.0) ==  (float)0.0);

    // Test tao_clamp generic macro.
    test(tao_clamp( 1.0, -2.0, 3.0) ==  1.0);
    test(tao_clamp(-4.0, -2.0, 3.0) == -2.0);
    test(tao_clamp( 4.0, -2.0, 3.0) ==  3.0);
    test(tao_clamp( (float)1.0, -(float)2.0, (float)3.0) ==  (float)1.0);
    test(tao_clamp(-(float)4.0, -(float)2.0, (float)3.0) == -(float)2.0);
    test(tao_clamp( (float)4.0, -(float)2.0, (float)3.0) ==  (float)3.0);

    // Test tao_safe_clamp generic macro.
    test(tao_safe_clamp( 1.0, -2.0, 3.0, 11.0) ==  1.0);
    test(tao_safe_clamp(-4.0, -2.0, 3.0, 11.0) == -2.0);
    test(tao_safe_clamp( 4.0, -2.0, 3.0, 11.0) ==  3.0);
    test(tao_safe_clamp( (float)1.0, -(float)2.0, (float)3.0, (float)11.0) ==  (float)1.0);
    test(tao_safe_clamp(-(float)4.0, -(float)2.0, (float)3.0, (float)11.0) == -(float)2.0);
    test(tao_safe_clamp( (float)4.0, -(float)2.0, (float)3.0, (float)11.0) ==  (float)3.0);
    test(tao_safe_clamp((double)NAN, -2.0, 3.0, 11.0) == 11.0);
    show(stdout, "%g", tao_clamp((double)NAN, -2.0, 3.0));
    show(stdout, "%g", tao_safe_clamp((double)NAN, -2.0, 3.0, 11.0));
    test(tao_safe_clamp( (float)NAN, -(float)2.0, (float)3.0, (float)11.0) == (float)11.0);

    // Summary.
    fprintf(stdout, "%ld test(s) passed\n", ntests - nerrors);
    fprintf(stdout, "%ld test(s) failed\n", nerrors);

    return (nerrors == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
