// tao-time.h -
//
// Definitions of time types and functions in TAO.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2019-2024, Éric Thiébaut.

#ifndef TAO_TIME_H_
#define TAO_TIME_H_ 1

#include <tao-basics.h>
#include <tao-macros.h>
#include <tao-buffers.h>

TAO_BEGIN_DECLS

/**
 * @defgroup Time  Time tools
 *
 * @ingroup Utilities
 *
 * @brief Measurement of time and time intervals.
 *
 * Time is stored in a structure of type @ref tao_time and which has a nanosecond
 * resolution. The actual precision however depends on the resolution of the functions
 * provided by the system to get a time. The maximum time amplitude that can be
 * represented is @f$\approx\pm2.9\times10^{11}@f$ years (nearly 20 times the age of the
 * Universe). So it is probably sufficient to represent any absolute time.
 *
 * The function tao_get_monotonic_time() can be used to precisely measure time intervals,
 * while the function tao_get_current_time() can be called to get the current time.
 *
 * @{
 */

/**
 * Type for the members of a time structure.
 */
typedef int64_t tao_time_member;

/**
 * Structure to store time with nanosecond resolution.
 */
typedef struct tao_time_ {
    tao_time_member  sec; ///< Number of seconds
    tao_time_member nsec; ///< Number of nanoseconds
} tao_time;

/**
 * Build a time value.
 *
 * The call `TAO_TIME(s,ns)` yields a compound literal of type @ref tao_time for `s`
 * seconds and `ns` nanoseconds.
 */
#define TAO_TIME(s, ns) ((tao_time){.sec = (s), .nsec = (ns)})

/**
 * Time value when unknown/unset.
 *
 * This macro yields the constant of type @ref tao_time used to represent unknown or unset
 * time.
 */
#define TAO_UNKNOWN_TIME TAO_TIME(0,0)

/**
 * Maximum number of seconds in a TAO time structure.
 *
 * This macro yields the maximum number of seconds that fit in a
 * `tao_time_member` integer.
 *
 * @see TAO_MAX_TIME_SECONDS.
 */
#define TAO_MAX_TIME_SECONDS TAO_MAX_SIGNED_INT(tao_time_member)

/**
 * Minimum number of seconds in a TAO time structure.
 *
 * This macro yields the minimum number of seconds that fit in a `tao_time_member`
 * integer.
 *
 * @see TAO_MAX_TIME_SECONDS.
 */
#define TAO_MIN_TIME_SECONDS TAO_MIN_SIGNED_INT(tao_time_member)


/**
 * Number of nanoseconds per second.
 */
#define TAO_NANOSECONDS_PER_SECOND  (1000000000)

/**
 * Number of microseconds per second.
 */
#define TAO_MICROSECONDS_PER_SECOND  (1000000)

/**
 * Number of milliseconds per second.
 */
#define TAO_MILLISECONDS_PER_SECOND  (1000)

/**
 * One nanosecond in SI units (seconds).
 */
#define TAO_NANOSECOND (1e-9*TAO_SECOND)

/**
 * One microsecond in SI units (seconds).
 */
#define TAO_MICROSECOND (1e-6*TAO_SECOND)

/**
 * One millisecond in SI units (seconds).
 */
#define TAO_MILLISECOND (1e-3*TAO_SECOND)

/**
 * One second in SI units (seconds).
 */
#define TAO_SECOND (1.0)

/**
 * One minute in SI units (seconds).
 */
#define TAO_MINUTE (60*TAO_SECOND)

/**
 * One hour in SI units (seconds).
 */
#define TAO_HOUR (60*TAO_MINUTE)

/**
 * One day in SI units (seconds).
 */
#define TAO_DAY (24*TAO_HOUR)

/**
 * One year in SI units (seconds).
 */
#define TAO_YEAR (365.25*TAO_DAY)

/**
 * @def TAO_TIMEOUT_MIN
 *
 * Macro `TAO_TIMEOUT_MIN` yields the minimum relative timeout. This value should not be
 * smaller than the clock resolution (one nanosecond) and should be comparable to the time
 * resolution of the real-time task scheduler (a few microseconds). Any relative timeouts
 * smaller (in absolute value) than `TAO_TIMEOUT_MIN` are assumed to be equal to zero in
 * practice.
 *
 * The current value of `TAO_TIMEOUT_MIN` is 1 µs.
 */
#define TAO_TIMEOUT_MIN 1.0e-6

/**
 * @def TAO_TIMEOUT_MAX
 *
 * Macro `TAO_TIMEOUT_MAX` yields the maximum relative timeout. This value should not be
 * larger than @ref TAO_MAX_TIME_SECONDS. Any relative timeouts larger than
 * `TAO_TIMEOUT_MAX` are assumed to be infinite in practice.
 */
#define TAO_TIMEOUT_MAX ((double)TAO_MAX_TIME_SECONDS)

/**
 * Possible values returned by tao_get_timeout()
 *
 * The function tao_get_timeout()
 */
typedef enum tao_timeout_ {
    TAO_TIMEOUT_ERROR  = -2,
    TAO_TIMEOUT_PAST   = -1,
    TAO_TIMEOUT_NOW    =  0,
    TAO_TIMEOUT_FUTURE =  1,
    TAO_TIMEOUT_NEVER  =  2
} tao_timeout;

/**
 * Sleep for a specified hight-resolution number of seconds.
 *
 * This function causes the calling thread to sleep either until the number of specified
 * seconds have elapsed or until a signal arrives which is not ignored.
 *
 * @param secs   The amount of time to sleep in seconds. Must be nonnegative and less than
 *               the maximum value of a `tao_time_member` integer which is is fairly large
 *               (at least about 68 years). Can have a high resolution, at least one
 *               microsecond but, depepending on the operationg system, it may be as good
 *               as one nanosecond.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure or
 *         interrupt.
 */
extern tao_status tao_sleep(
    double secs);

/**
 * Copy time.
 *
 * Copy time in structure pointed by @a src to structure pointed by @a dst.
 *
 * @note It is probably faster (and simpler) to just do `*dst = *src` as is allowed by
 * ANSI C to copy structures.
 *
 * @param dst   The address of the destination time structure.
 * @param src   The address of the source time structure.
 *
 * @return @a dst.
 *
 * @see TAO_COPY_TIME.
 */
extern tao_time* tao_copy_time(
    tao_time* dst,
    const tao_time* src);

/**
 * Get monotonic time.
 *
 * This function yields a monotonic time since some unspecified starting point but which
 * is not affected by discontinuous jumps in the system time (e.g., if the system
 * administrator manually changes the clock), but is affected by the incremental
 * adjustments performed by adjtime() and NTP.
 *
 * @warning For systems where it is not possible to retrieve a monotonic time, the time
 *          given by tao_get_current_time() is returned instead.
 *
 * @param dest   Address to store the time. In case of failure, @ref TAO_UNKNOWN_TIME is
 *               stored in @b dest.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_get_monotonic_time(
    tao_time* dest);

/**
 * Get the current time.
 *
 * This function yields the current time since the
 * [Epoch](https://en.wikipedia.org/wiki/Unix_time), that is 00:00:00 UTC, 1 January 1970.
 *
 * @param dest   Address to store the time.  In case of failure,
 *               @ref TAO_UNKNOWN_TIME is stored in @b dest.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_get_current_time(
    tao_time* dest);

/**
 * @def TAO_TIME_NORMALIZE(T, s, ns)
 *
 * This macro normalize the time specified by the L-values `s` and `ns` (respectively the
 * number of seconds and of nanoseconds). Argument `T` is the integer type to use for
 * temporaries. This macro ensures that the number of nanoseconds is in the range 0 to
 * 999,999,999.
 *
 * @see tao_time_normalize.
 */
#define TAO_TIME_NORMALIZE(T, s, ns)                            \
    do {                                                        \
        if ((ns) < 0) {                                         \
            T adj_ = 1 - (ns)/TAO_NANOSECONDS_PER_SECOND;       \
            (s) -= adj_;                                        \
            (ns) += adj_*TAO_NANOSECONDS_PER_SECOND;            \
        } else if ((ns) >= TAO_NANOSECONDS_PER_SECOND) {        \
            T adj_ = ((ns)/TAO_NANOSECONDS_PER_SECOND);         \
            (s) += adj_;                                        \
            (ns) -= adj_*TAO_NANOSECONDS_PER_SECOND;            \
        }                                                       \
    } while (false)

/**
 * Normalize time.
 *
 * This function adjusts the members of a @ref tao_time structure so that the time stored
 * in the structure is correct. More specifically it ensures that the number of
 * nanoseconds is in the range 0 to 999,999,999.
 *
 * @param ts   Address of a  @ref tao_time structure to adjust.
 *
 * @return The address @a ts.
 *
 * @see TAO_TIME_NORMALIZE.
 */
extern tao_time* tao_time_normalize(
    tao_time* ts);

/**
 * Add times.
 *
 * This function adds 2 times.
 *
 * @warning This function is meant to be fast. It makes no checking about the validity of
 * the arguments nor integer oveflows. Normally the destination time is such that the
 * number of nanoseconds is nonnegative and strictly less than 1,000,000,000.
 *
 * @param dest   Address to store the result.
 * @param a      Address of first time value.
 * @param b      Address of second time value.
 *
 * @return The address @a dest.
 */
extern tao_time* tao_time_add(
    tao_time* dest,
    const tao_time* a,
    const tao_time* b);

/**
 * Subtract times.
 *
 * This function subtracts 2 times.
 *
 * @warning This function is meant to be fast. It makes no checking about the validity of
 * the arguments nor integer oveflows. Normally the destination time is such that the
 * number of nanoseconds is nonnegative and strictly less than 1,000,000,000.
 *
 * @param dest   Address to store the result.
 * @param a      Address of first time value.
 * @param b      Address of second time value.
 *
 * @return The address @a dest.
 */
extern tao_time* tao_time_subtract(
    tao_time* dest,
    const tao_time* a,
    const tao_time* b);

/**
 * Convert time in seconds.
 *
 * @param t      Address of time value.
 *
 * @return The number of seconds given by the time stored in @b t.
 */
extern double tao_time_to_seconds(
    const tao_time* t);

/**
 * Convert time in milliseconds.
 *
 * @param t      Address of time value.
 *
 * @return The number of milliseconds given by the time stored in @b t.
 */
extern double tao_time_to_milliseconds(
    const tao_time* t);

/**
 * Convert time in microseconds.
 *
 * @param t      Address of time value.
 *
 * @return The number of microseconds given by the time stored in @b t.
 */
extern double tao_time_to_microseconds(
    const tao_time* t);

/**
 * Convert time in nanoseconds.
 *
 * @param t      Address of time value.
 *
 * @return The number of nanoseconds given by the time stored in @b t.
 */
extern double tao_time_to_nanoseconds(
    const tao_time* t);

/**
 * Elapsed time in seconds.
 *
 * @param t      Address of time value.
 * @param t0     Address of time value at origin.
 *
 * @return The number of elapsed seconds at time @b t since time @b t0.
 */
extern double tao_elapsed_seconds(
    const tao_time* t,
    const tao_time* t0);

/**
 * Elapsed time in milliseconds.
 *
 * @param t      Address of time value.
 * @param t0     Address of time value at origin.
 *
 * @return The number of elapsed milliseconds at time @b t since time @b t0.
 */
extern double tao_elapsed_milliseconds(
    const tao_time* t,
    const tao_time* t0);

/**
 * Elapsed time in microseconds.
 *
 * @param t      Address of time value.
 * @param t0     Address of time value at origin.
 *
 * @return The number of elapsed microseconds at time @b t since time @b t0.
 */
extern double tao_elapsed_microseconds(
    const tao_time* t,
    const tao_time* t0);

/**
 * Elapsed time in nanoseconds.
 *
 * @param t      Address of time value.
 * @param t0     Address of time value at origin.
 *
 * @return The number of elapsed nanoseconds at time @b t since time @b t0.
 */
extern double tao_elapsed_nanoseconds(
    const tao_time* t,
    const tao_time* t0);

/**
 * Convert a number of seconds into a time structure.
 *
 * @param dest   Address to store the result.
 * @param secs   A fractional number of seconds.
 *
 * @return The address @b dest.
 *
 * @warning This function never fails. If @b secs is too large (in amplitude) to be
 * represented, `INT64_MAX` or `INT64_MIN` seconds and 0 nanoseconds are assumed. If @b
 * secs is a NaN (Not a Number), 0 seconds and -1 nanoseconds are assumed. Otherwise, the
 * number of seconds stored in @b dest is strictly greater than `INT64_MIN` and strictly
 * less than `INT64_MAX` while the number of nanoseconds is greater or equal 0 and
 * strictly less than 1,000,000,000. It is therefore always possible guess from the stored
 * time whether @b secs was representable as a time structure with nanosecond precision.
 */
extern tao_time* tao_seconds_to_time(
    tao_time* dest,
    double secs);

/**
 * Convert a timespec structure into a time structure.
 *
 * This function converts a `timespec` structure into a time structure taking
 * care of normalizing the result.
 *
 * @param dst   Address to store the result.
 * @param src   Address of timespec structure (must not be `NULL`).
 *
 * @return The address @b dst.
 */
extern tao_time* tao_timespec_to_time(
    tao_time* dst,
    const struct timespec* src);

/**
 * Convert a timeval structure into a time structure.
 *
 * This function convert a `timeval` structure into a time structure taking
 * care of normalizing the result.
 *
 * @param dst   Address to store the result.
 * @param src   Address of timeval structure (must not be `NULL`).
 *
 * @return The address @b dst.
 *
 * @warning Include `<time.h>` before `<tao-time.h>` to have this function declared.
 */
#if defined(TAO_DOXYGEN_) || defined(CLOCK_PER_SEC) // <time.h> included?
extern tao_time* tao_timeval_to_time(
    tao_time* dst,
    const struct timeval* src);
#endif


/**
 * Possible formats for printing a time-stamp.
 */
typedef enum tao_time_format_ {
    TAO_TIME_FORMAT_FRACTIONAL_SECONDS,
    TAO_TIME_FORMAT_DATE_WITH_SECONDS,
    TAO_TIME_FORMAT_DATE_WITH_MILLISECONDS,
    TAO_TIME_FORMAT_DATE_WITH_MICROSECONDS,
    TAO_TIME_FORMAT_DATE_WITH_NANOSECONDS
} tao_time_format;

/**
 * Print a time-stamp in a human readable form to a string.
 *
 * @param str    Destination string (must have at least 64 characters
 *               including the terminating null).
 *
 * @param fmt    Format.
 *
 * @param ts     Time-stamp.  The current time is used if `NULL`.
 *
 * @return The address @b str.
 */
extern char* tao_time_sprintf(
    char* str,
    tao_time_format fmt,
    const tao_time* ts);

/**
 * Print a time-stamp in a human readable form to a string.
 *
 * If the destination @a str is non-`NULL`, this function writes as much as possible of
 * the resulting string in @a str but no more than `size` bytes and with a terminating
 * null. In any cases, the length of the string corresponding to the complete result
 * (excluding the terminating null) is returned.
 *
 * If `format` is @ref TAO_TIME_FORMAT_FRACTIONAL_SECONDS, the result is of the form
 * `seconds.nanoseconds`; otherwise, the format is `"%Y-%m-%d %H:%M:%S"` followed by the
 * number of nanoseconds.
 *
 * @param str    Destination string (nothing is written there if `NULL`).
 *
 * @param size   Number of bytes available in the destination string.
 *
 * @param fmt    Format.
 *
 * @param ts     Time-stamp.  The current time is used if `NULL`.
 *
 * @return The number of bytes needed to store the complete formatted string (excluding
 *         the terminating null). Thus, a return value of @a size or more means that the
 *         output was truncated.
 */
extern long tao_time_snprintf(
    char* str,
    long size,
    tao_time_format fmt,
    const tao_time* ts);

/**
 * Print a time-stamp in a human readable form to a file stream.
 *
 * See tao_time_snprintf() for details.
 *
 * @param stream Destination stream.
 *
 * @param fmt    Format.
 *
 * @param ts     Time-stamp.  The current time is used if `NULL`.
 *
 * @warning Include `<stdio.h>` before `<tao-time.h>` to have this function declared.
 */
#if defined(TAO_DOXYGEN_) || defined(BUFSIZ) // <stdio.h> included?
extern void tao_time_fprintf(
    FILE* stream,
    tao_time_format fmt,
    const tao_time* ts);
#endif

/**
 * Adjust high resolution ttime.
 *
 * This function adjusts a given high resolution time by a given number of seconds and
 * taking care of overflows.
 *
 * @param time   Address of high resolution time to adjust. Must not be `NULL`
 *
 * @param secs   Relative number of seconds.
 *
 * @return @ref TAO_OK on success or @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_adjust_time(
    tao_time* time,
    double secs);

/**
 * Determine kind of time limit.
 *
 * This function determines the kind of time limit set by a duration relative to the
 * current time (as given by the clock `CLOCK_REALTIME`) and returns one of the following
 * values:
 *
 * - @ref TAO_TIMEOUT_NEVER if the duration sets no time limit in practice.
 *
 * - @ref TAO_TIMEOUT_FUTURE if the time limit is in a reachable future. In that case, the
 *   absolute time limit is stored in the provided @ref tao_time structure (if not
 *   `NULL`).
 *
 * - @ref TAO_TIMEOUT_NOW if the absolute value of the duration is smaller than the time
 *   resolution (given by @ref TAO_TIMEOUT_MIN).
 *
 * - @ref TAO_TIMEOUT_PAST if the time limit is in the past.
 *
 * - @ref TAO_TIMEOUT_ERROR if some errors occurred.
 *
 * @param abstime  If not `NULL`, address of `tao_time` structure where the absolute time
 *                 limit is stored when @ref TAO_TIMEOUT_FUTURE is returned. Contents is
 *                 irrelevant for other returned values.
 *
 * @param secs     Number of seconds from now.
 *
 * @return A @ref tao_timeout value.
 *
 * @see tao_timeout, TAO_TIMEOUT_MIN, tao_get_maximum_absolute_time().
 */
extern tao_timeout tao_get_timeout(
    tao_time* abstime,
    double secs);

/**
 * Maximum number of seconds since the Epoch.
 *
 * This function yields the maximum number of seconds that fit in a `tao_time_member`
 * integer. Type `tao_time_member` is 64-bit signed integer which gives a maximum of about
 * 9.223372036854776e18 seconds (more than 2.9e11 years) since the Epoch.
 *
 * @return A large number of seconds.
 *
 * @see TAO_MAX_TIME_SECONDS.
 */
extern double tao_get_maximum_absolute_time(
    void);

/**
 * Structure to collect time statistics data.
 */
typedef struct tao_time_stat_data_ {
    double  min; ///< Minimum time
    double  max; ///< Maximum time
    double sum1; ///< Sum of times
    double sum2; ///< Sum of squared times
    size_t numb; ///< Number of collected samples
} tao_time_stat_data;

/**
 * Structure to store time statistics.
 */
typedef struct tao_time_stat_ {
    double  min; ///< Minimum time
    double  max; ///< Maximum time
    double  avg; ///< Mean time
    double  std; ///< Standard deviation
    size_t numb; ///< Number of tests
} tao_time_stat;

/**
 * Initialize or reset time statistics data.
 *
 * Call this function before integrating time statistics with
 * tao_update_time_statistics().
 *
 * @param tsd  Pointer to time statistics data structure.
 */
extern void tao_initialize_time_statistics(
    tao_time_stat_data* tsd);

/**
 * Integrate time statistics data.
 *
 * This function accounts for a new time sample.
 *
 * @warning All time samples must be given with the same units.
 *
 * @param tsd  Pointer to time statistics data structure.
 * @param t    New time sample.
 */
extern void tao_update_time_statistics(
    tao_time_stat_data* tsd,
    double t);

/**
 * Compute time statistics.
 *
 * This function compute time statistics in the same temporal units as the collected time
 * samples.
 *
 * @param ts   Pointer to destination time statistics structure.
 * @param tsd  Pointer to source time statistics data structure.
 *
 * @return The destination.
 */
extern tao_time_stat* tao_compute_time_statistics(
    tao_time_stat* ts,
    tao_time_stat_data const* tsd);

/**
 * Print time statistics.
 *
 * It is assumed that time samples are in seconds.
 *
 * @param out    Output file stream.
 * @param pfx    Suffix for each printed lines.
 * @param ts     Pointer to time statistics structure.
 *
 * @warning Include `<stdio.h>` before `<tao-time.h>` to have this function declared.
 */
#if defined(TAO_DOXYGEN_) || defined(BUFSIZ) // <stdio.h> included?
extern void tao_print_time_statistics(
    FILE* out,
    char const* pfx,
    tao_time_stat const* ts);
#endif

/**
 * @}
 */

//-----------------------------------------------------------------------------

/**
 * @defgroup Commands  Command parsing
 *
 * @ingroup Utilities
 *
 * @brief Parsing of textual commands.
 *
 * These functions are provided to split lines of commands into arguments and
 * conversely to pack arguments into a command-line.
 *
 * Just like the `argv` array in C `main` function, a command is a list of
 * "words" which are ordinary C-strings.  All characters are allowed in a word
 * except the null character `\0` as it serves as a marker of the end of the
 * string.
 *
 * In order to communicate, processes may exchange commands packed into a
 * single string and sent through communication channels.  In order to allow
 * for sendind/receiving several successive commands and for coping with
 * partially transmitted commands, their size must be part of the sent data or
 * they must be terminated by some given marker.  In order to make things
 * simple, it has been chosen that successive commands be separated by a single
 * line-feed character (`'\n'`, ASCII code `0x0A`).  This also simplify the
 * writing of commands into scripts.
 *
 * String commands have to be parsed into words before being used.  Since any
 * character (but the null) is allowed in such words there must be means to
 * separate words in command strings and to allow for having a line-feed
 * character in a word (not a true one because it is used to indicate end of
 * the command string).
 *
 * The following rules are applied to parse a command string into words:
 *
 * 1- An end-of-line (EOL) sequence at the end of the command string is allowed
 *    and ignored.  To cope with different styles, an EOL can be any of the
 *    following sequences: a single carriage-return (CR, ASCII code `0x0D`)
 *    character, a single line-feed (LF, ASCII code `0x0A`) character or a
 *    sequence of the two characters CR-LF.
 *
 * 2- Leading and trailing spaces in a command string are ignored (trailing
 *    spaces may occur before the EOL sequence if any but not after).  Space
 *    characters are ordinary spaces `' '` (ASCII code `0x20`) or tabulations
 *    `'\t'` (ASCII code `0x09`).
 *
 * 3- Words are separated by one or more space characters.  A word is either a
 *    sequence of contiguous ordinary characters (non-space, non-quote,
 *    non-escape, non-forbidden) or a quoted string (see next).
 *
 * 6- Strings start with a quoting character (either a single or a double
 *    quote) and end with the same quoting character.  The opening and closing
 *    quotes are not part of the resulting word.  There must be at least one
 *    space character after (respectively before) the openning (respectively
 *    closing) quote if the string is not the first (respectively last) word of
 *    the sequence.  That is, quotes cannot be part of non-quoted words and are
 *    therefore not considered as ordinary characters.  There are 2 forms of
 *    quoted strings: strings enclosed in single quotes are extracted literaly
 *    (there may be double quotes inside but no single quotes and the escape
 *    character is considered as an ordinary character in literal strings) and
 *    strings enclosed in double quotes which may contain escape sequence to
 *    represent some special characters.  The following escape sequences are
 *    allowed and recognized in double quoted strings (any other occurrences of
 *    the escape character is considered as an error):
 *
 *   - `\t` yields an horizontal tabulation character;
 *   - `\n` yields a line-feed character;
 *   - `\r` yields a carriage-return character;
 *   - `\"` yields a double-quote character;
 *   - `\\` yields a backslash character.
 *
 * Thus quoted strings can have embedded spaces.  To have a zero-length word, a
 * quoted string like `''` or `""` must be used.
 *
 * The following errors may occur:
 *
 *  - Illegal characters: Anywhere but at the end of the command string, CR and
 *    LF characters are considered as an error.  This is because LF are used to
 *    separate successive commands in communication channels.  The null
 *    character must not appear in the command string (as it serves as end of
 *    string marker).
 *
 *  - Successive quoted strings not separated by a space.  Quotes appearing in
 *    a non-quoted word.  Unclosed quoted strings.  Unterminated escape
 *    sequences.
 *
 * @{
 */

/**
 * Split command in individual words.
 *
 * @param list   Address of a variable to store the list.  The list and its
 *               constitutive words are stored in a single block of memory.
 * @param cmd    Command line string to split.
 * @param len    Optional length of @b cmd.  If @b len is nonnegative, it is
 *               assumed to give the number of characters of the command line
 *               (which must not then be null-terminated).  Otherwise, @b len
 *               may be equal to `-1` and the command line must be
 *               null-terminated.
 *
 * The caller is responsible for properly initializing the list pointer to
 * `NULL` and calling free() when the list is no longer needed.  The list
 * argument can be re-used several times to parse different command lines.
 *
 * @fixme What are the advantages of this kind of allocation?  This is only
 * useful if we can avoid reallocating such lists.  There is no portable way to
 * query the size of a malloc'ed block of memory given its address.  The only
 * possibility is to make a hack an allocate more bytes to store the size
 * (however beware of alignment).  On my laptop, malloc() and free() of blocks
 * or random sizes (between 1 and 10,000 bytes) takes less than 100ns on
 * average.
 *
 * @return The number of words in the list; `-1` in case of failure.
 */
extern int tao_split_command(
    const char*** list,
    const char* cmd,
    long len);

/**
 * Pack words into a command-line.
 *
 * This function assembles given words into a single command-line.  This
 * function does the opposite of tao_unpack_words().
 *
 * @param dest   Address of an i/o buffer to store the result.  The resulting
 *               command-line is appended to any existing contents of @b dest.
 * @param argv   List of words.  The elements of the list must be non-null
 *               pointers to ordinary C-strings terminated by a null character.
 *               If @b argc is equal to `-1`, the list must have one more
 *               pointer then the number of words, this last pointer being set
 *               to `NULL` to mark the end of the list.
 * @param argc   Optional number of words in @b argv.  If @b argc is
 *               nonnegative, it is assumed to give the number of words in the
 *               list.  Otherwise, @b argc can be equal to `-1` to indicate
 *               that the first null-pointer in @b argv marks the end of the
 *               list.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 *
 * @note In case of failure, the contents of @b dest existing prior to the call
 * is untouched but its location may have change.
 *
 * @see tao_unpack_words.
 */
extern tao_status tao_pack_words(
    tao_buffer* dest,
    const char* argv[],
    int argc);

/**
 * Read an `int` value in a word.
 *
 * This function is intended to parse an integer value from a word as obtained
 * by splitting commands with tao_split_command().  To be valid, the word must
 * contains single integer in a human redable form and which fits in an `int`.
 *
 * @param str    Input word to parse.
 * @param ptr    Address to write parsed value.  This value is left unchanged
 *               in case of failure.
 * @param base   Base to use for conversion, `0` to apply C-style conventions.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.  Whatever
 *         the result, the caller's last error is left unchanged.
 */
extern tao_status tao_parse_int(
    const char* str,
    int* ptr,
    int base);

/**
 * Read a `long` value in a word.
 *
 * This function is intended to parse an integer value from a word as obtained
 * by splitting commands with tao_split_command().  To be valid, the word must
 * contains single integer in a human redable form and which fits in a `long`.
 *
 * @param str    Input word to parse.
 * @param ptr    Address to write parsed value.  This value is left unchanged
 *               in case of failure.
 * @param base   Base to use for conversion, `0` to apply C-style conventions.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.  Whatever
 *         the result, the caller's last error is left unchanged.
 */
extern tao_status tao_parse_long(
    const char* str,
    long* ptr,
    int base);

/**
 * Read a `double` value in a word.
 *
 * This function is intended to parse a floating point value from a word as
 * obtained by splitting commands with tao_split_command().  To be valid, the
 * word must contains single floating value in a human redable form.
 *
 * @param str    Input word to parse.
 * @param ptr    Address to write parsed value.  This value is left unchanged
 *               in case of failure.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.  Whatever
 *         the result, the caller's last error is left unchanged.
 */
extern tao_status tao_parse_double(
    const char* str,
    double* ptr);

/**
 * Transcode an ASCII string into a Unicode string.
 *
 * @param src   Input string. The input string must be a valid sequence of ASCII
 *              encoded characters stored in a null-terminated C-string. As a
 *              consequence, the input string cannot contains embedded nulls
 *              other than the mandatory final null.
 *
 * @param dst   Output string. On success, the output string is a valid
 *              null-terminated Unicode string.
 *
 * @param len   Number of characters available in output string. No more
 *              than this number of characters is written to the output string,
 *              including the final null.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure. It is
 *         considered as an error (@ref TAO_BAD_CHARACTER) if the source string
 *         contains non-ASCII character(s). The other possible error is that
 *         the output string is too short (@ref TAO_BAD_SIZE).
 */
extern tao_status tao_transcode_utf8_string_to_unicode(
    const char* src,
    wchar_t *dst,
    long len);

/**
 * Transcode a Unicode string into an ASCII string.
 *
 * @param src   Input string. The input string must be null-terminated
 *              sequence of valid Unicode characters. As a consequence, the
 *              input string cannot contains embedded nulls other than the
 *              mandatory final null.
 *
 * @param dst   Output string. On success, the output string is a valid
 *              null-terminated ASCII string.
 *
 * @param len   Number of characters available in output string. No more
 *              than this number of characters is written to the output string,
 *              including the final null.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure. It is
 *         considered as an error (@ref TAO_BAD_CHARACTER) if the source string
 *         contains non-ASCII character(s). The other possible error is that
 *         the output string is too short (@ref TAO_BAD_SIZE).
 */
tao_status tao_transcode_unicode_string_to_ascii(
    const wchar_t* src,
    char *dst,
    long len);

/**
 * Transcode an UTF-8 string into a Unicode string.
 *
 * @param src   Input string. The input string must be a valid sequence of UTF-8
 *              encoded characters stored in a null-terminated C-string. As a
 *              consequence, the input string cannot contains embedded nulls
 *              other than the mandatory final null.
 *
 * @param dst   Output string. On success, the output string is a valid
 *              null-terminated Unicode string.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure. It is
 *         considered as an error (@ref TAO_BAD_CHARACTER) if the source string
 *         contains invalid UTF-8 sequence(s). The other possible error is that
 *         the output string is too short (@ref TAO_BAD_SIZE).
 */
extern tao_status tao_transcode_utf8_string_to_unicode(
    const char* src,
    wchar_t *dst,
    long len);

/**
 * Transcode a Unicode string into an UTF-8 string.
 *
 * @param src   Input string. The input string must be null-terminated
 *              sequence of valid Unicode characters. As a consequence, the
 *              input string cannot contains embedded nulls other than the
 *              mandatory final null.
 *
 * @param dst   Output string. On success, the output string is a valid
 *              null-terminated UTF-8 string.
 *
 * @param len   Number of characters available in output string. No more
 *              than this number of characters is written to the output string,
 *              including the final null.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure. It is
 *         considered as an error (@ref TAO_BAD_CHARACTER) if the source string
 *         contains invalid Unicode character(s). The other possible error is
 *         that the output string is too short (@ref TAO_BAD_SIZE).
 */
tao_status tao_transcode_unicode_string_to_utf8(
    const wchar_t* src,
    char *dst,
    long len);

/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_TIME_H_
