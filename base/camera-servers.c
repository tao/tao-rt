// camera-servers.c -
//
// Implementation of camera servers for TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2022-2024, Éric Thiébaut.

#include <stdbool.h>
#include <stdio.h> // *before* <tao-errors.h>
#include <string.h>
#include <math.h>

#include "tao-errors.h"
#include "tao-config.h"
#include "tao-utils.h"
#include "tao-generic.h"
#include "tao-pixels.h"
#include "tao-camera-servers.h"
#include "tao-cameras-private.h"
#include "tao-remote-cameras-private.h"
#include "tao-shared-arrays-private.h"

//=============================================================================
// ANSI CODES (see https://en.wikipedia.org/wiki/ANSI_escape_code)

// Escape character.
#define ANSI_ESC "\x1B"

// Control Sequence Introducer
#define ANSI_CSI ANSI_ESC "["

// Operating System Command.
//
// Starts a control string for the operating system to use, terminated by
// `ANSI_ST`.
#define ANSI_OSC ANSI_ESC "]"

// String Terminator.
//
// Terminates strings in other controls.  Can be `ANSI_BEL` on XTerm.
#define ANSI_ST ANSI_ESC "\\"

// Select Graphic Rendition.
//
// Sets colors and style of the characters following this code.
//
// Several attributes (separated by semicolons) can be set in the same sequence.
// If the sequence is empty, "0" is assumed, that is "reset".
#define ANSI_SGR(str) ANSI_CSI str "m"

// Reset or normal.
//
// All attributes off.
#define ANSI_RESET "0"

// Bold or increased intensity.
#define ANSI_BOLD  "1"

// Faint, decreased intensity, or dim.
//
// May be implemented as a light font weight like bold.
#define ANSI_FAINT "2"

// Italic.
//
// Not widely supported. Sometimes treated as inverse or blink.
#define ANSI_ITALIC "3"

// Underline.
//
// Style extensions exist for Kitty, VTE, mintty and iTerm2.
#define ANSI_UNDERLINE "4"

// Slow blink.
//
// Sets blinking to less than 150 times per minute.
#define ANSI_SLOW_BLINK "5"

// Rapid blink.
//
// MS-DOS ANSI.SYS, 150+ per minute; not widely supported
#define ANSI_FAST_BLINK "6"

// Reverse video or invert.
//
// Swap foreground and background colors; inconsistent emulation.
#define ANSI_REVERSE "7"
#define ANSI_INVERT  ANSI_REVERSE

// Conceal or hide.
//
// Not widely supported.
#define ANSI_CONCEAL "8"

// Crossed-out, or strike.
//
// Characters legible but marked as if for deletion.
#define ANSI_STRIKE "9"

// Font.
//
// Argument `n` is in the range 10 (primary or default font) to 20 (Gothic).
#define ANSI_FONT(n) #n

// Fraktur (Gothic) font.
//
// Rarely supported
#define ANSI_FRAKTUR "20"
#define ANSI_GOTHIC ANSI_FRAKTUR

// Change the window title.
#define ANSI_TITLE(str) ANSI_OSC "0;" str ANSI_ST

#define ANSI_DEBUG   "0;36" // foreground light-blue
#define ANSI_INFO    "0;32" // foreground green
#define ANSI_WARN    "0;33" // foreground yellow
#define ANSI_ERROR   "0;31" // foreground red
#define ANSI_ASSERT  ANSI_ERROR
#define ANSI_FATAL   ANSI_ERROR
#define ANSI_MESG    "0;34" // foreground blue

//=============================================================================
// DEFINITION OF PRIVATE METHODS

static void report(
    tao_status status,
    tao_message_level level,
    const tao_camera_server* srv,
    const char* format,
    ...) TAO_FORMAT_PRINTF(4,5);

static tao_pixels_processor* get_processor(
    tao_preprocessing preprocessing,
    tao_eltype dst_type,
    tao_encoding src_enc);

static tao_status update_processing_parameters(
    tao_camera_server* srv);

//=============================================================================
// BASIC OPERATIONS
//
// To simplify the coding (a lot), some basic operations (such as locking or
// unlocking a mutex, waiting or signalling a condition variable), are
// considered as fatal if they fail.
//
// The functions implementing these operations directly take a camera server as
// argument because the server name is needed for error reporting.
//
// We also define a number of macros to access the members/components of a
// server.  Most of these macros yield an L-value which is suitable for
// assignment.
//
// The names of these functions and macros shall make clear which resource
// must be locked by the caller and how.

//-----------------------------------------------------------------------------
// Basic operations on the server structure.

#define server_config(srv)    (srv)->arg.config
#define server_runlevel(srv)  (srv)->runlevel
#define server_state(srv)     (srv)->state
#define server_task(srv)      (srv)->task

static void server_lock(
    tao_camera_server* srv)
{
    tao_status status = tao_mutex_lock(&srv->mutex);
    if (status != TAO_OK) {
        report(status, TAO_MESG_FATAL, srv, "Failed to lock server");
    }
}

static void server_unlock(
    tao_camera_server* srv)
{
    tao_status status = tao_mutex_unlock(&srv->mutex);
    if (status != TAO_OK) {
        report(status, TAO_MESG_FATAL, srv, "Failed to unlock server");
    }
}

static void server_notify(
    tao_camera_server* srv)
{
    // FIXME: can be tao_condition_signal
    tao_status status = tao_condition_broadcast(&srv->cond);
    if (status != TAO_OK) {
        report(status, TAO_MESG_FATAL, srv, "Failed to notify server change");
    }
}

static void server_wait(
    tao_camera_server* srv)
{
    tao_status status = tao_condition_wait(&srv->cond, &srv->mutex);
    if (status != TAO_OK) {
        report(status, TAO_MESG_FATAL, srv, "Failed to wait server change");
    }
}

//-----------------------------------------------------------------------------
// Basic operations on the remote camera.

#define remote_camera(srv)               (srv)->remote
#define remote_camera_state(srv)         remote_camera(srv)->base.state
#define remote_camera_serial(srv)        remote_camera(srv)->base.serial
#define remote_camera_command(srv)       remote_camera(srv)->base.command
#define remote_camera_ncmds(srv)         remote_camera(srv)->base.ncmds
#define remote_camera_config(srv)        remote_camera(srv)->config
#define remote_camera_newconfig(srv)     remote_camera(srv)->arg.config
#define remote_camera_owner(srv)         remote_camera(srv)->base.owner

static void remote_camera_lock(
    tao_camera_server* srv)
{
    tao_status status = tao_remote_camera_lock(remote_camera(srv));
    if (status != TAO_OK) {
        report(status, TAO_MESG_FATAL, srv, "Failed to lock remote camera");
    }
}

static void remote_camera_unlock(
    tao_camera_server* srv)
{
    tao_status status = tao_remote_camera_unlock(remote_camera(srv));
    if (status != TAO_OK) {
        report(status, TAO_MESG_FATAL, srv, "Failed to unlock remote camera");
    }
}

static void remote_camera_notify(
    tao_camera_server* srv)
{
    tao_status status = tao_remote_camera_broadcast_condition(
        remote_camera(srv));
    if (status != TAO_OK) {
        report(status, TAO_MESG_FATAL, srv,
               "Failed to notify remote camera change");
    }
}

static void remote_camera_wait(
    tao_camera_server* srv)
{
    tao_status status =  tao_remote_camera_wait_condition(remote_camera(srv));
    if (status != TAO_OK) {
        report(status, TAO_MESG_FATAL, srv,
               "Failed to wait remote camera change");
    }
}

//-----------------------------------------------------------------------------
// Basic operations on the camera device.

#define camera_device(srv)          (srv)->device
#define camera_device_runlevel(srv) camera_device(srv)->runlevel
#define camera_device_config(srv)   camera_device(srv)->config
#define camera_device_state(srv)    tao_camera_get_state(camera_device(srv))

static void camera_device_lock(
    tao_camera_server* srv)
{
    tao_status status = tao_camera_lock(camera_device(srv));
    if (status != TAO_OK) {
        report(status, TAO_MESG_FATAL, srv, "Failed to lock camera device");
    }
}

static void camera_device_unlock(
    tao_camera_server* srv)
{
    tao_status status = tao_camera_unlock(camera_device(srv));
    if (status != TAO_OK) {
        report(status, TAO_MESG_FATAL, srv, "Failed to unlock camera device");
    }
}

// Low-level function called by the worker thread to wait for the next
// acquisition buffer by the camera device.  The camera device and the server
// shall be locked by the caller.  Errors are not returned but are logged.
static void camera_device_wait_buffer(
    tao_camera_server* srv,
    tao_acquisition_buffer* buf)
{
    tao_status status = tao_camera_wait_acquisition_buffer(
        camera_device(srv), buf, srv->timeout, srv->drop);
    if (status != TAO_OK) {
        buf->data = NULL;
        if (status != TAO_TIMEOUT) {
            report(status, TAO_MESG_ERROR, srv,
                   "Failed to wait for acquisition buffer");
        }
    }
}

// Function called by the worker to configure the camera device.  The camera
// device and the server shall be locked by the caller.  Errors are not
// returned but are logged.
static void camera_device_configure(
    tao_camera_server* srv,
    const tao_camera_config* cfg)
{
    // Check camera configuration before attempting to apply it.
    tao_status status = tao_camera_check_configuration(
        camera_device(srv), cfg);
    if (status != TAO_OK) {
        report(status, TAO_MESG_ERROR, srv,
               "Invalid configuration for camera device");
        tao_clear_error(NULL);
    } else {
        status = tao_camera_set_configuration(camera_device(srv), cfg);
        if (status != TAO_OK) {
            report(status, TAO_MESG_ERROR, srv,
                   "Failed to configure camera device");
            tao_clear_error(NULL);
        }
    }
}

// Low-level function called by the worker thread to start acquisition by the
// camera device.  The camera device and the server shall be locked by the
// caller.  Errors are not returned but are logged.
static void camera_device_start_acquisition(
    tao_camera_server* srv)
{
    if (srv->proc.processor == NULL) {
        report(TAO_OK, TAO_MESG_ERROR, srv, "Cannot start image "
               "acquisition with invalid processing parameters");
    } else if (srv->locked == NULL) {
        report(TAO_OK, TAO_MESG_ERROR, srv, "Cannot start image "
               "acquisition with no locked output image");
    } else {
        tao_status status = tao_camera_start_acquisition(camera_device(srv));
        if (status != TAO_OK) {
            report(status, TAO_MESG_ERROR, srv,
                   "Failed to start image acquisition");
            tao_clear_error(NULL);
        }
    }
}

// Low-level function called by the worker thread to stop acquisition by the
// camera device.  The camera device and the server shall be locked by the
// caller.  Errors are not returned but are logged.
static void camera_device_stop_acquisition(
    tao_camera_server* srv)
{
    tao_status status = tao_camera_stop_acquisition(camera_device(srv));
    if (status != TAO_OK) {
        report(status, TAO_MESG_ERROR, srv,
               "Failed to stop image acquisition");
        tao_clear_error(NULL);
    }
}

// Low-level function called by the worker thread to reset the camera device.
// The camera device and the server shall be locked by the caller.  Errors are
// not returned but are logged.
static void camera_device_reset(
    tao_camera_server* srv)
{
    tao_status status = tao_camera_reset(camera_device(srv));
    if (status != TAO_OK) {
        report(status, TAO_MESG_ERROR, srv,
               "Failed to reset camera device");
        tao_clear_error(NULL);
    }
}

//=============================================================================
// UTILITIES

static inline void release_shared_array(
    tao_status* status,
    tao_shared_array** arr_ptr,
    tao_shmid* shmid_ptr)
{
    *shmid_ptr = TAO_BAD_SHMID;
    if (*arr_ptr != NULL) {
        if (tao_shared_array_detach(*arr_ptr) != TAO_OK) {
            *status = TAO_ERROR;
        }
        *arr_ptr = NULL;
    }
}

#define TIME_FORMAT TAO_INT64_FORMAT(,d) "." TAO_INT64_FORMAT(09,d)

static void report_prefix(
    FILE* output,
    const tao_camera_server* srv,
    tao_message_level level,
    const char* date)
{
#define XSTR(srv,str,esc) ((srv)->fancy ? ANSI_SGR(esc) str ANSI_SGR("") : str)
    const char* message_level_name;
    switch (level) {
    case TAO_MESG_DEBUG:
        message_level_name = XSTR(srv, " debug", ANSI_DEBUG);
        break;
    case TAO_MESG_INFO:
        message_level_name = XSTR(srv, "  info", ANSI_INFO);
        break;
    case TAO_MESG_WARN:
        message_level_name = XSTR(srv, "  warn", ANSI_WARN);
        break;
    case TAO_MESG_ERROR:
        message_level_name = XSTR(srv, " error", ANSI_ERROR);
        break;
    case TAO_MESG_ASSERT:
        message_level_name = XSTR(srv, "assert", ANSI_ASSERT);
        break;
    case TAO_MESG_FATAL:
        message_level_name = XSTR(srv, " fatal", ANSI_FATAL);
        break;
    default:
        message_level_name = XSTR(srv, "  mesg", ANSI_MESG);
        break;
    }
#undef XSTR
    fprintf(output, "%s [%s %s]", tao_camera_server_get_owner(srv), date,
            message_level_name);
}

static void report(
    tao_status status,
    tao_message_level level,
    const tao_camera_server* srv,
    const char* format,
    ...)
{
    if (level >= srv->loglevel) {
        char date[64];
        tao_time_sprintf(date, TAO_TIME_FORMAT_DATE_WITH_MICROSECONDS, NULL);
        FILE* output = (srv->logfile == NULL ? stdout : srv->logfile);
        if (status == TAO_ERROR) {
            report_prefix(output, srv, TAO_MESG_ERROR, date);
            tao_report_error_to_stream(output, NULL, ": ", ".\n");
        } else if (status == TAO_TIMEOUT) {
            report_prefix(output, srv, TAO_MESG_WARN, date);
            fputs(": Timeout occurred\n", output);
        }
        report_prefix(output, srv, level, date);
        fputs(": ", output);
        va_list args;
        va_start(args, format);
        vfprintf(output, format, args);
        va_end(args);
        fputc('\n', output);
        fflush(output);
    }
    if (level >= TAO_MESG_FATAL) {
        exit(EXIT_FAILURE);
    }
}

//=============================================================================
// IMAGE PROCESSING

#define encode_simple_conversion(verb,sfx)              \
    static void simple_conversion_##sfx(                \
        const tao_pixels_processor_context* ctx)        \
    {                                                   \
        tao_pixels_##verb##_##sfx(                      \
            ctx->dat,                                   \
            ctx->width,                                 \
            ctx->height,                                \
            ctx->raw,                                   \
            ctx->stride);                               \
    }

encode_simple_conversion(copy,    u8_to_u8);
encode_simple_conversion(convert, u8_to_u16);
encode_simple_conversion(convert, u8_to_u32);
encode_simple_conversion(convert, u8_to_flt);
encode_simple_conversion(convert, u8_to_dbl);

encode_simple_conversion(convert, p12_to_u16);
encode_simple_conversion(convert, p12_to_u32);
encode_simple_conversion(convert, p12_to_flt);
encode_simple_conversion(convert, p12_to_dbl);

encode_simple_conversion(copy,    u16_to_u16);
encode_simple_conversion(convert, u16_to_u32);
encode_simple_conversion(convert, u16_to_flt);
encode_simple_conversion(convert, u16_to_dbl);

encode_simple_conversion(copy,    u32_to_u32);
encode_simple_conversion(convert, u32_to_flt);
encode_simple_conversion(convert, u32_to_dbl);

#undef encode_simple_conversion

#define encode_affine_correction(sfx)                   \
    static void affine_correction_##sfx(                \
        const tao_pixels_processor_context* ctx)        \
    {                                                   \
        tao_pixels_preprocess_affine_##sfx(             \
            ctx->dat,                                   \
            ctx->width,                                 \
            ctx->height,                                \
            ctx->preproc[0],                            \
            ctx->preproc[1],                            \
            ctx->raw,                                   \
            ctx->stride);                               \
    }

encode_affine_correction(u8_to_flt);
encode_affine_correction(u8_to_dbl);

encode_affine_correction(p12_to_flt);
encode_affine_correction(p12_to_dbl);

encode_affine_correction(u16_to_flt);
encode_affine_correction(u16_to_dbl);

encode_affine_correction(u32_to_flt);
encode_affine_correction(u32_to_dbl);

#undef encode_affine_correction

#define encode_full_preprocessing(sfx)                  \
    static void full_preprocessing_##sfx(               \
        const tao_pixels_processor_context* ctx)        \
    {                                                   \
        tao_pixels_preprocess_full_##sfx(               \
            ctx->dat,                                   \
            ctx->wgt,                                   \
            ctx->width,                                 \
            ctx->height,                                \
            ctx->preproc[0],                            \
            ctx->preproc[1],                            \
            ctx->preproc[2],                            \
            ctx->preproc[3],                            \
            ctx->raw,                                   \
            ctx->stride);                               \
    }

encode_full_preprocessing(u8_to_flt);
encode_full_preprocessing(u8_to_dbl);

encode_full_preprocessing(p12_to_flt);
encode_full_preprocessing(p12_to_dbl);

encode_full_preprocessing(u16_to_flt);
encode_full_preprocessing(u16_to_dbl);

encode_full_preprocessing(u32_to_flt);
encode_full_preprocessing(u32_to_dbl);

#undef encode_full_preprocessing

#define MONO(nbits) TAO_ENCODING_MONO(nbits)
#define PACKED_12   TAO_ENCODING_MONO_PKT(12, 24)

static tao_pixels_processor* get_processor(
    tao_preprocessing preprocessing,
    tao_eltype dst_type,
    tao_encoding src_enc)
{
    unsigned colorant = TAO_ENCODING_COLORANT(src_enc);
    unsigned bits_per_pixel = TAO_ENCODING_BITS_PER_PIXEL(src_enc);
    unsigned bits_per_packet = TAO_ENCODING_BITS_PER_PACKET(src_enc);
    unsigned flags = TAO_ENCODING_FLAGS(src_enc);
    if (colorant != TAO_COLORANT_MONO) {
        fputs("For now, only monochrome encoding is supported by TAO\n",
              stderr);
        goto bad_src_enc;
    }
    if (bits_per_packet > bits_per_pixel &&
        bits_per_packet < 2*bits_per_pixel) {
        // Source pixels are stored in packets of larger width but each
        // packet contains a single pixel value.
        if (flags == TAO_ENCODING_FLAGS_MSB_PAD) {
            // The most significant bits are set to zero, we can assume that
            // the source is a simple monochrome encoding.
            src_enc = MONO(bits_per_packet);
        }
    }
    if (preprocessing == TAO_PREPROCESSING_NONE) {
        if (src_enc == MONO(8)) {
            if (dst_type == TAO_UINT8) {
                return simple_conversion_u8_to_u8;
            } else if (dst_type == TAO_UINT16) {
                return simple_conversion_u8_to_u16;
            } else if (dst_type == TAO_UINT32) {
                return simple_conversion_u8_to_u32;
            } else if (dst_type == TAO_FLOAT) {
                return simple_conversion_u8_to_flt;
            } else if (dst_type == TAO_DOUBLE) {
                return simple_conversion_u8_to_dbl;
            } else {
                goto bad_dst_type;
            }
        } else if (src_enc == PACKED_12) {
            if (dst_type == TAO_UINT16) {
                return simple_conversion_p12_to_u16;
            } else if (dst_type == TAO_UINT32) {
                return simple_conversion_p12_to_u32;
            } else if (dst_type == TAO_FLOAT) {
                return simple_conversion_p12_to_flt;
            } else if (dst_type == TAO_DOUBLE) {
                return simple_conversion_p12_to_dbl;
            } else {
                goto bad_dst_type;
            }
        } else if (src_enc == MONO(16)) {
            if (dst_type == TAO_UINT16) {
                return simple_conversion_u16_to_u16;
            } else if (dst_type == TAO_UINT32) {
                return simple_conversion_u16_to_u32;
            } else if (dst_type == TAO_FLOAT) {
                return simple_conversion_u16_to_flt;
            } else if (dst_type == TAO_DOUBLE) {
                return simple_conversion_u16_to_dbl;
            } else {
                goto bad_dst_type;
            }
        } else if (src_enc == MONO(32)) {
            if (dst_type == TAO_UINT32) {
                return simple_conversion_u32_to_u32;
            } else if (dst_type == TAO_FLOAT) {
                return simple_conversion_u32_to_flt;
            } else if (dst_type == TAO_DOUBLE) {
                return simple_conversion_u32_to_dbl;
            } else {
                goto bad_dst_type;
            }
        } else {
            goto bad_src_enc;
        }
    } else if (preprocessing == TAO_PREPROCESSING_AFFINE) {
        if (src_enc == MONO(8)) {
            if (dst_type == TAO_FLOAT) {
                return affine_correction_u8_to_flt;
            } else if (dst_type == TAO_DOUBLE) {
                return affine_correction_u8_to_dbl;
            } else {
                goto bad_dst_type;
            }
        } else if (src_enc == PACKED_12) {
            if (dst_type == TAO_FLOAT) {
                return affine_correction_p12_to_flt;
            } else if (dst_type == TAO_DOUBLE) {
                return affine_correction_p12_to_dbl;
            } else {
                goto bad_dst_type;
            }
        } else if (src_enc == MONO(16)) {
            if (dst_type == TAO_FLOAT) {
                return affine_correction_u16_to_flt;
            } else if (dst_type == TAO_DOUBLE) {
                return affine_correction_u16_to_dbl;
            } else {
                goto bad_dst_type;
            }
        } else if (src_enc == MONO(32)) {
            if (dst_type == TAO_FLOAT) {
                return affine_correction_u32_to_flt;
            } else if (dst_type == TAO_DOUBLE) {
                return affine_correction_u32_to_dbl;
            } else {
                goto bad_dst_type;
            }
        } else {
            goto bad_src_enc;
        }
    } else if (preprocessing == TAO_PREPROCESSING_FULL) {
        if (src_enc == MONO(8)) {
            if (dst_type == TAO_FLOAT) {
                return full_preprocessing_u8_to_flt;
            } else if (dst_type == TAO_DOUBLE) {
                return full_preprocessing_u8_to_dbl;
            } else {
                goto bad_dst_type;
            }
        } else if (src_enc == PACKED_12) {
            if (dst_type == TAO_FLOAT) {
                return full_preprocessing_p12_to_flt;
            } else if (dst_type == TAO_DOUBLE) {
                return full_preprocessing_p12_to_dbl;
            } else {
                goto bad_dst_type;
            }
        } else if (src_enc == MONO(16)) {
            if (dst_type == TAO_FLOAT) {
                return full_preprocessing_u16_to_flt;
            } else if (dst_type == TAO_DOUBLE) {
                return full_preprocessing_u16_to_dbl;
            } else {
                goto bad_dst_type;
            }
        } else if (src_enc == MONO(32)) {
            if (dst_type == TAO_FLOAT) {
                return full_preprocessing_u32_to_flt;
            } else if (dst_type == TAO_DOUBLE) {
                return full_preprocessing_u32_to_dbl;
            } else {
                goto bad_dst_type;
            }
        } else {
            goto bad_src_enc;
        }
    } else {
        // Invalid pre-processing method.
        tao_store_error(__func__, TAO_BAD_PREPROCESSING);
        return NULL;
    }

bad_dst_type:
  tao_store_error(__func__, TAO_BAD_TYPE);
  return NULL;

bad_src_enc:
  tao_store_error(__func__, TAO_BAD_ENCODING);
  return NULL;
}

static const char* encoding_sprintf(
    char* str,
    tao_encoding enc)
{
    if (tao_format_encoding(str, enc) != TAO_OK) {
        sprintf(str, "0x" TAO_INT32_FORMAT(08,x), enc);
    }
    return str;
}

// Unlock the output image locked by the server if any.  Caller must own the
// lock on the server structure.
static tao_status unlock_output_image(
    tao_camera_server* srv)
{
    if (srv->locked != NULL) {
        tao_status status = tao_shared_array_unlock(srv->locked);
        srv->locked = NULL; // never unlock more than once
        if (status != TAO_OK) {
            report(status, TAO_MESG_ERROR, srv,
                   "Failed to unlock output image");
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

// This functions prepares the next output image (in `srv->locked`).
//
// The server structure must be locked by the caller.
//
// The former ouptput image, if any, is unlocked.
//
// The next output image is locked for read-write access.  A new output image
// is automatically created if image settings have changed, if no output image
// is currently attached, or if the attached output image cannot be immediately
// locked.
static tao_status prepare_next_output_image(
    tao_camera_server* srv,
    bool publish)
{
    // Get current pre-processing settings.
    tao_pixels_processor_context* ctx = &srv->proc;
    bool weighted = (ctx->preprocessing == TAO_PREPROCESSING_FULL);

    // Maybe increment number of produced images.
    if (publish && srv->locked != NULL) {
        srv->locked->serial = ++srv->serial;
    }

    // Get next image in cyclic list of output images and its index.
    long index = srv->serial % srv->nbufs;
    tao_shared_array* arr = srv->images[index];

    // Is the output image reusable?
    bool reusable = (arr != NULL && arr->eltype == ctx->pixeltype &&
                     arr->ndims == (weighted ? 3 : 2) &&
                     arr->dims[0] == ctx->width  &&
                     arr->dims[1] == ctx->height &&
                     (!weighted || arr->dims[2] == 2));

    // There is nothing to do if the output image is reusable and if it is the
    // one that is currently locked by the server process.
    if (reusable && srv->locked == arr) {
        return TAO_OK;
    }

    // Unlock the output image locked by the server if any.
    unlock_output_image(srv);

    // If the output image is reusable, try to lock it for read-write access.
    tao_status status;
    if (reusable) {
        status = tao_shared_array_try_wrlock(arr);
        if (status == TAO_OK) {
            arr->serial = 0; // this image is about to be overwritten
            srv->locked = arr;
            return TAO_OK;
        }
        if (status != TAO_TIMEOUT) {
            report(status, TAO_MESG_ERROR, srv, "Failed to attempt "
                   "locking output image for read-write access");
            return TAO_ERROR;
        }
    }

    // Detach existing output image.
    if (arr != NULL) {
        srv->shmids[index] = TAO_BAD_SHMID;
        srv->images[index] = NULL;
        status = tao_shared_array_detach(arr);
        if (status != TAO_OK) {
            report(status, TAO_MESG_ERROR, srv,
                   "Failed to detach output image");
            return TAO_ERROR;
        }
    }

    // A new output image must be created.
    if (weighted) {
        arr = tao_shared_array_create_3d(
            ctx->pixeltype, ctx->width, ctx->height, 2, srv->flags);
    } else {
        arr = tao_shared_array_create_2d(
            ctx->pixeltype, ctx->width, ctx->height, srv->flags);
    }
    if (arr == NULL) {
        report(TAO_ERROR, TAO_MESG_ERROR, srv,
               "Failed to create new output image");
        return TAO_ERROR;
    }
    status = tao_shared_array_try_wrlock(arr);
    if (status != TAO_OK) {
        report(status, TAO_MESG_ERROR, srv, "Failed to immediately "
                   "lock new output image for read-write access");
        tao_shared_array_detach(arr); // ignore errors here if any
        return TAO_ERROR;
    }
    arr->serial = 0; // this image has not yet been published
    srv->images[index] = arr;
    srv->shmids[index] = arr->base.base.shmid;
    srv->locked = arr;
    return TAO_OK;
}

// Allocate arrays for pre-processing parameters according to current
// camera device configuration.
//
// The caller must own the lock on the camera device (to safely read its
// configuration), the remote camera (to safely update its configuration
// (FIXME:?) and the shmid's of the pre-processing arrays), and the server (to
// update the address of the shared arrays and update the processor context).
static tao_status update_processing_parameters(
    tao_camera_server* srv)
{
    // Invalidate all processor settings so that we can return immediately in
    // case of error.
    tao_pixels_processor_context* ctx = &srv->proc;
    memset(ctx, 0, sizeof(*ctx));

    // Get relevant parameters for pre-processing of images.
    const tao_camera_config* cfg = &camera_device_config(srv);
    tao_eltype pixeltype = cfg->pixeltype;
    tao_encoding rawencoding = cfg->rawencoding;
    long width = cfg->roi.width;
    long height = cfg->roi.height;
    unsigned flags = srv->flags;
    tao_preprocessing preprocessing = cfg->preprocessing;

    // Get processor according to pre-processing parameters.
    tao_pixels_processor* processor = get_processor(
        preprocessing, pixeltype, rawencoding);
    if (processor == NULL) {
        report(TAO_ERROR, TAO_MESG_ERROR, srv,
               "Invalid settings for image pre-processing");
        return TAO_ERROR;
    }

    // Allocate arrays for pre-processing parameters according to current
    // camera device configuration.
    static struct {
        double fill;
        const char* name;
    } param[] = {
        { 1.0,  "pixel correction factor" },
        { 0.0,  "pixel correction bias" },
        { 1e30, "pixel precision numerator" },
        { 1e30, "pixel precision denominator offset" },
    };
    for (int i = 0; i < 4; ++i) {
        bool required = (preprocessing == TAO_PREPROCESSING_FULL ||
                         (preprocessing == TAO_PREPROCESSING_AFFINE && i < 2));
        if (srv->preproc[i] != NULL) {
            // Detach existing pre-processing array if not required or if its
            // parameters are not the current ones.
            tao_shared_array* arr = srv->preproc[i];
            bool detach = (! required || arr->eltype != pixeltype ||
                           arr->ndims != 2 || arr->dims[0] != width ||
                           arr->dims[1] != height);
            if (detach) {
                srv->preproc[i] = NULL;
                remote_camera(srv)->preproc[i] = TAO_BAD_SHMID;
                tao_status status = tao_shared_array_detach(arr);
                if (status != TAO_OK) {
                    report(status, TAO_MESG_ERROR, srv,
                           "Failed to detach shared array for %s",
                           param[i].name);
                    return TAO_ERROR;
                }
            }
        }
        if (required && srv->preproc[i] == NULL) {
            // Create new pre-processing array with correct parameters and
            // filled with a sensitive value.
            tao_shared_array* arr = tao_shared_array_create_2d(
                pixeltype, width, height, flags);
            if (arr == NULL) {
                report(TAO_ERROR, TAO_MESG_ERROR, srv,
                       "Failed to create shared array for %s",
                       param[i].name);
                return TAO_ERROR;
            }
            srv->preproc[i] = tao_shared_array_fill(arr, param[i].fill);
            remote_camera(srv)->preproc[i] = arr->base.base.shmid;
        }
    }

    // Update the processor context.  This must be done before creating the
    // next output image.
    long bits_per_pixel = TAO_ENCODING_BITS_PER_PIXEL(ctx->rawencoding);
    ctx->preprocessing = preprocessing;
    ctx->rawencoding = rawencoding;
    ctx->pixeltype = pixeltype;
    ctx->width = width;
    ctx->height = height;
    ctx->stride = 0;
    ctx->stride_min = (bits_per_pixel*ctx->width + 7)/8;
    ctx->dat = NULL;
    ctx->wgt = NULL;
    ctx->raw = NULL;
    for (int i = 0; i < 4; ++i) {
        ctx->preproc[i] = tao_shared_array_get_data(srv->preproc[i]);
    }
    ctx->processor = processor;

    // Prepare next output array.
    tao_status status = prepare_next_output_image(srv, false);
    if (status != TAO_OK) {
        report(status, TAO_MESG_ERROR, srv,
               "Failed to prepare next output image");
        return TAO_ERROR;
    }

    return TAO_OK;
}

static inline tao_time get_monotonic_time(void)
{
    tao_time t;
    tao_get_monotonic_time(&t);
    return t;
}

// This function shall only be called if there is anything to do.
static bool process_acquisition_buffer(
    tao_camera_server* srv,
    tao_shared_array* arr,       // output image
    const tao_acquisition_buffer* buf) // input acquisition buffer
{
    tao_pixels_processor_context* ctx = &srv->proc;
    if (ctx->processor == NULL || buf->data == NULL) {
        // Nothing to do.
        return false;
    }

    // Check compatibility of acquisition buffer.
    if (buf->encoding != ctx->rawencoding) {
        char str1[TAO_ENCODING_STRING_SIZE];
        char str2[TAO_ENCODING_STRING_SIZE];
        report(TAO_OK, TAO_MESG_ASSERT, srv,
               "Bad acquisition buffer encoding `%s`, should be `%s`",
               encoding_sprintf(str1, buf->encoding),
               encoding_sprintf(str2, ctx->rawencoding));
        return false;
    }
    if (buf->width != ctx->width || buf->height != ctx->height) {
        report(TAO_OK, TAO_MESG_ASSERT, srv, "Bad acquisition buffer "
               "dimensions %ld×%ld pixels, should be %ld×%ld",
               buf->width, buf->height, ctx->width, ctx->height);
        return false;
    }
    if (buf->stride < ctx->stride_min) {
        report(TAO_OK, TAO_MESG_ASSERT, srv, "Bad acquisition buffer "
               "stride %ld bytes, should be at least %ld",
               buf->stride, ctx->stride_min);
        return false;
    }
    if (buf->size < buf->height*buf->stride) {
        report(TAO_OK, TAO_MESG_ASSERT, srv, "Bad acquisition buffer "
               "size %ld bytes, should be at least %ld",
               buf->size, buf->height*buf->stride);
        return false;
    }

    // Check compatibility of output image.
    if (arr->eltype != ctx->pixeltype) {
        report(TAO_OK, TAO_MESG_ASSERT, srv,
               "Bad output image pixel type `%s`, should be `%s`",
               tao_name_of_eltype(arr->eltype),
               tao_name_of_eltype(ctx->pixeltype));
        return false;
    }
    bool weighted = (ctx->preprocessing == TAO_PREPROCESSING_FULL);
    if (weighted && arr->eltype != TAO_FLOAT && arr->eltype != TAO_DOUBLE) {
        report(TAO_OK, TAO_MESG_ASSERT, srv, "Bad output image "
               "pixel type `%s`, should be a floating-point type",
               tao_name_of_eltype(arr->eltype));
        return false;
    }
    if (arr->ndims != (weighted ? 3 : 2)) {
        report(TAO_OK, TAO_MESG_ASSERT, srv, "Bad output image "
               "number of dimensions %d, should be %d",
               arr->ndims, (weighted ? 3 : 2));
        return false;
    }
    if (arr->dims[0] != ctx->width || arr->dims[1] != ctx->height) {
        report(TAO_OK, TAO_MESG_ASSERT, srv, "Bad output image "
               "leading dimensions %ld×%ld pixels, should be %ld×%ld",
               arr->dims[0], arr->dims[1], ctx->width, ctx->height);
        return false;
    }
    if (weighted && arr->dims[2] != 2) {
        report(TAO_OK, TAO_MESG_ASSERT, srv, "Bad output image "
               "depth %ld, should be 2", arr->dims[2]);
        return false;
    }

    // Instantiate processing context and run processor.
    ctx->raw = buf->data;
    ctx->stride = buf->stride;
    ctx->dat = tao_shared_array_get_data(arr);
    if (weighted) {
        size_t size = (arr->dims[0]*arr->dims[1])*
            (arr->eltype == TAO_FLOAT ? sizeof(float) : sizeof(double));
        ctx->wgt = TAO_COMPUTED_ADDRESS(ctx->dat, size);
    } else {
        ctx->wgt = NULL;
    }
    arr->ts[0] = buf->frame_start;
    arr->ts[1] = buf->frame_end;
    arr->ts[2] = buf->buffer_ready;
    arr->ts[3] = get_monotonic_time();
    ctx->processor(ctx);
    arr->ts[4] = get_monotonic_time();

    // Returns that an output image has been produced.
    return true;
}

//=============================================================================

static void show_state(
    const tao_camera_server* srv,
    tao_state state)
{
    if (srv->fancy) {
        const char* state_name = (state == TAO_STATE_WAITING ? "idle" :
                                  tao_state_get_name(state));
       fprintf(stdout, ANSI_TITLE("%s [%s]"),
               tao_camera_server_get_owner(srv), state_name);
       fflush(stdout);
    }
}

static const char* runlevel_name(
    int runlevel)
{
    switch (runlevel) {
    case  0: return "initializing";
    case  1: return "idle";
    case  2: return "acquiring";
    default: return "unreachable";
    }
}

// Update the run-level of the worker thread.
//
// This function shall be called by the worker thread while owning the locks on
// the server structure and on the camera device but not on the remote camera.
//
// Argument `expected_runlevel` indicates the expected resulting run-level of
// the worker thread.  This indicative value is to decide the type of message
// when a run-level change is logged.  When `expected_runlevel ≥ 3`, it is
// however assumed that the worker thread must be marked as quitting.
//
// If argument `update_remote_camera` is true, the remote camera may be
// temporarily locked to update its state if needed.
//
// The run-level of the camera device is checked.  If it has a recoverable
// error, a reset is attempted.
//
// The next run-level of the worker thread is determined from that of the
// camera device, from the value of `expected_runlevel`, and form the current
// run-level of the worker thread.
//
// Any changes are applied, notified, and logged.
//
static void update_worker_runlevel(
    tao_camera_server* srv,
    int expected_runlevel,
    bool update_remote_camera)
{
    // Figure out the run-level that the worker thread should have considering
    // that of the camera device.
    int runlevel = camera_device_runlevel(srv);
    if (runlevel == 3) {
        // Attempt a reset.
        camera_device_reset(srv);
        runlevel = camera_device_runlevel(srv);
    }
    if (runlevel != 1 && runlevel != 2) {
        report(TAO_OK, TAO_MESG_ASSERT, srv,
               "Unexpected camera device run-level (%d)", runlevel);
        runlevel = 3;
    }
    if (expected_runlevel > 2) {
        // Force the expected run-level which has priority in that case.
        runlevel = expected_runlevel;
    }

    // If there is no change in run-level or if the change is not allowed (it
    // is not allowed to reduce the run-level of the worker unless when going
    // from "working" to "waiting"), there is nothing else to do.
    if (server_runlevel(srv) == runlevel
        || (server_runlevel(srv) > runlevel
            && (server_runlevel(srv) != 2 || runlevel != 1))) {
        return;
    }

    // Determine the worker state.
    tao_state state;
    if (runlevel == 0) {
        state = TAO_STATE_INITIALIZING;
    } else if (runlevel == 1) {
        state = TAO_STATE_WAITING;
    } else if (runlevel == 2) {
        state = TAO_STATE_WORKING;
    } else {
        state = TAO_STATE_UNREACHABLE;
    }
    if (server_state(srv) == state) {
        update_remote_camera = false;
    }

    // Update the server structure and notify changes.
    bool unexpected_change = (runlevel != expected_runlevel);
    server_runlevel(srv) = runlevel;
    server_state(srv) = state;
    server_notify(srv);
    report(TAO_OK, (unexpected_change ? TAO_MESG_WARN : TAO_MESG_INFO),
           srv, "Switching to run-level %d (%s)", runlevel,
           runlevel_name(runlevel));
    show_state(srv, state);

    // If needed, update the remote camera state and notify changes.
    if (update_remote_camera) {
        remote_camera_lock(srv);
        if (remote_camera_state(srv) != state) {
            remote_camera_state(srv) = state;
            remote_camera_notify(srv);
        }
        remote_camera_unlock(srv);
    }
}

//=============================================================================
// WORKER THREAD FUNCTIONS

static void forbidden_command(
    tao_camera_server* srv)
{
    report(TAO_OK, TAO_MESG_ERROR, srv, "Command `%s` is forbidden while %s",
           tao_command_get_name(server_task(srv)),
           runlevel_name(server_runlevel(srv)));
}

// High-level function called by the worker thread to execute the current
// command queued by the server thread.
//
// Caller must have only locked the server structure.
static void execute_command(
    tao_camera_server* srv)
{
    // Lock the camera device and make sure the run-levels of the worker thread
    // and of the camera device are consistent.  No changes are however
    // expected.
    camera_device_lock(srv);
    update_worker_runlevel(srv, server_runlevel(srv), true);

    // Execute pending task.  All errors get reported by the sub-routines.  If
    // anything may have changed for the camera device, the run-level of the
    // worker thread is updated but not the state of the remote camera (as this
    // is done later).
    bool update = false;
    switch (server_task(srv)) {
    case TAO_COMMAND_CONFIG:
        if (server_runlevel(srv) == 1) {
            camera_device_configure(srv, &server_config(srv));
            update_worker_runlevel(srv, 1, false);
            update = true;
        } else {
            forbidden_command(srv);
        }
        break;
    case TAO_COMMAND_START:
        if (server_runlevel(srv) == 1) {
            camera_device_start_acquisition(srv);
            update_worker_runlevel(srv, 2, false);
        } else if (server_runlevel(srv) != 2) {
            forbidden_command(srv);
        }
        break;
    case TAO_COMMAND_STOP:
    case TAO_COMMAND_ABORT:
        if (server_runlevel(srv) == 2) {
            camera_device_stop_acquisition(srv);
            update_worker_runlevel(srv, 1, false);
        } else if (server_runlevel(srv) != 1) {
            forbidden_command(srv);
        }
        break;
    case TAO_COMMAND_RESET:
        if (server_runlevel(srv) > 0 && server_runlevel(srv) < 3) {
            camera_device_reset(srv);
            update_worker_runlevel(srv, 1, false);
        } else {
            forbidden_command(srv);
        }
        break;
    case TAO_COMMAND_KILL:
        if (server_runlevel(srv) < 3) {
            if (server_runlevel(srv) == 2) {
                camera_device_stop_acquisition(srv);
            }
            update_worker_runlevel(srv, 3, false);
        } else {
            forbidden_command(srv);
        }
        break;
    default:
        report(TAO_OK, TAO_MESG_ERROR, srv,
               "Unexpected command `%s` (%d) received by worker ",
               tao_command_get_name(server_task(srv)),
               (int)server_task(srv));
    }

    // Lock the remote camera to increment its command counter and reflect any
    // changes of the camera device, update image processing parameters in
    // server and remote camera.
    remote_camera_lock(srv);
    remote_camera_command(srv) = TAO_COMMAND_NONE;
    remote_camera_ncmds(srv) += 1;
    remote_camera_state(srv) = server_state(srv);
    if (update) {
        // Copy the camera device parameters into the remote camera, and update
        // image processing parameters.
        tao_camera_config_copy(&remote_camera_config(srv),
                               &camera_device_config(srv));
        tao_status status = update_processing_parameters(srv);
        if (status != TAO_OK) {
            report(status, TAO_MESG_ERROR, srv,
                   "Invalid preprocessing settings");
            tao_clear_error(NULL);
        }
    }
    remote_camera_notify(srv);
    remote_camera_unlock(srv);

    // Clear the command so that the server knows that the command has been
    // executed and notify changes to the server thread.
    server_task(srv) = TAO_COMMAND_NONE;
    show_state(srv, server_state(srv));
    server_notify(srv);

    // Unlock the camera device (run-levels have been synchronized).
    camera_device_unlock(srv);
}

// Copy camera information that may have changed while acquisition is running
// from source `src` to destination `dst`.  The caller must have correctly
// locked the two resources.
static inline void copy_running_camera_information(
    tao_camera_config* dst,
    const tao_camera_config* src)
{
    dst->frames        = src->frames;
    dst->droppedframes = src->droppedframes;
    dst->overruns      = src->overruns;
    dst->lostframes    = src->lostframes;
    dst->overflows     = src->overflows;
    dst->lostsyncs     = src->lostsyncs;
    dst->timeouts      = src->timeouts;
}

// High-level function called by the worker thread to wait for the next
// acquisition buffer.
//
// Caller must have only locked the server structure.
static void wait_buffer(
    tao_camera_server* srv,
    tao_acquisition_buffer* buf)
{
    // Lock the camera device and make sure the run-levels of the worker thread
    // and of the camera device are consistent.  No changes are however
    // expected.
    camera_device_lock(srv);
    update_worker_runlevel(srv, server_runlevel(srv), true);

    // Wait for next image if acquiring.  Temporarily unlock server while
    // waiting.  Something may have changed while waiting, so we synchronize
    // the run-levels.
    if (server_runlevel(srv) == 2) {
        server_unlock(srv);
        camera_device_wait_buffer(srv, buf);
        server_lock(srv);
        update_worker_runlevel(srv, 2, true);
    } else {
        buf->data = NULL;
    }

    // Copy information that may have changed.
    copy_running_camera_information(&srv->config, &srv->device->config);

    // Unlock the camera device.
    camera_device_unlock(srv);
}

// High-level function called by the worker thread to eventually stop
// acquisition.
//
// Caller must have only locked the server structure.
static void stop_acquisition(
    tao_camera_server* srv,
    int runlevel)
{
    camera_device_lock(srv);
    update_worker_runlevel(srv, server_runlevel(srv), true);
    if (server_runlevel(srv) == 2) {
        camera_device_stop_acquisition(srv);
        update_worker_runlevel(srv, 1, true);
    }
    camera_device_unlock(srv);
}

// Publish/erase shared memory identifier of remote camera connected to the
// server.  The caller may own the lock on the remote camera but this is not
// mandatory as the needed information (owner name and shared memory
// identifier) are immutable.
static void remote_camera_write_shmid(
    tao_camera_server* srv,
    bool publish)
{
    tao_shmid shmid;
    if (publish) {
        shmid = tao_remote_camera_get_shmid(remote_camera(srv));
        report(TAO_OK, TAO_MESG_INFO, srv,
               "Remote camera available at shmid=%d", (int)shmid);
    } else {
        shmid = TAO_BAD_SHMID;
    }
    tao_status status = tao_config_write_long(
        tao_camera_server_get_owner(srv), shmid);
    if (status != TAO_OK) {
        report(status, TAO_MESG_ERROR, srv, "Failed to %s remote camera shmid",
               (publish ? "publish" : "erase"));
    }
}

// This is the main function executed by the "worker" thread.
//
// Caller must have only locked the server structure.
//
// The worker may use the server resources, the camera device, and the remote
// camera.  It communicates with the server via the mutex and the condition
// variable of the server structure.
static void* run_worker(
    void* arg)
{
    // The argument is the camera server.
    tao_camera_server* srv = arg;

    // Lock the server and check its run-level.
    server_lock(srv);
    if (server_runlevel(srv) != 0) {
        report(TAO_OK, TAO_MESG_ERROR, srv,
               "Unexpected worker run-level (%d) on worker start",
               server_runlevel(srv));
        server_runlevel(srv) = 3;
        server_state(srv) = TAO_STATE_UNREACHABLE;
    }

    // Lock the camera device and check its run-level.
    camera_device_lock(srv);
    if (camera_device_runlevel(srv) != 1) {
        report(TAO_OK, TAO_MESG_ERROR, srv,
               "Unexpected camera device run-level (%d) on worker start",
               camera_device_runlevel(srv));
        server_runlevel(srv) = 3;
        server_state(srv) = TAO_STATE_UNREACHABLE;
    }

    // Set the worker run-level to signal that the worker is waiting.  This is
    // expected by the thread running the server loop.
    if (server_runlevel(srv) == 0) {
        update_worker_runlevel(srv, 1, false);
    }

    // Update remote camera information and unlock camera device.
    remote_camera_lock(srv);
    remote_camera_state(srv) = server_state(srv);
    tao_camera_config_copy(&remote_camera_config(srv),
                           &camera_device_config(srv));
    update_processing_parameters(srv);
    remote_camera_notify(srv);
    remote_camera_unlock(srv);
    camera_device_unlock(srv);


    // Publish the shmid's of the shared resources (it is not needed to have
    // locked the remote camera, just the server).
    if (server_runlevel(srv) == 1 || server_runlevel(srv) == 2) {
        remote_camera_write_shmid(srv, true);
    }

    // Event loop.
    while (true) {
        // Wait for anything to do.
        while (server_runlevel(srv) == 1 &&
               server_task(srv) == TAO_COMMAND_NONE) {
            server_wait(srv);
        }
        if (server_runlevel(srv) > 2) {
            // Worker has been asked to quit.
            break;
        }

        // Check whether there is a command to execute.
        if (server_task(srv) != TAO_COMMAND_NONE) {
            execute_command(srv);
        }

        // Check whether acquisition is running.
        if (server_runlevel(srv) == 2) {
            // Wait for next image.  Temporarily unlock server while waiting.
            // Something may have changed while waiting, so we may update the
            // runlevel.
            tao_acquisition_buffer buf;
            wait_buffer(srv, &buf);

            // Process acquisition buffer unless, timeout (i.e., a NULL buffer
            // data), no longer acquiring, aborting, or quitting.
            bool process_buffer = (buf.data != NULL &&
                                   server_runlevel(srv) == 2 &&
                                   server_task(srv) != TAO_COMMAND_ABORT &&
                                   server_task(srv) != TAO_COMMAND_KILL);
            if (process_buffer) {
                tao_shared_array* arr = srv->locked;
                bool flag = process_acquisition_buffer(srv, arr, &buf);
                prepare_next_output_image(srv, flag);
            }

            // Update remote camera information.
            remote_camera_lock(srv);
            remote_camera_serial(srv) = srv->serial;
            copy_running_camera_information(&srv->remote->config, &srv->config);
            remote_camera_notify(srv);
            remote_camera_unlock(srv);
        }
    } // end of main loop

    // Stop acquisition if running and set the run-level to 3 (quitting).
    stop_acquisition(srv, 3);

    // Invalidate the shmid's of the shared resources (it is not needed to have
    // locked the remote camera, just the server).
    remote_camera_write_shmid(srv, false);

    // Unlock the server and exit.
    server_unlock(srv);
    return NULL;
}

//=============================================================================
// SERVER THREAD FUNCTIONS

// Start the worker thread.
//
// This function is called by the server thread to start the worker thred.  The
// caller must own the lock on the server structure but neither the remote
// camera nor the camera device.
static tao_status server_start_worker(
    const char* func,
    tao_camera_server* srv)
{
    if (server_runlevel(srv) != 0) {
        report(TAO_OK, TAO_MESG_ERROR, srv,
               "Unexpected worker run-level (%d) on worker start",
               server_runlevel(srv));
        tao_store_error(__func__, TAO_BAD_STAGE);
        return TAO_ERROR;
    }
    tao_status status = tao_thread_create(&srv->worker, NULL, run_worker, srv);
    if (status != TAO_OK) {
        report(status, TAO_MESG_ERROR, srv,
               "Failed to start worker thread");
        return TAO_ERROR;
    }
    while (status == TAO_OK && srv->runlevel == 0) {
        status = tao_condition_wait(&srv->cond, &srv->mutex);
    }
    if (status != TAO_OK) {
        report(status, TAO_MESG_ERROR, srv,
               "Failed to wait worker thread");
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Kill the worker thread.
//
// This function can be called at the end of the server thread main loop
// or when the server camera is destroyed.
//
// The caller must have locked the server structure but neither the remote
// camera nor the camera device.
static tao_status server_kill_worker(
    const char* func,
    tao_camera_server* srv)
{
    // If the worker thread is running, simulate the sending of a "kill"
    // command to require the worker to quit immediately (any other pending
    // command is overridden) and notify the clients.
    if (server_runlevel(srv) > 0 && server_runlevel(srv) < 3) {
        remote_camera_lock(srv);
        if (server_task(srv) != TAO_COMMAND_NONE) {
            // Pretend pending command has been executed.
            remote_camera_command(srv) = TAO_COMMAND_NONE;
            remote_camera_ncmds(srv) += 1;
        }
        remote_camera_state(srv) = TAO_STATE_QUITTING;
        remote_camera_notify(srv);
        remote_camera_unlock(srv);
        server_task(srv) = TAO_COMMAND_KILL;
        server_notify(srv);
        while (server_runlevel(srv) < 3) {
            server_wait(srv);
        }
    }

    // Join the worker thread if the run-level indicates that the worker thread
    // has exited but has not yet been joinded.
    tao_status status = TAO_OK;
    if (server_runlevel(srv) == 3) {
        if (tao_thread_join(srv->worker, NULL) != TAO_OK) {
            status = TAO_ERROR;
            report(status, TAO_MESG_ERROR, srv,
                   "Failed to join worker thread");
            server_runlevel(srv) = 4;
        } else {
            server_runlevel(srv) = 0;
        }
    }

    // Make sure acquisition has been stopped.
    if (camera_device(srv) != NULL
        && (server_runlevel(srv) < 1 || server_runlevel(srv) > 2)) {
        camera_device_lock(srv);
        if (camera_device(srv)->runlevel == 2) {
            if (tao_camera_stop_acquisition(
                    camera_device(srv)) != TAO_OK) {
                status = TAO_ERROR;
                report(status, TAO_MESG_ERROR, srv,
                       "Failed to stop acquisition");
            }
        }
        if (status == TAO_OK && camera_device(srv)->runlevel != 1) {
            tao_store_error(__func__, TAO_BAD_STAGE);
            status = TAO_ERROR;
        }
        camera_device_unlock(srv);
    }

    return status;
}

// Run server loop. Caller has locked the remote camera, started the worker and published
// the shmid of the server.
static tao_status run_server(
    tao_camera_server* srv)
{
    // Remote object to communicate with clients.
    tao_remote_object* remote = &srv->remote->base;

    // Run server loop. On entry of the loop, we own the lock on the remote camera.
    tao_serial ncmds = remote_camera(srv)->base.ncmds;
    while (true) {
        // Wait for next command (we need to check the remote camera state as well because
        // this is how the worker may notify us that it has exited).
        while (remote->state < TAO_STATE_UNREACHABLE &&
               (remote->ncmds < ncmds || remote->command == TAO_COMMAND_NONE)) {
            remote_camera_wait(srv);
        }
        if (remote->state >= TAO_STATE_UNREACHABLE) {
            break;
        }

        // A new command has been queued by a client. Acknowledge the client that command
        // is being executed by setting the remote camera state to a transitory state.
        tao_camera_config config;
        bool relay_command = true;
        report(TAO_OK, TAO_MESG_INFO, srv, "Received `%s` command",
               tao_command_get_name(remote->command));
        ncmds = remote->ncmds + 1; // serial number of the command
        switch (remote->command) {
        case TAO_COMMAND_RESET:
            remote->state = TAO_STATE_RESETTING;
            break;
        case TAO_COMMAND_CONFIG:
            remote->state = TAO_STATE_CONFIGURING;
            break;
        case TAO_COMMAND_START:
            remote->state = TAO_STATE_STARTING;
            break;
        case TAO_COMMAND_STOP:
            remote->state = TAO_STATE_STOPPING;
            break;
        case TAO_COMMAND_ABORT:
            remote->state = TAO_STATE_ABORTING;
            break;
        case TAO_COMMAND_KILL:
            remote->state = TAO_STATE_QUITTING;
            break;
        default:
            // Invalid command. Such a command is not transmitted to the worker so we
            // increment the command counter ourself. There is no transitory state, the
            // remote camera state remains unchanged.
            report(TAO_OK, TAO_MESG_ERROR, srv, "Unexpected `%s` command (%d)",
                   tao_command_get_name(remote->command), (int)remote->command);
            relay_command = false;
            remote->ncmds = ncmds;
            remote->command = TAO_COMMAND_NONE;
        }
        show_state(srv, remote->state);
        remote_camera_notify(srv);

        // If the command is valid, relay the command to the worker.
        if (relay_command) {
            // The remote camera is unlocked while relaying the command to the worker to
            // let the clients know that the command is being executed via the transitory
            // state.
            remote_camera_unlock(srv);
            // Lock the server structure.
            server_lock(srv);
            // Wait for the worker to have exited or to be ready for a new command.
            while (server_runlevel(srv) < 3 && server_task(srv) != TAO_COMMAND_NONE) {
                server_wait(srv);
            }
            bool quitting = (server_runlevel(srv) > 2);
            if (! quitting) {
                // The task can be queued for the worker.  We let him decide if
                // the run-level is appropriate.
                if (remote->command == TAO_COMMAND_CONFIG) {
                    // Copy memorized settings for the worker. FIXME: can be avoided
                    memcpy(&server_config(srv), &remote_camera_newconfig(srv), sizeof(config));
                }
                server_task(srv) = remote->command;
                server_notify(srv);
            }
            // Unlock the server structure.
            server_unlock(srv);
            // Re-lock the remote camera.
            remote_camera_lock(srv);
            if (quitting) {
                break;
            }
        }
    }
    return TAO_OK;
}

//=============================================================================
// PUBLIC API.

tao_status tao_camera_server_run_loop(
    tao_camera_server* srv)
{
    // Check arguments.
    if (srv == NULL || remote_camera(srv) == NULL ||
        camera_device(srv) == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }

    // Start the worker while locking the server.
    server_lock(srv);
    tao_status status = server_start_worker(__func__, srv);
    int runlevel = server_runlevel(srv);
    server_unlock(srv);

    // Run the server loop while locking the remote camera.
    if (status == TAO_OK && (runlevel == 1 || runlevel == 2)) {
        remote_camera_lock(srv);
        status = run_server(srv);
        remote_camera_unlock(srv);
    }

    // Join the worker thread.
    server_lock(srv);
    if (server_kill_worker(__func__, srv) != TAO_OK) {
        status = TAO_ERROR;
    }
    server_unlock(srv);
    return status;
}

const char* tao_camera_server_get_owner(
    const tao_camera_server* srv)
{
    return (srv == NULL || remote_camera(srv) == NULL) ? "" :
        remote_camera_owner(srv);
}

tao_camera_server* tao_camera_server_create(
    const char* owner,
    tao_camera* device,
    long        nbufs,
    unsigned    flags)
{
    // Minimal check of arguments.
    if (device == NULL) {
        tao_store_error(__func__, TAO_BAD_DEVICE);
        return NULL;
    }
    if (nbufs < 2) {
        tao_store_error(__func__, TAO_BAD_BUFFERS);
        return NULL;
    }

    // Camera device must be at runlevel 1 (idle).
    tao_status status = tao_camera_lock(device);
    if (status == TAO_OK) {
        // Camera device is locked.
        if (status == TAO_OK && device->runlevel == 2) {
            status = tao_camera_stop_acquisition(device);
        }
        if (status == TAO_OK && device->runlevel != 1) {
            tao_store_error(__func__, TAO_BAD_STAGE);
            status = TAO_ERROR;
        }
        if (tao_camera_unlock(device) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    if (status != TAO_OK) {
        return NULL;
    }

    // Create remote camera to communicate with clients.
    tao_remote_camera* remote = tao_remote_camera_create(owner, nbufs, flags);
    if (remote == NULL) {
        return NULL;
    }

    // Allocate memory for the context and instantiate it.
    size_t offset = offsetof(tao_camera_server, images);
    size_t size = offset + nbufs*sizeof(tao_shared_array*);
    tao_camera_server* srv = tao_malloc(size);
    if (srv == NULL) {
        tao_remote_camera_detach(remote);
        return NULL;
    }
    memset(srv, 0, size);
    srv->mutex = TAO_MUTEX_INITIALIZER;
    srv->cond = TAO_COND_INITIALIZER;
    srv->remote = remote;
    srv->device = device;
    tao_camera_config_copy(&srv->config, &device->config);
    srv->logfile = stdout;
    srv->loglevel = 0;
    srv->fancy = true;
    srv->runlevel = 0;
    srv->state = TAO_STATE_INITIALIZING;
    srv->task = TAO_COMMAND_NONE;
    srv->drop = 1; // drop old frames
    srv->flags = flags;
    srv->timeout = 0.5;
    tao_forced_store(&srv->nbufs, nbufs);
    srv->serial = 0;
    srv->locked = NULL;
    for (int i = 0; i < 4; ++i) {
        srv->preproc[i] = NULL;
    }
    srv->shmids = TAO_COMPUTED_ADDRESS(remote, remote->base.offset);
    for (long i = 0; i < nbufs; ++i) {
        srv->images[i] = NULL;
    }

    // Allocate resources for processing images and update camera information.
    server_lock(srv);
    camera_device_lock(srv);
    remote_camera_lock(srv);
    remote_camera_command(srv) = TAO_COMMAND_NONE;
    remote_camera_state(srv) = server_state(srv);
    status = update_processing_parameters(srv);
    tao_camera_config_copy(&remote_camera_config(srv),
                           &camera_device_config(srv));
    remote_camera_notify(srv);
    remote_camera_unlock(srv);
    camera_device_unlock(srv);
    server_unlock(srv);
    if (status != TAO_OK) {
        tao_camera_server_destroy(srv);
        return NULL;
    }

    return srv;
}

tao_status tao_camera_server_destroy(
    tao_camera_server* srv)
{
    tao_status status = TAO_OK;
    if (srv != NULL) {
        // Lock camera server.
        server_lock(srv);

        // Make sure the worker thread is not running.
        if (server_kill_worker(__func__, srv) != TAO_OK) {
            status = TAO_ERROR;
        }

        // Unlock shared array to store the next acquirred image.
        if (srv->locked != NULL) {
            if (tao_shared_array_unlock(srv->locked) != TAO_OK) {
                status = TAO_ERROR;
                report(status, TAO_MESG_ERROR, srv,
                       "Failed to unlock output image");
            }
        }

        // Destroy resources shared with clients.
        if (remote_camera(srv) != NULL) {
            // Lock remote camera and indicate that server is unreachable.
            remote_camera_lock(srv);
            remote_camera_state(srv) = TAO_STATE_UNREACHABLE;

            // Release shared arrays storing acquirred images and
            // pre-processing parameters.
            for (int i = 0; i < 4; ++i) {
                release_shared_array(&status, &srv->preproc[i],
                                     &remote_camera(srv)->preproc[i]);
            }
            for (long i = 0; i < srv->nbufs; ++i) {
                release_shared_array(&status, &srv->images[i],
                                     &srv->shmids[i]);
            }

            // Notify orthers, unlock and detach the remote camera.
            remote_camera_notify(srv);
            remote_camera_unlock(srv);
            if (tao_remote_camera_detach(remote_camera(srv)) != TAO_OK) {
                status = TAO_ERROR;
                report(status, TAO_MESG_ERROR, srv,
                       "Failed to detach remote camera");
            }
        }

        // Unlock camera server and free it.
        server_unlock(srv);
        tao_free(srv);
    }
    return status;
}
