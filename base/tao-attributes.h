// tao-attributes.h -
//
// Definitions for named attributes in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2023-2024, Éric Thiébaut.

#ifndef TAO_ATTRIBUTES_H_
#define TAO_ATTRIBUTES_H_ 1

#include <tao-basics.h>

/**
 * @addtogroup NamedAttributes Named attributes.
 *
 * @brief Named attributes to associate a name to a value.
 *
 * A named attribute in TAO is a key-value pair. The key is a string, the value
 * can have different types (Boolean, integer, floating-point, string, etc.)
 * and properties (read-only, read-write, etc.).
 *
 * An important restriction is that named attributes are designed to be stored
 * into shared memory so as to be seen by different processes. Hence a list of
 * named attributes must not contains things such as memory addresses which are
 * only meaningful for a given process (use memory offsets instead).
 *
 * - Keys and string values must be interpretable as C strings with UTF-8
 *   encoding. Hence, they are null-terminated and may contains any ASCII or
 *   UTF-8 characters but must have no embedded nulls.
 *
 * - Lists of named attributes are **immutable** in the sense that the list of
 *   keys, their order, their types, and the maximal length of string values
 *   cannot be changed.
 *
 * @{
 */

/**
 * Attribute type.
 *
 * - `TAO_ATTR_BOOLEAN` is a boolean stored as `attr->val.b`.
 *
 * - `TAO_ATTR_INTEGER` is an integer value stored as `attr->val.i`.
 *
 * - `TAO_ATTR_FLOAT` is a floating-point value stored as `attr->val.f`.
 *
 * - `TAO_ATTR_STRING` is a textual value stored as `attr->val.s`, its length
 *   (including the final null) is at most `attr->val_size`.
 *
 */
typedef enum tao_attr_type_ {
    TAO_ATTR_NONE    = 0, ///< Atribute does not exists.
    TAO_ATTR_BOOLEAN = 1,
    TAO_ATTR_INTEGER = 2,
    TAO_ATTR_FLOAT   = 3,
    TAO_ATTR_STRING  = 4
} tao_attr_type;

/**
 * Named attribute.
 *
 * Named atributes associate a key (the name of the attribute) and a value in a
 * small fixed size object that can be stored in shared memory. This structure
 * is organized to be reasonably compact in memory.
 *
 * @warning The attribute key and string value are only valid while the
 * associated resource has not been destroyed by the caller.
 *
 * Once created, attribute keys are assumed to be immutable; hence, the
 * attribute key, as returned by @ref `tao_attr_get_key(attr)` or by @ref
 * `TAO_ATTR_KEY(attr)`, shall be considered as read-only.
 *
 * If `attr->type` is @ref `TAO_ATTR_BOOLEAN`, then the attribute value is
 * `attr->val.b`.
 *
 * If `attr->type` is @ref `TAO_ATTR_INTEGER`, then the attribute value is
 * `attr->val.i`.
 *
 * If `attr->type` is @ref `TAO_ATTR_FLOAT`, then the attribute value is
 * `attr->val.f`.
 *
 * If `attr->type` is @ref `TAO_ATTR_STRING`, then the attribute value is
 * `attr->val.s` and its maximal size (including the terminal null) is @ref
 * `TAO_ATTR_VAL_SIZE(attr)` or @ref `tao_attr_val_size(attr)` in bytes.
 */
typedef struct tao_attr_ {
    const uint8_t  type;       ///< Attribute type.
    const uint8_t  flags;      ///< Attribute flags.
    const uint16_t key_offset; ///< Offset to attribute key.
    const uint16_t key_size;   ///< Size for attribute key.
    union {
        bool    b; ///< Boolean value.
        int64_t i; ///< Integer value.
        double  f; ///< Floating-point value.
        char s[0]; ///< String value (actual size is given by `TAO_ATTR_VAL_SIZE`).
    } val; ///< Attribute value.
} tao_attr;

/**
 * @def TAO_ATTR_KEY_OFFSET
 *
 * The macro `TAO_ATTR_KEY_OFFSET(attr)` yields the offset to the key of the
 * named attribute `attr` relative to `attr`.
 */
#define TAO_ATTR_KEY_OFFSET(attr)  ((size_t)((attr)->key_offset))

/**
 * @def TAO_ATTR_KEY_SIZE
 *
 * The macro `TAO_ATTR_KEY_SIZE(attr)` yields the maximum number of bytes
 * (including the final null) of the key of the named attribute `attr`.
 */
#define TAO_ATTR_KEY_SIZE(attr)  ((size_t)((attr)->key_size))

/**
 * @def TAO_ATTR_VAL_OFFSET
 *
 * The macro `TAO_ATTR_VAL_OFFSET` expands to a constant expression equal to
 * the offset of the value field of a named attribute.
 */
#define TAO_ATTR_VAL_OFFSET  offsetof(tao_attr, val)

/**
 * @def TAO_ATTR_VAL_SIZE
 *
 * The macro `TAO_ATTR_VAL_SIZE(attr)` yields the maximum number of bytes
 * (including the final null if it is a string) of the value of the named
 * attribute `attr`.
 */
#define TAO_ATTR_VAL_SIZE(attr)  (TAO_ATTR_KEY_OFFSET(attr) - TAO_ATTR_VAL_OFFSET)

/**
 * @def TAO_ATTR_MAX_KEY_SIZE
 *
 * The macro `TAO_ATTR_MAX_KEY_SIZE` expands to a constant expression equal to
 * the maximum possible number of bytes for the key of a named attribute.
 */
#define TAO_ATTR_MAX_KEY_SIZE  (0xFFFF)

/**
 * @def TAO_ATTR_MAX_KEY_OFFSET
 *
 * The macro `TAO_ATTR_MAX_KEY_SIZE` expands to a constant expression equal to
 * the maximum possible offset for the key of a named attribute.
 */
#define TAO_ATTR_MAX_KEY_OFFSET  (0xFFFF)

/**
 * @def TAO_ATTR_MAX_VAL_SIZE
 *
 * The macro `TAO_ATTR_MAX_VAL_SIZE` expands to a constant expression equal to
 * the maximum possible number of bytes for the value of a named attribute.
 */
#define TAO_ATTR_MAX_VAL_SIZE  (TAO_ATTR_MAX_KEY_OFFSET - TAO_ATTR_VAL_OFFSET)

/**
 * @def TAO_ATTR_TYPE
 *
 * Macro `TAO_ATTR_TYPE(attr)` yields the value type of the named attribute
 * `attr`.
 *
 * @see tao_attr_get_type.
 */
#define TAO_ATTR_TYPE(attr) ((tao_attr_type)((attr)->type))

/**
 * @brief Retrieve the type of the value of a named attribute.
 *
 * This function yields the same result as the macro @ref `TAO_ATTR_TYPE(attr)`
 * except that `attr` may be `NULL`.
 *
 * @param attr   Address of named attribute.
 *
 * @return The value type of the attribute on success, `TAO_ATTR_NONE` on
 *         failure.
 *
 * @warning This simple accessor function leaves the last error of the thread
 *          unchanged.
 *
 * @see TAO_ATTR_TYPE.
 */
extern tao_attr_type tao_attr_get_type(
    const tao_attr* attr);

/**
 * @def TAO_ATTR_FLAGS
 *
 * Macro `TAO_ATTR_FLAGS(attr)` reveals the properties (flags) of named
 * attribute `attr`.
 */
#define TAO_ATTR_FLAGS(attr) ((unsigned)((attr)->flags))

/**
 * @brief Retrieve the bitwise flags of a named attribute.
 *
 * This function yields the same result as the macro @ref
 * `TAO_ATTR_FLAGS(attr)` except that `attr` may be `NULL`.
 *
 * @param attr   Address of named attribute.
 *
 * @return The bitwise flags of the attribute on success, `0` on failure.
 *
 * @warning This simple accessor function leaves the last error of the thread
 *          unchanged.
 *
 * @see TAO_ATTR_FLAGS.
 */
extern unsigned tao_attr_get_flags(
    const tao_attr* attr);

/**
 * @def TAO_ATTR_READABLE
 *
 * Macro `TAO_ATTR_READABLE` can be bitwise or'ed with attribute type to
 * indicate that the attribute is readable which indicates whether the value of
 * the attribute is currently available.
 */
#define TAO_ATTR_READABLE  (0x01 << 0)

/**
 * @def TAO_ATTR_WRITABLE
 *
 * Macro `TAO_ATTR_WRITABLE` can be bitwise or'ed with attribute type to
 * indicate that the attribute is writable by clients.
 */
#define TAO_ATTR_WRITABLE  (0x01 << 1)

/**
 * @def TAO_ATTR_VARIABLE
 *
 * Macro `TAO_ATTR_VARIABLE` can be bitwise or'ed with attribute type to
 * indicate that the attribute may vary spontaneously (e.g., the temperature).
 */
#define TAO_ATTR_VARIABLE  (0x01 << 2)

/**
 * @def TAO_ATTR_ACCESS
 *
 * Macro `TAO_ATTR_ACCESS(attr)` yields the r/w bitwise flags of named
 * attribute `attr`.
 */
#define TAO_ATTR_ACCESS(attr) ((attr)->flags & (TAO_ATTR_READABLE|TAO_ATTR_WRITABLE))

/**
 * @def TAO_ATTR_IS_VARIABLE
 *
 * Macro `TAO_ATTR_IS_VARIABLE(attr)` yields named whether attribute `attr` may
 * vary spontaneously.
 */
#define TAO_ATTR_IS_VARIABLE(attr) (((attr)->flags & TAO_ATTR_VARIABLE) == TAO_ATTR_VARIABLE)

/**
 * @def TAO_ATTR_IS_READABLE
 *
 * Macro `TAO_ATTR_IS_READABLE(attr)` yields whether named attribute `attr` is
 * readable by clients.
 */
#define TAO_ATTR_IS_READABLE(attr) (((attr)->flags & TAO_ATTR_READABLE) == TAO_ATTR_READABLE)

/**
 * @def TAO_ATTR_IS_WRITABLE
 *
 * Macro `TAO_ATTR_IS_WRITABLE(attr)` yields whether named attribute `attr` is
 * writable by clients.
 */
#define TAO_ATTR_IS_WRITABLE(attr) (((attr)->flags & TAO_ATTR_WRITABLE) == TAO_ATTR_WRITABLE)

/**
 * @brief Retrieve the key of a named attribute.
 *
 * @param attr   Address of named attribute.
 *
 * @return Address of attribute key on success, `NULL` on failure.
 *
 * @note This simple accessor function leaves the last error of the thread
 *       unchanged.
 *
 * @warning If non-`NULL`, the returned address is only valid until the
 *          ressources storing the attribute get released.
 */
extern const char* tao_attr_get_key(
    const tao_attr* attr);

/**
 * @brief Retrieve the maximum size of a named attribute key.
 *
 * @param attr   Address of named attribute.
 *
 * @return Maximum number of bytes that can be used for the attribute key
 *         including the final zero.
 *
 * @note This simple accessor function leaves the last error of the thread
 *       unchanged.
 */
extern size_t tao_attr_get_max_key_size(
    const tao_attr* attr);

/**
 * @brief Retrieve the maximum size of a named attribute value.
 *
 * @param attr   Address of named attribute.
 *
 * @return Maximum number of bytes that can be used for the attribute value
 *         including the final zero if the value is a string.
 *
 * @note This simple accessor function leaves the last error of the thread
 *       unchanged.
 */
extern size_t tao_attr_get_max_val_size(
    const tao_attr* attr);

/**
 * @brief Retrieve whether a named attribute is readable.
 *
 * @param attr   Address of named attribute.
 *
 * @return `0` or `1`.
 *
 * @note This simple accessor function leaves the last error of the thread
 *       unchanged.
 */
extern int tao_attr_is_readable(
    const tao_attr* attr);

/**
 * @brief Retrieve whether a named attribute is writable.
 *
 * @param attr   Address of named attribute.
 *
 * @return `0` or `1`.
 *
 * @note This simple accessor function leaves the last error of the thread
 *       unchanged.
 */
extern int tao_attr_is_writable(
    const tao_attr* attr);

/**
 * @brief Retrieve whether a named attribute is variable.
 *
 * @param attr   Address of named attribute.
 *
 * @return `0` or `1`.
 *
 * @note This simple accessor function leaves the last error of the thread
 *       unchanged.
 */
extern int tao_attr_is_variable(
    const tao_attr* attr);

/**
 * @brief Retrieve the boolean value of a named attribute.
 *
 * @param attr       Address of named attribute.
 * @param ptr        Address to store the value.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on failure.
 */
extern tao_status tao_attr_get_boolean_value(
    const tao_attr* attr,
    int* ptr);

/**
 * @brief Retrieve the integer value of a named attribute.
 *
 * This function behaves as tao_attr_get_boolean_value().
 */
extern tao_status tao_attr_get_integer_value(
    const tao_attr* attr,
    int64_t* ptr);

/**
 * @brief Retrieve the floating-point value of a named attribute.
 *
 * This function behaves as tao_attr_get_boolean_value().
 */
extern tao_status tao_attr_get_float_value(
    const tao_attr* attr,
    double* ptr);

/**
 * @brief Retrieve the string value of a named attribute.
 *
 * This function behaves as tao_attr_get_boolean_value().
 *
 * @warning If non-`NULL`, the address stored at `ptr` is only valid until the
 *          ressources storing the attribute get released.
 */
extern tao_status tao_attr_get_string_value(
    const tao_attr* attr,
    const char** ptr);

/**
 * @brief Set the boolean value of a named attribute.
 *
 * @param attr       Address of named attribute.
 * @param val        Value to set.
 * @param overwrite  Whether to set the value even though it is not writable.
 *                   This should only be allowed by the owner (the server) of
 *                   the resource. A client may change a read-only value but
 *                   this will be detected as an error by the server side or
 *                   when checking the settings.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on failure.
 */
extern tao_status tao_attr_set_boolean_value(
    tao_attr* attr,
    int val,
    int overwrite);

/**
 * @brief Set the integer value of a named attribute.
 *
 * This function behaves as tao_attr_set_boolean_value().
 */
extern tao_status tao_attr_set_integer_value(
    tao_attr* attr,
    int64_t val,
    int overwrite);

/**
 * @brief Set the floating-point value of a named attribute.
 *
 * This function behaves as tao_attr_set_boolean_value().
 */
extern tao_status tao_attr_set_float_value(
    tao_attr* attr,
    double val,
    int overwrite);

/**
 * @brief Set the string value of a named attribute.
 *
 * This function behaves as tao_attr_set_boolean_value().
 */
extern tao_status tao_attr_set_string_value(
    tao_attr* attr,
    const char* val,
    int overwrite);

#define TAO_ATTR_CHECK_NAME  (1U << 0)
#define TAO_ATTR_CHECK_FLAGS (1U << 1)
#define TAO_ATTR_CHECK_TYPE  (1U << 2)
#define TAO_ATTR_CHECK_VALUE (1U << 3)

/**
 * @brief Compare or check changes in named attributes.
 *
 * This function compares the fields of two named attributes. Note that the
 * values of the attributes can only be considered as identical if the types
 * are also identical.
 *
 * @param attr1    Address of a named attribute.
 *
 * @param attr2    Address of another named attribute.
 *
 * @param flags    Bitwise combination indicating which fields to compare:
 *                 `TAO_ATTR_CHECK_NAME` to check whether the attribute names
 *                 are the same, `TAO_ATTR_CHECK_FLAGS` to check whether the
 *                 attribute flags are the same, `TAO_ATTR_CHECK_TYPE` to check
 *                 whether the attribute types are the same, and
 *                 `TAO_ATTR_CHECK_VALUE` to check whether the attribute values
 *                 are the same.
 *
 * @return A bitwise combination of bits indicating which fields have changed.
 *         Note that `TAO_ATTR_CHECK_TYPE` and `TAO_ATTR_CHECK_VALUE` are both
 *         set in the returned value if the type has changed and at least one
 *         of `TAO_ATTR_CHECK_TYPE` or `TAO_ATTR_CHECK_VALUE` was requested.
 *
 * @warning This function leaves the caller's last error unchanged.
 */
extern unsigned int tao_attr_check(
    const tao_attr* attr1,
    const tao_attr* attr2,
    unsigned int flags);

//-----------------------------------------------------------------------------

/**
 * Table of named attributes.
 *
 * Named atributes associate a key (the name of the attribute) and a value
 * in a small fixed size object that can be stored in shared memory.
 *
 * Functions that search or retrieve an attribute from a table all return a
 * read-only pointer. By convention, the owner of the resource is allowed to
 * cast it into a non-`const` pointer to change the attribute value.
 *
 * The full table data is self-consistent for any process, it may be safely
 * copied from one place to another without loosing its meaning.
 *
 * In memory, the table structure is immediately (after proper padding for
 * alignment) followed by a vector of `length` offsets (of type `size_t`) to
 * the named attributes which are all stored after these offsets (perhaps with
 * some padding for alignment). These offsets cannot be a flexible array
 * because a table of named attributes may be stored anywhere, e.g. in the
 * middle of another structure.
 */
typedef struct tao_attr_table_ {
    size_t size;       ///< Full size of table in bytes
    long   length;     ///< Number of keys (must be the last)
} tao_attr_table;

/**
 * @brief Create a table of named attributes with given size.
 *
 * The caller is responsible of eventually calling tao_attr_table_destroy() to
 * free the resources associated with the table.
 *
 * If this function is successful, the actual table size is given by
 * tao_attr_table_get_size().
 *
 * @param size    Minimum number of bytes to allocate.
 *
 * @return Address of new table on success, `NULL` on failure.
 */
extern tao_attr_table* tao_attr_table_create_with_size(
    size_t size);

/**
 * @brief Set the size of a table of named attributes.
 *
 * This function sets the size of a table of named attributes and clear the
 * table contents (as if it has no entries). The table has been allocated by
 * the caller by other means than tao_attr_table_create() or
 * tao_attr_table_create_with_size().
 *
 * @param table   Address of table.
 * @param size    The maximal number of bytes available for the table.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on failure.
 */
extern tao_status tao_attr_table_set_size(
    tao_attr_table* table,
    size_t size);

/**
 * Description of a named attribute.
 *
 * This structure is used to define the keys and types of associated value in a
 * table of named attributes.
 */
typedef struct tao_attr_descr_ {
    const char* key;    ///< Name of attribute.
    uint8_t     type;   ///< Attribute value type.
    uint8_t     flags;  ///< Attribute bitwise flags.
    size_t      size;   ///< Max. size of value.
} tao_attr_descr;

/**
 * @brief Create a table of named attributes with given attribute definitions.
 *
 * The caller is responsible of eventually calling tao_attr_table_destroy()
 * to free the resources associated with the table.
 *
 * @param defs    List of named attribute definitions.
 * @param num     Number of entries in `defs`.
 *
 * @return Address of new table on success, `NULL` on failure.
 */
extern tao_attr_table* tao_attr_table_create(
    const tao_attr_descr defs[],
    long num);

/**
 * @brief Destroy a table of named attributes.
 *
 * This function frees the resources associated with a table created by
 * tao_attr_table_create() or by tao_attr_table_create_with_size().
 *
 * @param table   Address of existing table (may be `NULL`).
 */
extern void tao_attr_table_destroy(
    tao_attr_table* table);

/**
 * @brief Retrieve the size of a table of named attributes.
 *
 * @param table   Address of existing table (may be `NULL`).
 *
 * @return The maximal size (in bytes) available for the table, `0` if `table`
 *         is `NULL`. This function never modifyies the thread last error.
 */
extern size_t tao_attr_table_get_size(
    tao_attr_table* table);

/**
 * @brief Retrieve the number of entries in a table of named attributes.
 *
 * @param table   Address of existing table (may be `NULL`).
 *
 * @return The number of entries in the table, `0` if `table` is `NULL`. This
 *         function never modifyies the thread last error.
 */
extern long tao_attr_table_get_length(
    tao_attr_table* table);

/**
 * @brief Instantiate an existing table of named attributes with given
 *        attribute definitions.
 *
 * This function is similar to tao_attr_table_create() except that the table
 * already exists. The table may have been allocated by
 * tao_attr_table_create_with_size() or initialized by
 * tao_attr_table_set_size().
 *
 * The call may fail if the table size is too small or if some attribute
 * definitions are invalid.
 *
 * @param table   Address of existing table.
 * @param defs    List of named attributedefinitions.
 * @param num     Number of entries in `defs`.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on failure.
 */
extern tao_status tao_attr_table_instantiate(
    tao_attr_table* table,
    const tao_attr_descr defs[],
    long num);

/**
 * @brief Check the validity of the entries in a table of named attributes.
 *
 * @param table   Address of existing table.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on failure.
 */
extern tao_status tao_attr_table_check(
    const tao_attr_table* table);

/**
 * @brief Retrieve a named attribute by its index.
 *
 * @warning It is the caller's responsible to ensure that the resource storing
 * the attribute table is not destroyed while using the fields of the returned
 * attribute.
 *
 * @warning Once created, attribute keys are assumed to be immutable; hence,
 *          the attribute key shall be considered as read-only.
 *
 * @param table   Address of existing table.
 * @param index   Attribute index in table.
 *
 * @return The address `attr` on success, `NULL` on failure.
 *
 * @warning If non-`NULL`, the returned address is only valid until the
 *          ressources storing the table of attributes get released.
 */
extern const tao_attr* tao_attr_table_get_entry(
    const tao_attr_table* table,
    long index);

/**
 * @brief Retrieve a named attribute by its index.
 *
 * This function is similar to tao_attr_table_get_entry() except that is does
 * not check its arguments. It is the caller's responsibility to ensure that
 * `attr` and `table` are valid addresses and that `index` is in range.
 */
extern const tao_attr* tao_attr_table_unsafe_get_entry(
    const tao_attr_table* table,
    long index);

/**
 * @brief Attempt to find an attribute in a table of named attributes.
 *
 * @param table   Address of table.
 * @param key     Attribute name to search.
 *
 * @return `-1` if attribute is not found, to `-2` if any argument is invalid,
 *         or a non-negative index if `key` is found. This function never
 *         modifies the thread's last error.
 */
extern long tao_attr_table_try_find_entry(
    const tao_attr_table* table,
    const char* key);

/**
 * @brief Retrieve the index of an attribute in a table of named attributes.
 *
 * @param table   Address of table.
 * @param key     Attribute name to search.
 *
 * @return The address of the attribute if `key` is found in `table`, `NULL`
 *         otherwise (with thread last error sets so as to indicate the
 *         reason).
 *
 * @warning If non-`NULL`, the returned address is only valid until the
 *          ressources storing the table of attributes get released.
 */
extern const tao_attr* tao_attr_table_find_entry(
    const tao_attr_table* table,
    const char* key);

/**
 * @brief Retrieve the boolean value in a table of named attributes.
 *
 * @param table      Address of table of named attributes.
 * @param key        Attribute name.
 * @param ptr        Address to store the value.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on failure.
 */
extern tao_status tao_attr_table_get_boolean_value(
    const tao_attr_table* table,
    const char* key,
    int* ptr);

/**
 * @brief Retrieve the integer value in a table of named attributes.
 *
 * This function behaves as tao_attr_table_get_boolean_value().
 */
extern tao_status tao_attr_table_get_integer_value(
    const tao_attr_table* table,
    const char* key,
    int64_t* ptr);

/**
 * @brief Retrieve the floating-point value in a table of named attributes.
 *
 * This function behaves as tao_attr_table_get_boolean_value().
 */
extern tao_status tao_attr_table_get_float_value(
    const tao_attr_table* table,
    const char* key,
    double* ptr);

/**
 * @brief Retrieve the string value in a table of named attributes.
 *
 * This function behaves as tao_attr_table_get_boolean_value().
 *
 * @warning If non-`NULL`, the address stored at `ptr` is only valid until the
 *          ressources storing the attribute get released.
 */
extern tao_status tao_attr_table_get_string_value(
    const tao_attr_table* table,
    const char* key,
    const char** ptr);

/**
 * @brief Set the boolean value in a table of named attributes.
 *
 * @param table      Address of table of named attributes.
 * @param key        Attribute name.
 * @param val        Value to set.
 * @param overwrite  Whether to set the value even though it is not writable.
 *                   This should only be allowed by the owner (the server) of
 *                   the resource. A client may change a read-only value but
 *                   this will be detected as an error by the server or when
 *                   checking the settings.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on failure.
 */
extern tao_status tao_attr_table_set_boolean_value(
    tao_attr_table* table,
    const char* key,
    int val,
    int overwrite);

/**
 * @brief Set the integer value in a table of named attributes.
 *
 * This function behaves as tao_attr_table_set_boolean_value().
 */
extern tao_status tao_attr_table_set_integer_value(
    tao_attr_table* table,
    const char* key,
    int64_t val,
    int overwrite);

/**
 * @brief Set the floating-point value in a table of named attributes.
 *
 * This function behaves as tao_attr_table_set_boolean_value().
 */
extern tao_status tao_attr_table_set_float_value(
    tao_attr_table* table,
    const char* key,
    double val,
    int overwrite);

/**
 * @brief Set the string value in a table of named attributes.
 *
 * This function behaves as tao_attr_table_set_boolean_value().
 */
extern tao_status tao_attr_table_set_string_value(
    tao_attr_table* table,
    const char* key,
    const char* val,
    int overwrite);

/**
 * @}
 */

#endif // TAO_ATTRIBUTES_H_
