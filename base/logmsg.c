// logmsg.c --
//
// Logging messages.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2018-2024, Éric Thiébaut.

#include <stdarg.h>
#include <stdio.h> // *before* <tao-utils.h>

#if HAVE_CONFIG_H
#  include "../config.h"
#endif

#include "tao-utils.h"
#include "tao-threads.h"

// Mutex to protect message handler settings.
static tao_mutex message_mutex = TAO_MUTEX_INITIALIZER;

// Current level of verbosity (protected by above mutex).
static tao_message_level message_threshold = TAO_MESG_DEBUG;

// Current message output file (protected by above mutex).
static FILE* message_output = NULL;

static const char* message_prefix(
    tao_message_level level)
{
    switch (level) {
    case TAO_MESG_DEBUG:
        return "(TAO-DEBUG) ";
    case TAO_MESG_INFO:
        return "(TAO-INFO) ";
    case TAO_MESG_WARN:
        return "(TAO-WARN) ";
    case TAO_MESG_ERROR:
        return "(TAO-ERROR) ";
    case TAO_MESG_ASSERT:
        return "(TAO-ASSERT) ";
    case TAO_MESG_FATAL:
        return "(TAO-FATAL) ";
    default:
        return "(TAO-MESG) ";
    }
}

void tao_message_threshold_set(
    tao_message_level level)
{
    if (level < TAO_MESG_DEBUG) {
        level = TAO_MESG_DEBUG;
    }
    if (level > TAO_MESG_QUIET) {
        level = TAO_MESG_QUIET;
    }
    if (pthread_mutex_lock(&message_mutex) == 0) {
        message_threshold = level;
        (void)pthread_mutex_unlock(&message_mutex);
    }
}

tao_message_level tao_message_threshold_get(
    void)
{
    tao_message_level level = TAO_MESG_DEBUG;
    if (pthread_mutex_lock(&message_mutex) == 0) {
        level = message_threshold;
        (void)pthread_mutex_unlock(&message_mutex);
    }
    return level;
}

void tao_inform(
    FILE* output,
    tao_message_level level,
    const char* format,
    ...)
{
    if (output == NULL) {
        output = stdout;
    }
    if (pthread_mutex_lock(&message_mutex) == 0) {
        if (level >= message_threshold) {
            va_list ap;
            FILE* output = (message_output == NULL ? stderr : message_output);
            (void)fputs(message_prefix(level), output);
            va_start(ap, format);
            (void)vfprintf(output, format, ap);
            va_end(ap);
        }
        (void)pthread_mutex_unlock(&message_mutex);
    }
}
