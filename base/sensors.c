// shackhartmann.c -
//
// Implementation of Shack-Hartmann wavefront sensors in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2022, Éric Thiébaut.

#include <math.h>
#include <string.h>

#include "tao-errors.h"
#include "tao-layouts.h"
#include "tao-generic.h"
#include "tao-sensors.h"

static tao_status check_algorithm(
    const char* func,
    tao_algorithm algorithm)
{
    if (algorithm != TAO_CENTER_OF_GRAVITY &&
        algorithm != TAO_LINEARIZED_MATCHED_FILTER) {
        tao_store_error(func, TAO_BAD_ALGORITHM);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status check_bounding_box(
    const char* func,
    const tao_bounding_box* box)
{
    if (box->width < 1 || box->height < 1) {
        tao_store_error(func, TAO_BAD_BOUNDING_BOX);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status check_subimage(
    const char* func,
    const tao_subimage* img)
{
    if (check_bounding_box(func, &img->box) != TAO_OK) {
        return TAO_ERROR;
    }
    if (!isfinite(img->ref.x) || !isfinite(img->ref.y)) {
        tao_store_error(func, TAO_BAD_REFERENCE);
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status tao_sensor_control_check(
    const tao_sensor_control* ctrl)
{
    if (ctrl == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (!isfinite(ctrl->forgetting_factor) || ctrl->forgetting_factor < 0) {
        tao_store_error(__func__, TAO_BAD_FORGETTING_FACTOR);
        return TAO_ERROR;
    }
    if (!isfinite(ctrl->smoothness_level) || ctrl->smoothness_level < 0) {
        tao_store_error(__func__, TAO_BAD_SMOOTHNESS_LEVEL);
        return TAO_ERROR;
    }
    if (!isfinite(ctrl->restoring_force) || ctrl->restoring_force < 0) {
        tao_store_error(__func__, TAO_BAD_RESTORING_FORCE);
        return TAO_ERROR;
    }
    if (!isnan(ctrl->max_excursion) || ctrl->max_excursion < 0) {
        tao_store_error(__func__, TAO_BAD_MAX_EXCURSION);
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status tao_sensor_params_check(
    const tao_sensor_params* params,
    const long* inds,
    long ninds,
    const tao_subimage* subs,
    long nsubs)
{
    if (params == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }

    // Check layout.
    long dim1 = params->dims[0];
    long dim2 = params->dims[1];
    if (dim1 < 0 || dim2 < 0 || ninds != dim1*dim2) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return TAO_ERROR;
    }
    if (ninds > 0 && inds == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    long count = tao_indexed_layout_check(inds, dim1, dim2);
    if (count < 0) {
         return TAO_ERROR;
    }

    // Check sub-images.
    if (nsubs != params->nsubs || nsubs != count) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return TAO_ERROR;
    }
    if (nsubs > 0 && subs == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    for (long i = 0; i < nsubs; ++i) {
        if (check_subimage(__func__, &subs[i]) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Check name of camera.
    if (tao_check_owner_name(params->camera) != TAO_OK) {
        return TAO_ERROR;
    }

    // Check other parameters.
    if (tao_sensor_control_check(&params->ctrl) != TAO_OK) {
        return TAO_ERROR;
    }
    if (check_algorithm(__func__, params->algorithm) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}
