// fitsfiles.c -
//
// Loading of FITS files for TAO (TAO is a library for Adaptive Optics software).
//
// This file is part of the TAO software (https://git-cral.univ-lyon1.fr/tao)
// licensed under the MIT license.
//
// Copyright (c) 2018-2024, Éric Thiébaut.

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#if HAVE_CONFIG_H
#  include "../config.h"
#endif

// If FITSIO is not available, we define macro `_FITSIO2_H` and define type `fitsfile` to
// be an opaque structure in order to be able to compile the code.
#undef _FITSIO2_H
#if TAO_USE_CFITSIO
#  include <fitsio2.h>
#else
#  define _FITSIO2_H // to avoid #include <fitsio2.h> in "tao-fits.h"
typedef struct fitsfile fitsfile; // opaque structure
#endif

#include "tao-fits.h"
#include "tao-errors.h"

#if TAO_USE_CFITSIO
static void get_fits_error_details(
    int errcode,
    const char** reason,
    const char** info)
{
    if (reason != NULL) {
        *reason = NULL;
    }
    if (info != NULL) {
        *info = NULL;
    }
}
#endif // TAO_USE_CFITSIO

#if TAO_USE_CFITSIO
static void push_fits_error(
    const char* func,
    int errcode)
{
    tao_store_other_error(func, errcode, get_fits_error_details);
}
#endif // TAO_USE_CFITSIO

#if TAO_USE_CFITSIO
// Yields TAO element type given FITSIO bitpix, -1 in case of error.  Beware
// that the BITPIX symbolic names used in FITSIO are somewhat misleading (for
// instance LONG_IMG means 32-bit signed integers, not necessarily C long
// integer type).
static tao_eltype bitpix_to_element_type(
    int bitpix)
{
    switch (bitpix) {
    case SBYTE_IMG:    return TAO_INT8;
    case BYTE_IMG:     return TAO_UINT8;
    case SHORT_IMG:    return TAO_INT16;
    case USHORT_IMG:   return TAO_UINT16;
    case LONG_IMG:     return TAO_INT32;
    case ULONG_IMG:    return TAO_UINT32;
    case LONGLONG_IMG: return TAO_INT64;
    case FLOAT_IMG:    return TAO_FLOAT;
    case DOUBLE_IMG:   return TAO_DOUBLE;
    default:           return -1;
    }
}
#endif // TAO_USE_CFITSIO

#if TAO_USE_CFITSIO
// Yields FITSIO bitpix given TAO element type, -1 in case of error.
static int element_type_to_bitpix(
    tao_eltype eltype)
{
    switch (eltype) {
    case TAO_INT8:   return SBYTE_IMG;
    case TAO_UINT8:  return BYTE_IMG;
    case TAO_INT16:  return SHORT_IMG;
    case TAO_UINT16: return USHORT_IMG;
    case TAO_INT32:  return LONG_IMG;
    case TAO_UINT32: return ULONG_IMG;
    case TAO_INT64:  return LONGLONG_IMG;
    case TAO_UINT64: return LONGLONG_IMG; // hope for the best!
    case TAO_FLOAT:  return FLOAT_IMG;
    case TAO_DOUBLE: return DOUBLE_IMG;
    default:         return -1;
    }
}
#endif // TAO_USE_CFITSIO

#if TAO_USE_CFITSIO
// Yields FITSIO datatype given TAO element type, -1 in case of error.
static int element_type_to_datatype(
    tao_eltype eltype)
{

#define SIGNED(n)                                       \
    ((n) == 1 ? TSBYTE :                                \
     ((n) == sizeof(short) ? TSHORT :                   \
      ((n) == sizeof(int) ? TINT :                      \
       ((n) == sizeof(long) ? TLONG :                   \
        ((n) == sizeof(LONGLONG) ? TLONGLONG : -1)))))

#define UNSIGNED(n)                             \
    ((n) == 1 ? TBYTE :                         \
     ((n) == sizeof(short) ? TUSHORT :          \
      ((n) == sizeof(int) ? TUINT :             \
       ((n) == sizeof(long) ? TULONG : -1))))

    switch (eltype) {
    case TAO_INT8:   return   SIGNED(1);
    case TAO_UINT8:  return UNSIGNED(1);
    case TAO_INT16:  return   SIGNED(2);
    case TAO_UINT16: return UNSIGNED(2);
    case TAO_INT32:  return   SIGNED(4);
    case TAO_UINT32: return UNSIGNED(4);
    case TAO_INT64:  return   SIGNED(8);
    case TAO_UINT64: return UNSIGNED(8);
    case TAO_FLOAT:  return TFLOAT;
    case TAO_DOUBLE: return TDOUBLE;
    default:         return -1;
    }

#undef UNSIGNED
#undef SIGNED

}
#endif // TAO_USE_CFITSIO

tao_array* tao_load_array_from_fits_file(
    const char* filename,
    char* extname)
{
#if TAO_USE_CFITSIO
    int errcode = 0;
    fitsfile* fptr;
    tao_array* arr = NULL;

    fits_clear_errmsg();
    if (extname == NULL) {
        // Open the FITS file and move to the first IMAGE HDU.
        fits_open_image(&fptr, filename, READONLY, &errcode);
    } else {
        // Open the FITS file and move to the specified IMAGE extension.
        if (fits_open_diskfile(&fptr, filename, READONLY, &errcode) == 0) {
            fits_movnam_hdu(fptr, IMAGE_HDU, extname, 0, &errcode);
        }
    }
    if (errcode == 0) {
        arr = tao_load_array_from_fits_handle(fptr);
        fits_close_file(fptr, &errcode);
    }
    if (errcode != 0) {
        if (arr != NULL) {
            tao_unreference_array(arr);
        }
        push_fits_error(__func__, errcode);
        return NULL;
    }
    return arr;
#else
    tao_store_error(__func__, TAO_NO_FITS_SUPPORT);
    return NULL;
#endif
}

tao_array* tao_load_array_from_fits_handle(
    fitsfile* fptr)
{
#if TAO_USE_CFITSIO
    int bitpix, ndims;
    int errcode = 0;
    long dims[TAO_MAX_NDIMS];

    // Get the type of the elements.
    if (fits_get_img_equivtype(fptr, &bitpix, &errcode) != 0) {
        push_fits_error("fits_get_img_equivtype", errcode);
        return NULL;
    }
    tao_eltype eltype = bitpix_to_element_type(bitpix);
    int datatype = element_type_to_datatype(eltype);
    if (eltype == -1 || datatype == -1) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return NULL;
    }

    // Get the dimensions array.
    if (fits_get_img_dim(fptr, &ndims, &errcode) != 0) {
        push_fits_error("fits_get_img_dim", errcode);
        return NULL;
    }
    if (ndims > TAO_MAX_NDIMS) {
        tao_store_error(__func__, TAO_BAD_RANK);
        return NULL;
    }
    if (fits_get_img_size(fptr, ndims, dims, &errcode) != 0) {
        push_fits_error("fits_get_img_size", errcode);
        return NULL;
    }

    // Create the array.
    tao_array* arr = tao_create_array(eltype, ndims, dims);
    if (arr == NULL) {
        return NULL;
    }

    // Read the data.
    void* data = tao_get_array_data(arr);
    long nelem = tao_get_array_length(arr);
    int anynull;
    if (fits_read_img(fptr, datatype, 1, nelem,
                      NULL, data, &anynull, &errcode) != 0) {
        push_fits_error("fits_read_img", errcode);
        tao_unreference_array(arr);
        return NULL;
    }
    return arr;
#else
    tao_store_error(__func__, TAO_NO_FITS_SUPPORT);
    return NULL;
#endif
}

tao_status tao_save_array_to_fits_file(
    const tao_array* arr,
    const char* filename,
    bool overwrite)
{
#if TAO_USE_CFITSIO
    int errcode = 0;
    fitsfile* fptr;

    fits_clear_errmsg();
    if (! overwrite) {
        // Check that output file does not exists.
        int fd = open(filename, O_CREAT|O_EXCL, 0644);
        if (fd == -1) {
            tao_store_error(__func__, TAO_ALREADY_EXIST);
            return TAO_ERROR;
        }
        close(fd);
    }
    unlink(filename);
    if (fits_create_file(&fptr, filename, &errcode) != 0) {
        push_fits_error("fits_create_file", errcode);
        return TAO_ERROR;
    }
    tao_status status = tao_save_array_to_fits_handle(arr, fptr,
                                                        NULL, NULL);
    if (fits_close_file(fptr, &errcode) != 0) {
        push_fits_error("fits_close_file", errcode);
        status = TAO_ERROR;
    }
    return status;
#else
    tao_store_error(__func__, TAO_NO_FITS_SUPPORT);
    return TAO_ERROR;
#endif
}

tao_status tao_save_array_to_fits_handle(
    const tao_array* arr,
    fitsfile* fptr,
    int (*callback)(
        fitsfile*,
        void*),
    void* ctx)
{
#if TAO_USE_CFITSIO
    // Get BITPIX and datatype from array element type.
    if (arr == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_eltype eltype = tao_get_array_eltype(arr);
    int bitpix = element_type_to_bitpix(eltype);
    int datatype = element_type_to_datatype(tao_get_array_eltype(arr));
    if (bitpix == -1 || datatype == -1) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }

    // Create image HDU.
    long dims[TAO_MAX_NDIMS];
    int ndims = tao_get_array_ndims(arr);
    for (int d = 1; d <= ndims; ++d) {
        dims[d-1] = tao_get_array_dim(arr, d);
    }
    int errcode = 0;
    if (fits_create_img(fptr, bitpix, ndims, dims, &errcode) != 0) {
        push_fits_error("fits_create_img", errcode);
        return TAO_ERROR;
    }

    // Update header if requested so.
    if (callback != NULL) {
        if (callback(fptr, ctx) != 0) {
            return TAO_ERROR;
        }
    }

    // Write array contents.
    long nelem = tao_get_array_length(arr);
    void* data = tao_get_array_data(arr);
    if (fits_write_img(fptr, datatype, 1, nelem, data, &errcode) != 0) {
        push_fits_error("fits_write_img", errcode);
        return TAO_ERROR;
    }
    return TAO_OK;
#else
    tao_store_error(__func__, TAO_NO_FITS_SUPPORT);
    return TAO_ERROR;
#endif
}
