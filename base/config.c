// config.c -
//
// Management of global configuration parameters in TAO library.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2018-2024, Éric Thiébaut.

#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h> // *before* <tao-errors.h>
#include <string.h>
#include <sys/stat.h>

#include "tao-config.h"
#include "tao-errors.h"
#include "tao-macros.h"

FILE* tao_file_open(
    const char* path,
    const char* mode)
{
    // First try to just open the file.  This may fail if intermediate
    // directories do not exist yet.
    FILE* file = fopen(path, mode);
    if (file != NULL) {
        return file;
    }
    int code = errno;
    if (code != ENOENT || path == NULL || path[0] == '\0'
        || mode == NULL || (mode[0] != 'a' && mode[0] != 'w')) {
        tao_store_error("fopen", code);
        return NULL;
    }

    // Copy path in writable buffer, avoiding duplicate '/' and remembering the
    // index of the last '/'.
    char buf[PATH_MAX];
    long j = 0;
    long last = -1;
    for (long i = 0; path[i] != '\0'; ++i) {
        if (path[i] == '/') {
            if (last == j) {
                continue;
            }
            last = j + 1;
        }
        if (j >= PATH_MAX - 1) {
            // Filename is too long.
            goto bad_filename;
        }
        buf[j] = path[i];
        j += 1;
    }
    buf[j] = '\0';
    if (last < 0 || buf[j-1] == '/') {
        // No sub-directories or invalid file name.
        goto bad_filename;
    }

    // Create intermediate directories.
    for (long i = 1; i <= last; ++i) {
        if (buf[i] != '/') {
            continue;
        }
        buf[i] = '\0';
        struct stat sb;
        if (stat(buf, &sb) != 0) {
            if (errno == ENOENT) {
                // Parent path does not exist, try to make the directory.
                if (mkdir(buf, 0777) != 0) {
                    tao_store_system_error("mkdir");
                    return NULL;
                }
            } else {
                tao_store_system_error("stat");
                return NULL;
            }
        } else {
            if ((sb.st_mode & S_IFMT) != S_IFDIR) {
                // Parent path exists but is not a directory.
                tao_store_error(__func__, ENOTDIR);
                return NULL;
            }
        }
        buf[i] = '/';
    }

    // Re-try to open the file.
    file = fopen(path, mode);
    if (file == NULL) {
        tao_store_system_error("fopen");
        return NULL;
    }
    return file;

bad_filename:
    tao_store_error(__func__, TAO_BAD_FILENAME);
    return NULL;
}

tao_status tao_file_close(
    FILE* file)
{
    if (file != NULL && fclose(file) != 0) {
        tao_store_system_error("fclose");
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status tao_config_path(
    char*       path,
    long        size,
    const char* name)
{
    // Concatenate path.
    const char* configdir = TAO_CONFIG_DIR;
    long name_length = TAO_STRLEN(name);
    long root_length = strlen(configdir);
    if (root_length + name_length + 2 > size) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return TAO_ERROR;
    }
    memcpy(path, configdir, root_length);
    path[root_length] = '/';
    memcpy(path + root_length + 1, name, name_length);
    path[root_length + name_length + 1] = '\0';
    return TAO_OK;
}

FILE* tao_config_open(
    const char* name,
    const char* mode)
{
    char path[PATH_MAX];
    if (tao_config_path(path, sizeof(path), name) != TAO_OK) {
        return NULL;
    }
    return tao_file_open(path, mode);
}

tao_status tao_config_read_long(
    const char* name,
    long*       ptr)
{
    if (ptr == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    FILE* file = tao_config_open(name, "r");
    if (file == NULL) {
        return TAO_ERROR;
    }
    char buf[2];
    tao_status status = TAO_OK;
    if (fscanf(file, "%ld %1s", ptr, buf) != 1) {
        tao_store_error(__func__, TAO_BAD_VALUE);
        status = TAO_ERROR;
    }
    if (tao_file_close(file) != TAO_OK) {
         status = TAO_ERROR;
    }
    return status;
}

tao_status tao_config_write_long(
    const char* name,
    long        value)
{
    FILE* file = tao_config_open(name, "w");
    if (file == NULL) {
        return TAO_ERROR;
    }
    tao_status status = TAO_OK;
    if (fprintf(file, "%ld\n", value) < 0) {
        tao_store_system_error("fprintf");
        status = TAO_ERROR;
    }
    if (tao_file_close(file) != TAO_OK) {
         status = TAO_ERROR;
    }
    return status;
}

tao_shmid tao_config_read_shmid(
    const char* name)
{
    tao_shmid shmid = TAO_BAD_SHMID;
    long value;
    if (tao_config_read_long(name, &value) != TAO_OK) {
        tao_clear_error(NULL);
    } else if (value >= 0 && (tao_shmid)value == value) {
        shmid = value;
    }
    return shmid;
}

int tao_config_read(
    const char* name,
    const char* format,
    ...)
{
    long format_length = (format == NULL ? 0 : strlen(format));
    if (format_length < 1) {
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
         return TAO_ERROR;
    }
    FILE* file = tao_config_open(name, "r");
    if (file == NULL) {
        return TAO_ERROR;
    }
    va_list args;
    va_start(args, format);
    int retval = vfscanf(file, format, args);
    va_end(args);
    if (retval < 0) {
        tao_store_system_error("vfscanf");
    }
    if (tao_file_close(file) != TAO_OK) {
        retval = -1;
    }
    return retval;
}

tao_status tao_config_write(
    const char* name,
    const char* format,
    ...)
{
    long format_length = (format == NULL ? 0 : strlen(format));
    if (format_length < 1) {
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
         return TAO_ERROR;
    }
    FILE* file = tao_config_open(name, "w");
    if (file == NULL) {
        return TAO_ERROR;
    }
    tao_status status = TAO_OK;
    va_list args;
    va_start(args, format);
    int code = vfprintf(file, format, args);
    va_end(args);
    if (code < 0) {
        tao_store_system_error("vfprintf");
        status = TAO_ERROR;
    }
    if (code >= 0 && format[format_length - 1] != '\n') {
        if (fputc('\n', file) == EOF) {
            tao_store_system_error("fputc");
            status = TAO_ERROR;
        }
    }
    if (tao_file_close(file) != TAO_OK) {
        status = TAO_ERROR;
    }
    return status;
}
