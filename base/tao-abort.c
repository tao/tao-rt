// tao-abort.c -
//
// Send a simple command to a TAO server.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2024, Éric Thiébaut.

#include <stdio.h> // *before* <tao-errors.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "tao-errors.h"
#include "tao-config.h"
#include "tao-shared-objects.h"
#include "tao-remote-objects.h"
#include "tao-remote-cameras.h"
#include "tao-remote-mirrors.h"
#include "tao-remote-sensors.h"
#include "tao-macros.h"

static void report_error(
    FILE* output,
    const char* progname,
    const char* mesg)
{
    fprintf(output, "%s: %s.", progname, mesg);
    if (tao_any_errors(NULL)) {
        tao_report_error_to_stream(output, NULL, "  ", ".\n");
    } else {
        fputc('\n', output);
    }
}

int main(
    int argc,
    char* argv[])
{
    // Determine program name and command.
    const char* progname = tao_basename(argv[0]);
    tao_command command = TAO_COMMAND_NONE;
    if (progname[0] == 't' && progname[1] == 'a' && progname[2] == 'o' &&
        progname[3] == '_') {
        if (strcmp(progname + 4, "abort") == 0) {
            command = TAO_COMMAND_ABORT;
        } else if (strcmp(progname + 4, "reset") == 0) {
            command = TAO_COMMAND_RESET;
        } else if (strcmp(progname + 4, "kill") == 0) {
            command = TAO_COMMAND_KILL;
        } else if (strcmp(progname + 4, "start") == 0) {
            command = TAO_COMMAND_START;
        } else if (strcmp(progname + 4, "stop") == 0) {
            command = TAO_COMMAND_STOP;
        }
    }
    if (command == TAO_COMMAND_NONE) {
        fprintf(stderr, "%s: Unknown command (check the link name).\n",
                progname);
        return EXIT_FAILURE;
    }

    // Parse arguments.
    char dummy;
    double secs = 5.0;
    const char* server = NULL;
    const char* usage = "Usage: %s [OPTIONS] [--] SERVER\n";
    int iarg = 1;
    while (iarg < argc) {
        if (argv[iarg][0] != '-') {
            break;
        }
        if (argv[iarg][1] == '-' && argv[iarg][2] == '\0') {
            ++iarg;
            break;
        }
        if (strcmp(argv[iarg], "-timeout") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option %s\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%lf %c", &secs, &dummy) != 1
                || !isfinite(secs) || secs < 0) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option %s\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            iarg += 2;
            continue;
        }
        if (strcmp(argv[iarg], "-h") == 0
            || strcmp(argv[iarg], "-help") == 0
            || strcmp(argv[iarg], "--help") == 0) {
            printf(usage, progname);
            printf("\n");
            printf("Send a `%s` command to SERVER.\n",
                   tao_command_get_name(command));
            printf("\n");
            printf("Arguments:\n");
            printf("  SERVER             Server name or shared memory "
                   "identifier.\n");
            printf("\n");
            printf("Options:\n");
            printf("  -timeout SECS      Maximum number of seconds to wait "
                   "[%g].\n", secs);
            printf("  -h, -help, --help  Print this help.\n");
            return EXIT_SUCCESS;
        }
        fprintf(stderr, "%s: Unknown option %s\n", progname, argv[iarg]);
        return EXIT_FAILURE;
    }
    if (argc - iarg != 1) {
        fprintf(stderr, "%s: Invalid number of arguments.\n", progname);
        fprintf(stderr, usage, progname);
        return EXIT_FAILURE;
    }
    server = argv[iarg];
    bool flag = false;
    tao_shmid shmid = TAO_BAD_SHMID;
    int c = server[0];
    if (c >= '0' && c <= '9') {
        long val;
        if (sscanf(server, "%ld %c", &val, &dummy) == 1) {
            shmid = val;
            flag = true;
        }
    }
    if (!flag) {
        shmid = tao_config_read_shmid(server);
    }
    if (shmid == TAO_BAD_SHMID) {
        report_error(stderr, progname,
                     "Invalid server or shared memory identifier");
        return EXIT_FAILURE;
    }
    int exit_code = EXIT_SUCCESS;
    tao_remote_object* obj = tao_remote_object_attach(shmid);
    if (obj == NULL) {
        report_error(stderr, progname, "Failed to attach to server");
        return EXIT_FAILURE;
    }
    tao_object_type type = tao_remote_object_get_type(obj);
    tao_serial cmdnum = -2; // assume no command sent
    tao_serial datnum = 0;
    if (type == TAO_REMOTE_CAMERA) {
        tao_remote_camera* cam = (tao_remote_camera*)obj;
        switch (command) {
        case TAO_COMMAND_START:
            cmdnum = tao_remote_camera_start(cam, secs);
            break;
        case TAO_COMMAND_STOP:
            cmdnum = tao_remote_camera_stop(cam, secs);
            break;
        case TAO_COMMAND_ABORT:
            cmdnum = tao_remote_camera_abort(cam, secs);
            break;
        case TAO_COMMAND_RESET:
            cmdnum = tao_remote_camera_reset(cam, secs);
            break;
        case TAO_COMMAND_KILL:
            cmdnum = tao_remote_camera_kill(cam, secs);
            break;
        default:
            break;
        }
    } else if (type == TAO_REMOTE_MIRROR) {
        tao_remote_mirror* dm = (tao_remote_mirror*)obj;
        switch (command) {
        case TAO_COMMAND_RESET:
            cmdnum = tao_remote_mirror_reset(dm, 0, secs, &datnum);
            break;
        case TAO_COMMAND_KILL:
            cmdnum = tao_remote_mirror_kill(dm, secs);
            break;
        default:
            break;
        }
    } else if (type == TAO_REMOTE_SENSOR) {
        tao_remote_sensor* wfs = (tao_remote_sensor*)obj;
        switch (command) {
        case TAO_COMMAND_START:
            cmdnum = tao_remote_sensor_start(wfs, secs);
            break;
        case TAO_COMMAND_STOP:
            cmdnum = tao_remote_sensor_stop(wfs, secs);
            break;
        case TAO_COMMAND_KILL:
            cmdnum = tao_remote_sensor_kill(wfs, secs);
            break;
        default:
            break;
        }
    }
    if (cmdnum <= 0) {
        if (cmdnum == 0) {
            fprintf(stderr, "%s: Timeout.\n", progname);
        } else if (cmdnum == -2) {
            fprintf(stderr, "%s: Invalid server type for this command.\n",
                    progname);
            exit_code = EXIT_FAILURE;
        } else {
            report_error(stderr, progname, "Some error occurred");
            exit_code = EXIT_FAILURE;
        }
    }
    if (obj != NULL) {
        tao_remote_object_detach(obj);
    }
    if (datnum > 0) {
        fprintf(stdout, TAO_INT64_FORMAT(,d) " " TAO_INT64_FORMAT(,d)  "\n",
                cmdnum, datnum);
    } else {
        fprintf(stdout, TAO_INT64_FORMAT(,d) "\n", cmdnum);
    }
    return exit_code;
}
