// utils.c --
//
// Utility functions.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2018-2024, Éric Thiébaut.

#include <assert.h>
#include <errno.h>
//#include <float.h>
//#include <math.h>
#include <stdbool.h>
#include <stdio.h> // *before* <tao-errors.h> and <tao-utils.h>
#include <stdlib.h>
#include <string.h>
//#include <time.h> // *before* <tao-utils.h>

#include "tao-utils.h"
#include "tao-errors.h"
#include "tao-macros.h"

//-----------------------------------------------------------------------------
// LIBRARY VERSION

void tao_version_get(
    int* major,
    int* minor,
    int* patch)
{
    if (major != NULL) {
        *major = TAO_VERSION_MAJOR;
    }
    if (minor != NULL) {
        *minor = TAO_VERSION_MINOR;
    }
    if (patch != NULL) {
        *patch = TAO_VERSION_PATCH;
    }
}

//-----------------------------------------------------------------------------
// DYNAMIC MEMORY

void* tao_malloc(
    size_t size)
{
    void* ptr = malloc(size);
    if (ptr == NULL) {
        tao_store_system_error("malloc");
    }
    return ptr;
}

void* tao_calloc(
    size_t nelem,
    size_t elsize)
{
    void* ptr = calloc(nelem, elsize);
    if (ptr == NULL) {
        tao_store_system_error("calloc");
    }
    return ptr;
}

void* tao_realloc(
    void* ptr,
    size_t size)
{
    void* newptr = realloc(ptr, size);
    if (newptr == NULL) {
        tao_store_system_error("realloc");
    }
    return newptr;
}

void tao_free(
    void* ptr)
{
    if (ptr != NULL) {
        free(ptr);
    }
}

//-----------------------------------------------------------------------------
// STRINGS

size_t tao_strlen(
    const char* str)
{
    return TAO_STRLEN(str);
}

const char* tao_basename(
    const char* path)
{
    if (path != NULL) {
        const char* ptr = strrchr(path, '/');
        if (ptr != NULL) {
            return ptr + 1;
        }
    }
    return path;
}

long tao_check_string(const char* str, long siz)
{
    if (str == NULL) {
        return 0;
    }
    for (long len = 0; len < siz; ++len) {
        if (str[len] == '\0') {
            return len;
        }
    }
    return -1;
}

tao_status tao_parse_int(
    const char* str,
    int* ptr,
    int base)
{
    long val;
    if (ptr == NULL) {
        errno = EFAULT;
        return TAO_ERROR;
    }
    if (tao_parse_long(str, &val, base) != TAO_OK) {
        return TAO_ERROR;
    }
    if (val < INT_MIN || val > INT_MAX) {
        errno = ERANGE;
        return TAO_ERROR;
    }
    *ptr = (int)val;
    return TAO_OK;
}

tao_status tao_parse_long(
    const char* str,
    long* ptr,
    int base)
{
    char* end;
    long val;
    if (str == NULL || ptr == NULL) {
        errno = EFAULT;
        return TAO_ERROR;
    }
    errno = 0; // to detect errors, e.g. overflows
    val = strtol(str, &end, base);
    if (errno != 0) {
        return TAO_ERROR;
    }
    if (end == str || *end != '\0') {
        errno = EINVAL;
        return TAO_ERROR;
    }
    *ptr = val; // only change value in case of success
    return TAO_OK;
}

tao_status tao_parse_double(
    const char* str,
    double* ptr)
{
    char* end;
    double val;
    if (str == NULL || ptr == NULL) {
        errno = EFAULT;
        return TAO_ERROR;
    }
    errno = 0; // to detect errors
    val = strtod(str, &end);
    if (errno != 0) {
        return TAO_ERROR;
    }
    if (end == str || *end != '\0') {
        errno = EINVAL;
        return TAO_ERROR;
    }
    *ptr = val; // only change value in case of success
    return TAO_OK;
}

// Yields whether unsigned integer `c` is a valid ASCII code. Use _Generic
// statement to enforce that argument be unsigned.
#define IS_ASCII(c)                             \
    _Generic((c),                               \
             uint8_t:        (c) < 0x80,        \
             uint16_t:       (c) < 0x80,        \
             uint32_t:       (c) < 0x80,        \
             uint64_t:       (c) < 0x80)

// Yield whether a byte is the 1st byte of a 1-byte, 2-byte, 3-byte, or 4-byte
// UTF-8 sequence.
static inline bool is_utf8_1(uint8_t b) { return  b < 0x80; }
static inline bool is_utf8_2(uint8_t b) { return (b & 0xE0) == 0xC0; }
static inline bool is_utf8_3(uint8_t b) { return (b & 0xF0) == 0xE0; }
static inline bool is_utf8_4(uint8_t b) { return (b & 0xF8) == 0xF0; }

// Yields whether a byte is another byte than the 1st one of an UTF-8 sequence.
static inline bool is_utf8_cont(uint8_t b) { return (b & 0xC0) == 0x80; }

tao_status tao_transcode_ascii_string_to_unicode(
    const char* src_,
    wchar_t *dst,
    long len)
{
    if (src_ == NULL || dst == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    assert(sizeof(char) == 1);
    const uint8_t* src = (const uint8_t*)src_;
    tao_error_code code = TAO_BAD_SIZE; // assume too short destination if loop is exited
    for (long i = 0; i < len; ++i) {
        uint8_t c = src[i];
        if (IS_ASCII(c)) {
            dst[i] = c;
            if (c == 0) {
                return TAO_OK;
            }
        } else {
            code = TAO_BAD_CHARACTER;
            break;
        }
    }
    if (len > 0) {
        // Truncate the destination string in case caller ignores the error.
        dst[len - 1] = 0;
    }
    tao_store_error(__func__, code);
    return TAO_ERROR;
}

tao_status tao_transcode_unicode_string_to_ascii(
    const wchar_t* src_,
    char *dst,
    long len)
{
    if (src_ == NULL || dst == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    assert(sizeof(wchar_t) == 4);
    const uint32_t* src = (const uint32_t*)src_;
    tao_error_code code = TAO_BAD_SIZE; // assume too short destination if loop is exited
    for (long i = 0; i < len; ++i) {
        uint32_t c = src[i];
        if (IS_ASCII(c)) {
            dst[i] = c;
            if (c == 0) {
                return TAO_OK;
            }
        } else {
            code = TAO_BAD_CHARACTER;
            break;
        }
    }
    if (len > 0) {
        // Truncate the destination string in case caller ignores the error.
        dst[len - 1] = 0;
    }
    tao_store_error(__func__, code);
    return TAO_ERROR;
}

tao_status tao_transcode_utf8_string_to_unicode(
    const char* src_,
    wchar_t *dst_,
    long len)
{
    if (src_ == NULL || dst_ == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    assert(sizeof(char) == 1);
    assert(sizeof(wchar_t) == 4);
    const uint8_t* src = (const uint8_t*)src_;
    uint32_t* dst = (uint32_t*)dst_;
    long j = 0; // index in source
    tao_error_code code = TAO_BAD_SIZE; // assume too short destination if loop is exited
    for (long i = 0; i < len; ++i) {
        uint8_t b1 = src[j++];
        if (is_utf8_1(b1)) {
            // 1-byte UTF-8 sequence (ASCII).
            dst[i] = b1;
            if (b1 == 0x00) {
                return TAO_OK;
            }
        } else {
            uint32_t u;
            uint8_t b2 = src[j++];
            if (! is_utf8_cont(b2)) {
                code = TAO_BAD_CHARACTER;
                break;
            }
            if (is_utf8_2(b1)) {
                // 2-byte UTF-8 sequence.
                u = ((uint32_t)(b1 & 0x1F) << 6) |
                    ((uint32_t)(b2 & 0x3F)     );
            } else {
                uint8_t b3 = src[j++];
                if (! is_utf8_cont(b3)) {
                    code = TAO_BAD_CHARACTER;
                    break;
                }
                if (is_utf8_3(b1)) {
                    // 3-byte UTF-8 sequence.
                    u = ((uint32_t)(b1 & 0x0F) << 12) |
                        ((uint32_t)(b2 & 0x3F) <<  6) |
                        ((uint32_t)(b3 & 0x3F)      );
                } else {
                    uint8_t b4 = src[j++];
                    if (! is_utf8_cont(b4)) {
                        code = TAO_BAD_CHARACTER;
                        break;
                    }
                    if (is_utf8_4(b1)) {
                        // 4-byte UTF-8 sequence.
                        u = ((uint32_t)(b1 & 0x07) << 18) |
                            ((uint32_t)(b2 & 0x3F) << 12) |
                            ((uint32_t)(b3 & 0x3F) <<  6) |
                            ((uint32_t)(b4 & 0x3F)      );
                    } else {
                        code = TAO_BAD_CHARACTER;
                        break;
                    }
                }
            }
            dst[i] = u;
        }
    }
    if (len > 0) {
        // Truncate the destination string in case caller ignores the error.
        dst[len - 1] = 0;
    }
    tao_store_error(__func__, code);
    return TAO_ERROR;
}

tao_status tao_transcode_unicode_string_to_utf8(
    const wchar_t* src_,
    char *dst_,
    long len)
{
    if (src_ == NULL || dst_ == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    assert(sizeof(wchar_t) == 4);
    const uint32_t* src = (const uint32_t*)src_;
    assert(sizeof(char) == 1);
    uint8_t* dst = (uint8_t*)dst_;
    long i = 0; // index in destination
    long j = 0; // index in source
    tao_error_code code = TAO_BAD_SIZE; // assume too short destination if loop is exited
    while (true) {
        uint32_t u = src[j++];
        if (u < 0x00000080) {
            if (i >= len) break;
            dst[i++] = u;
            if (u == 0) {
                return TAO_OK;
            }
        } else if (u < 0x00000800) {
            if (i >= len - 1) break;
            dst[i++] = 0xC0 | ((uint8_t)(u >>  6) & 0x1F);
            dst[i++] = 0x80 | ((uint8_t)(u      ) & 0x3F);
        } else if (u < 0x00010000) {
            if (i >= len - 2) break;
            dst[i++] = 0xE0 | ((uint8_t)(u >> 12) & 0x0F);
            dst[i++] = 0x80 | ((uint8_t)(u >>  6) & 0x3F);
            dst[i++] = 0x80 | ((uint8_t)(u      ) & 0x3F);
        } else if (u < 0x00110000) {
            if (i >= len - 3) break;
            dst[i++] = 0xF0 | ((uint8_t)(u >> 18) & 0x07);
            dst[i++] = 0x80 | ((uint8_t)(u >> 12) & 0x3F);
            dst[i++] = 0x80 | ((uint8_t)(u >>  6) & 0x3F);
            dst[i++] = 0x80 | ((uint8_t)(u      ) & 0x3F);
        } else {
            // Invalid unicode.
            code = TAO_BAD_CHARACTER;
            break;
        }
    }
    if (len > 0) {
        // Truncate the destination string in case caller ignores the error.
        dst[len - 1] = 0;
    }
    tao_store_error(__func__, code);
    return TAO_ERROR;
}
