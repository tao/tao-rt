// tao-docs.h -
//
// Dummy header file used to define top-level Doxygen modules in the
// documentation of the TAO C Library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2022, Éric Thiébaut.

/**
 * @defgroup Utilities  Utility functions
 */

/**
 * @defgroup Arrays  Multi-dimensional arrays
 */

/**
 * @defgroup ParallelProgramming  Parallel programming
 *
 * @brief Tools for parallel (multi-thread and/or multi-process) programming.
 *
 * This module provides:
 *
 * - means to control access to shared resources: exclusive locks, conditions
 *   variables, read/write locks, and semaphores that may be shared by
 *   processes;
 *
 * - shared memory;
 *
 * - thread-pools;
 *
 * - shared objects whose contents are stored in shared memory.
 *
 * Many of the most basic functions are simple wrappers to their counterpart in
 * the POSIX Thread Library except that they take care of reporting errors like
 * other error prone functions of the TAO Library.
 */

/**
 * @defgroup Cameras  Cameras
 *
 * @brief Management of cameras.
 *
 * There are several kinds of objects to manage cameras in TAO:
 *
 * - @ref tao_camera provides a unified interface to a camera device.
 *
 * - @ref tao_remote_camera stores the current state of a camera in shared
 *   memory.
 */

/**
 * @defgroup DeformableMirrors  Deformable mirrors
 *
 * @brief Management of deformable mirrors.
 *
 */

/**
 * @defgroup WavefrontSensors  Wavefront sensors
 *
 * @brief Management of wavefront sensors.
 *
 */

/**
 * @defgroup AlpaoMirrors  Alpao deformable mirrors
 *
 * @brief Handling of Alpao deformable mirrors.
 */

/**
 * @defgroup AndorCameras  Andor cameras
 *
 * @brief Handling of Andor cameras.
 */

/**
 * @defgroup PhoenixCameras  Phoenix cameras
 *
 * @brief Handling of cameras connected via ActiveSilicon *Phoenix* frame-grabbers.
 */
