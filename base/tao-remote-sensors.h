// tao-remote-sensors.h -
//
// Definitions for remote wavefront sensors in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2022-2024, Éric Thiébaut.

#ifndef TAO_REMOTE_SENSORS_H_
#define TAO_REMOTE_SENSORS_H_ 1

#include <tao-sensors.h>
#include <tao-remote-objects.h>

TAO_BEGIN_DECLS

/**
 * @defgroup RemoteSensors  Remote wavefront sensors
 *
 * @ingroup WavefrontSensors
 *
 * @brief Client/server interface for wavefront sensors.
 *
 * A remote wavefront sensor instance is a structure stored in shared memory
 * which is used to communicate with a wavefront sensor server. The shared
 * structure contains the current settings of the wavefront sensor and an
 * history of the measures made by the wavefront sensor.
 *
 * @{
 */

/**
 * Remote wavefront sensor.
 *
 * A remote wavefront sensor is created by a server by calling
 * tao_remote_sensor_create(), clients call tao_remote_sensor_attach() to
 * connect to the remote wavefront sensor. The server and the clients call
 * tao_remote_sensor_detach() when they no longer need access to the remote
 * sensor. A remote wavefront sensor is a remote shared object with the
 * following additional components:
 *
 * - A 2-dimensional array of indices describing the layout of the sub-images
 *   which can be retrieved by calling tao_remote_sensor_get_layout().
 *
 * - An internal configuration storing, among others, the bounding boxes and
 *   the reference position of the sub-images. These settings can be retrieved
 *   by calling tao_remote_sensor_get_configuration() and which is set by
 *   tao_remote_sensor_set_configuration().
 *
 * - An internal buffer storing the requested commands which can be retrieved
 *   by calling tao_remote_sensor_get_requested_commands() and which is set by
 *   tao_remote_sensor_send_commands().
 *
 * - An internal buffer storing the actual commands which can be retrieved by
 *   calling tao_remote_sensor_get_actual_commands() and which is set by
 *   tao_remote_sensor_send_commands() after the commands have been applied to
 *   the device.
 *
 * - A cyclic list of output buffers storing an history of the measurements.
 *   Waiting for a given output buffer is done by calling
 *   tao_remote_sensor_wait_output() and retrieving the contents of an output
 *   buffer is done by calling tao_remote_sensor_fetch_data().
 *
 * Note that measurements are relative to the reference positions.
 */
typedef struct tao_remote_sensor_ tao_remote_sensor;

/**
 * Create a new instance of a remote wavefront sensor.
 *
 * This function creates the resources in shared memory to manage a remote
 * wavefront sensor and its telemetry. This function shall be called by the
 * server in charge of a wavefront sensor device. Clients shall call
 * tao_remote_sensor_attach() to connect to the remote wavefront sensor. The
 * clients and the server are responsible of eventually calling
 * tao_remote_sensor_detach() to release the resources.
 *
 * @param owner      The name of the server.
 *
 * @param nbufs      The number of cyclic data-frame buffers.
 *
 * @param max_ninds  The maximum number of nodes in of the 2-dimensional layout
 *                   grid.
 *
 * @param max_nsubs  The maximum number of sub-images.
 *
 * @param flags      Permissions for clients and options.
 *
 * @return The address of the new remote wavefront sensor instance or `NULL` in
 *         case of errors.
 */
extern tao_remote_sensor* tao_remote_sensor_create(
    const char* owner,
    long        nbufs,
    long    max_ninds,
    long    max_nsubs,
    unsigned    flags);

/**
 * @brief Attach an existing remote wavefront sensor to the address space of
 * the caller.
 *
 * This function attaches an existing remote wavefront sensor to the address
 * space of the caller. As a result, the number of attachments on the returned
 * sensor is incremented by one. When the sensor is no longer used by the
 * caller, the caller is responsible of calling tao_remote_sensor_detach() to
 * detach the sensor from its address space, decrement its number of
 * attachments by one and eventually free the shared memory associated with the
 * sensor.
 *
 * In principle, the same process may attach a remote wavefront sensor more
 * than once but each attachment, due to tao_remote_sensor_attach() or to
 * tao_remote_sensor_create(), should be matched by a
 * tao_remote_sensor_detach() with the corresponding address in the caller's
 * address space.
 *
 * @param shmid  Shared memory identifier.
 *
 * @return The address of the remote wavefront sensor in the address space of
 *         the caller; `NULL` in case of failure. Even tough the arguments are
 *         correct, an error may arise if the sensor has been destroyed before
 *         attachment completes.
 *
 * @see tao_remote_sensor_detach().
 */
extern tao_remote_sensor* tao_remote_sensor_attach(
    tao_shmid shmid);

/**
 * @brief Detach a remote wavefront sensor from the address space of the
 * caller.
 *
 * This function detaches a remote wavefront sensor from the address space of
 * the caller and decrements the number of attachments of the remote wavefront
 * sensor. If the number of attachments reaches zero, the shared memory segment
 * backing the storage of the sensor is destroyed (unless bit @ref
 * TAO_PERSISTENT was set at sensor creation).
 *
 * @warning The detached object must not be locked by the calling process. This
 * cannot be checked, it is therefore the caller responsibility to ensure that.
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 *
 * @see tao_remote_sensor_attach().
 */
extern tao_status tao_remote_sensor_detach(
    tao_remote_sensor* wfs);

/**
 * @brief Get the size of a remote wavefront sensor.
 *
 * This function yields the number of bytes of shared memory occupied by the
 * remote wavefront sensor. The size is constant for the life of the sensor, it
 * is thus not necessary to have locked the sensor to retrieve its identifier.
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller.
 *
 * @return The number of bytes of the shared memory segment backing the storage
 *         of the remote wavefront sensor, `0` if @a wfs is `NULL`. Whatever
 *         the result, this accessor function leaves the caller's last error
 *         unchanged.
 */
extern size_t tao_remote_sensor_get_size(
    const tao_remote_sensor* wfs);

/**
 * @brief Get the type identifier of a remote wavefront sensor.
 *
 * This function yields the identifier of the type of the remote wavefront
 * sensor. The type identifier is constant for the life of the sensor, it is
 * thus not necessary to have locked the sensor to retrieve its identifier.
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller.
 *
 * @return The type identifier of the remote wavefront sensor,
 *         `TAO_NULL_OBJECT` if @a wfs is `NULL`. Whatever the result, this
 *         accessor function leaves the caller's last error unchanged.
 */
extern tao_object_type tao_remote_sensor_get_type(
    const tao_remote_sensor* wfs);

/**
 * @brief Get the flags of a remote sensor.
 *
 * This function behaves as tao_shared_object_get_flags().
 */
extern uint32_t tao_remote_sensor_get_flags(
    const tao_remote_sensor* obj);

/**
 * @brief Get the access permissions of a remote sensor.
 *
 * This function behaves as tao_shared_object_get_perms().
 */
extern uint32_t tao_remote_sensor_get_perms(
    const tao_remote_sensor* obj);

/**
 * @brief Get the shared memory identifier of a remote wavefront sensor.
 *
 * This function yields the shared memory identifier of the remote wavefront
 * sensor. This value can be used by another process to attach to its address
 * space the remote wavefront sensor. The shared memory identifier is constant
 * for the life of the sensor, it is thus not necessary to have locked the
 * sensor to retrieve its identifier.
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller.
 *
 * @return The identifier of the remote wavefront sensor data, `TAO_BAD_SHMID`
 *         if @a wfs is `NULL`. Whatever the result, this accessor function
 *         leaves the caller's last error unchanged.
 *
 * @see tao_remote_sensor_attach.
 */
extern tao_shmid tao_remote_sensor_get_shmid(
    const tao_remote_sensor* wfs);

/**
 * @brief Publish shared memory identifier of an owned remote sensor.
 *
 * This function behaves as tao_remote_object_publish_shmid().
 */
extern tao_status tao_remote_sensor_publish_shmid(
    tao_remote_sensor* obj);

/**
 * @brief Un-publish shared memory identifier of an owned remote sensor.
 *
 * This function behaves as tao_remote_object_unpublish_shmid().
 */
extern tao_status tao_remote_sensor_unpublish_shmid(
    tao_remote_sensor* obj);

/**
 * Lock a remote wavefront sensor for exclusive access.
 *
 * This function locks a remote wavefront sensor for exclusive (read and write)
 * access. The sensor must be attached to the address space of the caller. In
 * case of success, the caller is responsible for calling
 * tao_unlock_shared_sensor() to eventually release the lock.
 *
 * @warning The same thread/process must not attempt to lock the same sensor
 * more than once and should unlock it as soon as possible.
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_remote_sensor_lock(
    tao_remote_sensor* wfs);

/**
 * Unlock a remote wavefront sensor.
 *
 * This function unlocks a remote wavefront sensor that has been successfully
 * locked by the caller.
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_remote_sensor_unlock(
    tao_remote_sensor* wfs);

/**
 * Attempt to immediately lock a remote wavefront sensor for exclusive access.
 *
 * This function attempts to lock a remote wavefront sensor for exclusive (read
 * and write) access without blocking. The caller is responsible for eventually
 * releasing the lock with tao_remote_sensor_unlock().
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller.
 *
 * @return @ref TAO_OK on success, @ref TAO_TIMEOUT if the lock cannot be
 *         immediately acquired, or @ref TAO_ERROR on failure.
 */
extern tao_status tao_remote_sensor_try_lock(
    tao_remote_sensor* wfs);

/**
 * Attempt to lock a remote wavefront sensor for exclusive access with an
 * absolute time limit.
 *
 * This function attempts to lock a remote wavefront sensor for exclusive (read
 * and write) access without blocking beyond a given time limit. The caller is
 * responsible for eventually releasing the lock with
 * tao_remote_sensor_unlock().
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller.
 *
 * @param lim    Absolute time limit.
 *
 * @return @ref TAO_OK if the lock has been locked by the caller before the
 *         specified time limit, @ref TAO_TIMEOUT if timeout occurred before or
 *         @ref TAO_ERROR in case of error.
 */
extern tao_status tao_remote_sensor_abstimed_lock(
    tao_remote_sensor* wfs,
    const tao_time* lim);

/**
 * Attempt to lock a remote wavefront sensor for exclusive access with a
 * relative time limit.
 *
 * This function attempts to lock a remote wavefront sensor for exclusive (read
 * and write) access without blocking more than a given duration. The caller is
 * responsible for eventually releasing the lock with
 * tao_remote_sensor_unlock().
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller.
 *
 * @param secs   Maximum time to wait (in seconds). If this amount of time is
 *               very large, e.g. more than @ref TAO_MAX_TIME_SECONDS, the
 *               effect is the same as calling tao_remote_sensor_lock(). If
 *               this amount of time is very short, the effect is the same as
 *               calling tao_remote_sensor_try_lock().
 *
 * @return @ref TAO_OK if the lock has been locked by the caller before the
 *         specified time limit, @ref TAO_TIMEOUT if timeout occurred before or
 *         @ref TAO_ERROR in case of error.
 */
extern tao_status tao_remote_sensor_timed_lock(
    tao_remote_sensor* wfs,
    double secs);

/**
 * Type of callback/predicate applicable to a remote sensor.
 *
 * Argument `obj` is the object, argument `data` is anything else needed to
 * execute the callback.
 */
typedef tao_status tao_remote_sensor_callback(tao_remote_sensor* obj, void* data);

/**
 * @brief Temporarily lock a remote sensor while executing a task.
 *
 * This function behaves as tao_remote_object_lock_do().
 */
extern tao_status tao_remote_sensor_lock_do(
    tao_remote_sensor* obj,
    tao_remote_sensor_callback* task,
    void* task_data,
    double secs);

/**
 * @brief Temporarily unlock a remote sensor while executing a task.
 *
 * This function behaves as tao_remote_object_unlock_do().
 */
extern tao_status tao_remote_sensor_unlock_do(
    tao_remote_sensor* obj,
    tao_remote_sensor_callback* task,
    void* task_data,
    double secs);

/**
 * Signal a condition variable to at most one thread waiting on a remote
 * wavefront sensor.
 *
 * This function restarts one of the threads that are waiting on the condition
 * variable of the sensor. Nothing happens, if no threads are waiting on the
 * condition variable.
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller.
 *
 * @return @ref TAO_OK if successful; @ref TAO_ERROR in case of failure.
 *
 * @see tao_remote_sensor_broadcast_condition(),
 *      tao_remote_sensor_wait_condition().
 */
extern tao_status tao_remote_sensor_signal_condition(
    tao_remote_sensor* wfs);

/**
 * Signal a condition to all threads waiting on a remote wavefront sensor.
 *
 * This function behaves like tao_remote_sensor_signal_condition() except that
 * all threads waiting on the condition variable of the sensor are restarted.
 * Nothing happens, if no threads are waiting on the condition variable.
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller.
 *
 * @return @ref TAO_OK if successful; @ref TAO_ERROR in case of failure.
 *
 * @see tao_remote_sensor_signal_condition(),
 *      tao_remote_sensor_wait_condition().
 */
extern tao_status tao_remote_sensor_broadcast_condition(
    tao_remote_sensor* wfs);

/**
 * Wait for a condition to be signaled for a remote wavefront sensor.
 *
 * This function atomically unlocks the exclusive lock associated with the
 * remote wavefront sensor and waits for its associated condition variable to
 * be signaled. The thread execution is suspended and does not consume any CPU
 * time until the condition variable is signaled. The mutex of the sensor must
 * have been locked (e.g., with tao_remote_sensor_lock()) by the calling thread
 * on entrance to this function. Before returning to the calling thread, this
 * function re-acquires the mutex.
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 *
 * @see tao_remote_sensor_lock(), tao_remote_sensor_signal_condition().
 */
extern tao_status tao_remote_sensor_wait_condition(
    tao_remote_sensor* wfs);

/**
 * Wait for a condition to be signaled for a remote wavefront sensor without
 * blocking longer than an absolute time limit.
 *
 * This function behaves like tao_remote_sensor_wait_condition() but blocks no
 * longer than a given duration.
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller.
 *
 * @param lim    Absolute time limit with the same conventions as
 *               tao_get_current_time().
 *
 * @return @ref TAO_OK if the lock has been locked by the caller before the
 *         specified time limit, @ref TAO_TIMEOUT if timeout occurred before or
 *         @ref TAO_ERROR in case of error.
 */
extern tao_status tao_remote_sensor_abstimed_wait_condition(
    tao_remote_sensor* wfs,
    const tao_time* lim);

/**
 * Wait for a condition to be signaled for a remote wavefront sensor without
 * blocking longer than a relative time limit.
 *
 * This function behaves like tao_remote_sensor_wait_condition() but blocks no
 * longer than a given duration.
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller.
 *
 * @param secs   Maximum amount of time (in seconds). If this amount of time is
 *               very large, e.g. more than @ref TAO_MAX_TIME_SECONDS, the
 *               effect is the same as calling
 *               tao_remote_sensor_wait_condition().
 *
 * @return @ref TAO_OK if the lock has been locked by the caller before the
 *         specified time limit, @ref TAO_TIMEOUT if timeout occurred before or
 *         @ref TAO_ERROR in case of error.
 */
extern tao_status tao_remote_sensor_timed_wait_condition(
    tao_remote_sensor* wfs,
    double secs);

/**
 * @brief Wait for some given conditions on a remote sensor.
 *
 * This function behaves as tao_shared_object_wait()
 */
extern tao_status tao_remote_sensor_wait(
    tao_remote_sensor* obj,
    tao_remote_sensor_callback* pred,
    void* pred_data,
    double secs);

/**
 * @brief Execute a task when some given conditions hold for a remote sensor.
 *
 * This function behaves as tao_remote_object_lock_wait_do()
 */
extern tao_status tao_remote_sensor_lock_wait_do(
    tao_remote_sensor* obj,
    tao_remote_sensor_callback* pred,
    void* pred_data,
    tao_remote_sensor_callback* task,
    void* task_data,
    double secs);

/**
 * Get the name of the owner of a remote wavefront sensor.
 *
 * This function yields the name of the owner of the remote wavefront sensor.
 * This information is immutable and the sensor needs not be locked by the
 * caller.
 *
 * @param wfs     Pointer to a remote wavefront sensor attached to the address
 *                space of the caller.
 *
 * @return The name of the remote wavefront sensor owner or an empty string
 *         `""` for a `NULL` sensor pointer. Whatever the result, this accessor
 *         function leaves the caller's last error unchanged.
 */
extern const char* tao_remote_sensor_get_owner(
    const tao_remote_sensor* wfs);

/**
 * Get the process identifier of the owner of a remote sensor.
 *
 * This function behaves as tao_remote_object_get_pid().
 */
extern pid_t tao_remote_sensor_get_pid(
    const tao_remote_sensor* obj);

/**
 * Get the number of output data-frames of a remote wavefront sensor.
 *
 * This function yields the length of the cyclic list of data-frames memorized
 * by the owner of a remote wavefront sensor. This information is immutable and
 * the sensor needs not be locked by the caller.
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller and locked by the caller.
 *
 * @return The length of the list of shared arrays memorized by the owner of
 *         the remote wavefront sensor, `0` if @a wfs is `NULL`. Whatever the
 *         result, this accessor function leaves the caller's last error
 *         unchanged.
 *
 * @see tao_remote_sensor_lock.
 */
extern long tao_remote_sensor_get_nbufs(
    const tao_remote_sensor* wfs);

/**
 * Get the serial number of the last available data-frame for a remote
 * wavefront sensor.
 *
 * This function yields the serial number of the last data-frame available from
 * a remote wavefront sensor. This is also the number of data-frames posted so
 * far by the server owning the remote wavefront sensor.
 *
 * The serial number of last data-frame may change (i.e., when acquisition is
 * running), but serial number is stored in an *atomic* variable, so the caller
 * needs not lock the remote wavefront sensor.
 *
 * @param wfs    Pointer to a remote wavefront sensor attached to the address
 *               space of the caller and locked by the caller.
 *
 * @return A nonnegative integer. A strictly positive value which is the serial
 *         number of the last available image if any, `0` if image acquisition
 *         has not yet started of if `wfs` is `NULL`. Whatever the result, this
 *         accessor function leaves the caller's last error unchanged.
 *
 * @see tao_remote_sensor_lock.
 */
extern tao_serial tao_remote_sensor_get_serial(
    const tao_remote_sensor* wfs);

/**
 * Get the number of commands processed by the server owning a remote wavefront
 * sensor.
 *
 * This function yields the the number of commands processed so far by the
 * owner of the remote wavefront sensor.
 *
 * The number of processed commands is stored in an *atomic* variable, so the
 * caller needs not lock the remote sensor.
 *
 * @param wfs     Pointer to a remote wavefront sensor attached to the address
 *                space of the caller.
 *
 * @return The number processed commands, a nonnegative integer which may be
 *         `0` if no commands have been ever processed or if `wfs` is `NULL`.
 *         Whatever the result, this accessor function leaves the caller's last
 *         error unchanged.
 */
extern tao_serial tao_remote_sensor_get_ncmds(
    const tao_remote_sensor* wfs);

/**
 * Get the current state of the server owning a remote wavefront sensor.
 *
 * This function yields the current state of the server owning the remote
 * sensor.
 *
 * The server state is stored in an *atomic* variable, so the caller needs not
 * lock the remote wavefront sensor.
 *
 * @param wfs     Pointer to a remote wavefront sensor attached to the address
 *                space of the caller.
 *
 * @return The state of the remote server, @ref TAO_STATE_UNREACHABLE if `wfs`
 *         is `NULL`. Whatever the result, this accessor function leaves the
 *         caller's last error unchanged.
 */
extern tao_state tao_remote_sensor_get_state(
    const tao_remote_sensor* wfs);

/**
 * @brief Set the state of an owned remote sensor object.
 *
 * This function behaves as tao_remote_object_set_state().
 */
extern tao_status tao_remote_sensor_set_state(
    tao_remote_sensor* obj,
    tao_state state);

/**
 * @brief Retrieve pending command of an owned remote sensor.
 *
 * This function behaves as tao_remote_object_get_command().
 */
extern tao_command tao_remote_sensor_get_command(
    tao_remote_sensor* obj);

/**
 * @brief Report execution of the pending command of an owned remote sensor
 * object.
 *
 * This function behaves as tao_remote_object_command_done().
 */
extern tao_status tao_remote_sensor_command_done(
    tao_remote_sensor* obj);

/**
 * Check whether the server owning a remote wavefront sensor is alive.
 *
 * This function uses the current state of the server owning the remote
 * wavefront sensor to determine whether the server is alive.
 *
 * The server state is stored in an *atomic* variable, so the caller needs not
 * lock the remote wavefront sensor.
 *
 * @param wfs     Pointer to a remote wavefront sensor attached to the address
 *                space of the caller.
 *
 * @return A boolean result; `false` if `wfs` is `NULL`. Whatever the result,
 *         this accessor function leaves the caller's last error unchanged.
 */
extern int tao_remote_sensor_is_alive(
    const tao_remote_sensor* wfs);

/**
 * Get the number of layout indices of a remote wavefront sensor.
 *
 * The number of layout indices is the product of the layout dimensions.
 *
 * @param wfs     Pointer to a remote wavefront sensor attached to the address
 *                space of the caller.
 *
 * @return The number of layout indices of the remote wavefront sensor; `0` if
 *         `wfs` is `NULL`. Whatever the result, this accessor function leaves
 *         the caller's last error unchanged.
 */
extern long tao_remote_sensor_get_ninds(
    const tao_remote_sensor* wfs);

/**
 * Get the maximum number of layout indices of a remote wavefront sensor.
 *
 * The number of layout indices is the product of the layout dimensions.
 *
 * @param wfs     Pointer to a remote wavefront sensor attached to the address
 *                space of the caller.
 *
 * @return The maximum number of layout indices of the camera of the remote
 *         wavefront sensor; `0` if `wfs` is `NULL`. Whatever the result, this
 *         accessor function leaves the caller's last error unchanged.
 */
extern long tao_remote_sensor_get_max_ninds(
    const tao_remote_sensor* wfs);

/**
 * Get the layout dimensions of a remote wavefront sensor.
 *
 * @param wfs     Pointer to a remote wavefront sensor attached to the address
 *                space of the caller.
 *
 * @return The base address of an array of 2 integers storing the dimensions of
 *         the layout of the remote wavefront sensor; `NULL` if `wfs` is
 *         `NULL`. Whatever the result, this accessor function leaves the
 *         caller's last error unchanged.
 */
extern const long* tao_remote_sensor_get_dims(
    const tao_remote_sensor* wfs);

/**
 * Get the layout of a remote wavefront sensor.
 *
 * @param wfs     Pointer to a remote wavefront sensor attached to the address
 *                space of the caller.
 *
 * @param dims    The base address of an array of 2 integers to store the
 *                layout dimensions. Unused if `NULL`.
 *
 * @return The base address of an array of integers storing the layout indices
 *         of the remote wavefront sensor; `NULL` if `wfs` is `NULL`. Whatever
 *         the result, this accessor function leaves the caller's last error
 *         unchanged.
 */
extern const long* tao_remote_sensor_get_inds(
    const tao_remote_sensor* wfs,
    long* dims);

/**
 * Get the number of sub-images of a remote wavefront sensor.
 *
 * @param wfs     Pointer to a remote wavefront sensor attached to the address
 *                space of the caller.
 *
 * @return The number of sub-images of the remote wavefront sensor; `0` if
 *         `wfs` is `NULL`. Whatever the result, this accessor function leaves
 *         the caller's last error unchanged.
 */
extern long tao_remote_sensor_get_nsubs(
    const tao_remote_sensor* wfs);

/**
 * Get the maximum number of sub-images of a remote wavefront sensor.
 *
 * @param wfs     Pointer to a remote wavefront sensor attached to the address
 *                space of the caller.
 *
 * @return The maximum number of sub-images of the remote wavefront sensor; `0`
 *         if `wfs` is `NULL`. Whatever the result, this accessor function
 *         leaves the caller's last error unchanged.
 */
extern long tao_remote_sensor_get_max_nsubs(
    const tao_remote_sensor* wfs);

/**
 * Get the parameters of the sub-images of a remote wavefront sensor.
 *
 * @param wfs     Pointer to a remote wavefront sensor attached to the address
 *                space of the caller.
 *
 * @param nsubs   The address of an integer to store the number of sub-images.
 *                Unused if `NULL`.
 *
 * @return The base address of an array of the sub-image parameters of the
 *         remote wavefront sensor; `NULL` if `wfs` is `NULL`. Whatever the
 *         result, this accessor function leaves the caller's last error
 *         unchanged.
 */
extern const tao_subimage* tao_remote_sensor_get_subs(
    const tao_remote_sensor* wfs, long* nsubs);

/**
 * Get the camera owner of a remote wavefront sensor.
 *
 * @param wfs     Pointer to a remote wavefront sensor attached to the address
 *                space of the caller.
 *
 * @return The name of the server owning the camera of the remote wavefront
 *         sensor; an empty string if `wfs` is `NULL` or currently has no
 *         associated remote camera. Whatever the result, this accessor
 *         function leaves the caller's last error unchanged.
 */
extern const char* tao_remote_sensor_get_camera_owner(
    const tao_remote_sensor* wfs);

/**
 * Fetch wavefront sensor data-frame.
 *
 * This function shall be called to copy the contents of a wavefront sensor
 * data-frame from shared memory as quickly as possible before it get
 * overwritten.
 *
 * Wavefront sensor data-frames are stored in a cyclic list of buffers, so
 * their contents must be retrieved before they get overwritten. This is
 * indicated by this function returning @ref TAO_TIMEOUT.
 *
 * The shared data shall not be locked by the caller.
 *
 * @param obj      Pointer to remote sensor in caller's address space.
 *
 * @param datnum   Serial number of the data-frame to fetch.  This value is
 *                 used to assert that the data-frame has not been overwritten
 *                 in the mean time.  Typically, this value has been obtained
 *                 by calling tao_remote_sensor_wait_output().
 *
 * @param data     Buffer to store sub-images data, not used if `NULL`.
 *
 * @param nvals    Number of elements to copy in the buffer `data` If `data` is
 *                 not `NULL`, `nvals` must be equal to the number of
 *                 sub-images of the wavefront sensor and the `data` buffer
 *                 must have at least that number of elements.
 *
 * @param info     Pointer to retrieve the data-frame information, not used if
 *                 `NULL`.
 *
 * @return @ref TAO_OK on success, @ref TAO_TIMEOUT if the data-frame gets
 *         overwritten before its contents is copied, or @ref TAO_ERROR in case
 *         of failure. If @ref TAO_ERROR is returned, no output buffer is
 *         modified. If @ref TAO_TIMEOUT is returned, all output buffers are
 *         zero-filled and, if `info` is not `NULL`, `info->serial` is set to 0
 *         to indicate that the requested frame is too new (true timeout) and
 *         to -1 to indicate that the requested frame is too old (its contents
 *         has been overwritten by a newer one). It is recommeneded to always
 *         specify a non-`NULL` valid address for `info` and check that
 *         `info->serial == serial` holds on return.
 */
extern tao_status tao_remote_sensor_fetch_data(
    const tao_remote_sensor* obj,
    tao_serial               datnum,
    tao_shackhartmann_data*  data,
    long                     nvals,
    tao_dataframe_info*      info);

extern tao_status tao_remote_sensor_get_control(
    tao_remote_sensor* wfs,
    tao_sensor_control* ctrl);

/**
 * Tune the run-time parameters of a remote wavefront sensor.
 *
 * This function sets the run-time parameters of a remote wavefront sensor.
 *
 * The caller must have locked the remote wavefront sensor. Once the lock is
 * released, the new parameters will be used by the server for subsequent
 * iterations. Calling this function automatically signal any changes. Only the
 * run-time parameters of this configuration can be different from those of the
 * current server configuration. So a typical usage is:
 *
 * @code {.c}
 * tao_sensor_control ctrl;
 * tao_remote_sensor_lock(wfs);
 * tao_remote_sensor_get_control(wfs, &ctrl);
 * ctrl.forgetting_factor = new_forgetting_factor_value;
 * tao_remote_sensor_set_control(wfs, &ctrl);
 * tao_remote_sensor_unlock(wfs);
 * @endcode
 *
 * @param wfs     The destination remote wavefront sensor.
 *
 * @param ctrl    The real-time parameters.  Must not be `NULL`.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_remote_sensor_set_control(
    tao_remote_sensor* wfs,
    const tao_sensor_control* ctrl);

/**
 * Get the current configuration of a remote wavefront sensor.
 *
 * This function retrieves the current settings of a remote wavefront sensor.
 *
 * @warning The caller must have locked the remote wavefront sensor for
 * exclusive access.
 *
 * @param wfs           The source remote wavefront sensor.
 *
 * @param cfg           The destination configuration.  Must not be `NULL`.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_remote_sensor_get_config(
    const tao_remote_sensor* wfs,
    tao_sensor_config* cfg);

/**
 * Change the configuration of a remote wavefront sensor.
 *
 * This function send a command to change the settings of a remote wavefront
 * sensor.  The arguments are the same as those of
 * tao_remote_sensor_check_config.
 *
 * The command is executed asynchronously.  The returned value is the serial
 * number of the command so that the caller can call
 * tao_remote_sensor_wait_command() to make sure that the command has been
 * completed.
 *
 * The caller must not have locked the remote wavefront sensor.
 *
 * @param wfs           The target remote wavefront sensor.
 *
 * @param cfg           The configuration.  Must not be `NULL`.
 *
 * @param camera_owner  The name of the server owning the wavefront sensor
 *                      camera.  Not used if `NULL`.
 *
 * @param inds          The 2-dimensional array of indices of the wavefront
 *                      sensor sub-images.  Assumed unchanged if `NULL`;
 *                      otherwise, must have `cfg->dims[0]*cfg->dims[1]`
 *                      entries.
 *
 * @param subs          The descriptors of the wavefront sensor sub-images.
 *                      Assumed unchanged if `NULL`; otherwise, must have
 *                      `cfg->nsubs` entries.
 *
 * @param secs          The maximum number of seconds to wait.
 *
 * @return The serial number of the "*start*" command, 0 if the command cannot
 *         be sent before the time limit, -1 in case of error.
 */
extern tao_serial tao_remote_sensor_configure(
    tao_remote_sensor* wfs,
    const tao_sensor_config* cfg,
    double secs);

/**
 * @brief Copy the secondary configuration of a remote wavefront sensor as the
 *        primary one.
 *
 * This function checks the settings of the secondary configuration posted by a
 * client via tao_remote_sensor_configure() and, if the secondary configuration
 * is valid, copies it as the primary configuration and signal changes to others.
 *
 * @warning The caller must be the owner of the remote object have locked the
 * remote object for exclusive access.
 *
 * @param wfs           The target remote wavefront sensor.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_remote_sensor_accept_config(
    tao_remote_sensor* wfs);

/**
 * Start processing by a remote wavefront sensor.
 *
 * A client can call this function to start processing by a remote wavefront
 * sensor.
 *
 * The command is executed asynchronously.  The returned value is the serial
 * number of the command so that the caller can call
 * tao_remote_sensor_wait_command() to make sure that the command has been
 * completed.
 *
 * The caller must not have locked the remote wavefront sensor.
 *
 * @param obj   Remote wavefront sensor instance.
 *
 * @param secs  Maximum number of seconds to wait.
 *
 * @return The serial number of the "*start*" command, 0 if the command cannot
 *         be sent before the time limit, -1 in case of error.
 */
extern tao_serial tao_remote_sensor_start(
    tao_remote_sensor* obj,
    double secs);

extern tao_serial tao_remote_sensor_abort(
    tao_remote_sensor* obj,
    double secs);

extern tao_serial tao_remote_sensor_reset(
    tao_remote_sensor* obj,
    double secs);

/**
 * Stop processing by a remote wavefront sensor.
 *
 * A client can call this function to stop processing by a remote wavefront
 * sensor.  The command is executed asynchronously.  The returned value is the
 * serial number of the command so that the caller can call
 * tao_remote_sensor_wait_command() to make sure that the command has been
 * completed.
 *
 * The caller must not have locked the remote wavefront sensor.
 *
 * @param obj   Remote wavefront sensor instance.
 *
 * @param secs  Maximum number of seconds to wait.
 *
 * @return The serial number of the "*stop*" command, 0 if the command cannot
 *         be sent before the time limit, -1 in case of error.
 */
extern tao_serial tao_remote_sensor_stop(
    tao_remote_sensor* obj,
    double secs);

/**
 * Kill a remote wavefront sensor server.
 *
 * A client can call this function to kill the server owning a remote wavefront
 * sensor.
 *
 * The command is executed asynchronously.  The returned value is the serial
 * number of the command so that the caller can call
 * tao_remote_sensor_wait_command() to make sure that the command has been
 * completed.
 *
 * The caller must not have locked the remote wavefront sensor.
 *
 * @param cam   Remote wavefront sensor instance.
 *
 * @param secs  Maximum number of seconds to wait.
 *
 * @return The serial number of the "*kill*" command, 0 if the command cannot
 *         be sent before the time limit, -1 in case of error.
 */
extern tao_serial tao_remote_sensor_kill(
    tao_remote_sensor* wfs,
    double secs);

/**
 * @brief Send a command to a remote sensor server.
 *
 * This function behaves as tao_remote_object_send_command().
 */
extern tao_serial tao_remote_sensor_send_command(
    tao_remote_sensor* obj,
    tao_command cmd,
    tao_remote_sensor_callback* task,
    void* task_data,
    double secs);

/**
 * Wait for a given command to have been processed.
 *
 * This function waits for a specific command sent to the server owning a
 * remote wavefront sensor to have been processed.
 *
 * @warning The caller must not have locked the remote wavefront sensor.
 *
 * @param wfs     Pointer to a remote wavefront sensor attached to the address
 *                space of the caller.
 *
 * @param num     The serial number of the command to wait for.
 *
 * @param secs    Maximum amount of time to wait (in seconds).
 *
 * @return @ref TAO_OK on success, @ref TAO_TIMEOUT if the command has not been
 *         processed before the time limit, and @ref TAO_ERROR on failure.
 */
extern tao_status tao_remote_sensor_wait_command(
    tao_remote_sensor* wfs,
    tao_serial         num,
    double             secs);

/**
 * Wait for a given wavefront sensor data-frame.
 *
 * This function waits for a specific data-frame to be available.  Upon
 * success, the contents of the data-frame should be copied as soon as possible
 * with tao_remote_sensor_fetch_data().
 *
 * The caller must not have locked the wavefront sensor.
 *
 * Typical usage:
 *
 * ~~~~~{.c}
 * tao_shmid shmid = tao_config_read_shmid(sensor_name);
 * tao_remote_sensor* wfs = tao_remote_sensor_attach(shmid);
 * FIXME: long nsubs = tao_remote_sensor_get_nsubs(wfs);
 * FIXME: size_t size = data->nsubs*sizeof(double);
 * FIXME: double* refs = malloc(size);
 * FIXME: double* cmds = malloc(size);
 * tao_serial serial = tao_remote_sensor_wait_output(wfs, 0, 3.2);
 * if (serial > 0) {
 *     tao_dataframe_info info;
 *     tao_status status = tao_remote_sensor_fetch_data(
 *         wfs, serial, refs, cmds, data->nsubs, &info);
 *     if (status == TAO_OK) {
 *        // Process data-frame.
 *        ....
 *     }
 * } else {
 *     // Deal with exception.
 *     ...;
 * }
 * ~~~~~
 *
 * @param wfs      Pointer to remote wavefront sensor in caller's address
 * space.
 *
 * @param serial   The serial number of the frame to wait for.  If less or
 *                 equal zero, the next frame is waited for.
 *
 * @param secs     Maximum number of seconds to wait.
 *
 * @return A strictly positive number which is the serial number of the
 *         requested frame, `0` if the requested frame is not available before
 *         the time limit (i.e. timeout), `-1` if the requested frame is too
 *         old (it has been overwritten by some newer frames or it is beyond
 *         the last available frame), `-2` if the server has been killed and
 *         the requested frame is beyond the last available one, or `-3` in
 *         case of failure.  In the latter case, error details are reflected by
 *         the caller's last error.
 */
extern tao_serial tao_remote_sensor_wait_output(
    tao_remote_sensor* wfs,
    tao_serial         serial,
    double             secs);

/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_REMOTE_SENSORS_H_
