// tao-sensors.h -
//
// Definitions for Shack-Hartmann wavefront sensors in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2022-2024, Éric Thiébaut.

#ifndef TAO_SENSORS_H_
#define TAO_SENSORS_H_ 1

#include <tao-basics.h>
#include <tao-remote-objects.h>

TAO_BEGIN_DECLS

/**
 * @defgroup WavefrontSensors  Shack-Hartmann wavefront sensors
 *
 * @brief Definitions for Shack-Hartmann wavefront sensors.
 *
 * @{
 */

/**
 * Possible wavefront sensing algorithms.
 */
typedef enum tao_algorithm_ {
    TAO_CENTER_OF_GRAVITY = 0,
    TAO_LINEARIZED_MATCHED_FILTER = 1,
} tao_algorithm;

/**
 * Bounding-box for sub-images.
 */
typedef struct tao_bounding_box_ {
    // With the following settings, checking the validity of a bounding-box is
    // easier.
    uint16_t   xoff;///< Horizontal offset.
    uint16_t   yoff;///< Vertical offset.
    uint16_t  width;///< Horizontal size.
    uint16_t height;///< Vertical size.
} tao_bounding_box;

/**
 * 2-dimensional position.
 */
typedef struct tao_position_ {
    double x;///< Abscissa.
    double y;///< Ordinate.
} tao_position;

/**
 * Measured 2-dimensional position.
 */
typedef struct tao_measured_position_ {
    // The two first members must be the same as for a `tao_position` to allow
    // for a cast.
    double   x;///< Measured abscissa.
    double   y;///< Measured ordinate.
    double wxx;///< Precision of `x`.
    double wxy;///< Co-precision of `x` and `y`;
    double wyy;///< Precision of `y`.
} tao_measured_position;

/**
 * Definition of a wavefront sensor sub-image.
 */
typedef struct tao_subimage_ {
    tao_bounding_box box;///< bounding-box of the sub-image.
    tao_position     ref;///< Reference position in the sub-image.
} tao_subimage;

/**
 * Wavefront sensor elementary data.
 *
 * This structure represents an elementary data in the output data-frames
 * delivered by a Shack-Hartmann wavefront sensor.
 */
typedef struct tao_shackhartmann_data_ {
    // NOTE: The first members must be identical to those of the structure
    // `tao_subimage`.
    tao_bounding_box      box;///< bounding-box of the sub-image.
    tao_position          ref;///< Reference position in the sub-image.

    // Members not in `tao_subimage` structure.
    tao_measured_position pos;///< Measured position (relative to the
                              ///  reference).
    double              alpha;///< Intensity factor.
    double                eta;///< Quality factor.
} tao_shackhartmann_data;

/**
 * Real-time parameters for a Shack-Hartmann wavefront sensor.
 */
typedef struct tao_sensor_control_ {
    double forgetting_factor;///< Temporal spatial regularization for the template.
    double  smoothness_level;///< Spatial regularization for the template.
    double   restoring_force;
    double     max_excursion;
} tao_sensor_control;

extern tao_status tao_sensor_control_check(
    const tao_sensor_control* ctrl);

/**
 * Shack-Hartmann wavefront sensor configuration.
 *
 * This structure represents the fixed size part of a Shack-Hartmann wavefront
 * sensor configuration. The variable size parts (the layout of the sub-images
 * grid and the list of sub-images definitions) are provided separately.
 */
typedef struct tao_sensor_params_ {
    tao_algorithm  algorithm;
    tao_sensor_control  ctrl;
    long             dims[2];///< Dimensions of sub-image grid.
    long               nsubs;///< Number of sub-images.
    char              camera[TAO_OWNER_SIZE];//< Name of camera server.
} tao_sensor_params;

/**
 * Check Shack-Hartmann wavefront sensor configuration.
 *
 * @param cfg         Fixed size part of the wavefront sensor configuration.
 *
 * @param inds        Layout of the sub-images.
 *
 * @param ninds       Number of entries in `inds`.
 *
 * @param subs        Parameters of the sub-images.
 *
 * @param nsubs       Number of entries in `subs`.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_sensor_params_check(
    const tao_sensor_params* cfg,
    const long* inds,
    long ninds,
    const tao_subimage* subs,
    long nsubs);

/**
 * Tune the run-time parameters of a Shack-Hartmann wavefront sensor.
 *
 * This function tunes the run-time parameters of a Shack-Hartmann wavefront
 * sensor.
 *
 * Only the run-time parameters of the source configuration can be different
 * from those of the destination configuration.
 *
 * @param dst     The destination configuration.
 *
 * @param src     The source configuration.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_sensor_params_tune(
    tao_sensor_params* dst,
    const tao_sensor_params* src);

/**
 * Structure describing the complete configuration of a wavefront sensor.
 */
typedef struct tao_sensor_config_ {
    const size_t size;///< Allocated size of this structure.
    const size_t subs_offset;///< Offset to the sub-image definitions.
    // This structure has a fixed size part followed by two variable size
    // parts: the array `inds` storing the layout of the sub-images grid and
    // the array `subs` of sub-images definitions. Since the structure is meant
    // to be stored in shared memory, offsets relative to the address of the
    // structure (and not absolute addresses in memory) of the variable size
    // parts may be in the structure. If the first variable size part
    // immediately (with some padding for correct alignment) follows the fixed
    // size part, it can also be the last member of the structure.
    //
    // Having the offsets relative to the base structure make it possible to
    // copy the structure without worries.
    tao_sensor_params params;

    // Last member, actual size large enough for `max_ninds` elements.
    long inds[];///< Layout indices (as a flexible array member).
} tao_sensor_config;

/**
 * @brief Initialize a remote sensor configuration structure.
 *
 * This function fills the given configuration with zeros and set its
 * `size` member. This function must not be applied to a configuration
 * created by tao_sensor_config_create().
 *
 * @param cfg    Address of configuration.
 *
 * @param size   Number of bytes allocated for the configuration.
 *
 * @return @ref TAO_OK if successful; @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_sensor_config_initialize(
    tao_sensor_config* cfg,
    size_t size);

/**
 * @brief Allocate and initialize a remote sensor configuration structure.
 *
 * This function allocates and initialize a configuration structure of the
 * given size. The caller is responsible of calling tao_sensor_config_destroy()
 * to release the memory.
 *
 * @param size   Number of bytes to allocate for the configuration.
 *
 * @return The address of the allocated structure, `NULL` in case of failure.
 */
extern tao_sensor_config* tao_sensor_config_create(
    size_t size);

/**
 * @brief Destroy an allocated remote sensor configuration structure.
 *
 * This function releases the configuration allocated by
 * tao_sensor_config_create().
 *
 * @param cfg    Address of configuration.
 */
extern void tao_sensor_config_destroy(
    tao_sensor_config* cfg);

/**
 * @brief Check a remote sensor configuration.
 *
 * This function checks the validity of the given configuration.
 *
 * @param cfg    Address of configuration.
 *
 * @return @ref TAO_OK if successful; @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_sensor_config_check(
    const tao_sensor_config* cfg);

/**
 * @brief Copy a remote sensor configuration.
 *
 * This function checks the source configuration and copies it to the
 * destination configuration if it is valid and if it can fit in the
 * destination.
 *
 * @param dst    Destination.
 *
 * @param src    Source.
 *
 * @return @ref TAO_OK if successful; @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_sensor_config_copy(
    tao_sensor_config* dst,
    const tao_sensor_config* src);

/**
 * @brief Check and instanciate a remote sensor configuration structure.
 */
extern tao_status tao_sensor_config_instanciate(
    tao_sensor_config* config,
    const tao_sensor_params* params,
    const long* inds,
    long ninds,
    const tao_subimage* subs,
    long nsubs);

// Simple accessor, does not leave an error message.
extern tao_sensor_params* tao_sensor_config_get_base(
    tao_sensor_config* cfg);

// Simple accessor, output is always valid.
extern char* tao_sensor_config_get_camera(
    tao_sensor_config* cfg);

// Simple accessor, output is always valid.
extern long* tao_sensor_config_get_inds(
    tao_sensor_config* cfg,
    long* ninds);

// Simple accessor, output is always valid.
extern tao_subimage* tao_sensor_config_get_subs(
    tao_sensor_config* cfg,
    long* nsubs);

/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_SENSORS_H_
