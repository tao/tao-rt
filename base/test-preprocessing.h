// test-preprocessing.h -
//
// Manage to defines all variants of image pre-processing methods.
//
//---------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2020-2021, Éric Thiébaut.

#ifndef TEST_PREPROCESSING_H_
// First time this file is include'd.
#define TEST_PREPROCESSING_H_ 1
#define PREPROC_SCOPE static
#endif

#if TEST_PREPROCESSING_H_ == 1

// Enter second stage.
#undef TEST_PREPROCESSING_H_
#define TEST_PREPROCESSING_H_ 2

#define PREPROC_VARIANT 11
#include __FILE__
#define PREPROC_VARIANT 12
#include __FILE__
#define PREPROC_VARIANT 13
#include __FILE__
#define PREPROC_VARIANT 14
#include __FILE__

#define PREPROC_VARIANT 21
#include __FILE__
#define PREPROC_VARIANT 22
#include __FILE__
#define PREPROC_VARIANT 23
#include __FILE__
#define PREPROC_VARIANT 24
#include __FILE__

#define PREPROC_VARIANT 31
#include __FILE__
#define PREPROC_VARIANT 32
#include __FILE__
#define PREPROC_VARIANT 33
#include __FILE__
#define PREPROC_VARIANT 34
#include __FILE__

#define PREPROC_VARIANT 41
#include __FILE__
#define PREPROC_VARIANT 42
#include __FILE__
#define PREPROC_VARIANT 43
#include __FILE__
#define PREPROC_VARIANT 44
#include __FILE__

#define PREPROC_VARIANT 51
#include __FILE__
#define PREPROC_VARIANT 52
#include __FILE__
#define PREPROC_VARIANT 53
#include __FILE__
#define PREPROC_VARIANT 54
#include __FILE__

#define PREPROC_VARIANT 61
#include __FILE__
#define PREPROC_VARIANT 62
#include __FILE__
#define PREPROC_VARIANT 63
#include __FILE__
#define PREPROC_VARIANT 64
#include __FILE__

#define PREPROC_VARIANT 71
#include __FILE__
#define PREPROC_VARIANT 72
#include __FILE__
#define PREPROC_VARIANT 73
#include __FILE__
#define PREPROC_VARIANT 74
#include __FILE__

// Exit second stage.
#undef TEST_PREPROCESSING_H_
#define TEST_PREPROCESSING_H_ 1

// Undefine macros to make redefinition easier (and mandatory).
#undef TEST_PREPROC_FLOAT
#undef TEST_PREPROC_PIXEL
#undef TEST_PREPROC_FUNC

#else // TEST_PREPROCESSING_H_ defined

#define PREPROC_FUNC    TEST_PREPROC_FUNC
#define PREPROC_FLOAT   TEST_PREPROC_FLOAT
#define PREPROC_PIXEL   TEST_PREPROC_PIXEL
#include "tao-test-preprocessing.h"

#endif // TEST_PREPROCESSING_H_
