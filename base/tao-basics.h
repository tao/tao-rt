// tao-basics.h -
//
// Basic definitions for TAO library that are needed by other headers.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2018-2024, Éric Thiébaut.

#ifndef TAO_BASICS_H_
#define TAO_BASICS_H_ 1

#include <stdbool.h> // to use booleans
#include <stddef.h>  // NULL, offsetof, ptrdiff_t, wchar_t, size_t
#include <stdlib.h>  // malloc, free, ...
#include <stdint.h>  // for integer types like uint32_t

#ifdef __cplusplus
#  define TAO_BEGIN_DECLS extern "C" {
#  define TAO_END_DECLS }
#else
#  define TAO_BEGIN_DECLS
#  define TAO_END_DECLS
#endif

TAO_BEGIN_DECLS

/**
 * @defgroup LibraryVersion  Library version
 *
 * @ingroup Utilities
 *
 * @brief Library version number.
 *
 * @{
 */

/**
 * @def TAO_VERSION_MAJOR
 *
 * Major version number of the API implemented by TAO library.
 */
#define TAO_VERSION_MAJOR 0

/**
 * @def TAO_VERSION_MINOR
 *
 * Minor version number of the API implemented by TAO library.
 */
#define TAO_VERSION_MINOR 11

/**
 * @def TAO_VERSION_PATCH
 *
 * Patch level of TAO library.
 */
#define TAO_VERSION_PATCH 0

/**
 * Retrieve version number of TAO library.
 *
 * @param major   Address to store major version number, can be `NULL`.
 * @param minor   Address to store minor version number, can be `NULL`.
 * @param patch   Address to store patch version number, can be `NULL`.
 */
extern void tao_version_get(int* major, int* minor, int* patch);

/**
 * @}
 */

/**
 * @defgroup Basics   Basic definitions
 *
 * @ingroup Utilities
 *
 * @brief Basic header definitions.
 *
 * Header file @ref tao-basics.h provides definitions of basic types and macros
 * widely used in TAO library headers.
 *
 * @{
 */

/**
 * Status returned by most TAO Library functions.
 *
 * The values of @ref TAO_OK (0) and @ref TAO_ERROR (-1) reflect the
 * conventions of most functions in the standard C library (not the POSIX
 * Threads Library whose functions return 0 or the error code).  The other
 * possible value is @ref TAO_TIMEOUT (1) which may be returned by functions
 * that have a time limit.  Unless explicitly specified, a function returning
 * @ref TAO_ERROR has updated the caller's last error.
 */
typedef enum tao_status_ {
    TAO_ERROR   = -1, ///< An error occurred in called function.
    TAO_OK      =  0, ///< Called function completed successfully.
    TAO_TIMEOUT =  1  ///< Time-out occurred in called function.
} tao_status;

/**
 * Serial number.
 *
 * This integer type is used for serial numbers and counters.  It is a signed
 * 64-bit integer.  Having a signed integer is to simplify coding.  A 64-bit
 * signed integer is sufficient to run at 1kHz during more than 292Myr.
 */
typedef int64_t tao_serial;

/**
 * @def TAO_FORMAT_PRINTF(i,j)
 *
 * Mark a printf-like function with format given by i-th argument and arguments
 * to print starting at j-th argument.
 */
#if (defined(__GNUC__) && (__GNUC__ > 2)) || defined(__clang__)
#  define TAO_FORMAT_PRINTF(i,j) __attribute__((__format__(__printf__, i, j)))
#else
#  define TAO_FORMAT_PRINTF(i,j) // nothing
#endif

/**
 * @def TAO_FORMAT_SCANF(i,j)
 *
 * Mark a scanf-like function with format given by i-th argument and arguments
 * to print starting at j-th argument.
 */
#if (defined(__GNUC__) && (__GNUC__ > 2)) || defined(__clang__)
#  define TAO_FORMAT_SCANF(i,j) __attribute__((__format__(__scanf__, i, j)))
#else
#  define TAO_FORMAT_SCANF(i,j) // nothing
#endif

/**
 * @def TAO_NORETURN
 *
 * Mark a function that never returns.
 */
#if (defined(__GNUC__) && (__GNUC__ > 2)) || defined(__clang__)
#  define TAO_NORETURN __attribute__((noreturn))
#elif defined(_MSC_VER) && (_MSC_VER >= 1310)
#  define TAO_NORETURN _declspec(noreturn)
#else
#  define TAO_NORETURN // nothing
#endif

/**
 * @}
 */

/**
 * @defgroup ParallelProgramming  Parallel Programming
 *
 * @brief Macros, types, and methods for parallel programming.
 *
 * @{
 */

/**
 * Indicator of a process-private or process-shared resource.
 *
 * The constant `TAO_PROCESS_PRIVATE` indicates that a given resource (lock,
 * condition variable, or semaphore) shall be initialized so as to be private
 * to the threads of a given process.
 *
 * The constant `TAO_PROCESS_SHARED` indicates that a given resource (lock,
 * condition variable, or semaphore) shall be initialized so as to be sharable
 * by other process.  Note that a process-shared lock or condition variable
 * must be stored in shared memory (@see SharedMemory).
 */
typedef enum tao_process_sharing_ {
    TAO_PROCESS_PRIVATE,
    TAO_PROCESS_SHARED
} tao_process_sharing;

/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_BASICS_H_
