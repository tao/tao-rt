// remote-mirrors.c -
//
// Management of remote deformable mirrors.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2019-2024, Éric Thiébaut.

#include <assert.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdatomic.h>

#include "tao-basics.h"
#include "tao-config.h"
#include "tao-errors.h"
#include "tao-generic.h"
#include "tao-macros.h"
#include "tao-remote-mirrors-private.h"

// The default is to take the timestamp right after sending the commands to the
// mirror.
#ifndef TAKE_TIMESTAMP_BEFORE_SEND
#  define TAKE_TIMESTAMP_BEFORE_SEND 0
#endif

// Yields number of bytes rounded-up to `TAO_ALIGNMENT`.
static inline size_t aligned(size_t size)
{
    return TAO_ROUND_UP(size, TAO_ALIGNMENT);
}

//-----------------------------------------------------------------------------
// REMOTE DEFORMABLE MIRROR

// Yields n-th output buffer, serial number n must be > 0.
static inline void* remote_object_get_buffer(
    const tao_remote_object* obj,
    tao_serial n)
{
    size_t offset = obj->offset + ((n - 1)%obj->nbufs)*obj->stride;
    return TAO_COMPUTED_ADDRESS(obj, offset);
}

// Yields the DM interbal work-space to store actuators values:
// - which = 0 for the reference,
// - which = 1 for perturbations,
// - which = 2 for the requested commands,
// - which = 3 for the actual commands.
 static inline double* get_values(
    const tao_remote_mirror* dm,
    long which)
{
    return (double*)TAO_COMPUTED_ADDRESS(dm, dm->vals_offset) + which*dm->nacts;
}

static inline tao_dataframe_header* fetch_frame(
    const tao_remote_mirror* obj,
    tao_serial               serial,
    double**                 data)
{
    tao_dataframe_header* header = remote_object_get_buffer(
        tao_remote_object_cast(obj), serial);
    *data = TAO_COMPUTED_ADDRESS(
        header, TAO_ROUND_UP(sizeof(*header), sizeof(double)));
    return header;
}

tao_remote_mirror* tao_remote_mirror_create(
    const char* owner,
    long        nbufs,
    const long* inds,
    long        dim1,
    long        dim2,
    double      cmin,
    double      cmax,
    unsigned    flags)
{
    // Check arguments.
    if (inds == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    if (nbufs < 2) {
        tao_store_error(__func__, TAO_BAD_BUFFERS);
        return NULL;
    }
    if (dim1 < 1 || dim2 < 1) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return NULL;
    }
    long ninds = dim1*dim2;
    long nacts = 0;
    for (long i = 0; i < ninds; ++i) {
        if (inds[i] >= 0) {
            ++nacts;
        }
    }
    if (nacts < 1) {
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return NULL;
    }
    for (long i = 0; i < ninds; ++i) {
        if (inds[i] >= nacts) {
            tao_store_error(__func__, TAO_OUT_OF_RANGE);
            return NULL;
        }
    }
    if (!isfinite(cmin) || !isfinite(cmax) || cmin >= cmax) {
        tao_store_error(__func__, TAO_BAD_RANGE);
        return NULL;
    }

    // Compute sizes and offsets of members.
    size_t inds_size = ninds*sizeof(long);
    size_t cmds_size = nacts*sizeof(double);
    size_t inds_offset = offsetof(tao_remote_mirror, inds);
    size_t vals_offset = TAO_ROUND_UP(inds_offset + inds_size, sizeof(double));

    // Compute sizes and offsets of buffers.  The stride is the (rounded) size
    // of a data-frame buffer (header + refs + cmds).
    size_t offset = aligned(vals_offset + 4*cmds_size);
    size_t stride = aligned(
        TAO_ROUND_UP(sizeof(tao_dataframe_header), sizeof(double))
        + 4*cmds_size);

    // Total size of instance.
    size_t size = offset + nbufs*stride;

    // Allocate remote shared object.
    tao_remote_mirror* obj = (tao_remote_mirror*)tao_remote_object_create(
        owner, TAO_REMOTE_MIRROR, nbufs, offset, stride, size, flags);
    if (obj == NULL) {
        return NULL;
    }

    // Instantiate object.
    tao_forced_store(&obj->nacts,       nacts);
    tao_forced_store(&obj->dims[0],     dim1);
    tao_forced_store(&obj->dims[1],     dim2);
    tao_forced_store(&obj->vals_offset, vals_offset);
    tao_forced_store(&obj->cmin,        cmin);
    tao_forced_store(&obj->cmax,        cmax);
    long* dest_inds = (long*)obj->inds;
    for (long i = 0; i < ninds; ++i) {
        if (inds[i] >= 0) {
            dest_inds[i] = inds[i];
        } else {
            dest_inds[i] = -1;
        }
    }
    // Initially, the reference is equal to the median value of the range.
    double cavg = (cmin + cmax)/2;
    double* refs = get_values(obj, 0);
    for (long i = 0; i < nacts; ++i) {
        refs[i] = cavg;
    }
    return obj;
}

tao_serial tao_remote_mirror_get_mark(
    const tao_remote_mirror* obj)
{
    return (obj == NULL) ? 0 : obj->mark;
}

// Constant, no needs to lock object.
long tao_remote_mirror_get_nacts(
    const tao_remote_mirror* obj)
{
    return (obj == NULL) ? 0 : obj->nacts;
}

// Constant, no needs to lock object.
const long* tao_remote_mirror_get_dims(
    const tao_remote_mirror* obj)
{
    return (obj == NULL) ? NULL : obj->dims;
}

// Constant, no needs to lock object.
const long* tao_remote_mirror_get_layout(
    const tao_remote_mirror* obj,
    long dims[2])
{
    if (obj == NULL) {
        if (dims != NULL) {
            dims[0] = 0;
            dims[1] = 0;
        }
        return NULL;
    } else {
        if (dims != NULL) {
            dims[0] = obj->dims[0];
            dims[1] = obj->dims[1];
        }
        return obj->inds;
    }
}

double tao_remote_mirror_get_cmin(
    const tao_remote_mirror* obj)
{
    return (obj == NULL) ? NAN : obj->cmin;
}

double tao_remote_mirror_get_cmax(
    const tao_remote_mirror* obj)
{
    return (obj == NULL) ? NAN : obj->cmax;
}

double* tao_remote_mirror_get_reference(
    const tao_remote_mirror* obj)
{
    return (obj == NULL) ? NULL : get_values(obj, 0);
}

// Arguments for task to execute on configuration command by a client.
typedef struct {
    tao_serial* datnum;
    double* dest;
    const double* vals;
    long nvals;
} configure_args;

// Task to execute on configuration command by a client.
static tao_status configure_task(
    tao_remote_mirror* obj,
    void* data)
{
    configure_args* args = data;
    if (args->datnum != NULL) {
        // The effects of this command will be effective in the next
        // data-frame.
        *args->datnum = tao_max(tao_remote_object_cast(obj)->serial + 1, 1);
    }
    // Copy the values in the shared resources.
    memcpy(args->dest, args->vals, args->nvals*sizeof(double));
    // There is nothing else to be done by the server, so we increment our-self
    // the command counter and clear the pending command to not bother the
    // server and let anyone send a set of actuators commands.
    tao_remote_object_cast(obj)->ncmds += 1;
    tao_remote_object_cast(obj)->command = TAO_COMMAND_NONE;
    return TAO_OK;
}

// Generic method to change the reference or the pertubation for a remote
// deformable mirror.
static tao_serial change_actuators_offsets(
    const char* func,
    tao_remote_mirror* obj,
    const double* vals,
    long nvals,
    double secs,
    tao_serial* datnum,
    long which)
{
    // Check arguments.
    if (obj == NULL || vals == NULL) {
        tao_store_error(func, TAO_BAD_ADDRESS);
        return -1;
    }
    if (nvals != obj->nacts) {
        tao_store_error(func, TAO_BAD_SIZE);
        return -1;
    }

    // Execute the command.
    configure_args args = {
        .datnum = datnum,
        .dest = get_values(obj, which),
        .vals = vals,
        .nvals = nvals
    };
    return tao_remote_mirror_send_command(
        obj, TAO_COMMAND_CONFIG, configure_task, &args, secs);
}

tao_serial tao_remote_mirror_set_reference(
    tao_remote_mirror* obj,
    const double* vals,
    long nvals,
    double secs,
    tao_serial* datnum)
{
    return change_actuators_offsets(
        __func__, obj, vals, nvals, secs, datnum, 0);
}

tao_serial tao_remote_mirror_set_perturbation(
    tao_remote_mirror* obj,
    const double* vals,
    long nvals,
    double secs,
    tao_serial* datnum)
{
    return change_actuators_offsets(
        __func__, obj, vals, nvals, secs, datnum, 1);
}

// Arguments for task to execute on sending actuators commands by a client.
typedef struct {
    tao_serial mark;
    tao_serial* datnum;
    void* dest;
    const void* vals;
    size_t nbytes;
} send_args;

// Task to execute on sending actuators commands by a client.
static tao_status send_task(
    tao_remote_mirror* obj,
    void* data)
{
    send_args* args = data;
    if (args->datnum != NULL) {
        // The effects of this command will be effective in the next
        // data-frame.
        *args->datnum = tao_max(tao_remote_object_cast(obj)->serial + 1, 1);
    }
    // Copy/set actuators values.
    if (args->vals == NULL) {
        memset(args->dest, 0, args->nbytes);
    } else {
        memcpy(args->dest, args->vals, args->nbytes);
    }
    obj->mark = args->mark;
    return TAO_OK;
}

static tao_serial send_commands(
    tao_remote_mirror* obj,
    const double* vals,
    tao_serial mark,
    double secs,
    tao_serial* datnum)
{
    size_t nvals = obj->nacts;
    send_args args = {
        .mark   = mark,
        .datnum = datnum,
        .dest   = get_values(obj, 2),
        .vals   = vals,
        .nbytes = nvals*sizeof(double)
    };
    return tao_remote_mirror_send_command(
        obj, vals == NULL ? TAO_COMMAND_RESET : TAO_COMMAND_SEND,
        send_task, &args, secs);
}

tao_serial tao_remote_mirror_send_commands(
    tao_remote_mirror* obj,
    const double* vals,
    long nvals,
    tao_serial mark,
    double secs,
    tao_serial* datnum)
{
    // Check arguments.
    if (obj == NULL || vals == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return -1;
    }
    if (nvals != obj->nacts) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return -1;
    }

    // Execute the command.
    return send_commands(obj, vals, mark, secs, datnum);
}

tao_serial tao_remote_mirror_reset(
    tao_remote_mirror* obj,
    tao_serial mark,
    double secs,
    tao_serial* datnum)
{
    // Check arguments.
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return -1;
    }

    // Execute the command.
    return send_commands(obj, NULL, mark, secs, datnum);
}

// FIXME: check running?
tao_status tao_remote_mirror_fetch_data(
    const tao_remote_mirror* obj,
    tao_serial               datnum,
    double*                  refcmds,
    double*                  perturb,
    double*                  reqcmds,
    double*                  devcmds,
    long                     nvals,
    tao_dataframe_info*      info)
{
    // Check arguments, then copy and check that data did not get overwritten
    // in the mean time.
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (obj->nacts < 1 || obj->base.nbufs < 2) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    if ((refcmds != NULL || perturb != NULL ||
         reqcmds != NULL || devcmds != NULL) && nvals != obj->nacts) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return TAO_ERROR;
    }
    if (datnum < 1) {
        tao_store_error(__func__, TAO_BAD_SERIAL);
        return TAO_ERROR;
    }

    // Fetch the data.
    size_t nbytes = obj->nacts*sizeof(double);
    double* data;
    const tao_dataframe_header* header = fetch_frame(obj, datnum, &data);
    tao_serial frame_serial = atomic_load(&header->serial);
    if (frame_serial == datnum) {
        // Copy the contents of the data-frame now.
        if (info != NULL) {
            info->serial = frame_serial;
            info->mark   = header->mark;
            info->time   = header->time;
        }
        if (refcmds != NULL) {
            memcpy(refcmds, data, nbytes);
        }
        if (perturb != NULL) {
            memcpy(perturb, data + obj->nacts, nbytes);
        }
        if (reqcmds != NULL) {
            memcpy(reqcmds, data + 2*obj->nacts, nbytes);
        }
        if (devcmds != NULL) {
            memcpy(devcmds, data + 3*obj->nacts, nbytes);
        }
        // Check that data-frame has not been overwritten in the mean time.
        frame_serial = atomic_load(&header->serial);
        if (frame_serial == datnum) {
            return TAO_OK;
        }
        frame_serial = -1; // data has been overwritten
    } else {
        frame_serial = (frame_serial < datnum ? 0 : -1);
    }

    // The requested frame is either too new (timeout) or too old (overwrite).
    if (info != NULL) {
        info->serial = frame_serial;
        info->mark   = 0;
        info->time   = TAO_UNKNOWN_TIME;
    }
    if (refcmds != NULL) {
        memset(refcmds, 0, nbytes);
    }
    if (perturb != NULL) {
        memset(perturb, 0, nbytes);
    }
    if (reqcmds != NULL) {
        memset(reqcmds, 0, nbytes);
    }
    if (devcmds != NULL) {
        memset(devcmds, 0, nbytes);
    }
    return TAO_TIMEOUT;
}

tao_serial tao_remote_mirror_find_mark(
    const tao_remote_mirror* obj,
    tao_serial               mrk)
{
    if (obj != NULL && obj->base.nbufs > 0) {
        tao_serial serial = obj->base.serial;
        tao_serial limit = serial > obj->base.nbufs ? serial - obj->base.nbufs : 0;
        while (serial > limit) {
            const tao_dataframe_header* header = remote_object_get_buffer(
                tao_remote_object_cast(obj), serial);
            if (header->mark == mrk) {
                return serial;
            }
            --serial;
        }
    }
    return 0;
}

// Process a command while the shared structure is locked.
static tao_status process_command(
    tao_remote_mirror* obj,
    tao_remote_mirror_operations* ops,
    void* ctx)
{
    const char* owner = tao_remote_mirror_get_owner(obj);
    tao_command command = obj->base.command;
    const char* command_name = tao_command_get_name(command);
    if (command == TAO_COMMAND_RESET || command == TAO_COMMAND_SEND) {
        if (ops->debug) {
            fprintf(stderr, "%s: Execute \"%s\" command\n", owner, command_name);
        }

        // Extract the values associated with the "send" or "reset" command.
        long nacts = obj->nacts;
        double* cmd_0 = get_values(obj, 0); // reference commands
        double* cmd_1 = get_values(obj, 1); // perturbation commands
        double* cmd_2 = get_values(obj, 2); // requested commands
        double* cmd_3 = get_values(obj, 3); // actual commands

        // Store in `cmd_3` the commands to send to the deformable mirror device.
        if (command == TAO_COMMAND_SEND) {
            // A "send" command amounts to sending the sum of the reference, of the
            // perturbation, and of the actuators commands to the device.
            double cmin = obj->cmin;       // min. actuator command
            double cmax = obj->cmax;       // max. actuator command
            double cavg = (cmin + cmax)/2; // assumed command for a NaN
            for (long i = 0; i < nacts; ++i) {
                cmd_3[i] = tao_safe_clamp(cmd_0[i] + cmd_1[i] + cmd_2[i], cmin, cmax, cavg);
            }
        } else /* TAO_COMMAND_RESET */ {
            // A "reset" amounts to physically send null commands.
            memset(cmd_3, 0, nacts*sizeof(double));
        }

#if TAKE_TIMESTAMP_BEFORE_SEND
        // Get the time right before sending the commands.
        tao_time time;
        if (tao_get_monotonic_time(&time) != TAO_OK) {
            time = TAO_UNKNOWN_TIME;
        }
#endif

        // Send the total commands to the DM. Exit the loop if the
        // actuators commands could not be sent.
        if (ops->on_send(obj, ctx, cmd_3) != TAO_OK) {
            if (ops->debug) {
                fprintf(stderr, "%s: Failed to send actuators command\n", owner);
            }
            return TAO_ERROR;
        }

#if !TAKE_TIMESTAMP_BEFORE_SEND
        // Get the time right after sending the commands.
        tao_time time;
        if (tao_get_monotonic_time(&time) != TAO_OK) {
            time = TAO_UNKNOWN_TIME;
        }
#endif

        // Increment counter of published frames and fetch corresponding output buffer.
        tao_serial serial = ++obj->base.serial;
        double* data;
        tao_dataframe_header* header = fetch_frame(obj, serial, &data);

        // Write data-frame header and data. First set the data-frame serial number to
        // zero to let others know that data-frame is being overwritten. Finally set the
        // data-frame serial number to its value when all contents has been updated.
        atomic_store(&header->serial, 0); // 0 indicates invalid data-frame
        header->mark = obj->mark;
        header->time = time;
        if (command == TAO_COMMAND_SEND) {
            // Directly copy the 4 arrays of values in `data`.
            memcpy(data, cmd_0, 4*nacts*sizeof(double));
        } else {
            // Zero-fill telemetry values.
            memset(data, 0, 4*nacts*sizeof(double));
        }
        atomic_store(&header->serial, serial); // ≥ 1 indicates valid data-frame

        // Clear the perturbations which are only valid for a single "send" or "reset"
        // commands (even though it is ignored by the "reset" command).
        memset(cmd_1, 0, nacts*sizeof(double));

    } else if (command == TAO_COMMAND_KILL) {
        if (ops->debug) {
            fprintf(stderr, "%s: Execute \"%s\" command\n", owner, command_name);
        }
        obj->base.state = TAO_STATE_QUITTING;
    } else {
        if (ops->debug) {
            fprintf(stderr, "%s: Unknown command received (%d)\n", owner, (int)command);
        }
    }

    // Notify others that the remote object has changed, empty the command queue, and
    // update the number of processed commands.
    if (tao_remote_mirror_broadcast_condition(obj) != TAO_OK) {
        if (ops->debug) {
            fprintf(stderr, "%s: Failed to broadcast condition\n", owner);
        }
        return TAO_ERROR;
    }
    obj->base.command = TAO_COMMAND_NONE;
    obj->base.ncmds += 1;
    return TAO_OK;
}

tao_status tao_remote_mirror_run_loop(
    tao_remote_mirror* obj,
    tao_remote_mirror_operations* ops,
    void* ctx)
{
    // Check arguments.
    if (obj == NULL || ops == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (obj->nacts < 1) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return TAO_ERROR;
    }

    // Get server name for messages.
    const char* owner = tao_remote_mirror_get_owner(obj);

    // Lock remote mirror before anyone else has the opportunity to lock it.
    if (tao_remote_mirror_lock(obj) != TAO_OK) {
        if (ops->debug) {
            fprintf(stderr, "%s: Failed to lock remote mirror instance\n", owner);
        }
        return TAO_ERROR;
    }

    // Publish the shmid's of the shared resources.
    tao_status status = tao_remote_mirror_publish_shmid(obj);
    if (ops->debug) {
        if (status != TAO_OK) {
            fprintf(stderr, "%s: Failed to publish shmid\n", owner);
        } else {
            fprintf(stderr, "%s: Remote mirror available at shmid=%d\n", owner,
                    (int)tao_remote_mirror_get_shmid(obj));
        }
    }

    // Reset the deformable mirror on entry.
    if (status == TAO_OK) {
        obj->base.command = TAO_COMMAND_RESET;
        status = process_command(obj, ops, ctx);
    }

    // Set state to indicate that initialization is completed.
    if (status == TAO_OK) {
        obj->base.state = TAO_STATE_WAITING;
    } else {
        obj->base.state = TAO_STATE_QUITTING;
    }

    // Run loop (on entry of the loop we own the lock on the remote mirror instance).
    while (obj->base.state < TAO_STATE_QUITTING) {
        if (tao_has_pending_command(obj)) {
            // Execute command.
            status = process_command(obj, ops, ctx);
        } else {
            // Wait for next command or next change.
            status = tao_remote_mirror_wait_condition(obj);
        }
        if (status != TAO_OK) {
            obj->base.state = TAO_STATE_QUITTING;
        }
    }

    // Update command queue in case there is some pending command so as to indicate to
    // others that the command has been processed even though it has no effects.
    if (obj->base.command != TAO_COMMAND_NONE) {
        obj->base.command = TAO_COMMAND_NONE;
        obj->base.ncmds += 1;
    }

    // Reset the deformable mirror on exit.
    obj->base.command = TAO_COMMAND_RESET;
    if (process_command(obj, ops, ctx) != TAO_OK) {
        status = TAO_ERROR;
    }

    // The server is no longer running and is unreachable.
    if (tao_remote_mirror_unpublish_shmid(obj) != TAO_OK) {
         status = TAO_ERROR;
    }
    if (tao_remote_mirror_set_state(obj, TAO_STATE_UNREACHABLE) != TAO_OK) {
        status = TAO_ERROR;
    }
    if (tao_remote_mirror_unlock(obj) != TAO_OK) {
        status = TAO_ERROR;
    }
    return status;
}

#define TYPE remote_mirror
#define MAGIC TAO_REMOTE_MIRROR
#define REMOTE_OBJECT 2 // sub-type of remote object type
#include "./shared-methods.c"
