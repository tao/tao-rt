// shared-methods.c --
//
// Generic code for basic methods of TAO shared objects. The generated code depends on the
// following macros:
//
// - TYPE must be defined with the C type name (without the tao_ prefix).
//
// - MAGIC can be defined with the magic type identifier to automatically generate code to
//   attach objects of this type (not for basic shared, remote, and rwlocked object
//   types).
//
// - REMOTE_OBJECT should be defined to non-zero for types derived from tao_remote_object
//   (1 for a the basic type and 2 for sub-types) and left undefined or defined to zero
//   for other types.
//
// - RWLOCKED_OBJECT should be defined to non-zero for types derived from
//   tao_rwlocked_object (1 for a the basic type and 2 for sub-types) and left undefined
//   or defined to zero for other types.
//
// - SHARED_OBJECT should be defined to non-zero for types derived from tao_shared_object
//   (1 for a the basic type and 2 for sub-types) and left undefined or defined to zero
//   for other types. If one of REMOTE_OBJECT or RWLOCKED_OBJECT is defined and non-zero,
//   SHARED_OBJECT is automatically defined to 2 because the type must be a sub-type of
//   tao_shared_object.
//
// These macros are undefined at the end of this file so taht mutiple include's are
// allowed.
//
//----------------------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2018-2024, Éric Thiébaut.

#ifndef TYPE
# error Macro "TYPE" must be defined
#endif

// Check words that must not be expanded and thus must not exist as macros.
#ifdef shared_object
# error Macro "shared_object" must not be defined
#endif
#ifdef remote_object
# error Macro "remote_object" must not be defined
#endif
#ifdef rwlocked_object
# error Macro "rwlocked_object" must not be defined
#endif
#ifdef shared_array
# error Macro "shared_array" must not be defined
#endif
#ifdef remote_camera
# error Macro "remote_camera" must not be defined
#endif
#ifdef remote_mirror
# error Macro "remote_mirror" must not be defined
#endif
#ifdef remote_sensor
# error Macro "remote_sensor" must not be defined
#endif

// Trick to avoid expansion of prefix and suffix. See
// https://stackoverflow.com/questions/41734338/"prevent-macro-expansion-within-levels-of-indirection"
#define tao_build_name_(a,b,c) a##b##c
#define tao_build_name(a_,_a,b,c_,_c) tao_build_name_(a_##_a,b,c_##_c)

#define this_object                         tao_build_name(tao,_,TYPE,,)
#define this_object_abstimed_lock           tao_build_name(tao,_,TYPE,_,abstimed_lock)
#define this_object_abstimed_rdlock         tao_build_name(tao,_,TYPE,_,abstimed_rdlock)
#define this_object_abstimed_wait_condition tao_build_name(tao,_,TYPE,_,abstimed_wait_condition)
#define this_object_abstimed_wrlock         tao_build_name(tao,_,TYPE,_,abstimed_wrlock)
#define this_object_attach                  tao_build_name(tao,_,TYPE,_,attach)
#define this_object_broadcast_condition     tao_build_name(tao,_,TYPE,_,broadcast_condition)
#define this_object_callback                tao_build_name(tao,_,TYPE,_,callback)
#define this_object_command_done            tao_build_name(tao,_,TYPE,_,command_done)
#define this_object_detach                  tao_build_name(tao,_,TYPE,_,detach)
#define this_object_get_command             tao_build_name(tao,_,TYPE,_,get_command)
#define this_object_get_flags               tao_build_name(tao,_,TYPE,_,get_flags)
#define this_object_get_nbufs               tao_build_name(tao,_,TYPE,_,get_nbufs)
#define this_object_get_ncmds               tao_build_name(tao,_,TYPE,_,get_ncmds)
#define this_object_get_owner               tao_build_name(tao,_,TYPE,_,get_owner)
#define this_object_get_perms               tao_build_name(tao,_,TYPE,_,get_perms)
#define this_object_get_pid                 tao_build_name(tao,_,TYPE,_,get_pid)
#define this_object_get_serial              tao_build_name(tao,_,TYPE,_,get_serial)
#define this_object_get_shmid               tao_build_name(tao,_,TYPE,_,get_shmid)
#define this_object_get_size                tao_build_name(tao,_,TYPE,_,get_size)
#define this_object_get_state               tao_build_name(tao,_,TYPE,_,get_state)
#define this_object_get_type                tao_build_name(tao,_,TYPE,_,get_type)
#define this_object_is_alive                tao_build_name(tao,_,TYPE,_,is_alive)
#define this_object_kill                    tao_build_name(tao,_,TYPE,_,kill)
#define this_object_lock                    tao_build_name(tao,_,TYPE,_,lock)
#define this_object_lock_do                 tao_build_name(tao,_,TYPE,_,lock_do)
#define this_object_lock_wait_do            tao_build_name(tao,_,TYPE,_,lock_wait_do)
#define this_object_publish_shmid           tao_build_name(tao,_,TYPE,_,publish_shmid)
#define this_object_rdlock                  tao_build_name(tao,_,TYPE,_,rdlock)
#define this_object_rdlock_do               tao_build_name(tao,_,TYPE,_,rdlock_do)
#define this_object_send_command            tao_build_name(tao,_,TYPE,_,send_command)
#define this_object_set_state               tao_build_name(tao,_,TYPE,_,set_state)
#define this_object_signal_condition        tao_build_name(tao,_,TYPE,_,signal_condition)
#define this_object_timed_lock              tao_build_name(tao,_,TYPE,_,timed_lock)
#define this_object_timed_rdlock            tao_build_name(tao,_,TYPE,_,timed_rdlock)
#define this_object_timed_wait_condition    tao_build_name(tao,_,TYPE,_,timed_wait_condition)
#define this_object_timed_wrlock            tao_build_name(tao,_,TYPE,_,timed_wrlock)
#define this_object_try_lock                tao_build_name(tao,_,TYPE,_,try_lock)
#define this_object_try_rdlock              tao_build_name(tao,_,TYPE,_,try_rdlock)
#define this_object_try_wrlock              tao_build_name(tao,_,TYPE,_,try_wrlock)
#define this_object_unlock                  tao_build_name(tao,_,TYPE,_,unlock)
#define this_object_unlock_do               tao_build_name(tao,_,TYPE,_,unlock_do)
#define this_object_unpublish_shmid         tao_build_name(tao,_,TYPE,_,unpublish_shmid)
#define this_object_wait                    tao_build_name(tao,_,TYPE,_,wait)
#define this_object_wait_command            tao_build_name(tao,_,TYPE,_,wait_command)
#define this_object_wait_condition          tao_build_name(tao,_,TYPE,_,wait_condition)
#define this_object_wait_output             tao_build_name(tao,_,TYPE,_,wait_output)
#define this_object_wrlock                  tao_build_name(tao,_,TYPE,_,wrlock)
#define this_object_wrlock_do               tao_build_name(tao,_,TYPE,_,wrlock_do)

#ifndef RWLOCKED_OBJECT
# define RWLOCKED_OBJECT 0
#endif
#define IS_RWLOCKED_OBJECT (RWLOCKED_OBJECT > 0)

#ifndef REMOTE_OBJECT
# define REMOTE_OBJECT 0
#endif
#define IS_REMOTE_OBJECT (REMOTE_OBJECT > 0)

#ifndef SHARED_OBJECT
# if (IS_RWLOCKED_OBJECT || IS_REMOTE_OBJECT)
#  define SHARED_OBJECT 2 // must be a sub-type
# else
#  define SHARED_OBJECT 0
# endif
#endif
#define IS_SHARED_OBJECT (SHARED_OBJECT > 0)

#if IS_RWLOCKED_OBJECT && IS_REMOTE_OBJECT
# error Macros RWLOCKED_OBJECT > 0 and REMOTE_OBJECT > 0 are exclusive
#endif

#if (IS_RWLOCKED_OBJECT || IS_REMOTE_OBJECT) && SHARED_OBJECT != 2
# error Macro SHARED_OBJECT must be undefined or set to 2 for sub-types
#endif

#ifdef MAGIC
this_object* this_object_attach(
    tao_shmid shmid)
{
    tao_shared_object* obj = tao_shared_object_attach(shmid);
    if (obj != NULL && obj->type == MAGIC) {
        return (this_object*)obj;
    }
    tao_shared_object_detach(obj);
    tao_store_error(__func__, TAO_BAD_TYPE);
    return NULL;
}
#endif // MAGIC

#if IS_RWLOCKED_OBJECT || IS_REMOTE_OBJECT
tao_status this_object_detach(
    this_object* obj)
{
    return tao_shared_object_detach(tao_shared_object_cast(obj));
}
#endif // IS_RWLOCKED_OBJECT || IS_REMOTE_OBJECT

#if IS_SHARED_OBJECT

// Constant, no needs to lock object.
size_t this_object_get_size(
    const this_object* obj)
{
    return (obj == NULL) ? 0 : tao_shared_object_cast(obj)->size;
}

// Constant, no needs to lock object.
tao_object_type this_object_get_type(
    const this_object* obj)
{
    return (obj == NULL) ? 0 : tao_shared_object_cast(obj)->type;
}

// Constant, no needs to lock object.
uint32_t this_object_get_flags(
    const this_object* obj)
{
    return (obj == NULL) ? 0 : tao_shared_object_cast(obj)->flags;
}

// Constant, no needs to lock object.
uint32_t this_object_get_perms(
    const this_object* obj)
{
    return (obj == NULL) ? 0 : (tao_shared_object_cast(obj)->flags) & 0x01ff;
}

// Constant, no needs to lock object.
tao_shmid this_object_get_shmid(
    const this_object* obj)
{
    return (obj == NULL) ? TAO_BAD_SHMID : tao_shared_object_cast(obj)->shmid;
}

#endif // IS_SHARED_OBJECT

#if IS_SHARED_OBJECT && !IS_RWLOCKED_OBJECT

// Unlock a shared object.
tao_status this_object_unlock(
    this_object* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_mutex_unlock(
        &tao_shared_object_cast(obj)->mutex);
}

// Lock a shared object.
tao_status this_object_lock(
    this_object* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_mutex_lock(
        &tao_shared_object_cast(obj)->mutex);
}

// Attempt to lock a shared object without blocking.
tao_status this_object_try_lock(
    this_object* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_mutex_try_lock(
        &tao_shared_object_cast(obj)->mutex);
}

// Attempt to lock a shared object without blocking for longer than a given number of
// seconds.
tao_status this_object_timed_lock(
    this_object* obj,
    double secs)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_mutex_timed_lock(
        &tao_shared_object_cast(obj)->mutex,
        secs);
}

// Attempt to lock a shared object without blocking for longer than an absolute time
// limit.
tao_status this_object_abstimed_lock(
    this_object* obj,
    const tao_time* abstime)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_mutex_abstimed_lock(
        &tao_shared_object_cast(obj)->mutex,
        abstime);
}

tao_status this_object_lock_do(
    this_object* obj,
    this_object_callback* task,
    void* task_data,
    double secs)
{
    tao_status status = this_object_timed_lock(obj, secs);
    if (status == TAO_OK) {
        // Object is locked.
        if (task != NULL) {
            status = task(obj, task_data);
        }
        if (this_object_unlock(obj) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status this_object_unlock_do(
    this_object* obj,
    this_object_callback* task,
    void* task_data,
    double secs)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    // If unlock succeeds, we execute the task and relock. If the task fails, we set the
    // status to TAO_ERROR because TAO_TIMEOUT is only for relock.
    tao_status status = this_object_unlock(obj);
    if (status == TAO_OK) {
        if (task != NULL) {
            switch (task(obj, task_data)) {
            case TAO_OK:
                break;
            case TAO_ERROR:
                status = TAO_ERROR;
                break;
            case TAO_TIMEOUT:
                tao_store_error(__func__, TAO_EXHAUSTED_TIME);
                status = TAO_ERROR;
                break;
            default:
                tao_store_error(__func__, TAO_BAD_STATUS);
                status = TAO_ERROR;
            }
        }
        switch (this_object_timed_lock(obj, secs)) {
        case TAO_OK:
            break;
        case TAO_ERROR:
            status = TAO_ERROR;
            break;
        default:
            // Must be a timeout which must be reported in priority because this is the
            // only possibility for the caller to realize that object has not been
            // relocked.
            status = TAO_TIMEOUT;
        }
    }
    return status;
}

// Signal changes in a shared object to one other thread or process.
tao_status this_object_signal_condition(
    this_object* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_signal(
        &tao_shared_object_cast(obj)->cond);
}

// Signal changes in a shared object to all other threads and processes.
tao_status this_object_broadcast_condition(
    this_object* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_broadcast(
        &tao_shared_object_cast(obj)->cond);
}

// Wait for signaled changes in a shared object.
tao_status this_object_wait_condition(
    this_object* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_wait(
        &tao_shared_object_cast(obj)->cond,
        &tao_shared_object_cast(obj)->mutex);
}

// Wait for signaled changes in a shared object without blocking for longer than a given
// number of seconds.
tao_status this_object_timed_wait_condition(
    this_object* obj,
    double secs)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_timed_wait(
        &tao_shared_object_cast(obj)->cond,
        &tao_shared_object_cast(obj)->mutex,
        secs);
}

// Wait for signaled changes in a shared object without blocking for longer than an
// absolute time limit.
tao_status this_object_abstimed_wait_condition(
    this_object* obj,
    const tao_time* abstime)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_abstimed_wait(
        &tao_shared_object_cast(obj)->cond,
        &tao_shared_object_cast(obj)->mutex,
        abstime);
}

tao_status this_object_wait(
    this_object* obj,
    this_object_callback* pred,
    void* pred_data,
    double secs)
{
    if (obj == NULL || pred == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_time abstime;
    bool forever;
    switch (tao_get_timeout(&abstime, secs)) {
    case TAO_TIMEOUT_PAST:
        return TAO_TIMEOUT;

    case TAO_TIMEOUT_NOW:
        return pred(obj, pred_data);

    case TAO_TIMEOUT_FUTURE:
        forever = false;
        break;

    case TAO_TIMEOUT_NEVER:
        forever = true;
        break;

    default:
        return TAO_ERROR;
    }
    while (true) {
        tao_status status = pred(obj, pred_data);
        if (status != TAO_TIMEOUT) {
            // The conditions hold or an error occurred.
            return status;
        }
        status = forever ?
            this_object_wait_condition(obj) :
            this_object_abstimed_wait_condition(obj, &abstime);
        if (status != TAO_OK) {
            // Time limit exhausted or an error occurred.
            return status;
        }
    }
}

tao_status this_object_lock_wait_do(
    this_object* obj,
    this_object_callback* pred,
    void* pred_data,
    this_object_callback* task,
    void* task_data,
    double secs)
{
    // Minimal check.
    if (obj == NULL || pred == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }

    // Convert seconds to absolute time, then lock object and wait for conditions to hold.
    tao_status status;
    tao_time abstime;
    bool locked = false;
    switch (tao_get_timeout(&abstime, secs)) {
    case TAO_TIMEOUT_NEVER:
        status = this_object_lock(obj);
        locked = (status == TAO_OK);
        while (status == TAO_OK) {
            status = pred(obj, pred_data);
            if (status != TAO_TIMEOUT) {
                // The conditions hold or an error occurred.
                break;
            }
            status = this_object_wait_condition(obj);
        }
        break;
    case TAO_TIMEOUT_FUTURE:
        status = this_object_abstimed_lock(obj, &abstime);
        locked = (status == TAO_OK);
        while (status == TAO_OK) {
            status = pred(obj, pred_data);
            if (status != TAO_TIMEOUT) {
                // The conditions hold or an error occurred.
                break;
            }
            status = this_object_abstimed_wait_condition(obj, &abstime);
        }
        break;
    case TAO_TIMEOUT_NOW:
        status = this_object_try_lock(obj);
        locked = (status == TAO_OK);
        if (status == TAO_OK) {
            status = pred(obj, pred_data);
        }
        break;
    case TAO_TIMEOUT_PAST:
        // Return a value indicating a timeout.
        status = TAO_TIMEOUT;
        break;
    default:
        // The only remaining possibility is that `tao_get_timeout` returned an error
        // (TAO_TIMEOUT_ERROR).
        status = TAO_ERROR;
        break;
    }

    // If conditions hold and a task is provided, execute the task while object is still
    // locked.
    if (status == TAO_OK && task != NULL) {
        status = task(obj, task_data);
    }

    // Unlock resources.
    if (locked && this_object_unlock(obj) != TAO_OK) {
        status = TAO_ERROR;
    }
    return status;
}

#endif // IS_SHARED_OBJECT && !IS_RWLOCKED_OBJECT

#if IS_REMOTE_OBJECT


// Arguments for task to execute on sending of a command by a client.
typedef struct {
    const char*           func; // caller name
    tao_command        command; // command to send
    tao_serial           ncmds; // command counter
    this_object_callback* task; // task to execute for preparing
    void*            task_data; // arguments for task
} command_args;

// Yield whether caller shall wait to queue a command. Object has been locked by the
// caller.
static tao_status command_wait_callback(
    this_object* obj,
    void* data)
{
    if (tao_is_reachable(obj) && tao_has_pending_command(obj)) {
        return TAO_TIMEOUT;
    } else {
        return TAO_OK;
    }
}

// Task to execute on sending of a command by a client.
static tao_status command_task_callback(
    this_object* obj,
    void* data)
{
    // Retrieve arguments from anonymous data.
    command_args* args = data;

    // Server is either unreachable or ready for a new command.
    if (! tao_is_reachable(obj)) {
        // Server has been killed.
        if (args->command == TAO_COMMAND_KILL) {
            // Pretend kill command was sent but do not increment command counter
            args->ncmds = tao_remote_object_cast(obj)->ncmds;
            return TAO_OK;
        } else {
#if TAO_ASSUME_TIMOUT_IF_SERVER_KILLED
            return TAO_TIMEOUT;
#else
            tao_store_error(args->func, TAO_NOT_RUNNING);
            return TAO_ERROR;
#endif
        }
    }
    if (tao_has_pending_command(obj)) {
        tao_store_error(args->func, TAO_ASSERTION_FAILED);
        return TAO_ERROR;
    }

    // Check that sending a command is compatible with the current state of the remote
    // object.
    if (args->command == TAO_COMMAND_CONFIG) {
        tao_state state = ((tao_remote_object*)obj)->state;
        if (state != TAO_STATE_WAITING) {
            fprintf(stderr, "Cannot configure %s while %s\n",
                    tao_shared_object_get_typename((tao_shared_object*)obj),
                    tao_state_get_name(state));
            tao_store_error(args->func, TAO_BAD_STATE);
            return TAO_ERROR;
        }
    }

    // Notify others that shared ressources have changed. We do that before actually
    // modifying the ressources to not change anything in case of errors. This is
    // consistent because others can only be notified when the lock will be released.
    if (this_object_broadcast_condition(obj) != TAO_OK) {
        return TAO_ERROR;
    }

    // Enqueue the command and increment counter for the client (the server will do that
    // himself for the remote structure). NOTE: This must be done *before* executing the
    // associated sub-task because some commands (e.g. setting offsets for a deformable
    // mirror) can be completely handled by the client which just has to clear the command
    // queue and increment the command counter.
    tao_remote_object_cast(obj)->command = args->command;
    args->ncmds = tao_remote_object_cast(obj)->ncmds + 1;

    // Run the sub-task in charge of modifying the shared ressources for the command.
    return args->task == NULL ? TAO_OK : args->task(obj, args->task_data);
}

tao_serial this_object_send_command(
    this_object* obj,
    tao_command command,
    this_object_callback* task,
    void* task_data,
    double secs)
{
    // Minimal check.
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return -1;
    }
    if (command == TAO_COMMAND_NONE) {
        tao_store_error(__func__, TAO_BAD_COMMAND);
        return -1;
    }

    // Prepare arguments, send command, and return command serial number.
    command_args args = {
        .func      = __func__,
        .command   = command,
        .ncmds     = 0,
        .task      = task,
        .task_data = task_data
    };
    tao_status status = this_object_lock_wait_do(obj,
                                                 command_wait_callback, NULL,
                                                 command_task_callback, &args,
                                                 secs);
    if (status == TAO_OK) {
        return args.ncmds;
    } else if (status == TAO_TIMEOUT) {
        return 0;
    } else {
        return -1;
    }
}

tao_serial this_object_kill(
    this_object* obj,
    double secs)
{
    return this_object_send_command(obj, TAO_COMMAND_KILL, NULL, NULL, secs);
}

// Yield whether command with given serial number has been processed (or server
// is down).
static tao_status command_ready(
    this_object* obj,
    void* data)
{
    if (tao_is_reachable(obj) &&
        tao_remote_object_cast(obj)->ncmds < *(tao_serial*)data) {
        return TAO_TIMEOUT; // conditions do not hold yet
    } else {
        return TAO_OK; // conditions do hold
    }
}

tao_status this_object_wait_command(
    this_object* obj,
    tao_serial num,
    double secs)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (num <= 0) {
        tao_store_error(__func__, TAO_BAD_SERIAL);
        return TAO_ERROR;
    }
    return this_object_lock_wait_do(obj, command_ready, &num, NULL, NULL, secs);
}

// Yield whether output with given serial number may be available (or server is
// down).
static tao_status output_ready(
    this_object* obj,
    void* data)
{
    if (tao_is_reachable(obj) &&
        tao_remote_object_cast(obj)->serial < *(tao_serial*)data) {
        return TAO_TIMEOUT; // conditions do not hold yet
    } else {
        return TAO_OK; // conditions do hold
    }
}

tao_serial this_object_wait_output(
    this_object* obj,
    tao_serial num,
    double secs)
{
    // Check argument(s) and figure out which buffer to wait for.
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        goto error;
    }
    tao_serial last = tao_remote_object_cast(obj)->serial; // NOTE: ok because atomic
    if (last < 0) {
        tao_store_error(__func__, TAO_CORRUPTED);
        goto error;
    }
    if (num <= 0) {
        // Manage to wait for next buffer.
        num = last + 1;
    }

    // Wait for the conditions to hold.
    tao_status status = num <= last ? TAO_OK :
        this_object_lock_wait_do(obj, output_ready, &num, NULL, NULL, secs);

    // If no problems occurred, the result is the serial number of the requested output
    // buffer; otherwise, it is a nonpositive number indicating the problem. The serial
    // number of the last output buffer is loaded again to maximize the odds of success of
    // the operation.
    if (status != TAO_ERROR) {
        last = tao_remote_object_cast(obj)->serial; // NOTE: ok because atomic
        if (last < 0) {
            tao_store_error(__func__, TAO_CORRUPTED);
            goto error;
        }
        if (num <= last) {
            if (num > last - tao_remote_object_cast(obj)->nbufs) {
                // The requested buffer is available in the cyclic list of buffers.
                return num;
            } else {
                // The requested buffer is too old.
                return -1;
            }
        } else if (tao_is_reachable(obj)) {
            // A timeout occurred.
            return 0;
        } else {
            // The requested buffer will never be available because the server is no
            // longer alive.
            return -2;
        }
    }

    // An error has occurred.
error:
    return -3;
}

#endif // IS_REMOTE_OBJECT

#if IS_RWLOCKED_OBJECT

#ifndef RWLOCKED_OBJECT_IS_READABLE
#define RWLOCKED_OBJECT_IS_READABLE
static inline bool rwlocked_object_is_readable(
    tao_rwlocked_object* obj)
{
    return obj->users >= 0 && obj->writers == 0;
}
#endif

#ifndef RWLOCKED_OBJECT_IS_WRITABLE
#define RWLOCKED_OBJECT_IS_WRITABLE
static inline bool rwlocked_object_is_writable(
    tao_rwlocked_object* obj)
{
    return obj->users == 0;
}
#endif

tao_status this_object_unlock(
    this_object* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_rwlocked_object* root = tao_rwlocked_object_cast(obj);
    tao_status status = tao_mutex_lock(&root->base.mutex);
    if (status == TAO_OK) {
        // Object has been locked.
        bool notify = false;
        int errcode = TAO_SUCCESS;
        if (root->users == -1) {
            // Remove the write lock and notify others.
            root->users = 0;
            notify = true;
        } else if (root->users > 0) {
            // Remove one read lock and notify others if are there no remaining readers.
            root->users -= 1;
            notify = (root->users == 0);
        } else {
            // Something wrong has been done.
            errcode = (root->users == 0 ? TAO_NOT_LOCKED : TAO_CORRUPTED);
        }
        if (notify) {
            status = tao_condition_broadcast(&root->base.cond);
        }
        if (tao_mutex_unlock(&root->base.mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
        if (errcode != TAO_SUCCESS) {
            tao_store_error(__func__, errcode);
            status = TAO_ERROR;
        }
    }
    return status;
}

// Lock r/w object for read-only access.
tao_status this_object_rdlock(
    this_object* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_rwlocked_object* root = tao_rwlocked_object_cast(obj);
    tao_status status = tao_mutex_lock(&root->base.mutex);
    if (status == TAO_OK) {
        // Object has been locked.
        while (status == TAO_OK && ! rwlocked_object_is_readable(root)) {
            status = tao_condition_wait(&root->base.cond, &root->base.mutex);
        }
        if (status == TAO_OK) {
            root->users += 1;
        }
        if (tao_mutex_unlock(&root->base.mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status this_object_try_rdlock(
    this_object* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_rwlocked_object* root = tao_rwlocked_object_cast(obj);
    tao_status status = tao_mutex_try_lock(&root->base.mutex);
    if (status == TAO_OK) {
        // Object has been locked.
        if (rwlocked_object_is_readable(root)) {
            root->users += 1;
        } else {
            status = TAO_TIMEOUT;
        }
        if (tao_mutex_unlock(&root->base.mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status this_object_abstimed_rdlock(
    this_object* obj,
    const tao_time* abstime)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_rwlocked_object* root = tao_rwlocked_object_cast(obj);
    tao_status status = tao_mutex_abstimed_lock(&root->base.mutex, abstime);
    if (status == TAO_OK) {
        // Object has been locked.
        while (status == TAO_OK && ! rwlocked_object_is_readable(root)) {
            status = tao_condition_abstimed_wait(
                &root->base.cond, &root->base.mutex, abstime);
        }
        if (status == TAO_OK) {
            root->users += 1;
        }
        if (tao_mutex_unlock(&root->base.mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status this_object_timed_rdlock(
    this_object* obj,
    double secs)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_time abstime;
    switch (tao_get_timeout(&abstime, secs)) {
    case TAO_TIMEOUT_PAST:
        return TAO_TIMEOUT;
    case TAO_TIMEOUT_NOW:
        return this_object_try_rdlock(obj);
    case TAO_TIMEOUT_FUTURE:
        return this_object_abstimed_rdlock(obj, &abstime);
    case TAO_TIMEOUT_NEVER:
        return this_object_rdlock(obj);
    default:
        return TAO_ERROR;
    }
}

// Lock r/w object for read-write access.
tao_status this_object_wrlock(
    this_object* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_rwlocked_object* root = tao_rwlocked_object_cast(obj);
    tao_status status = tao_mutex_lock(&root->base.mutex);
    if (status == TAO_OK) {
        // Object has been locked.
        root->writers += 1; // one more pending writer
        while (status == TAO_OK && ! rwlocked_object_is_writable(root)) {
            status = tao_condition_wait(&root->base.cond, &root->base.mutex);
        }
        root->writers -= 1; // one less pending writer
        if (status == TAO_OK) {
            root->users = -1;
        }
        if (tao_mutex_unlock(&root->base.mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status this_object_try_wrlock(
    this_object* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_rwlocked_object* root = tao_rwlocked_object_cast(obj);
    tao_status status = tao_mutex_try_lock(&root->base.mutex);
    if (status == TAO_OK) {
        // Object has been locked.
        if (rwlocked_object_is_writable(root)) {
            root->users = -1;
        } else {
            status = TAO_TIMEOUT;
        }
        if (tao_mutex_unlock(&root->base.mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status this_object_abstimed_wrlock(
    this_object* obj,
    const tao_time* abstime)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_rwlocked_object* root = tao_rwlocked_object_cast(obj);
    tao_status status = tao_mutex_abstimed_lock(&root->base.mutex, abstime);
    if (status == TAO_OK) {
        // Object has been locked.
        root->writers += 1; // one more pending writer
        while (status == TAO_OK && ! rwlocked_object_is_writable(root)) {
            status = tao_condition_abstimed_wait(
                &root->base.cond, &root->base.mutex, abstime);
        }
        root->writers -= 1; // one less pending writer
        if (status == TAO_OK) {
            root->users = -1;
        }
        if (tao_mutex_unlock(&root->base.mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status this_object_timed_wrlock(
    this_object* obj,
    double secs)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_time abstime;
    switch (tao_get_timeout(&abstime, secs)) {
    case TAO_TIMEOUT_PAST:
        return TAO_TIMEOUT;
    case TAO_TIMEOUT_NOW:
        return this_object_try_wrlock(obj);
    case TAO_TIMEOUT_FUTURE:
        return this_object_abstimed_wrlock(obj, &abstime);
    case TAO_TIMEOUT_NEVER:
        return this_object_wrlock(obj);
    default:
        return TAO_ERROR;
    }
}

tao_status this_object_rdlock_do(
    this_object* obj,
    this_object_callback* task,
    void* task_data,
    double secs)
{
    tao_status status = this_object_timed_rdlock(obj, secs);
    if (status == TAO_OK) {
        if (task != NULL) {
            status = task(obj, task_data);
        }
        if (this_object_unlock(obj) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status this_object_wrlock_do(
    this_object* obj,
    this_object_callback* task,
    void* task_data,
    double secs)
{
    tao_status status = this_object_timed_wrlock(obj, secs);
    if (status == TAO_OK) {
        if (task != NULL) {
            status = task(obj, task_data);
        }
        if (this_object_unlock(obj) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status this_object_unlock_do(
    this_object* obj,
    this_object_callback* task,
    void* task_data,
    bool rw,
    double secs)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    // If unlock succeeds, we execute the task and relock. If the task fails, we set the
    // status to TAO_ERROR because TAO_TIMEOUT is only for relock.
    tao_status status = this_object_unlock(obj);
    if (status == TAO_OK) {
        if (task != NULL) {
            switch (task(obj, task_data)) {
            case TAO_OK:
                break;
            case TAO_ERROR:
                status = TAO_ERROR;
                break;
            case TAO_TIMEOUT:
                tao_store_error(__func__, TAO_EXHAUSTED_TIME);
                status = TAO_ERROR;
                break;
            default:
                tao_store_error(__func__, TAO_BAD_STATUS);
                status = TAO_ERROR;
            }
        }
        switch (rw ?
                this_object_timed_wrlock(obj, secs) :
                this_object_timed_rdlock(obj, secs)) {
        case TAO_OK:
            break;
        case TAO_ERROR:
            status = TAO_ERROR;
            break;
        default:
            // Must be a timeout which must be reported in priority because this is the
            // only possibility for the caller to realize that object has not been
            // relocked.
            status = TAO_TIMEOUT;
        }
    }
    return status;
}
#endif // IS_RWLOCKED_OBJECT


#if IS_REMOTE_OBJECT

// Constant, no needs to lock object.
const char* this_object_get_owner(
    const this_object* obj)
{
    return (obj == NULL) ? "" : tao_remote_object_cast(obj)->owner;
}

// Constant, no needs to lock object.
pid_t this_object_get_pid(
    const this_object* obj)
{
    return (obj == NULL) ? -1 : tao_remote_object_cast(obj)->pid;
}

// Constant, no needs to lock object.
long this_object_get_nbufs(
    const this_object* obj)
{
    return (obj == NULL) ? 0 : tao_remote_object_cast(obj)->nbufs;
}

// Variable but atomic, object may not be locked.
tao_serial this_object_get_serial(
    const this_object* obj)
{
    return (obj == NULL) ? 0 : tao_remote_object_cast(obj)->serial;
}

// Variable but atomic, object may not be locked.
tao_serial this_object_get_ncmds(
    const this_object* obj)
{
    return (obj == NULL) ? 0 : tao_remote_object_cast(obj)->ncmds;
}

// Variable but atomic, object may not be locked.
tao_state this_object_get_state(
    const this_object* obj)
{
    return (obj == NULL) ? TAO_STATE_UNREACHABLE :
        tao_remote_object_cast(obj)->state;
}

// Object must be locked by its owner.
tao_status this_object_set_state(
    this_object* obj,
    tao_state state)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (tao_remote_object_cast(obj)->state == state) {
        // Nothing to do.
        return TAO_OK;
    }
    if (! tao_is_reachable(obj) && state != TAO_STATE_UNREACHABLE) {
        // Unreachable object cannot come back to live.
        tao_store_error(__func__, TAO_BAD_STATE);
        return TAO_ERROR;
    }
    // Modify object and notify others.
    tao_remote_object_cast(obj)->state = state;
    return this_object_broadcast_condition(obj);
}

// Variable but atomic, object may not be locked.
int this_object_is_alive(
    const this_object* obj)
{
    return obj != NULL && tao_is_reachable(obj);
}

tao_status this_object_publish_shmid(
    this_object* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_config_write_long(
        tao_remote_object_cast(obj)->owner,
        tao_shared_object_cast(obj)->shmid);
}

tao_status this_object_unpublish_shmid(
    this_object* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_config_write_long(
        tao_remote_object_cast(obj)->owner,
        TAO_BAD_SHMID);
}

tao_command this_object_get_command(
    this_object* obj)
{
    return obj == NULL ? TAO_COMMAND_NONE : tao_remote_object_cast(obj)->command;
}

tao_status this_object_command_done(
    this_object* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (tao_remote_object_cast(obj)->command == TAO_COMMAND_NONE) {
        return TAO_OK;
    }
    tao_remote_object_cast(obj)->command = TAO_COMMAND_NONE;
    tao_remote_object_cast(obj)->ncmds += 1;
    return this_object_broadcast_condition(obj);
}

#endif // IS_REMOTE_OBJECT

#undef TYPE
#undef MAGIC
#undef IS_RWLOCKED_OBJECT
#undef IS_REMOTE_OBJECT
#undef IS_SHARED_OBJECT
