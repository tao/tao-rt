// remote-sensors.c -
//
// Management of remote wavefront sensors.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2019-2024, Éric Thiébaut.

#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdatomic.h>

#include "tao-basics.h"
#include "tao-config.h"
#include "tao-errors.h"
#include "tao-generic.h"
#include "tao-remote-sensors-private.h"

// Alignment for parts which do not need to be align on cache line size.
#define ALIGN_SIZE 8

static inline size_t round_size(
    size_t size,
    size_t align)
{
    // FIXME: Can be (size + align - 1) & (-align) when align is a power of 2
    //        (which it should be).
    return TAO_ROUND_UP(size, align);
}

// Encode two versions of the same accessor depending whether input can only be read or
// can be read and written.
#define ENCODE(out, func, inp, getter)                          \
    static inline out* func##_rw(inp* arg_1) {                  \
        return getter(arg_1);                                   \
    }                                                           \
    static inline const out* func##_ro(const inp* arg_1) {      \
        return getter(arg_1);                                   \
    }

// Get primary configuration.
#define GETTER(wfs) TAO_COMPUTED_ADDRESS((wfs),(wfs)->config1_offset)
ENCODE(tao_sensor_config, primary_config, tao_remote_sensor, GETTER);
#undef GETTER
#define primary_config(wfs) _Generic(                           \
        (wfs),                                                  \
        tao_remote_sensor      *: primary_config_rw,            \
        tao_remote_sensor const*: primary_config_ro)(wfs)

// Get secondary configuration.
#define GETTER(wfs) TAO_COMPUTED_ADDRESS((wfs),(wfs)->config2_offset)
ENCODE(tao_sensor_config, secondary_config, tao_remote_sensor, GETTER);
#undef GETTER
#define secondary_config(wfs) _Generic(                         \
        (wfs),                                                  \
        tao_remote_sensor      *: secondary_config_rw,          \
        tao_remote_sensor const*: secondary_config_ro)(wfs)

// Get layout.
#define GETTER(cfg) (cfg)->inds
ENCODE(long, config_get_inds, tao_sensor_config, GETTER);
#undef GETTER
#define get_inds(cfg) _Generic(                                 \
        (cfg),                                                  \
        tao_sensor_config      *: config_get_inds_rw,           \
        tao_sensor_config const*: config_get_inds_ro)(cfg)

// Get sub-images.
#define GETTER(cfg) TAO_COMPUTED_ADDRESS((cfg), (cfg)->subs_offset)
ENCODE(tao_subimage, config_get_subs, tao_sensor_config, GETTER);
#undef GETTER
#define get_subs(cfg) _Generic(                                 \
        (cfg),                                                  \
        tao_sensor_config      *: config_get_subs_rw,           \
        tao_sensor_config const*: config_get_subs_ro)(cfg)

#undef ENCODE //---------------------------------------------------------------

// Get number of indices in layout.
static inline long params_get_ninds(
    const tao_sensor_params* cfg)
{
    return cfg->dims[0] * cfg->dims[1];
}
static inline long config_get_ninds(
    const tao_sensor_config* cfg)
{
    return params_get_ninds(&cfg->params);
}
#define get_ninds(cfg) _Generic(                                \
        (cfg),                                                  \
        tao_sensor_params      *: params_get_ninds,             \
        tao_sensor_params const*: params_get_ninds,             \
        tao_sensor_config      *: config_get_ninds,             \
        tao_sensor_config const*: config_get_ninds)(cfg)

// Get number of sub-images.
static inline long params_get_nsubs(
    const tao_sensor_params* cfg)
{
    return cfg->nsubs;
}
static inline long config_get_nsubs(
    const tao_sensor_config* cfg)
{
    return cfg->params.nsubs;
}
#define get_nsubs(cfg) _Generic(                                \
        (cfg),                                                  \
        tao_sensor_params      *: params_get_nsubs,             \
        tao_sensor_params const*: params_get_nsubs,             \
        tao_sensor_config      *: config_get_nsubs,             \
        tao_sensor_config const*: config_get_nsubs)(cfg)

//------------------------------------------------------------------------------

// Assume aligning to ALIGN_SIZE is suitable for sub-images parameters.
#define SUBS_OFFSET(nbytes) round_size(nbytes, ALIGN_SIZE)

static size_t config_size(
    size_t* subs_offset_ptr,
    long ninds,
    long nsubs)
{
    size_t subs_offset = SUBS_OFFSET(
        sizeof(tao_sensor_config) + sizeof(double[ninds]));
    if (subs_offset_ptr != NULL) {
        *subs_offset_ptr = subs_offset;
    }
    return subs_offset + sizeof(tao_subimage[nsubs]);
}

//------------------------------------------------------------------------------
// CONFIGURATION (FIXME: should be in sensors.c)

tao_status tao_sensor_config_initialize(
    tao_sensor_config* cfg,
    size_t size)
{
    if (cfg == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    size_t subs_offset, min_size = config_size(&subs_offset, 0 ,0);
    if (size < min_size) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return TAO_ERROR;
    }
    memset(cfg, 0, size);
    tao_forced_store(&cfg->size, size);
    tao_forced_store(&cfg->subs_offset, subs_offset);
    strcpy(cfg->params.camera, "unknown");
    return TAO_OK;
}

tao_sensor_config* tao_sensor_config_create(
    size_t size)
{
    size_t subs_offset, min_size = config_size(&subs_offset, 0 ,0);
    if (size < min_size) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return NULL;
    }
    tao_sensor_config* cfg = malloc(size);
    if (cfg == NULL) {
        tao_store_system_error(__func__);
        return NULL;
    }
    tao_sensor_config_initialize(cfg, size);
    return cfg;
}

void tao_sensor_config_destroy(
    tao_sensor_config* cfg)
{
    if (cfg != NULL) {
        free(cfg);
    }
}

tao_status tao_sensor_config_check(
    const tao_sensor_config* cfg)
{
    if (cfg == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_sensor_params_check(&cfg->params,
                                   get_inds(cfg), get_ninds(cfg),
                                   get_subs(cfg), get_nsubs(cfg));
}

tao_status tao_sensor_config_copy(
    tao_sensor_config* dst,
    const tao_sensor_config* src)
{
    if (dst == NULL || src == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_sensor_config_instanciate(dst, &src->params,
                                         get_inds(src), get_ninds(src),
                                         get_subs(src), get_nsubs(src));
}

tao_status tao_sensor_config_instanciate(
    tao_sensor_config* config,
    const tao_sensor_params* params,
    const long* inds,
    long ninds,
    const tao_subimage* subs,
    long nsubs)
{
    // Check all entries.
    if (config == NULL || params == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (tao_sensor_params_check(params, inds, ninds, subs, nsubs) != TAO_OK) {
        return TAO_ERROR;
    }

    // Compute offset of sub-images array and minimal full configuration size.
    size_t subs_offset, min_size = config_size(&subs_offset, ninds, nsubs);
    if (config->size < min_size) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return TAO_ERROR;
    }

    // Everything is ok, instanciate the full configuration.
    tao_forced_store(&config->subs_offset, subs_offset);
    memcpy(&config->params, params, sizeof(*params));
    if (ninds > 0) {
        memcpy(get_inds(config), inds, sizeof(double[ninds]));
    }
    if (nsubs > 0) {
        memcpy(get_subs(config), subs, sizeof(tao_subimage[nsubs]));
    }
    return TAO_OK;
}

tao_sensor_params* tao_sensor_config_get_params(
    tao_sensor_config* cfg)
{
    return cfg == NULL ? NULL : &cfg->params;
}

char* tao_sensor_config_get_camera(
    tao_sensor_config* cfg)
{
    return cfg == NULL ? NULL : cfg->params.camera;
}

long* tao_sensor_config_get_inds(
    tao_sensor_config* cfg,
    long* ninds)
{
    if (cfg == NULL) {
        if (ninds != NULL) {
            *ninds = 0;
        }
        return NULL;
    }
    if (ninds != NULL) {
        *ninds = get_ninds(cfg);
    }
    return get_inds(cfg);
}

tao_subimage* tao_sensor_config_get_subs(
    tao_sensor_config* cfg,
    long* nsubs)
{
    if (cfg == NULL) {
        if (nsubs != NULL) {
            *nsubs = 0;
        }
        return NULL;
    }
    if (nsubs != NULL) {
        *nsubs = get_nsubs(cfg);
    }
    return get_subs(cfg);
}

//------------------------------------------------------------------------------

extern tao_remote_sensor* tao_remote_sensor_create(
    const char* owner,
    long        nbufs,
    long    max_ninds,
    long    max_nsubs,
    unsigned    flags)
{
    // Check arguments.
    if (tao_check_owner_name(owner) != TAO_OK) {
        return NULL;
    }
    if (nbufs < 2) {
        tao_store_error(__func__, TAO_BAD_BUFFERS);
        return NULL;
    }
    if (max_nsubs < 1 || max_ninds < 1) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return NULL;
    }
    if (max_nsubs > max_ninds) {
        tao_store_error(__func__, TAO_BAD_NUMBER);
        return NULL;
    }

    // Compute size of variable-size structure and offsets to the different
    // parts. The layout is:
    //
    //     [params]
    //     [primary config]
    //     [secondary config]
    //     [data-frame 1]
    //     [data-frame 2]
    //     ...
    //     [data-frame nbufs]

    // Offsets of primary and secondary configurations in `tao_remote_sensor`
    // structure.
    size_t subs_offset, cfg_size = config_size(&subs_offset, max_ninds, max_nsubs);
    size_t config1_offset = round_size(sizeof(tao_remote_sensor), ALIGN_SIZE);
    size_t config2_offset = round_size(config1_offset + cfg_size, ALIGN_SIZE);

    // Offset to the first data-frame. Data-frames are aligned to cache line
    // size.
    size_t offset = round_size(config2_offset + cfg_size, TAO_ALIGNMENT);

    // Stride is the size of a data-frame rounded to cache line size.
    size_t stride = round_size(
        sizeof(tao_remote_sensor_dataframe)
        + sizeof(tao_shackhartmann_data[max_nsubs]), TAO_ALIGNMENT);

    // Total size.
    size_t size = offset + nbufs*stride;

    // Allocate remote shared object.
    tao_remote_sensor* wfs = (tao_remote_sensor*)tao_remote_object_create(
        owner, TAO_REMOTE_SENSOR, nbufs, offset, stride, size, flags);
    if (wfs == NULL) {
        return NULL;
    }

    // Instantiate object.
    tao_forced_store(&wfs->config1_offset, config1_offset);
    tao_forced_store(&wfs->config2_offset, config2_offset);
    tao_forced_store(&wfs->max_ninds, max_ninds);
    tao_forced_store(&wfs->max_nsubs, max_nsubs);
    tao_sensor_config_initialize(primary_config(wfs), cfg_size);
    tao_sensor_config_initialize(secondary_config(wfs), cfg_size);

    return wfs;
}

long tao_remote_sensor_get_max_ninds(
    const tao_remote_sensor* wfs)
{
    return (wfs == NULL) ? 0 : wfs->max_ninds;
}

long tao_remote_sensor_get_max_nsubs(
    const tao_remote_sensor* wfs)
{
    return (wfs == NULL) ? 0 : wfs->max_nsubs;
}

const char* tao_remote_sensor_get_camera(
     const tao_remote_sensor* wfs)
{
    return (wfs == NULL) ? 0 : primary_config(wfs)->params.camera;
}

long tao_remote_sensor_get_nsubs(
     const tao_remote_sensor* wfs)
{
    return (wfs == NULL) ? 0 : get_nsubs(primary_config(wfs));
}

long tao_remote_sensor_get_ninds(
    const tao_remote_sensor* wfs)
{
    return (wfs == NULL) ? 0 : get_ninds(primary_config(wfs));
}

const long* tao_remote_sensor_get_dims(
    const tao_remote_sensor* wfs)
{
    return (wfs == NULL) ? NULL : primary_config(wfs)->params.dims;
}

const long* tao_remote_sensor_get_inds(
    const tao_remote_sensor* wfs,
    long* dims)
{
    if (wfs == NULL) {
        if (dims != NULL) {
            dims[0] = 0;
            dims[1] = 0;
        }
        return NULL;
    } else {
        if (dims != NULL) {
            dims[0] = primary_config(wfs)->params.dims[0];
            dims[1] = primary_config(wfs)->params.dims[1];
        }
        return get_inds(primary_config(wfs));
    }
}

const tao_subimage* tao_remote_sensor_get_subs(
    const tao_remote_sensor* wfs,
    long* nsubs)
{
    if (wfs == NULL) {
        if (nsubs != NULL) {
            *nsubs = 0;
        }
        return NULL;
    } else {
        if (nsubs != NULL) {
            *nsubs = get_nsubs(primary_config(wfs));
        }
        return get_subs(primary_config(wfs));
    }
}

tao_status tao_remote_sensor_get_control(
    tao_remote_sensor* wfs,
    tao_sensor_control* ctrl)
{
    if (wfs == NULL || ctrl == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    memcpy(ctrl, &primary_config(wfs)->params.ctrl, sizeof(*ctrl));
    return TAO_OK;
}

tao_status tao_remote_sensor_set_control(
    tao_remote_sensor* wfs,
    const tao_sensor_control* ctrl)
{
    if (wfs == NULL || ctrl == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (tao_sensor_control_check(ctrl) != TAO_OK) {
        return TAO_ERROR;
    }
    memcpy(&primary_config(wfs)->params.ctrl, ctrl, sizeof(*ctrl));
    return TAO_OK;
}

tao_status tao_remote_sensor_get_config(
    const tao_remote_sensor* wfs,
    tao_sensor_config* cfg)
{
    if (wfs == NULL || cfg == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_sensor_config_copy(cfg, primary_config(wfs));
}

// Argiments for task to execute on confguration command by a client.
typedef struct {
    const tao_sensor_config* cfg;
} configure_args;

// Task to execute on configuration command by a client.
static tao_status configure_task(
    tao_remote_sensor* wfs,
    void* data)
{
    // Retrieve arguments.
    configure_args *args = data;

    // Copy the proposed configuration to the secondary configuration of the
    // shared resources. NOTE: This also checks the proposed configuration.
    return tao_sensor_config_copy(secondary_config(wfs), args->cfg);
}

tao_serial tao_remote_sensor_configure(
    tao_remote_sensor* wfs,
    const tao_sensor_config* cfg,
    double secs)
{
    if (wfs == NULL || cfg == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return -1;
    }
    configure_args args = { .cfg = cfg };
    return tao_remote_sensor_send_command(
        wfs, TAO_COMMAND_CONFIG, configure_task, &args, secs);
}

tao_status tao_remote_sensor_accept_config(
    tao_remote_sensor* wfs)
{
    // The destination is the primary configuration.
    tao_sensor_config* dst = primary_config(wfs);

    // The source is the secondary configuration.
    tao_sensor_config* src = secondary_config(wfs);

    // Signal changes to others.
    if (tao_remote_sensor_broadcast_condition(wfs) != TAO_OK) {
        return TAO_ERROR;
    }

    // Copy the secondary configuration into the primary one. NOTE: This also
    // checks the source configuration.
    return tao_sensor_config_copy(dst, src);
}

tao_serial tao_remote_sensor_start(
    tao_remote_sensor* wfs,
    double secs)
{
    return tao_remote_sensor_send_command(wfs, TAO_COMMAND_START, NULL, NULL, secs);
}

tao_serial tao_remote_sensor_stop(
    tao_remote_sensor* wfs,
    double secs)
{
    return tao_remote_sensor_send_command(wfs, TAO_COMMAND_STOP, NULL, NULL, secs);
}

tao_serial tao_remote_sensor_abort(
    tao_remote_sensor* wfs,
    double secs)
{
    return tao_remote_sensor_send_command(wfs, TAO_COMMAND_ABORT, NULL, NULL, secs);
}

tao_serial tao_remote_sensor_reset(
    tao_remote_sensor* wfs,
    double secs)
{
    return tao_remote_sensor_send_command(wfs, TAO_COMMAND_RESET, NULL, NULL, secs);
}

static inline void* remote_object_get_buffer(
    const tao_remote_object* obj,
    tao_serial serial)
{
    size_t offset = obj->offset + ((serial - 1)%obj->nbufs)*obj->stride;
    return TAO_COMPUTED_ADDRESS(obj, offset);
}

// Offset of the reference values in a data-frame.
#define DATA_OFFSET_IN_DATAFRAME \
    TAO_ROUND_UP(sizeof(tao_dataframe_header), sizeof(double))

static inline tao_dataframe_header* fetch_frame(
    const tao_remote_sensor* obj,
    tao_serial               serial,
    tao_shackhartmann_data** data)
{
    tao_dataframe_header* header = remote_object_get_buffer(
        tao_remote_object_cast(obj), serial);
    *data = TAO_COMPUTED_ADDRESS(header, DATA_OFFSET_IN_DATAFRAME);
    return header;
}

tao_status tao_remote_sensor_fetch_data(
    const tao_remote_sensor* obj,
    tao_serial               serial,
    tao_shackhartmann_data*  data,
    long                     ndata,
    tao_dataframe_info*      info)
{
    // Check arguments, then copy and check that data did not get overwritten
    // in the mean time.
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    long nsubs = get_nsubs(primary_config(obj));
    if (nsubs < 0 || obj->base.nbufs < 2) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    if (ndata != nsubs) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return TAO_ERROR;
    }
    size_t nbytes = sizeof(tao_shackhartmann_data[ndata]);
    if (serial < 1) {
        if (serial != 0) {
            tao_store_error(__func__, TAO_BAD_SERIAL);
            return TAO_ERROR;
        }
        if (info != NULL) {
            memset(info, 0, sizeof(*info));
        }
        if (data != NULL && nbytes > 0) {
            memset(data, 0, nbytes);
        }
    } else {
        // FIXME: check whether serial is too high, server is running, etc.
        tao_shackhartmann_data* src_data;
        const tao_dataframe_header* header = fetch_frame(
            obj, serial, &src_data);
        if (atomic_load(&header->serial) != serial) {
            if (info != NULL) {
                info->serial = 0;
                info->mark   = 0;
                info->time   = TAO_TIME(0, 0);
            }
            return TAO_TIMEOUT;
        }
        if (info != NULL) {
            info->serial = serial;
            info->mark   = header->mark;
            info->time   = header->time;
        }
        if (data != NULL && nbytes > 0) {
            memcpy(data, src_data, nbytes);
        }
        if (atomic_load(&header->serial) != serial) {
            return TAO_TIMEOUT;
        }
    }
    return TAO_OK;
}

#define TYPE remote_sensor
#define MAGIC TAO_REMOTE_SENSOR
#define REMOTE_OBJECT 2 // sub-type of remote object type
#include "./shared-methods.c"
