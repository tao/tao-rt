// attributes.c -
//
// Named attributes in TAO library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2023-2024, Éric Thiébaut.

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h> // *before* <tao-errors.h>
#include <stdlib.h>
#include <string.h>

#include "tao-attributes.h"
#include "tao-errors.h"
#include "tao-macros.h"
#include "tao-generic.h"

#define PEEK(type, addr, off)   ((type*)((uint8_t*)(addr) + (off)))

//-----------------------------------------------------------------------------
// ACCESSORS

static inline const char* attr_get_key_ro(
    const tao_attr* attr)
{
    return PEEK(const char, attr, TAO_ATTR_KEY_OFFSET(attr));
}

static inline char* attr_get_key_rw(
    tao_attr* attr)
{
    return PEEK(char, attr, TAO_ATTR_KEY_OFFSET(attr));
}

#define attr_get_key(attr)                              \
    _Generic(attr,                                      \
             const tao_attr*: attr_get_key_ro,          \
             /***/ tao_attr*: attr_get_key_rw)(attr)

const char* tao_attr_get_key(
    const tao_attr* attr)
{
    return attr == NULL ? NULL : attr_get_key(attr);
}

size_t tao_attr_get_max_key_size(
    const tao_attr* attr)
{
    return attr == NULL ? 0 : TAO_ATTR_KEY_SIZE(attr);
}

size_t tao_attr_get_max_val_size(
    const tao_attr* attr)
{
    return attr == NULL ? 0 : TAO_ATTR_VAL_SIZE(attr);
}

tao_attr_type tao_attr_get_type(
    const tao_attr* attr)
{
    return attr == NULL ? TAO_ATTR_NONE : TAO_ATTR_TYPE(attr);
}

unsigned tao_attr_get_flags(
    const tao_attr* attr)
{
    return attr == NULL ? 0 : TAO_ATTR_FLAGS(attr);
}

int tao_attr_is_readable(
    const tao_attr* attr)
{
    return attr != NULL && TAO_ATTR_IS_READABLE(attr) ? 1 : 0;
}

int tao_attr_is_writable(
    const tao_attr* attr)
{
    return attr != NULL && TAO_ATTR_IS_WRITABLE(attr) ? 1 : 0;
}

int tao_attr_is_variable(
    const tao_attr* attr)
{
    return attr != NULL && TAO_ATTR_IS_VARIABLE(attr) ? 1 : 0;
}

//-----------------------------------------------------------------------------

static inline bool attr_ok_for_reading(
    const char* func,
    const tao_attr* attr,
    void* ptr,
    tao_attr_type type)
{
    if (attr == NULL || ptr == NULL) {
        tao_store_error(func, TAO_BAD_ADDRESS);
        return false;
    } else if (TAO_ATTR_TYPE(attr) != type) {
        tao_store_error(func, TAO_BAD_TYPE);
        return false;
    } else {
        return true;
    }
}

tao_status tao_attr_get_boolean_value(
    const tao_attr* attr,
    int* ptr)
{
    if (attr_ok_for_reading(__func__, attr, ptr, TAO_ATTR_BOOLEAN)) {
        *ptr = attr->val.b ? 1 : 0;
        return TAO_OK;
    } else {
        return TAO_ERROR;
    }
}

tao_status tao_attr_get_integer_value(
    const tao_attr* attr,
    int64_t* ptr)
{
    if (attr_ok_for_reading(__func__, attr, ptr, TAO_ATTR_INTEGER)) {
        *ptr = attr->val.i;
        return TAO_OK;
    } else {
        return TAO_ERROR;
    }
}

tao_status tao_attr_get_float_value(
    const tao_attr* attr,
    double* ptr)
{
    if (attr_ok_for_reading(__func__, attr, ptr, TAO_ATTR_FLOAT)) {
        *ptr = attr->val.f;
        return TAO_OK;
    } else {
        return TAO_ERROR;
    }
}

tao_status tao_attr_get_string_value(
    const tao_attr* attr,
    const char** ptr)
{
    if (attr_ok_for_reading(__func__, attr, ptr, TAO_ATTR_STRING)) {
        *ptr = attr->val.s;
        return TAO_OK;
    } else {
        return TAO_ERROR;
    }
}

//-----------------------------------------------------------------------------

static inline bool attr_ok_for_writing(
    const char* func,
    tao_attr* attr,
    int overwrite,
    tao_attr_type type)
{
    if (attr == NULL) {
        tao_store_error(func, TAO_BAD_ADDRESS);
        return false;
    } else if (TAO_ATTR_TYPE(attr) != type) {
        tao_store_error(func, TAO_BAD_TYPE);
        return false;
    } else if (!(overwrite || TAO_ATTR_IS_WRITABLE(attr))) {
        tao_store_error(func, TAO_UNWRITABLE);
        return false;
    } else {
        return true;
    }
}

tao_status tao_attr_set_boolean_value(
    tao_attr* attr,
    int val,
    int overwrite)
{
    if (attr_ok_for_writing(__func__, attr, overwrite, TAO_ATTR_BOOLEAN)) {
        attr->val.b = (val != 0);
        return TAO_OK;
    } else {
        return TAO_ERROR;
    }
}

tao_status tao_attr_set_integer_value(
    tao_attr* attr,
    int64_t val,
    int overwrite)
{
    if (attr_ok_for_writing(__func__, attr, overwrite, TAO_ATTR_INTEGER)) {
        attr->val.i = val;
        return TAO_OK;
    } else {
        return TAO_ERROR;
    }
}

tao_status tao_attr_set_float_value(
    tao_attr* attr,
    double val,
    int overwrite)
{
    if (attr_ok_for_writing(__func__, attr, overwrite, TAO_ATTR_FLOAT)) {
        attr->val.f = val;
        return TAO_OK;
    } else {
        return TAO_ERROR;
    }
}

// Copy string value. Assume arguments are correct (non NULL). May yield an
// error if string is too long.
static inline tao_status attr_set_string_value(
    const char* func,
    tao_attr* attr,
    const char* val /* NOTE non-NULL */)
{
    size_t size = strlen(val) + 1;
    if (size > TAO_ATTR_VAL_SIZE(attr)) {
        tao_store_error(func, TAO_BAD_VALUE_SIZE);
        return TAO_ERROR;
    }
    memcpy(attr->val.s, val, size);
    return TAO_OK;
}

tao_status tao_attr_set_string_value(
    tao_attr* attr,
    const char* val,
    int overwrite)
{
    if (val == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (attr_ok_for_writing(__func__, attr, overwrite, TAO_ATTR_STRING)) {
        return attr_set_string_value(__func__, attr, val);
    } else {
        return TAO_ERROR;
    }
}

//-----------------------------------------------------------------------------

static inline bool same_string(
    const char* s1,
    size_t size1, // max. num. char including final null
    const char* s2,
    size_t size2) // max. num. char including final null
{
    size_t size = tao_min(size1, size2);
    for (size_t i = 0; i < size; ++i) {
        char c = s1[i];
        if (s2[i] != c) {
            break;
        }
        if (c == '\0') {
            return true;
        }
    }
    return false;
}

unsigned int tao_attr_check(
    const tao_attr* attr1,
    const tao_attr* attr2,
    unsigned flags)
{
    unsigned result = 0;
    if (attr1 != NULL && attr2 != NULL) {
        if ((flags & TAO_ATTR_CHECK_NAME) != 0
            && !same_string(attr_get_key(attr1), TAO_ATTR_KEY_SIZE(attr1),
                            attr_get_key(attr2), TAO_ATTR_KEY_SIZE(attr2))) {
            result |= TAO_ATTR_CHECK_NAME;
        }
        if ((flags & TAO_ATTR_CHECK_FLAGS) != 0
            && TAO_ATTR_FLAGS(attr1) != TAO_ATTR_FLAGS(attr2)) {
            result |= TAO_ATTR_CHECK_FLAGS;
        }
        if ((flags & (TAO_ATTR_CHECK_TYPE|TAO_ATTR_CHECK_VALUE)) != 0
            && TAO_ATTR_TYPE(attr1) != TAO_ATTR_TYPE(attr2)) {
            // If the type has changed, assume that the value has changed.
            result |= (TAO_ATTR_CHECK_TYPE|TAO_ATTR_CHECK_VALUE);
        }
        if ((flags & TAO_ATTR_CHECK_VALUE) != 0
            && TAO_ATTR_TYPE(attr1) == TAO_ATTR_TYPE(attr2)) {
            // Check value knowing that the type is the same.
            switch (TAO_ATTR_TYPE(attr1)) {
            case TAO_ATTR_BOOLEAN:
                if (attr1->val.b != attr2->val.b) {
                    result |= TAO_ATTR_CHECK_VALUE;
                }
                break;
            case TAO_ATTR_INTEGER:
                if (attr1->val.i != attr2->val.i) {
                    result |= TAO_ATTR_CHECK_VALUE;
                }
                break;
            case TAO_ATTR_FLOAT:
                if (isnan(attr1->val.f) ? !isnan(attr2->val.f) : attr1->val.f != attr2->val.f) {
                    result |= TAO_ATTR_CHECK_VALUE;
                }
                break;
            case TAO_ATTR_STRING:
                if (!same_string(attr1->val.s, TAO_ATTR_VAL_SIZE(attr1),
                                 attr2->val.s, TAO_ATTR_VAL_SIZE(attr2))) {
                    result |= TAO_ATTR_CHECK_VALUE;
                }
                break;
            default:
                break;
            }
        }
    }
    return result;
}

//-----------------------------------------------------------------------------
// TABLE OF NAMED ATTRIBUTES

static inline size_t round_size(size_t a, size_t b)
{
    return TAO_ROUND_UP(a, b);
}

// Offset (relative to the table) of the array of offsets to the named
// attributes.
#define ATTR_TABLE_OFFSET_OF_OFFSETS \
    round_size(sizeof(tao_attr_table), sizeof(size_t))

#define ATTR_TABLE_OFFSETS(table) \
    PEEK(size_t, table, ATTR_TABLE_OFFSET_OF_OFFSETS)

static inline const size_t* attr_table_offsets_ro(
    const tao_attr_table* table)
{
    return ATTR_TABLE_OFFSETS(table);
}

static inline size_t* attr_table_offsets_rw(
    tao_attr_table* table)
{
    return ATTR_TABLE_OFFSETS(table);
}

#define attr_table_offsets(table)                                       \
    _Generic(table,                                                     \
             const tao_attr_table*: attr_table_offsets_ro,              \
             /***/ tao_attr_table*: attr_table_offsets_rw)(table)

static tao_attr_table* attr_table_clear(
    tao_attr_table* table)
{
    // Clear the table (not its size!).
    size_t offset = offsetof(tao_attr_table, length);
    memset((uint8_t*)table + offset, 0, table->size - offset);
    return table;
}

tao_attr_table* tao_attr_table_create_with_size(
    size_t size)
{
    if (size < sizeof(tao_attr_table)) {
        size = sizeof(tao_attr_table);
    }
    tao_attr_table* table = malloc(size);
    if (table == NULL) {
        tao_store_system_error(__func__);
        return NULL;
    }
    table->size = size;
    return attr_table_clear(table);
}

tao_status tao_attr_table_set_size(
    tao_attr_table* table,
    size_t size)
{
    if (table == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (size < sizeof(tao_attr_table)) {
        tao_store_error(__func__, TAO_BAD_TABLE_SIZE);
        return TAO_ERROR;
    }
    table->size = size;
    attr_table_clear(table);
    return TAO_OK;
}

void tao_attr_table_destroy(
    tao_attr_table* table)
{
    if (table != NULL) {
        free(table);
    }
}

size_t tao_attr_table_get_size(
    tao_attr_table* table)
{
    return table == NULL ? 0 : table->size;
}

long tao_attr_table_get_length(
    tao_attr_table* table)
{
    return table == NULL ? 0 : table->length;
}

// NOTE: This macro evaluates its arguments more than once.
#define ATTR_TABLE_GET_ENTRY(T, table, index) \
    ((T)((uint8_t*)(table) + attr_table_offsets(table)[index]))

static inline const tao_attr* attr_table_get_entry_ro(
    const tao_attr_table* table,
    long index)
{
    return ATTR_TABLE_GET_ENTRY(const tao_attr*, table, index);
}

static inline tao_attr* attr_table_get_entry_rw(
    tao_attr_table* table,
    long index)
{
    return ATTR_TABLE_GET_ENTRY(tao_attr*, table, index);
}

#define attr_table_get_entry(table, index)                              \
    _Generic(table,                                                     \
             const tao_attr_table*: attr_table_get_entry_ro,            \
             /***/ tao_attr_table*: attr_table_get_entry_rw)(table, index)

static tao_attr_table* attr_table_build(
    const char* func,
    tao_attr_table* table,
    const tao_attr_descr defs[],
    long length)
{
    if (defs == NULL) {
        tao_store_error(func, TAO_BAD_ADDRESS);
        return NULL;
    }
    if (length < 0) {
        tao_store_error(func, TAO_BAD_NUMBER);
        return NULL;
    }

    // Alignement size.
    const size_t alignment = tao_max(sizeof(double), sizeof(void*));

    // Offset of key relative to attribute.
    const size_t val_offset = offsetof(tao_attr, val);

    // Offset of list of attributes relative to table (correctly aligned and
    // leaving enough space for the table and the list of offsets).
    size_t attr_offset = round_size(
        ATTR_TABLE_OFFSET_OF_OFFSETS + sizeof(size_t[length + 1]),
        alignment);

    // First pass is to check arguments and determine the size of the table,
    // second pass is to instantiate the table.
    size_t* table_offsets = NULL;
    for (int pass = 1; pass <= 2; ++pass) {
        size_t nbytes = attr_offset;
        for (long i = 0; i < length; ++i) {
            // Storage in memory is: attribute structure + enough bytes for
            // value, immediately followed by attribute key. We just allow for
            // enlarging a bit the maximal value size to make use of extra
            // bytes needed for alignment.
            size_t key_length = TAO_STRLEN(defs[i].key);
            if (key_length < 1) {
                // Keys must have at least one character.
                tao_store_error(func, TAO_BAD_NAME);
                return NULL;
            }
            size_t key_size = key_length + 1;
            size_t val_size = sizeof(tao_attr) - val_offset;
            switch (defs[i].type) {
            case TAO_ATTR_BOOLEAN:
            case TAO_ATTR_INTEGER:
            case TAO_ATTR_FLOAT:
                break;
            case TAO_ATTR_STRING:
                if (defs[i].type == TAO_ATTR_STRING && defs[i].size > val_size) {
                    val_size = defs[i].size;
                }
                break;
            default:
                tao_store_error(func, TAO_BAD_TYPE);
                return NULL;
            }
            size_t attr_size = round_size(val_offset + val_size + key_size, alignment);
            val_size = attr_size - (val_offset + key_size); // adjust max. value size
            size_t key_offset = val_offset + val_size;

            // Keys and values must not be too large.
            if (key_size > TAO_ATTR_MAX_KEY_SIZE) {
                tao_store_error(func, TAO_BAD_KEY_SIZE);
                return NULL;
            }
            if (key_offset > TAO_ATTR_MAX_KEY_OFFSET) {
                // This should only occurs when value size is too large.
                tao_store_error(func, TAO_BAD_VALUE_SIZE);
                return NULL;
            }

            if (pass == 2) {
                // Instantiate the table.
                table_offsets[i] = nbytes;
                tao_attr* attr = PEEK(tao_attr, table, nbytes);
                char* key = PEEK(char, attr, key_offset);
                if (key_length > 0) {
                    memcpy(key, defs[i].key, key_length);
                }
                key[key_length] = '\0';
                tao_forced_store(&attr->type,       defs[i].type);
                tao_forced_store(&attr->flags,      defs[i].flags);
                tao_forced_store(&attr->key_offset, key_offset);
                tao_forced_store(&attr->key_size,   key_size);
            }
            nbytes += attr_size;
        }
        if (pass == 1) {
            // This is the end of the first pass.
            if (table == NULL) {
                // Allocate the table.
                table = tao_attr_table_create_with_size(nbytes);
                if (table == NULL) {
                    return NULL;
                }
            } else {
                // Check that existing table is large enough.
                if (table->size < nbytes) {
                    tao_store_error(func, TAO_BAD_TABLE_SIZE);
                    return NULL;
                }
            }
            attr_table_clear(table);
            table->length = length;
            table_offsets = attr_table_offsets(table);
        } else {
            // Offset to last + 1 entry. This may be used for error checking.
            table_offsets[length] = nbytes;
        }
    }
    return table;
}

tao_attr_table* tao_attr_table_create(
    const tao_attr_descr defs[],
    long num)
{
    return attr_table_build(__func__, NULL, defs, num);
}

tao_status tao_attr_table_instantiate(
    tao_attr_table* table,
    const tao_attr_descr defs[],
    long num)
{
    if (table == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    } else if (attr_table_build(__func__, table, defs, num) == NULL) {
        return TAO_ERROR;
    } else {
        return TAO_OK;
    }
}

static inline bool has_final_null(
    const char* str,
    size_t size)
{
    for (size_t i = 0; i < size; ++i) {
        if (str[i] == 0) {
            return true;
        }
    }
    return false;
}

tao_status tao_attr_table_check(
    const tao_attr_table* table)
{
    if (table == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (table->size < sizeof(tao_attr_table)) {
        // Not enough space for `length` member.
        tao_store_error(__func__, TAO_BAD_TABLE_SIZE);
        return TAO_ERROR;
    }
    if (table->length < 0) {
        // Empty table.
        if (table->length < 0) {
            tao_store_error(__func__, TAO_BAD_NUMBER);
            return TAO_ERROR;
        }
        return TAO_OK;
    }
    if (table->size < ATTR_TABLE_OFFSET_OF_OFFSETS + table->length*sizeof(size_t)) {
        // Not enough space for array of offsets.
        tao_store_error(__func__, TAO_BAD_TABLE_SIZE);
        return TAO_ERROR;
    }

    // Alignement size.
    const size_t alignment = tao_max(sizeof(double), sizeof(void*));

    // Array of offsets to attributes.
    const size_t* offsets = attr_table_offsets(table);

    // Offset to value in attribute.
    size_t val_offset = TAO_ATTR_VAL_OFFSET;

    // Start with offset to the first attribute, immediately after the array of
    // offsets.
    size_t offset = round_size(
        ATTR_TABLE_OFFSET_OF_OFFSETS + sizeof(size_t[table->length + 1]),
        alignment);

    // For each attribute, check offsets, sizes, and null-terminated strings.
    for (long i = 0; i < table->length; ++i) {
        if (offsets[i] != offset) {
            tao_store_error(__func__, TAO_CORRUPTED);
            return TAO_ERROR;
        }
        if (table->size < offset + val_offset) {
            // Insufficient space to read attribute header.
            tao_store_error(__func__, TAO_BAD_TABLE_SIZE);
            return TAO_ERROR;
        }
        const tao_attr* attr = PEEK(const tao_attr, table, offset);
        size_t key_offset = TAO_ATTR_KEY_OFFSET(attr);
        if (key_offset < val_offset) {
            // Value should be stored before key.
            tao_store_error(__func__, TAO_CORRUPTED);
            return TAO_ERROR;
        }
        size_t val_min_size;
        switch (attr->type) {
        case TAO_ATTR_BOOLEAN:
            val_min_size = sizeof(attr->val.b);
            break;
        case TAO_ATTR_INTEGER:
            val_min_size = sizeof(attr->val.i);
            break;
        case TAO_ATTR_FLOAT:
            val_min_size = sizeof(attr->val.f);
            break;
        case TAO_ATTR_STRING:
            val_min_size = 1; // at least final null
            break;
        default:
            tao_store_error(__func__, TAO_BAD_TYPE);
            return TAO_ERROR;
        }
        size_t val_size = TAO_ATTR_VAL_SIZE(attr);
        if (val_size < val_min_size) {
            tao_store_error(__func__, TAO_BAD_VALUE_SIZE);
            return TAO_ERROR;
        }
        size_t key_size = TAO_ATTR_KEY_SIZE(attr);
        if (key_size < 2) {
            // Key must have at least one character plus a terminating null.
            tao_store_error(__func__, TAO_BAD_KEY_SIZE);
            return TAO_ERROR;
        }
        size_t attr_size = val_offset + key_offset + key_size;
        if (attr_size%alignment != 0 || offsets[i+1] != offsets[i] + attr_size) {
            tao_store_error(__func__, TAO_CORRUPTED);
            return TAO_ERROR;
        }
        if (table->size < offset + attr_size) {
            // Insufficient space to read attribute.
            tao_store_error(__func__, TAO_BAD_TABLE_SIZE);
            return TAO_ERROR;
        }
        const char* key = attr_get_key(attr);
        if (!has_final_null(key, key_size)) {
            tao_store_error(__func__, TAO_BAD_NAME);
            return TAO_ERROR;
        }
        if (attr->type == TAO_ATTR_STRING && !has_final_null(attr->val.s, val_size)) {
            // String value must have at least one character plus a terminating null.
            tao_store_error(__func__, TAO_BAD_STRING);
            return TAO_ERROR;
        }
        offset += attr_size; // offset to next attribute
    }
    return TAO_OK;
}

const tao_attr* tao_attr_table_get_entry(
    const tao_attr_table* table,
    long index)
{
    if (table == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    if (index < 0 || index >= table->length) {
        tao_store_error(__func__, TAO_OUT_OF_RANGE);
        return NULL;
    }
    return attr_table_get_entry(table, index);
}

const tao_attr* tao_attr_table_unsafe_get_entry(
    const tao_attr_table* table,
    long index)
{
    return attr_table_get_entry(table, index);
}

static long attr_table_find_entry(
    const tao_attr_table* table,
    const char* key /* NOTE non-NULL */)
{
    int c = key[0];
    if (c != '\0') {
        for (long i = 0; i < table->length; ++i) {
            const tao_attr* attr = attr_table_get_entry(table, i);
            const char* attr_key = attr_get_key(attr);
            if (attr_key[0] == c && strncmp(attr_key, key, TAO_ATTR_KEY_SIZE(attr)) == 0) {
                return i;
            }
        }
    }
    return -1;
}

long tao_attr_table_try_find_entry(
    const tao_attr_table* table,
    const char* key)
{
    return (table == NULL || key == NULL) ? -2 : attr_table_find_entry(table, key);
}

const tao_attr* tao_attr_table_find_entry(
    const tao_attr_table* table,
    const char* key)
{
    if (table == NULL || key == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
    } else {
        long index = attr_table_find_entry(table, key);
        if (index >= 0) {
            return attr_table_get_entry(table, index);
        }
        tao_store_error(__func__, TAO_NOT_FOUND);
    }
    return NULL;
}

//-----------------------------------------------------------------------------

static inline const tao_attr* attr_table_get_attr_for_reading(
    const char* func,
    const tao_attr_table* table,
    const char* key,
    void* ptr,
    tao_attr_type type)
{
    if (table == NULL || key == NULL || ptr == NULL) {
        tao_store_error(func, TAO_BAD_ADDRESS);
        return NULL;
    }
    long index = attr_table_find_entry(table, key);
    if (index < 0) {
        tao_store_error(func, TAO_NOT_FOUND);
        return NULL;
    }
    const tao_attr* attr = attr_table_get_entry(table, index);
    if (TAO_ATTR_TYPE(attr) != type) {
        tao_store_error(func, TAO_BAD_TYPE);
        return NULL;
    }
    return attr;
}

tao_status tao_attr_table_get_boolean_value(
    const tao_attr_table* table,
    const char* key,
    int* ptr)
{
    const tao_attr* attr = attr_table_get_attr_for_reading(
        __func__, table, key, ptr, TAO_ATTR_BOOLEAN);
    if (attr == NULL) {
        return TAO_ERROR;
    }
    *ptr = attr->val.b ? 1 : 0;
    return TAO_OK;
}

tao_status tao_attr_table_get_integer_value(
    const tao_attr_table* table,
    const char* key,
    int64_t* ptr)
{
    const tao_attr* attr = attr_table_get_attr_for_reading(
        __func__, table, key, ptr, TAO_ATTR_INTEGER);
    if (attr == NULL) {
        return TAO_ERROR;
    }
    *ptr = attr->val.i;
    return TAO_OK;
}

tao_status tao_attr_table_get_float_value(
    const tao_attr_table* table,
    const char* key,
    double* ptr)
{
    const tao_attr* attr = attr_table_get_attr_for_reading(
        __func__, table, key, ptr, TAO_ATTR_FLOAT);
    if (attr == NULL) {
        return TAO_ERROR;
    }
    *ptr = attr->val.f;
    return TAO_OK;
}

tao_status tao_attr_table_get_string_value(
    const tao_attr_table* table,
    const char* key,
    const char** ptr)
{
    const tao_attr* attr = attr_table_get_attr_for_reading(
        __func__, table, key, ptr, TAO_ATTR_STRING);
    if (attr == NULL) {
        return TAO_ERROR;
    }
    *ptr = attr->val.s;
    return TAO_OK;
}

//-----------------------------------------------------------------------------

static inline tao_attr* attr_table_get_attr_for_writing(
    const char* func,
    tao_attr_table* table,
    const char* key,
    int overwrite,
    tao_attr_type type)
{
    if (table == NULL || key == NULL) {
        tao_store_error(func, TAO_BAD_ADDRESS);
        return NULL;
    }
    long index = attr_table_find_entry(table, key);
    if (index < 0) {
        tao_store_error(func, TAO_NOT_FOUND);
        return NULL;
    }
    tao_attr* attr = attr_table_get_entry(table, index);
    if (TAO_ATTR_TYPE(attr) != type) {
        tao_store_error(func, TAO_BAD_TYPE);
        return NULL;
    }
    if (!(overwrite || TAO_ATTR_IS_WRITABLE(attr))) {
        tao_store_error(func, TAO_UNWRITABLE);
        return NULL;
    }
    return attr;
}

tao_status tao_attr_table_set_boolean_value(
    tao_attr_table* table,
    const char* key,
    int val,
    int overwrite)
{
    tao_attr* attr = attr_table_get_attr_for_writing(
        __func__, table, key, overwrite, TAO_ATTR_BOOLEAN);
    if (attr == NULL) {
        return TAO_ERROR;
    }
    attr->val.b = (val != 0);
    return TAO_OK;
}

tao_status tao_attr_table_set_integer_value(
    tao_attr_table* table,
    const char* key,
    int64_t val,
    int overwrite)
{
    tao_attr* attr = attr_table_get_attr_for_writing(
        __func__, table, key, overwrite, TAO_ATTR_INTEGER);
    if (attr == NULL) {
        return TAO_ERROR;
    }
    attr->val.i = val;
    return TAO_OK;
}

tao_status tao_attr_table_set_float_value(
    tao_attr_table* table,
    const char* key,
    double val,
    int overwrite)
{
    tao_attr* attr = attr_table_get_attr_for_writing(
        __func__, table, key, overwrite, TAO_ATTR_FLOAT);
    if (attr == NULL) {
        return TAO_ERROR;
    }
    attr->val.f = val;
    return TAO_OK;
}

tao_status tao_attr_table_set_string_value(
    tao_attr_table* table,
    const char* key,
    const char* val,
    int overwrite)
{
    if (val == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_attr* attr = attr_table_get_attr_for_writing(
        __func__, table, key, overwrite, TAO_ATTR_STRING);
    if (attr == NULL) {
        return TAO_ERROR;
    }
    return attr_set_string_value(__func__, attr, val);
}

//-----------------------------------------------------------------------------
