// remote-objects.c -
//
// Management of basic shared objects used to communicate with remote server.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2018-2024, Éric Thiébaut.

#include <stdio.h> // *before* <tao-errors.h>
#include <string.h>
#include <unistd.h>

#include "tao-remote-objects-private.h"
#include "tao-errors.h"
#include "tao-macros.h"
#include "tao-generic.h"
#include "tao-config.h"

const char* tao_command_get_name(
    tao_command cmd)
{
    switch (cmd) {
    case TAO_COMMAND_NONE:   return "none";
    case TAO_COMMAND_RESET:  return "reset";
    case TAO_COMMAND_SEND:   return "send";
    case TAO_COMMAND_CONFIG: return "config";
    case TAO_COMMAND_START:  return "start";
    case TAO_COMMAND_STOP:   return "stop";
    case TAO_COMMAND_ABORT:  return "abort";
    case TAO_COMMAND_KILL:   return "kill";
    }
    // NOTE: Default is not considered in the switch statement to have the
    //       compiler throw a warning if some case is omiited.
    return "unknown";
}

const char* tao_state_get_name(
    tao_state state)
{
    switch (state) {
    case TAO_STATE_INITIALIZING: return "initializing";
    case TAO_STATE_WAITING:      return "waiting";
    case TAO_STATE_CONFIGURING:  return "configuring";
    case TAO_STATE_STARTING:     return "starting";
    case TAO_STATE_WORKING:      return "working";
    case TAO_STATE_STOPPING:     return "stopping";
    case TAO_STATE_ABORTING:     return "aborting";
    case TAO_STATE_ERROR:        return "error";
    case TAO_STATE_RESETTING:    return "resetting";
    case TAO_STATE_QUITTING:     return "quitting";
    case TAO_STATE_UNREACHABLE:  return "unreachable";
    }
    // NOTE: Default is not considered in the switch statement to have the
    //       compiler throw a warning if some case is omiited.
    return "unknown";
}

tao_status tao_check_owner_name(
    const char* owner)
{
    if (owner != NULL) {
        for (long i = 0; i < TAO_OWNER_SIZE; ++i) {
            if (owner[i] == '\0') {
                return TAO_OK;
            }
            if (owner[i] == '/') {
                break;
            }
        }
    }
    tao_store_error(__func__, TAO_BAD_OWNER);
    return TAO_ERROR;
}

tao_remote_object* tao_remote_object_create(
    const char* owner,
    tao_object_type type,
    long nbufs,
    long offset,
    long stride,
    size_t size,
    unsigned flags)
{
    if (tao_check_owner_name(owner) != TAO_OK) {
        return NULL;
    }
    if ((type & TAO_SHARED_SUPERTYPE_MASK) != TAO_REMOTE_OBJECT) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return NULL;
    }
    if (nbufs < 2) {
        tao_store_error(__func__, TAO_BAD_BUFFERS);
        return NULL;
    }
    if (offset < sizeof(tao_remote_object) || stride < 0 ||
        size < offset + nbufs*stride) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return NULL;
    }
    tao_remote_object* obj = (tao_remote_object*)tao_shared_object_create(
        type, size, flags);
    if (obj == NULL) {
        return NULL;
    }
    strcpy((char*)obj->owner, owner);
    tao_forced_store(&obj->pid,    getpid()); // NOTE getpid() never fails
    tao_forced_store(&obj->nbufs,  nbufs);
    tao_forced_store(&obj->offset, offset);
    tao_forced_store(&obj->stride, stride);
    obj->serial = 0;
    obj->state = TAO_STATE_INITIALIZING;
    obj->command = TAO_COMMAND_NONE;
    return obj;
}

tao_remote_object* tao_remote_object_attach(
    tao_shmid shmid)
{
    tao_shared_object* obj = tao_shared_object_attach(shmid);
    if (obj != NULL &&
        (obj->type & TAO_SHARED_SUPERTYPE_MASK) == TAO_REMOTE_OBJECT) {
        return (tao_remote_object*)obj;
    }
    tao_shared_object_detach(obj);
    tao_store_error(__func__, TAO_BAD_TYPE);
    return NULL;
}

// Basic methods.
#define TYPE remote_object
#define REMOTE_OBJECT 1 // basic remote object type
#include "./shared-methods.c"
