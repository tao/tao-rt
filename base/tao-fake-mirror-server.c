// tao-fake-mirror-server.c -
//
// Implementation of a fake remote deformable mirror server.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2021-2024, Éric Thiébaut.

#include <math.h>
#include <string.h>
#include <stdio.h>

#include "tao-basics.h"
#include "tao-layouts.h"
#include "tao-remote-mirrors-private.h"
#include "tao-generic.h"
#include "tao-errors.h"

// Private data needed for the cleanup callback.
static tao_remote_mirror* dm = NULL;
static long* inds = NULL;
static uint8_t* msk = NULL;

// Send the requested command.  On entry, `cmds` contains the `dm->nacts`
// actuators commands.  On return, `cmds` contains the actuators commands
// modified to account for the deformable mirror limitations.
static tao_status on_send(
    tao_remote_mirror* dm,
    void* ctx,
    double* cmds)
{
    // There is nothing to ;-)
    return TAO_OK;
}

// Release resources.  This function is automatically called on normal exit.
static void cleanup(void)
{
    if (dm != NULL) {
        if (dm->base.state != TAO_STATE_UNREACHABLE) {
            // FIXME: should lock
            dm->base.state = TAO_STATE_UNREACHABLE;
        }
        if (dm->base.command != TAO_COMMAND_NONE) {
            // FIXME: should lock
            dm->base.command = TAO_COMMAND_NONE;
        }
        tao_remote_mirror_detach(dm);
        dm = NULL;
    }
    if (msk != NULL) {
        tao_free(msk);
        msk = NULL;
    }
    if (inds != NULL) {
        tao_free(inds);
        inds = NULL;
    }
}

int main(
    int argc,
    char* argv[])
{
    // Determine program name.
    const char* progname = tao_basename(argv[0]);

    // Install function to free all allocated resources.
    if (atexit(cleanup) != 0) {
        fprintf(stderr, "%s: failed to install cleanup handler\n",
                progname);
        return EXIT_FAILURE;
    }

    // Parse arguments.
    const char* usage = "Usage: %s [OPTIONS ...] [--] NAME\n";
    const char* name = NULL;
    bool debug = false;
    long nbufs = 10000;
    long nacts = 97;
    unsigned int orient = 0;
    unsigned int perms = 0077;
    int iarg = 0;
    while (++iarg < argc) {
        char dummy;
        if (argv[iarg][0] != '-') {
            // Argument is not an option.
            --iarg;
            break;
        }
        if (argv[iarg][1] == '-' && argv[iarg][2] == '\0') {
            // Argument is last option.
            break;
        }
        if (strcmp(argv[iarg], "-h") == 0
            || strcmp(argv[iarg], "-help") == 0
            || strcmp(argv[iarg], "--help") == 0) {
            printf(usage, progname);
            printf("\n");
            printf("Arguments:\n");
            printf("  NAME               Name of server.\n");
            printf("\n");
            printf("Options:\n");
            printf("  -nacts NACTS       Number of actuators [%ld].\n",
                   nacts);
            printf("  -nbufs NBUFS       Number of frame buffers [%ld].\n",
                   nbufs);
            printf("  -orient ORIENT     Orientation of layout [%u].\n",
                   orient);
            printf("  -perms PERMS       Bitwise mask of permissions [0%o].\n",
                   perms);
            printf("  -debug             Debug mode [%s].\n",
                   (debug ? "true" : "false"));
            printf("  -h, -help, --help  Print this help.\n");
            return EXIT_SUCCESS;
        }
        if (strcmp(argv[iarg], "-nacts") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option `%s`.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%ld %c", &nacts, &dummy) != 1
                || nacts < 1) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option `%s`.\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-nbufs") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option `%s`.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%ld %c", &nbufs, &dummy) != 1
                || nbufs < 2) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option `%s`.\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-orient") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option `%s`.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%u %c", &orient, &dummy) != 1) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option `%s`.\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            orient &= 5;
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-perms") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option `%s`.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%u %c", &perms, &dummy) != 1) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option `%s`.\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            perms &= 0777;
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-debug") == 0) {
            debug = true;
            continue;
        }
        fprintf(stderr, "%s: Unknown option `%s`.\n", progname, argv[iarg]);
        return EXIT_FAILURE;
    }
    if (++iarg >= argc) {
        fprintf(stderr, "%s: Missing server name.\n", progname);
    bad_usage:
        fputc('\n', stderr);
        fprintf(stderr, usage, progname);
        fputc('\n', stderr);
        return EXIT_FAILURE;
    }
    name = argv[iarg];
    if (++iarg < argc) {
        fprintf(stderr, "%s: Too many arguments.\n", progname);
        goto bad_usage;
    }

    // Build mirror mask and layout indices.
    long dim = lround(2*sqrt(nacts/M_PI));
    msk = tao_malloc(dim*dim*sizeof(*msk));
    if (msk == NULL) {
        fprintf(stderr, "%s: Cannot allocate mirror mask of size %ld×%ld "
                "for %ld actuators.\n", progname, dim, dim, nacts);
        return EXIT_FAILURE;
    }
    inds = tao_malloc(dim*dim*sizeof(*inds));
    if (inds == NULL) {
        fprintf(stderr, "%s: Cannot allocate %ld×%ld array for layout "
                "indices.\n", progname, dim, dim);
        return EXIT_FAILURE;
    }
    if (tao_layout_mask_instantiate(msk, dim, dim, nacts, inds) == NULL) {
        fprintf(stderr, "%s: Failed to instantiate mirror mask\n",
                progname);
        return EXIT_FAILURE;
    }
    nacts = tao_indexed_layout_build(inds, msk, dim, dim, orient);
    if (nacts < 1) {
        fprintf(stderr, "%s: failed to build layout indices\n", progname);
        tao_report_error();
        return EXIT_FAILURE;
    }

    // Allocate the remote mirror instance.
    dm = tao_remote_mirror_create(
        name, nbufs, inds, dim, dim, -1.0, +1.0, perms);
    if (dm == NULL) {
        fprintf(stderr, "%s: failed to create remote mirror instance\n",
                progname);
        tao_report_error();
        return EXIT_FAILURE;
    }

    // Run loop (on entry of the loop we own the lock on the remote mirror
    // instance).
    tao_remote_mirror_operations ops = {
        .on_send = on_send,
        .name = name,
        .debug = debug
    };
    tao_status status = tao_remote_mirror_run_loop(dm, &ops, NULL);
    if (status != TAO_OK) {
        tao_report_error();
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
