// tao-shared-objects-private.h -
//
// Private definitions for TAO shared objects.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

#ifndef TAO_SHARED_OBJECTS_PRIVATE_H_
#define TAO_SHARED_OBJECTS_PRIVATE_H_ 1

#ifndef JULIA_WRAPPER // Skip this if building Julia wrapper code.
#  include <stdatomic.h>
#endif
#include <tao-shared-objects.h>
#include <tao-threads.h>

TAO_BEGIN_DECLS

/**
 * @defgroup PrivateSharedObjects   Private definitions for shared objects
 *
 * @ingroup Private
 *
 * @brief Private definitions for shared objects.
 *
 * Header file @ref tao-shared-objects-privates.h provides concrete definition of the
 * structure @ref tao_shared_object that is needed by all derived types.
 *
 * @{
 */

/**
 * @def tao_atomic(T,x)
 *
 * This macro is to mark variable `x` with type `T` as being atomic.
 */
#if !defined(JULIA_WRAPPER) || defined(TAO_DOXYGEN_)
#  define tao_atomic(T,x) volatile _Atomic T x
#else
#  define tao_atomic(T,x) T atomic_##x
#endif

struct tao_shared_object_ {
    tao_mutex       mutex;///< Mutually exclusive lock to control access.
    tao_cond         cond;///< Condition variable to signal or wait for
                          ///  changes.

    // The number of attachments is an atomic variable so that it can be
    // fetched and incremented or decremented atomically to determine whether
    // the object has to be destroyed without locking the structure.
#ifndef JULIA_WRAPPER
    volatile atomic_flag ctrl;///< Flag controlling the access to `nrefs`
                              ///  below. Must be different from above `mutex`
                              ///  to avoid that attach/detach takes too long,
                              ///  e.g. if `mutex` is locked to wait for some
                              ///  event like image acquisition.
#else
    bool          atomic_ctrl;
#endif
    int64_t             nrefs;///< Number of attachments, < 0 when the shared
                              ///  data is about to be destroyed.
    const size_t        size; ///< Total number of bytes allocated for the
                              ///  shared memory segment.
    const tao_shmid    shmid; ///< Shared memory identifier.
    const uint32_t     flags; ///< Options and granted access permissions.
    const uint32_t      type; ///< Object type (or class) identifier.
};

/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_SHARED_OBJECTS_PRIVATE_H_
