// tao-ping.c -
//
// Retrieve the status of a TAO server.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2024, Éric Thiébaut.

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "tao-errors.h"
#include "tao-config.h"
#include "tao-remote-objects.h"
#include "tao-macros.h"

int main(
    int argc,
    char* argv[])
{
    // Determine program name.
    const char* progname = tao_basename(argv[0]);

    // Parse arguments.
    char dummy;
    const char* server = NULL;
    const char* usage = "Usage: %s [OPTIONS] [--] SERVER\n";
    int iarg = 1;
    while (iarg < argc) {
        if (argv[iarg][0] != '-') {
            break;
        }
        if (argv[iarg][1] == '-' && argv[iarg][2] == '\0') {
            ++iarg;
            break;
        }
        if (strcmp(argv[iarg], "-h") == 0
            || strcmp(argv[iarg], "-help") == 0
            || strcmp(argv[iarg], "--help") == 0) {
            printf(usage, progname);
            printf("\n");
            printf("Retrieve the status of a TAO server.\n");
            printf("\n");
            printf("Arguments:\n");
            printf("  SERVER             Server name or shared memory identifier.\n");
            printf("\n");
            printf("Options:\n");
            printf("  -h, -help, --help  Print this help.\n");
            return EXIT_SUCCESS;
        }
        fprintf(stderr, "%s: Unknown option %s\n", progname, argv[iarg]);
        return EXIT_FAILURE;
    }
    if (argc - iarg != 1) {
        fprintf(stderr, "%s: Invalid number of arguments.\n", progname);
        fprintf(stderr, usage, progname);
        return EXIT_FAILURE;
    }
    server = argv[iarg];
    bool flag = false;
    tao_shmid shmid = TAO_BAD_SHMID;
    int c = server[0];
    if (c >= '0' && c <= '9') {
        long val;
        if (sscanf(server, "%ld %c", &val, &dummy) == 1) {
            shmid = val;
            flag = true;
        }
    }
    if (!flag) {
        shmid = tao_config_read_shmid(server);
    }
    // Try to connect.
    tao_remote_object* obj = shmid == TAO_BAD_SHMID ? NULL : tao_remote_object_attach(shmid);
    tao_state state = tao_remote_object_get_state(obj);
    tao_serial datnum = tao_remote_object_get_serial(obj);
    tao_serial cmdnum = tao_remote_object_get_ncmds(obj);
    if (obj != NULL) {
        tao_remote_object_detach(obj);
    }
    fprintf(stdout, "%s " TAO_INT64_FORMAT(,d) " " TAO_INT64_FORMAT(,d)  "\n",
            tao_state_get_name(state), cmdnum, datnum);
    return state == TAO_STATE_UNREACHABLE ? EXIT_FAILURE : EXIT_SUCCESS;
}
