// test-pthread.c --
//
// Test mutexes and r/w locks in POSIX Threads Library.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2021-2024, Éric Thiébaut.

#include <pthread.h>

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <tao-threads.h>
#include <tao-utils.h>
#include <tao-errors.h>

#define NEW(T) ((T*)malloc(sizeof(T)))

int main(
    int argc,
    char* argv[])
{
    tao_status status;
    tao_mutex* mutex = NEW(tao_mutex);

    printf("\nCalling `tao_mutex_initialize`...\n");
    status = tao_mutex_initialize(mutex, TAO_PROCESS_SHARED);
    if (status != TAO_OK) {
        tao_report_error();
    }
    printf("\nCalling `tao_mutex_lock`...\n");
    status = tao_mutex_lock(mutex);
    if (status != TAO_OK) {
        tao_report_error();
    }
    printf("\nCalling `tao_mutex_destroy`... (should fail with errno=EBUSY)\n");
    status = tao_mutex_destroy(mutex, false);
    if (status != TAO_OK) {
        tao_report_error();
    }
    printf("\nCalling `tao_mutex_unlock`...\n");
    status = tao_mutex_unlock(mutex);
    if (status != TAO_OK) {
        tao_report_error();
    }
    printf("\nCalling `tao_mutex_destroy`...\n");
    status = tao_mutex_destroy(mutex, false);
    if (status != TAO_OK) {
        tao_report_error();
    }
    printf("\nCalling `tao_mutex_lock`... (should fail with errno=EINVAL)\n");
    status = tao_mutex_lock(mutex);
    if (status != TAO_OK) {
        tao_report_error();
    }
    printf("\nCalling `tao_mutex_initialize`...\n");
    status = tao_mutex_initialize(mutex, TAO_PROCESS_SHARED);
    if (status != TAO_OK) {
        tao_report_error();
    }
    printf("\nCalling `tao_mutex_lock`...\n");
    status = tao_mutex_lock(mutex);
    if (status != TAO_OK) {
        tao_report_error();
    }
    printf("\nCalling `tao_mutex_unlock`...\n");
    status = tao_mutex_unlock(mutex);
    if (status != TAO_OK) {
        tao_report_error();
    }
    printf("\nCalling `tao_mutex_destroy`...\n");
    status = tao_mutex_destroy(mutex, false);
    if (status != TAO_OK) {
        tao_report_error();
    }
    tao_free(mutex);
    return EXIT_SUCCESS;
}
