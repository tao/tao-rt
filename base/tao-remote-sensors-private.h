// tao-remote-sensors-private.h -
//
// Private definitions for remote wavefront sensors in TAO library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2019-2024, Éric Thiébaut.

#ifndef TAO_REMOTE_SENSORS_PRIVATE_H_
#define TAO_REMOTE_SENSORS_PRIVATE_H_ 1

#include <tao-remote-sensors.h>
#include <tao-remote-objects-private.h>

TAO_BEGIN_DECLS

/**
 * Remote wavefront sensor structure.
 *
 * This structure is shared between and the wavefront sensor server and its
 * clients.
 */
struct tao_remote_sensor_ {
    tao_remote_object      base;///< Common part for all remote objects.
    const size_t config1_offset;///< Offset to the primary configuration.
    const size_t config2_offset;///< Offset to the secondary configuration.
    const long        max_ninds;///< Maximum number of indices in layout.
    const long        max_nsubs;///< Maximum number of sub-images.
};

/**
 * @brief Data-frame of remote wavefront sensor as stored in shared memory.
 */
typedef struct tao_remote_sensor_dataframe_ {
    tao_dataframe_header base;
    long nsubs;
    tao_shackhartmann_data data[];///< Flexible array of data for each sub-image.
} tao_remote_sensor_dataframe;

TAO_END_DECLS

#endif // TAO_REMOTE_SENSORS_PRIVATE__H_
