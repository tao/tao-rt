// test-preprocessing.c -
//
// Test performances of image pre-processing.
//
//---------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2020-2022, Éric Thiébaut.

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <float.h>

#ifndef COMPILER_COMMAND
#  warning Compiler command should be specified by COMPILER_COMMAND
#  define COMPILER_COMMAND "unknown"
#endif
#ifndef COMPILER_VERSION
#  warning Compiler version should be specified by COMPILER_VERSION
#  define COMPILER_VERSION "unknown"
#endif

//-----------------------------------------------------------------------------

#define _JOIN3(a1,a2,a3) a1##a2##a3
#define JOIN3(a1,a2,a3)  _JOIN3(a1,a2,a3)

#define TEST_PREPROC_FLOAT float
#define TEST_PREPROC_PIXEL uint8_t
#define TEST_PREPROC_FUNC  JOIN3(preprocess_v,PREPROC_VARIANT,_u8_flt)
#include "test-preprocessing.h"

#define TEST_PREPROC_FLOAT float
#define TEST_PREPROC_PIXEL uint16_t
#define TEST_PREPROC_FUNC  JOIN3(preprocess_v,PREPROC_VARIANT,_u16_flt)
#include "test-preprocessing.h"

#define TEST_PREPROC_FLOAT double
#define TEST_PREPROC_PIXEL uint8_t
#define TEST_PREPROC_FUNC  JOIN3(preprocess_v,PREPROC_VARIANT,_u8_d)
#include "test-preprocessing.h"

#define TEST_PREPROC_FLOAT double
#define TEST_PREPROC_PIXEL uint16_t
#define TEST_PREPROC_FUNC  JOIN3(preprocess_v,PREPROC_VARIANT,_u16_d)
#include "test-preprocessing.h"

//-----------------------------------------------------------------------------

static double elapsed_seconds(
    const struct timespec* t,
    const struct timespec* t0)
{
    return ((double)(t->tv_sec  - t0->tv_sec ) +
            (double)(t->tv_nsec - t0->tv_nsec)*1e-9);
}

/**
 * Structure to collect time statistics data.
 */
typedef struct time_stat_data_ {
    double min, max, sum1, sum2;
    size_t numb;
} time_stat_data;

/**
 * Structure to store time statistics.
 */
typedef struct time_stat_ {
    double min, max, avg, std;
    size_t numb;
} time_stat;

/**
 * Reset time statistics data.
 *
 * Call this function before integrating time statistics with
 * time_stat_update().
 *
 * @param tsd  Pointer to time statistics data structure.
 */
static void time_stat_init(
    time_stat_data* tsd)
{
    tsd->min = DBL_MAX;
    tsd->max = 0;
    tsd->sum1 = 0;
    tsd->sum2 = 0;
    tsd->numb = 0;
}

/**
 * Integrate time statistics data.
 *
 * @param tsd  Pointer to time statistics data structure.
 * @param t    New time sample.
 */
static void time_stat_update(
    time_stat_data* tsd,
    double t)
{
    if (tsd->numb < 1) {
        tsd->min = t;
        tsd->max = t;
        tsd->sum1 = t;
        tsd->sum2 = t*t;
        tsd->numb = 1;
    } else {
        tsd->min = (t < tsd->min ? t : tsd->min);
        tsd->max = (t > tsd->max ? t : tsd->max);
        tsd->sum1 += t;
        tsd->sum2 += t*t;
        tsd->numb += 1;
    }
}

/**
 * Compute time statistics.
 *
 * @param ts   Pointer to destination time statistics structure.
 * @param tsd  Pointer to source time statistics data structure.
 *
 * @return The destination.
 */
static time_stat* time_stat_compute(
    time_stat* ts,
    time_stat_data const* tsd)
{
    ts->min = tsd->min;
    ts->max = tsd->max;
    if (tsd->numb >= 1) {
        ts->avg = tsd->sum1/tsd->numb;
        if (tsd->numb >= 2) {
            ts->std = sqrt((tsd->sum2 - ts->avg*tsd->sum1)/(tsd->numb - 1));
        } else {
            ts->std = 0;
        }
    } else {
        ts->avg = 0;
        ts->std = 0;
    }
    ts->numb = tsd->numb;
    return ts;
}

/**
 * Print time statistics.
 *
 * It is assumed that time samples are in seconds.
 *
 * @param out    Output file stream.
 * @param pfx    Suffix for each printed lines.
 * @param ts     Pointer to time statistics structure.
 */
static void time_stat_print(
    FILE* out,
    char const* pfx,
    time_stat const* ts)
{
    if (out == NULL) {
        out = stdout;
    }
    double const scl = 1E6; // print timings in microseconds
    fprintf(out, "%snumber of evaluations: %ld\n", pfx, ts->numb);
    fprintf(out, "%smin. time: %7.3f µs\n", pfx, ts->min*scl);
    fprintf(out, "%smax. time: %7.3f µs\n", pfx, ts->max*scl);
    fprintf(out, "%savg. time: %7.3f ± %.3f µs\n", pfx,
            ts->avg*scl, ts->std*scl);
}

//-----------------------------------------------------------------------------
//#define GET_TIMESPEC(ts) timespec_get(ts, TIME_UTC)
//#define GET_TIMESPEC(ts) clock_gettime(CLOCK_REALTIME, ts)
#define GET_TIMESPEC(ts) clock_gettime(CLOCK_MONOTONIC, ts)

static void print_results(
    FILE* output,
    char const* func,
    time_stat_data const* tsd,
    bool verbose)
{
    double const scl = 1E6; // print timings in microseconds
    time_stat ts;
    time_stat_compute(&ts, tsd);
    if (verbose) {
        fprintf(output, "Results for %s:\n", func);
        time_stat_print(output, "  ", &ts);
    } else {
        fprintf(output, "%25s %10.3f %10.3f %10.3f %10.3f\n",
                func, ts.min*scl, ts.max*scl,
                ts.avg*scl, ts.std*scl);
    }
    fflush(output);
}

typedef void preprocess(
    long width,
    long height,
    long stride,
    void* restrict wgt,
    void* restrict dat,
    void const* img,
    void const* a,
    void const* b,
    void const* q,
    void const* r);

static void run_tests(
    long width,
    long height,
    long stride,
    void* restrict wgt,
    void* restrict dat,
    void const* raw,
    void const* a,
    void const* b,
    void const* q,
    void const* r,
    preprocess* func,
    char const* funcname,
    double warmup,
    long repeat,
    FILE* out,
    bool verbose)
{
    // initialization
    time_stat_data tsd;
    time_stat_init(&tsd);

    // warm-up
    struct timespec t0;
    struct timespec t1;
    double elapsed = 0;
    GET_TIMESPEC(&t0);
    while (elapsed < warmup) {
        func(width, height, stride, wgt, dat, raw, a, b, q, r);
        GET_TIMESPEC(&t1);
        elapsed = elapsed_seconds(&t1, &t0);
    }

    // measurements
    for (long j = 0; j < repeat; ++j) {
        GET_TIMESPEC(&t0);
        func(width, height, stride, wgt, dat, raw, a, b, q, r);
        GET_TIMESPEC(&t1);
        time_stat_update(&tsd, elapsed_seconds(&t1, &t0));
    }
    print_results(out, funcname, &tsd, verbose);
}

int main(
    int argc,
    char* argv[argc+1])
{
    long width = 380;
    long height = 380;
    long stride = 400;
    double warmup = 3.0; // seconds
    long repeat = 10000;
    bool verbose = false;
    char dummy;
    double total_time = 0;
    int bits_per_pixel = 8;
    bool double_precision = false;
    char const* cpu_model = "unknown";

    for (int i = 1; i < argc; ++i) {
        int n;
        if (strncmp(argv[i], "--cpu=", 6) == 0) {
            cpu_model = argv[i] + 6;
            continue;
        }
        n = sscanf(argv[i], "--width=%ld %c", &width, &dummy);
        if (n > 0) {
            if (n != 1 || width < 1) {
                fprintf(stderr, "Invalid argument of --width option.\n");
                return EXIT_FAILURE;
            }
            continue;
        }
        n = sscanf(argv[i], "--height=%ld %c", &height, &dummy);
        if (n > 0) {
            if (n != 1 || height < 1) {
                fprintf(stderr, "Invalid argument of --height option.\n");
                return EXIT_FAILURE;
            }
            continue;
        }
        n = sscanf(argv[i], "--stride=%ld %c", &stride, &dummy);
        if (n > 0) {
            if (n != 1 || stride < 1) {
                fprintf(stderr, "Invalid argument of --stride option.\n");
                return EXIT_FAILURE;
            }
            continue;
        }
        n = sscanf(argv[i], "--warmup=%lf %c", &warmup, &dummy);
        if (n > 0) {
            if (n != 1 || warmup < 0) {
                fprintf(stderr, "Invalid argument of --warmup option.\n");
                return EXIT_FAILURE;
            }
            continue;
        }
        n = sscanf(argv[i], "--repeat=%ld %c", &repeat, &dummy);
        if (n > 0) {
            if (n != 1 || repeat < 1) {
                fprintf(stderr, "Invalid argument of --repeat option.\n");
                return EXIT_FAILURE;
            }
            continue;
        }
        n = sscanf(argv[i], "--bpp=%d %c", &bits_per_pixel, &dummy);
        if (n > 0) {
            if (n != 1 || (bits_per_pixel != 8 && bits_per_pixel != 16)) {
                fprintf(stderr, "Invalid argument of --bpp option.\n");
                return EXIT_FAILURE;
            }
            continue;
        }
        if (strcmp(argv[i], "--double") == 0) {
            double_precision = true;
            continue;
        }
        if (strcmp(argv[i], "--verbose") == 0) {
            verbose = true;
            continue;
        }
        if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
            printf("Usage: %s [OPTIONS]\n", argv[0]);
            printf("    Measure execution times of variants of image "
                   "pre-processing\n    methods.\n");
            printf("Options:\n");
            printf("  --cpu=MODEL      CPU model [%s]\n",
                   cpu_model);
            printf("  --width=WIDTH    Number of pixels per row [%ld]\n",
                   width);
            printf("  --height=HEIGHT  Number of pixel rows [%ld]\n",
                   height);
            printf("  --stride=STRIDE  Raw image stride [%ld]\n",
                   stride);
            printf("  --bpp=N          Bits per pixel [%d]\n",
                   bits_per_pixel);
            printf("  --warmup=SECS    Number of warmup seconds [%g]\n",
                   warmup);
            printf("  --repeat=REPEAT  Number of calls to measure [%ld]\n",
                   repeat);
            printf("  --double         Use double precision floats.\n");
            printf("  --verbose        Print results in verbose format.\n");
            printf("  --help, -h       Print this help.\n");
            return EXIT_SUCCESS;
        }
        if (strcmp(argv[i], "--") != 0 || i < argc - 1) {
            fprintf(stderr, "Unknown option or too many arguments.\n");
            return EXIT_FAILURE;
        }
    }
    if (stride < width) {
        fprintf(stderr, "Stride must be at least equal to width.\n");
        return EXIT_FAILURE;
    }
    size_t raw_size;
    switch (bits_per_pixel) {
    case 8:
        raw_size = stride*height;
        break;
    case 16:
        raw_size = stride*height*2;
        break;
    default:
        fprintf(stderr, "Invalid Unknown option or too many arguments.\n");
        return EXIT_FAILURE;
    }
    size_t npixels = width*height;
    size_t arr_size = npixels*(double_precision ? sizeof(double) :
                               sizeof(float));

    FILE* output = stdout;
    void* raw = malloc(raw_size);
    void* wgt = malloc(arr_size);
    void* dat = malloc(arr_size);
    void* A   = malloc(arr_size);
    void* B   = malloc(arr_size);
    void* Q   = malloc(arr_size);
    void* R   = malloc(arr_size);
    int status = EXIT_SUCCESS;
    if (raw == NULL || wgt == NULL || dat == NULL ||
	A == NULL || B == NULL || Q == NULL || R == NULL) {
        fprintf(stderr, "Insufficient memory\n");
        status = EXIT_FAILURE;
    }
    if (status != EXIT_SUCCESS) {
        goto done;
    }

    for (size_t i = 0; i < raw_size; ++i) {
        *((unsigned char*)raw + i) = i%256;
    }
    if (double_precision) {
        for (size_t i = 0; i < npixels; ++i) {
            *((double*)wgt + i) = 1;
            *((double*)dat + i) = 0;
            *((double*)A   + i) = 1;
            *((double*)B   + i) = 0;
            *((double*)Q   + i) = 1;
            *((double*)R   + i) = 0.01;
        }
    } else {
        for (size_t i = 0; i < npixels; ++i) {
            *((float*)wgt + i) = 1;
            *((float*)dat + i) = 0;
            *((float*)A   + i) = 1;
            *((float*)B   + i) = 0;
            *((float*)Q   + i) = 1;
            *((float*)R   + i) = 0.01;
        }
    }

    char* const hline = ("----------------------------------"
                         "----------------------------------");
    //printf("Results for %s:\n", "clock_gettime");
    //time_stat_print(output, "  ", &tm[0]);
    if (!verbose) {
        fprintf(output,
                "# Timings for pre-processing of acquired images.\n"
                "#\n"
                "#   Compiled with: %s\n"
                "#   Compiler version: %s\n"
                "#   CPU model: %s\n"
                "#   Floating-point type: %s\n"
                "#   Image width:     %ld pixels\n"
                "#   Image height:    %ld pixels\n"
                "#   Image stride:    %ld pixels\n"
                "#   Depth:           %d bits per pixel\n"
                "#   Warm-up:         %g seconds\n"
                "#   Samples:         %ld\n"
                "#\n",
                COMPILER_COMMAND,
                COMPILER_VERSION,
                cpu_model,
                (double_precision ? "double" : "float"),
                width, height, stride, bits_per_pixel, warmup, repeat);
        fprintf(output, "#%s\n#%s\n",
                "        Method             Min (µs)   Max (µs)   "
                "Avg (µs)   Std (µs)", hline);
        fflush(output);
    }
#define TEST(func) run_tests(width, height, stride, wgt, dat, raw, \
                             A, B, Q, R, (preprocess*)func, #func, \
                             warmup, repeat, output, verbose)
    if (bits_per_pixel == 8) {
        if (double_precision) {
            TEST(preprocess_v11_u8_flt);
            TEST(preprocess_v12_u8_flt);
            TEST(preprocess_v13_u8_flt);
            TEST(preprocess_v14_u8_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v21_u8_flt);
            TEST(preprocess_v22_u8_flt);
            TEST(preprocess_v23_u8_flt);
            TEST(preprocess_v24_u8_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v31_u8_flt);
            TEST(preprocess_v32_u8_flt);
            TEST(preprocess_v33_u8_flt);
            TEST(preprocess_v34_u8_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v41_u8_flt);
            TEST(preprocess_v42_u8_flt);
            TEST(preprocess_v43_u8_flt);
            TEST(preprocess_v44_u8_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v51_u8_flt);
            TEST(preprocess_v52_u8_flt);
            TEST(preprocess_v53_u8_flt);
            TEST(preprocess_v54_u8_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v61_u8_flt);
            TEST(preprocess_v62_u8_flt);
            TEST(preprocess_v63_u8_flt);
            TEST(preprocess_v64_u8_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v71_u8_flt);
            TEST(preprocess_v72_u8_flt);
            TEST(preprocess_v73_u8_flt);
            TEST(preprocess_v74_u8_flt);
        } else {
            TEST(preprocess_v11_u8_flt);
            TEST(preprocess_v12_u8_flt);
            TEST(preprocess_v13_u8_flt);
            TEST(preprocess_v14_u8_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v21_u8_flt);
            TEST(preprocess_v22_u8_flt);
            TEST(preprocess_v23_u8_flt);
            TEST(preprocess_v24_u8_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v31_u8_flt);
            TEST(preprocess_v32_u8_flt);
            TEST(preprocess_v33_u8_flt);
            TEST(preprocess_v34_u8_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v41_u8_flt);
            TEST(preprocess_v42_u8_flt);
            TEST(preprocess_v43_u8_flt);
            TEST(preprocess_v44_u8_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v51_u8_flt);
            TEST(preprocess_v52_u8_flt);
            TEST(preprocess_v53_u8_flt);
            TEST(preprocess_v54_u8_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v61_u8_flt);
            TEST(preprocess_v62_u8_flt);
            TEST(preprocess_v63_u8_flt);
            TEST(preprocess_v64_u8_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v71_u8_flt);
            TEST(preprocess_v72_u8_flt);
            TEST(preprocess_v73_u8_flt);
            TEST(preprocess_v74_u8_flt);
        }
    } else if (bits_per_pixel == 16) {
        if (double_precision) {
            TEST(preprocess_v11_u16_flt);
            TEST(preprocess_v12_u16_flt);
            TEST(preprocess_v13_u16_flt);
            TEST(preprocess_v14_u16_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v21_u16_flt);
            TEST(preprocess_v22_u16_flt);
            TEST(preprocess_v23_u16_flt);
            TEST(preprocess_v24_u16_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v31_u16_flt);
            TEST(preprocess_v32_u16_flt);
            TEST(preprocess_v33_u16_flt);
            TEST(preprocess_v34_u16_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v41_u16_flt);
            TEST(preprocess_v42_u16_flt);
            TEST(preprocess_v43_u16_flt);
            TEST(preprocess_v44_u16_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v51_u16_flt);
            TEST(preprocess_v52_u16_flt);
            TEST(preprocess_v53_u16_flt);
            TEST(preprocess_v54_u16_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v61_u16_flt);
            TEST(preprocess_v62_u16_flt);
            TEST(preprocess_v63_u16_flt);
            TEST(preprocess_v64_u16_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v71_u16_flt);
            TEST(preprocess_v72_u16_flt);
            TEST(preprocess_v73_u16_flt);
            TEST(preprocess_v74_u16_flt);
        } else {
            TEST(preprocess_v11_u16_flt);
            TEST(preprocess_v12_u16_flt);
            TEST(preprocess_v13_u16_flt);
            TEST(preprocess_v14_u16_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v21_u16_flt);
            TEST(preprocess_v22_u16_flt);
            TEST(preprocess_v23_u16_flt);
            TEST(preprocess_v24_u16_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v31_u16_flt);
            TEST(preprocess_v32_u16_flt);
            TEST(preprocess_v33_u16_flt);
            TEST(preprocess_v34_u16_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v41_u16_flt);
            TEST(preprocess_v42_u16_flt);
            TEST(preprocess_v43_u16_flt);
            TEST(preprocess_v44_u16_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v51_u16_flt);
            TEST(preprocess_v52_u16_flt);
            TEST(preprocess_v53_u16_flt);
            TEST(preprocess_v54_u16_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v61_u16_flt);
            TEST(preprocess_v62_u16_flt);
            TEST(preprocess_v63_u16_flt);
            TEST(preprocess_v64_u16_flt);
            if (!verbose) {
                fprintf(output, "#%s\n", hline);
            }
            TEST(preprocess_v71_u16_flt);
            TEST(preprocess_v72_u16_flt);
            TEST(preprocess_v73_u16_flt);
            TEST(preprocess_v74_u16_flt);
        }
    }
    if (!verbose) {
        fprintf(output, "#%s\n", hline);
    }

 done:
    free(raw);
    free(wgt);
    free(dat);
    free(A);
    free(B);
    free(Q);
    free(R);
    return status;
}
