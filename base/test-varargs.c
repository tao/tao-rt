// test-varargs.c -
//
// Benchmark efficiency of variable arguments functions.
//
// Results: No measureable differences between `snprintf` and `snprintf2` which
// use `va_list`.  Both takes about 330 ns (on average) to print a few numbers
// and a short strin (CPU: Intel Core i7-5500U at 2.40GHz).
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2021, Éric Thiébaut.

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

static int snprintf2(
    char* buf,
    size_t siz,
    const char* format,
    ...)
{
    int res;
    va_list args;
    va_start(args, format);
    res = vsnprintf(buf, siz, format, args);
    va_end(args);
    return res;
}

static struct timespec t0;

static void tic(void)
{
    clock_gettime(CLOCK_MONOTONIC, &t0);
}

static double toc(void)
{
    struct timespec t1;
    clock_gettime(CLOCK_MONOTONIC, &t1);
    return (t1.tv_sec - t0.tv_sec) + (t1.tv_nsec - t0.tv_nsec)*1E-9;
}

void benchmark(
    int (*printer)(
        char* buf,
        size_t siz,
        const char* format,
        ...),
    char* buffer,
    size_t size,
    long repeat,
    int ival,
    long lval,
    double dval,
    const char* str)
{
    // Evaluate printer.
    double t, tsum, tmin, tmax, tavg;
    tsum = tmin = tmax = 0;
    for (int k = 0; k < repeat; ++k) {
        tic();
        printer(buffer, size, "%d %ld %g \"%s\"", ival, lval, dval, str);
        t  = toc();
        if (k == 0) {
            tmin = tmax = t;
        } else {
            if (t < tmin) tmin = t;
            if (t > tmax) tmax = t;
        }
        tsum += t;
    }
    tavg = tsum/repeat;
    printf("  average time spent: %.3fns\n", tavg*1e9);
    printf("  minimum time spent: %.3fns\n", tmin*1e9);
    printf("  maximum time spent: %.3fns\n", tmax*1e9);
}

int main(
    int argc,
    char *argv[])
{
    const int repeat = 1000000;
    double dval = 3.1415;
    long lval = 1234567890;
    int ival = -786433;
    char buffer[2000];
    const char* str = "help me!";

    memset(buffer, 0, sizeof(buffer));
    printf("\nsnprintf:\n");
    benchmark(snprintf, buffer, sizeof(buffer), repeat, ival, lval, dval, str);
    printf("  >>>%s<<<\n", buffer);

    memset(buffer, 0, sizeof(buffer));
    printf("\nvsnprintf:\n");
    benchmark(snprintf2, buffer, sizeof(buffer), repeat, ival, lval, dval, str);
    printf("  >>>%s<<<\n", buffer);
    return 0;
}
