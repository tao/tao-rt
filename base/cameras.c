// cameras.c -
//
// Unified API for camera devices in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2018-2024, Éric Thiébaut.

#include <errno.h>
#include <math.h>
#include <stdio.h> // *before* <tao-errors.h>
#include <string.h>
#include <wchar.h>

#if HAVE_CONFIG_H
#  include "../config.h"
#endif

#include "tao-cameras-private.h"
#include "tao-errors.h"
#include "tao-generic.h"

//-----------------------------------------------------------------------------
// REGIONS OF INTEREST

tao_camera_roi* tao_camera_roi_copy(
    tao_camera_roi* dest,
    const tao_camera_roi* src)
{
    *dest = *src;
    return dest;
}

tao_camera_roi* tao_camera_roi_define(
    tao_camera_roi* roi,
    long xbin,
    long ybin,
    long xoff,
    long yoff,
    long width,
    long height)
{
    roi->xbin   = xbin;
    roi->ybin   = ybin;
    roi->xoff   = xoff;
    roi->yoff   = yoff;
    roi->width  = width;
    roi->height = height;
    return roi;
}

tao_status tao_camera_roi_check(
    const tao_camera_roi* roi,
    long sensorwidth,
    long sensorheight)
{
    // Check all bounds to avoid integer overflows in the 2 last tests.
    if (roi->xbin < 1 || roi->xbin > sensorwidth ||
        roi->ybin < 1 || roi->ybin > sensorheight ||
        roi->xoff < 0 || roi->xoff >= sensorwidth ||
        roi->yoff < 0 || roi->yoff >= sensorheight ||
        roi->width < 1 || roi->width > sensorwidth ||
        roi->height < 1 || roi->height > sensorheight ||
        roi->xoff + roi->xbin*roi->width > sensorwidth ||
        roi->yoff + roi->ybin*roi->height > sensorheight) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

int tao_camera_roi_compare(
    const tao_camera_roi* a,
    const tao_camera_roi* b)
{
    if ((a == NULL)|(b == NULL)) {
        return ((a != NULL)|(b != NULL));
    } else {
        return ((a->xbin != b->xbin)|(a->ybin != b->ybin)|
                (a->xoff != b->xoff)|(a->yoff != b->yoff)|
                (a->width != b->width)|(a->height != b->height));
    }
}

//-----------------------------------------------------------------------------
// CAMERA CONFIGURATION

static inline void get_monotonic_time(
    tao_time* t)
{
    if (tao_get_monotonic_time(t) != TAO_OK) {
        tao_panic();
    }
}

tao_eltype tao_fast_pixel_type(
    tao_preprocessing proc,
    tao_encoding enc)
{
    // Only monochrome images are supported.
    unsigned colorant = TAO_ENCODING_COLORANT(enc);
    if (colorant != TAO_COLORANT_MONO) {
    bad_encoding:
        tao_store_error(__func__, TAO_BAD_ENCODING);
        return -1;
    }
    if (proc == TAO_PREPROCESSING_NONE) {
        unsigned bits_per_pixel = TAO_ENCODING_BITS_PER_PIXEL(enc);
        if (bits_per_pixel <= 8) {
            return TAO_UINT8;
        } else if (bits_per_pixel <= 16) {
            return TAO_UINT16;
        } else if (bits_per_pixel <= 32) {
            return TAO_UINT32;
        } else {
            goto bad_encoding;
        }
    } else if (proc == TAO_PREPROCESSING_AFFINE ||
               proc == TAO_PREPROCESSING_FULL) {
        return TAO_FLOAT;
    } else {
        // Invalid pre-processing method.
        tao_store_error(__func__, TAO_BAD_PREPROCESSING);
        return -1;
    }
}

void tao_camera_config_initialize(
    tao_camera_config* cfg)
{
    if (cfg == NULL) {
        return;
    }
    memset(cfg, 0, sizeof(*cfg));
    tao_forced_store(&cfg->sensorwidth,  1);
    tao_forced_store(&cfg->sensorheight, 1);
    cfg->roi.xbin = 1;
    cfg->roi.ybin = 1;
    cfg->roi.xoff = 0;
    cfg->roi.yoff = 0;
    cfg->roi.width = 1;
    cfg->roi.height = 1;
    cfg->pixeltype = TAO_FLOAT;
    cfg->rawencoding = TAO_ENCODING_UNKNOWN;
    cfg->buffers = 4;
    cfg->framerate = 25.0; // 25 Hz
    cfg->exposuretime = 0.001; // 1 ms
    cfg->preprocessing = TAO_PREPROCESSING_NONE;
    get_monotonic_time(&cfg->origin);
    cfg->frames = 0;
    cfg->droppedframes = 0;
    cfg->overruns = 0;
    cfg->lostframes = 0;
    cfg->overflows = 0;
    cfg->lostsyncs = 0;
    cfg->timeouts = 0;
    // The rest of the structure is occupied the table of named attribute.
    size_t attr_table_size =
        sizeof(tao_camera_config) - offsetof(tao_camera_config, attr_table);
    tao_attr_table_set_size(&cfg->attr_table, attr_table_size);
}

void tao_camera_config_copy(
    tao_camera_config* dst,
    const tao_camera_config* src)
{
    if (src != NULL && dst != NULL) {
        memcpy(dst, src, sizeof(*dst));
    }
}

static int print_encoding(
    FILE* out,
    const char* name,
    tao_encoding enc,
    const char* bad)
{
    char buf[TAO_ENCODING_STRING_SIZE];
    if (tao_format_encoding(buf, enc) != TAO_OK) {
        tao_clear_error(NULL);
        return fprintf(out, "%s: %s\n", name, bad);
    } else {
        int bpp = TAO_ENCODING_BITS_PER_PIXEL(enc);
        return fprintf(out, "%s: %s (bits per pixel: %d)\n", name, buf, bpp);
    }
}

tao_status tao_camera_config_print(
    FILE* out,
    const tao_camera_config* cfg)
{
    if (cfg == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (out == NULL) {
        out = stdout;
    }
    char buf[64];
    if (0 > fprintf(out, "Sensor size: %ld × %ld pixels\n",
                    cfg->sensorwidth, cfg->sensorheight) ||
        0 > fprintf(out, "Pixel binning: %ld×%ld\n",
                    cfg->roi.xbin, cfg->roi.ybin) ||
        0 > fprintf(out, "Region of interest: %ld×%ld at (%ld,%ld)\n",
                    cfg->roi.width, cfg->roi.height,
                    cfg->roi.xoff, cfg->roi.yoff) ||
        0 > print_encoding(out, "Encoding of raw images", cfg->rawencoding,
                           "IllegalEncoding") ||
        0 > print_encoding(out, "Pixel type",
                           tao_encoding_of_eltype(cfg->pixeltype),
                           "IllegalType") ||
        0 > fprintf(out, "Number of acquisition buffers: %ld\n",
                    cfg->buffers) ||
        0 > fprintf(out, "Frame rate: %g Hz\n", cfg->framerate) ||
        0 > fprintf(out, "Exposure time: %.3f ms\n", 1E3*cfg->exposuretime) ||
        0 > fprintf(out, "Origin of time: %s s\n",
                    tao_time_sprintf(buf, TAO_TIME_FORMAT_FRACTIONAL_SECONDS,
                                     &cfg->origin)) ||
        0 > fprintf(out, "Number of acquired frames: %" PRId64 "\n",
                    cfg->frames) ||
        0 > fprintf(out, "Number of dropped frames: %" PRId64 "\n",
                    cfg->droppedframes) ||
        0 > fprintf(out, "Number of overruns: %" PRId64 "\n",
                    cfg->overruns) ||
        0 > fprintf(out, "Number of lost frames: %" PRId64 "\n",
                    cfg->lostframes) ||
        0 > fprintf(out, "Number of overflows: %" PRId64 "\n",
                    cfg->overflows) ||
        0 > fprintf(out, "Number of lost synchronizations: %" PRId64 "\n",
                    cfg->lostsyncs) ||
        0 > fprintf(out, "Number of timeouts: %" PRId64 "\n",
                    cfg->timeouts)) {
        tao_store_system_error("fprintf");
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Encode accessors for clients.
#define ACCESSOR(type, prefix, member, def)                     \
    type tao_camera_config_get_##member(                        \
        const tao_camera_config* cfg)                           \
    {                                                           \
        return (cfg != NULL ? cfg->prefix member : (def));      \
    }
ACCESSOR(long,             , sensorwidth,    0)
ACCESSOR(long,             , sensorheight,   0)
ACCESSOR(tao_time,         , origin,         TAO_UNKNOWN_TIME)
ACCESSOR(tao_serial,       , frames,         0)
ACCESSOR(tao_serial,       , droppedframes,  0)
ACCESSOR(tao_serial,       , overruns,       0)
ACCESSOR(tao_serial,       , lostframes,     0)
ACCESSOR(tao_serial,       , overflows,      0)
ACCESSOR(tao_serial,       , lostsyncs,      0)
ACCESSOR(tao_serial,       , timeouts,       0)
ACCESSOR(long,         roi., xbin,           0)
ACCESSOR(long,         roi., ybin,           0)
ACCESSOR(long,         roi., xoff,           0)
ACCESSOR(long,         roi., yoff,           0)
ACCESSOR(long,         roi., width,          0)
ACCESSOR(long,         roi., height,         0)
ACCESSOR(double,            , framerate,     0.0)
ACCESSOR(double,            , exposuretime,  0.0)
ACCESSOR(tao_eltype,        , pixeltype,     TAO_UINT8)
ACCESSOR(tao_encoding,      , rawencoding,   TAO_ENCODING_UNKNOWN)
ACCESSOR(tao_preprocessing, , preprocessing, TAO_PREPROCESSING_NONE)
#undef ACCESSOR

// Encode mutators for clients.
#define MUTATOR(type, ptr, prefix, member)          \
    void tao_camera_config_set_##member(            \
        tao_camera_config* cfg, type ptr val)       \
    {                                               \
        cfg->prefix member = ptr val;               \
    }
MUTATOR(const tao_time,   *,     , origin)
MUTATOR(tao_serial,        ,     , frames)
MUTATOR(tao_serial,        ,     , droppedframes)
MUTATOR(tao_serial,        ,     , overruns)
MUTATOR(tao_serial,        ,     , lostframes)
MUTATOR(tao_serial,        ,     , overflows)
MUTATOR(tao_serial,        ,     , lostsyncs)
MUTATOR(tao_serial,        ,     , timeouts)
MUTATOR(long,              , roi., xbin)
MUTATOR(long,              , roi., ybin)
MUTATOR(long,              , roi., xoff)
MUTATOR(long,              , roi., yoff)
MUTATOR(long,              , roi., width)
MUTATOR(long,              , roi., height)
MUTATOR(double,            ,     , framerate)
MUTATOR(double,            ,     , exposuretime)
MUTATOR(tao_eltype,        ,     , pixeltype)
MUTATOR(tao_encoding,      ,     , rawencoding)
MUTATOR(tao_preprocessing, ,     , preprocessing)
#undef MUTATOR

//-----------------------------------------------------------------------------
// NAMED ATTRIBUTES
//
// NOTE: The idea is to not expose the attribute table, so we only provide API
//       to retrieve the number of attributes, to retrieve an attribute by
//       index, and to find an attribute by key.
//
// NOTE: Functions only return a read-only attribute pointer. By convention,
//       the owner of the resource is allowed to cast it a non-`const` pointer
//       to change the attribute value.

long tao_camera_get_attr_number(
    const tao_camera* cam)
{
    return cam == NULL ? 0 : cam->config.attr_table.length;
}

long tao_camera_config_get_attr_number(
    const tao_camera_config* cfg)
{
    return cfg == NULL ? 0 : cfg->attr_table.length;
}

const tao_attr* tao_camera_get_attr(
    const tao_camera* cam,
    long index)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    return tao_attr_table_get_entry(&cam->config.attr_table, index);
}

const tao_attr* tao_camera_config_get_attr(
    const tao_camera_config* cfg,
    long index)
{
    if (cfg == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    return tao_attr_table_get_entry(&cfg->attr_table, index);
}

const tao_attr* tao_camera_unsafe_get_attr(
    const tao_camera* cam,
    long index)
{
    return tao_attr_table_unsafe_get_entry(&cam->config.attr_table, index);
}

const tao_attr* tao_camera_config_unsafe_get_attr(
    const tao_camera_config* cfg,
    long index)
{
    return tao_attr_table_unsafe_get_entry(&cfg->attr_table, index);
}

long tao_camera_try_find_attr(
    const tao_camera* cam,
    const char* key)
{
    return cam == NULL ? -2 : tao_attr_table_try_find_entry(&cam->config.attr_table, key);
}

long tao_camera_config_try_find_attr(
    const tao_camera_config* cfg,
    const char* key)
{
    return cfg == NULL ? -2 : tao_attr_table_try_find_entry(&cfg->attr_table, key);
}

const tao_attr* tao_camera_find_attr(
    const tao_camera* cam,
    const char* key)
{
    if (cam == NULL || key == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    return tao_attr_table_find_entry(&cam->config.attr_table, key);
}

const tao_attr* tao_camera_config_find_attr(
    const tao_camera_config* cfg,
    const char* key)
{
    if (cfg == NULL || key == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    return tao_attr_table_find_entry(&cfg->attr_table, key);
}

//-----------------------------------------------------------------------------
// UNIFIED CAMERAS

static void invalid_runlevel(
    tao_camera* cam,
    const char* func)
{
    int code;
    switch (cam->runlevel) {
     case 0:
        code = TAO_NOT_READY;
        break;
     case 1:
        code = TAO_NOT_ACQUIRING;
        break;
     case 2:
        code = TAO_ACQUISITION_RUNNING;
        break;
     case 3:
        code = TAO_MUST_RESET;
        break;
    case 4:
        code = TAO_UNRECOVERABLE;
        break;
    default:
        code = TAO_CORRUPTED;
    }
    tao_store_error(func, code);
}

tao_camera* tao_camera_create(
    const tao_camera_ops* ops,
    void* ctx,
    size_t size)
{
    // Check that all virtual methods have a valid address.
    if (NULL == ops                ||
        NULL == ops->initialize    ||
        NULL == ops->finalize      ||
        NULL == ops->reset         ||
        NULL == ops->update_config ||
        NULL == ops->check_config  ||
        NULL == ops->set_config    ||
        NULL == ops->start         ||
        NULL == ops->stop          ||
        NULL == ops->wait_buffer) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    if (size < sizeof(tao_camera)) {
        size = sizeof(tao_camera);
    }
    tao_camera* cam = tao_malloc(size);
    if (cam == NULL) {
        return NULL;
    }
    memset(cam, 0, size);
    if (tao_mutex_initialize(&cam->mutex, TAO_PROCESS_PRIVATE) != TAO_OK) {
        goto error1;
    }
    if (tao_condition_initialize(&cam->cond, TAO_PROCESS_PRIVATE) != TAO_OK) {
       goto error2;
    }
    cam->ops = ops;
    tao_camera_config_initialize(&cam->config);
    cam->runlevel = 0;
    if (cam->ops->initialize(cam, ctx) != TAO_OK) {
        goto error3;
    }
    cam->runlevel = 1;
    return cam;

    // Branch here in case of errors.
 error3:
    pthread_cond_destroy(&cam->cond);
 error2:
    pthread_mutex_destroy(&cam->mutex);
 error1:
    free(cam);
    return NULL;
}

tao_status tao_camera_destroy(
    tao_camera* cam)
{
    tao_status status = TAO_OK;
    if (cam != NULL) {
        // Lock the camera.
        status = tao_mutex_lock(&cam->mutex);
        bool locked = (status == TAO_OK);

        // Stop acquisition if acquiring (ignoring errors).
        if (cam->runlevel == 2) {
            if (cam->ops->stop(cam) != TAO_OK) {
                status = TAO_ERROR;
            }
            cam->runlevel = 1;
        }

        // Call virtual "finalize" method.
        if (cam->runlevel < 4) {
            if (cam->ops->finalize(cam) != TAO_OK) {
                status = TAO_ERROR;
            }
            cam->runlevel = 4;
        }

        // Destroy condition variable.
        if (tao_condition_destroy(&cam->cond) != TAO_OK) {
            status = TAO_ERROR;
        }

        // Unlock the camera and immediately destroy its mutex.
        if (locked && tao_mutex_unlock(&cam->mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
        if (tao_mutex_destroy(&cam->mutex, true) != TAO_OK) {
            status = TAO_ERROR;
        }

        // Eventually release memory storing the instance.
        free((void*)cam);
    }
    return status;
}

tao_status tao_camera_lock(
    tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_mutex_lock(&cam->mutex);
}

tao_status tao_camera_try_lock(
    tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_mutex_try_lock(&cam->mutex);
}

tao_status tao_camera_unlock(
    tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_mutex_unlock(&cam->mutex);
}

tao_status tao_camera_signal(
    tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_signal(&cam->cond);
}

tao_status tao_camera_broadcast(
    tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_broadcast(&cam->cond);
}

tao_status tao_camera_wait(
    tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_wait(&cam->cond, &cam->mutex);
}

tao_status tao_camera_abstimed_wait(
    tao_camera* cam,
    const tao_time* abstime)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_abstimed_wait(&cam->cond, &cam->mutex, abstime);
}

tao_status tao_camera_timed_wait(
    tao_camera* cam,
    double secs)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_timed_wait(&cam->cond, &cam->mutex, secs);
}

tao_state tao_camera_get_state(
    const tao_camera* cam)
{
    if (cam != NULL) {
        switch (cam->runlevel) {
        case 0:
            return TAO_STATE_INITIALIZING;
        case 1:
            return TAO_STATE_WAITING;
        case 2:
            return TAO_STATE_WORKING;
        case 3:
            return TAO_STATE_ERROR;
        }
    }

    // Assume unrecoverable error.
    return TAO_STATE_UNREACHABLE;
}

tao_status tao_camera_start_acquisition(
    tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    switch (cam->runlevel) {
    case 1:
        // Allocate acquisition buffers, clear events and manage to have next
        // acquisition buffer information at index 0.
        if (cam->config.buffers < 2) {
            tao_store_error(__func__, TAO_BAD_BUFFERS);
            return TAO_ERROR;
        }

        // Call the "start" virtual method.  In case of success, update the
        // run-level and report success.  In case of errors, the run-level is
        // left unchanged but the virtual method is allowed to set it to a more
        // suitable value.
        if (cam->ops->start(cam) != TAO_OK) {
            return TAO_ERROR;
        }
        cam->runlevel = 2;
        return TAO_OK;

    case 2:
        // Camera is acquiring, nothing to do.
        return TAO_OK;

    default:
        // All other cases are hopeless!
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }
}

tao_status tao_camera_stop_acquisition(
    tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    switch (cam->runlevel) {
    case 1:
        // Camera is not acquiring, nothing to do.
        return TAO_OK;

    case 2:
        // Call the "stop" virtual method.  In case of success, update the
        // run-level and report success.  In case of errors, the run-level is
        // left unchanged but the virtual method is allowed to set it to a more
        // suitable value.
        if (cam->ops->stop(cam) != TAO_OK) {
            return TAO_ERROR;
        }
        cam->runlevel = 1;
        return TAO_OK;

    default:
        // All other cases are hopeless!
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }
}

tao_status tao_camera_wait_acquisition_buffer(
    tao_camera* cam,
    tao_acquisition_buffer* buf,
    double secs,
    int drop)
{

    // Check acquisition buffer and zero-fill it to prevent caller to use
    // buffer contents in case of errors.
    if (buf == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    memset(buf, 0, sizeof(*buf));

    // Check timeout value and whether we are acquiring (whether waiting for
    // images is implemented is checked later).
    if (isnan(secs) || secs < 0) {
        tao_store_error(__func__, TAO_OUT_OF_RANGE);
        return TAO_ERROR;
    }
    if (cam->runlevel != 2) {
        tao_store_error(__func__, TAO_NOT_ACQUIRING);
        return TAO_ERROR;
    }

    // Call `wait_buffer` virtual method.
    return cam->ops->wait_buffer(cam, buf, secs, drop);
}

tao_status tao_camera_reset(
    tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    switch (cam->runlevel) {
    case 1:
        // Camera is not acquiring, nothing else to do.
        return TAO_OK;

    case 2:
        // Resetting is equivalent to stopping acquisition.
        return tao_camera_stop_acquisition(cam);

    case 3:
        // Recoverable error. Attempt to reset the camera, then update its
        // configuration.
        if (cam->ops->reset(cam) != TAO_OK || cam->runlevel != 1) {
            cam->runlevel = 4;
            tao_store_error(__func__, TAO_UNRECOVERABLE);
            return TAO_ERROR;
        }
        return cam->ops->update_config(cam);

    default:
        // All other cases are hopeless!
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }
}

static tao_status check_configuration(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    if (tao_camera_configuration_check_preprocessing(cfg) != TAO_OK) {
        return TAO_ERROR;
    }
    if (cam->ops->check_config(cam, cfg) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status tao_camera_check_configuration(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    if (cam == NULL || cfg == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return check_configuration(cam, cfg);
}

tao_status tao_camera_configuration_check_preprocessing(
    const tao_camera_config* cfg)
{
    int code = TAO_SUCCESS;
    int nbits = TAO_ENCODING_BITS_PER_PIXEL(cfg->rawencoding);
    if (cfg->preprocessing == TAO_PREPROCESSING_NONE) {
        if ((cfg->pixeltype == TAO_UINT8  && nbits <=  8) ||
            (cfg->pixeltype == TAO_UINT16 && nbits <= 16) ||
            (cfg->pixeltype == TAO_UINT32 && nbits <= 32) ||
            (cfg->pixeltype == TAO_FLOAT                ) ||
            (cfg->pixeltype == TAO_DOUBLE               )) {
            return TAO_OK;
        }
        code = TAO_BAD_PIXELTYPE;
    } else if (cfg->preprocessing == TAO_PREPROCESSING_AFFINE ||
               cfg->preprocessing == TAO_PREPROCESSING_FULL) {
        if (cfg->pixeltype == TAO_FLOAT ||
            cfg->pixeltype == TAO_DOUBLE) {
            return TAO_OK;
        }
        code = TAO_BAD_PIXELTYPE;
    } else {
        code = TAO_BAD_PREPROCESSING;
    }
    tao_store_error(__func__, code);
    return TAO_ERROR;
}

// Update settings from hardware.
tao_status tao_camera_update_configuration(
    tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (cam->runlevel != 1) {
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }
    if (cam->ops->update_config(cam) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status tao_camera_get_configuration(
    const tao_camera* src,
    tao_camera_config* dst)
{
    if (src == NULL || dst == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    memcpy(dst, &src->config, sizeof(*dst));
    return TAO_OK;
}

tao_status tao_camera_set_configuration(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    if (cam == NULL || cfg == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (cam->runlevel != 1) {
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }
    if (check_configuration(cam, cfg) != TAO_OK) {
        return TAO_ERROR;
    }
    if (cam->ops->set_config(cam, cfg) != TAO_OK) {
        return TAO_ERROR;
    }
    if (cam->ops->update_config(cam) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status tao_camera_set_origin_of_time(
    tao_camera* cam,
    const tao_time* orig)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_time now;
    if (orig == NULL) {
        get_monotonic_time(&now);
        orig = &now;
    }
    cam->config.origin = *orig;
    return TAO_OK;
}

const tao_time* tao_camera_get_origin_of_time(
    tao_time* orig,
    const tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    if (orig == NULL) {
        return &cam->config.origin;
    } else {
        *orig = cam->config.origin;
        return orig;
    }
}

#define ENCODE(w)                                               \
    double tao_get_camera_elapsed_##w(                          \
        const tao_camera* cam,                                  \
        const tao_time* t)                                      \
    {                                                           \
        if (cam == NULL) {                                      \
            tao_store_error(__func__, TAO_BAD_ADDRESS);         \
            return NAN;                                         \
        }                                                       \
        tao_time dt;                                            \
        if (t == NULL) {                                        \
            get_monotonic_time(&dt);                            \
            t = &dt;                                            \
        }                                                       \
        return tao_time_to_##w(                                 \
            tao_time_subtract(&dt, t, &cam->config.origin));    \
    }
ENCODE(seconds);
ENCODE(milliseconds);
ENCODE(microseconds);
ENCODE(nanoseconds);
#undef ENCODE
