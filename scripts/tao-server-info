#! /bin/bash
#
# tao-server-info -
#
# Shell script to print information about a TAO camera server.
#
#------------------------------------------------------------------------------
#
# This file if part of TAO real-time software licensed under the MIT license
# (https://git-cral.univ-lyon1.fr/tao/tao-rt).
#
# Copyright (C) 2019-2021, Éric Thiébaut.
#

# Deal with numerical conversions.
export LANG=C
export LC_NUMERIC=C

progname=$(basename "$0")
if test "$progname" = ""; then
    progname="tao-server-info"
fi

function help() {
    cat <<EOF

Usage: $progname [OPTIONS...] [--] APT

Print information about camera server at access point APT.

Options are:

    -h, --help   Print this help.

EOF
    exit 0
}

function die() {
    echo >&2 "$progname: $*"
    exit 1
}

opt=yes
while test $# -gt 0; do
    arg=$1
    case "$arg" in
        -h | --help )
            help
            ;;
        -- )
            shift
            opt=no
            ;;
        -* )
            die "Invalid option \"$arg\""
            ;;
        * )
            opt=no
            ;;
    esac
    if test $opt = yes; then
        shift
    else
        break
    fi
done
if test $# -gt 1; then
    die "Too many arguments (use \"--help\" option for usage)"
elif test $# -lt 1; then
    die "Too few arguments (use \"--help\" option for usage)"
fi
APT="$1"
case "$APT" in
    *:* ) ;;
    *   ) APT="TAO:$APT";;
esac

echo "Server access point: $APT"

shmid=$(xpaget "$APT" shmid)
echo "Shared memory identifier: $shmid"

sensorwidth=$(xpaget "$APT" sensorwidth)
sensorheight=$(xpaget "$APT" sensorheight)
echo "Sensor size: $sensorwidth × $sensorheight"

xbin=$(xpaget "$APT" xbin)
ybin=$(xpaget "$APT" ybin)
xoff=$(xpaget "$APT" xoff)
yoff=$(xpaget "$APT" yoff)
width=$(xpaget "$APT" width)
height=$(xpaget "$APT" height)
echo "Pixel binning: $xbin × $ybin"
echo "Image size: $width × $height"
xmin=$xoff
xmax=$(($xoff + $width - 1))
ymin=$yoff
ymax=$(($yoff + $height - 1))
echo "Region of interest: [$xmin:$xmax] × [$ymin:$ymax]"

state=$(xpaget "$APT" state)
echo "Camera state: $state"

suspended=$(xpaget "$APT" suspended)
echo "Acquisition suspended: $suspended"

framerate=$(xpaget "$APT" framerate)
echo "Acquisition frame rate: $framerate Hz"

exposuretime=$(xpaget "$APT" exposuretime)
exposuretime=$(echo "1000*$exposuretime" | bc)
printf "Exposure time: %.3f ms\n" "$exposuretime"

buffers=$(xpaget "$APT" buffers)
echo "Number of acquisition buffers: $buffers"

bufferencoding=$(xpaget "$APT" bufferencoding)
echo "Encoding of acquisition buffers: $bufferencoding"

sensorencoding=$(xpaget "$APT" sensorencoding)
echo "Encoding of sensor pixels: $sensorencoding"

pixeltype=$(xpaget "$APT" pixeltype)
echo "Encoding of captured images: $pixeltype"

drop=$(xpaget "$APT" drop)
echo "Drop frames: $drop"

debuglevel=$(xpaget "$APT" debuglevel)
echo "Debug-level: $debuglevel"

# Get the "ping" time (in milliseconds).
t0=$(xpaget "$APT" ping)
t1=$(xpaget "$APT" ping)
ping=$(echo "($t1 - $t0)*1000/2" | bc)
printf "Server response time (ping): %.3f ms\n" "$ping"
