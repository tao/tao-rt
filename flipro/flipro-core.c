// flipro-core.h -
//
// Implementation of the TAO (a Toolkit for Adaptive Optics) interface to
// Finger Lakes Instrumentation cameras via the LIBFLIPRO SDK.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).

#include <stdint.h>
#include <stdio.h>
#include <math.h>

#include "libflipro.h"

#include "tao-flipro-cameras.h" // for the prototype of tao_flipro_camera_open
#include "tao-threads.h" // for mutexes
#include "tao-errors.h" // for error management
#include "tao-generic.h"
#include "tao-cameras-private.h" // for the definition of tao_camera

// Assumed maximum number of connected cameras.
#define MAX_CONNECTED_CAMERAS 8

// List of available cameras and mutex to protect access to this list.
FPRODEVICEINFO device_list[MAX_CONNECTED_CAMERAS];
long ndevices = 0;
tao_mutex mutex = TAO_MUTEX_INITIALIZER;

// Retrieve list of connected devices.  Caller must have locked the resource.
static tao_status update_device_list();

// A handle fo a camera (see <libflipro.h>).
typedef int32_t flipro_handle;

// Number of acquisition buffers.
#define NBUFS 2

typedef struct flipro_camera_ {
    tao_camera   base;///< A FLIPRO camera is also a TAO camera.
    flipro_handle handle;///< Handle to device.
    size_t     bufsiz;///< Size of acquisition buffers.
    void* bufs[NBUFS];///< Acquisition buffers.
    int       lastbuf;///< Index of last acquisition buffer.
} flipro_camera;

// Function to deal with errors.
static void flipro_error(const char* func, int code);

// All functions of the LIBFLIPRO SDK return either nothing (`void`) or an
// integer code which is nonnegative on success and negative on error.  The
// following macro is a helper to report errors with the name of the function
// from the LIBFLIPRO SDK.
#define FLIPRO_CALL(func, args, on_error)       \
    do {                                        \
        int code = func args;                   \
        if (code < 0) {                         \
            flipro_error(#func, code);          \
            on_error;                           \
        }                                       \
    } while (0)

// Update exposure time and frame rate from current settings.
static tao_status update_exposure(
    flipro_camera* fcam);

// Set the exposure time (in seconds) and the frame rate (in frames per
// second).
static tao_status set_exposure(
    flipro_camera* fcam,
    double exposuretime,
    double framerate);

// Update the pixel binning from current settings.
static tao_status update_binning(
    flipro_camera* fcam);

// Set the pixel binning to given settings.
static tao_status set_binning(
    flipro_camera* fcam,
    long xbin,
    long ybin);

// Update the image area from current settings.
static tao_status update_image_area(
    flipro_camera* fcam);

 // Set the the image area to given settings.
static tao_status set_image_area(
    flipro_camera* fcam,
    long xoff,
    long yoff,
    long width,
    long height);

// Free all acquisition buffers.
static void free_buffers(
    flipro_camera* fcam);

//-----------------------------------------------------------------------------
// Declaration of private methods and of the table grouping these methods for
// the unified camera API.

static tao_status on_initialize(
    tao_camera* cam,
    void* ptr);

static tao_status on_finalize(
    tao_camera* cam);

static tao_status on_reset(
    tao_camera* cam);

static tao_status on_update_config(
    tao_camera* cam);

static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_start(
    tao_camera* cam);

static tao_status on_stop(
    tao_camera* cam);

static tao_status on_wait_buffer(
    tao_camera* cam,
    tao_acquisition_buffer* buf,
    double secs,
    int drop);

static tao_camera_ops ops = {
    .name          = "FLIPROCamera",
    .initialize    = on_initialize,
    .finalize      = on_finalize,
    .reset         = on_reset,
    .update_config = on_update_config,
    .check_config  = on_check_config,
    .set_config    = on_set_config,
    .start         = on_start,
    .stop          = on_stop,
    .wait_buffer   = on_wait_buffer
};

//-----------------------------------------------------------------------------
// Definition of the public function to connect to a Finger Lakes
// Instrumentation camera with the LIBFLIPRO library.

tao_camera* tao_flipro_camera_open(
    int num)
{
    // First lock global resources.
    if (tao_mutex_lock(&mutex) != TAO_OK) {
        return NULL;
    }

    // Initialize result to NULL in case of error.
    tao_camera* cam = NULL;

    // Update list of devices.
    if (update_device_list() != TAO_OK) {
        goto unlock;
    }

    // Check whether camera index is in range.
    if (num < 0 || num >= ndevices) {
        tao_store_error(__func__, TAO_BAD_DEVICE);
        goto unlock;
    }

    // Allocate camera instance with the device information provided as
    // contextual data.  The remaining initialization will done by the
    // `on_initialize` virtual method.
    cam = tao_camera_create(&ops, &device_list[num], sizeof(flipro_camera));

    // Eventually unlock global resources.
unlock:
    if (tao_mutex_unlock(&mutex) != TAO_OK) {
        // Failed to unlock resources, destroy the camera if created.
        if (cam != NULL) {
            tao_camera_destroy(cam);
            cam = NULL;
        }
    }
    return cam;
}

static tao_status on_initialize(
    tao_camera* cam,
    void* ptr)
{
    // The camera is a FLIPRO camera.
    flipro_camera* fcam = (flipro_camera*)cam;

    // The contextual data is the device information.
    FPRODEVICEINFO* devinfo = ptr;

    // No acquisition buffers yet.
    fcam->bufsiz = 0;
    for (int i = 0; i < NBUFS; ++i) {
        fcam->bufs[i] = NULL;
    }

    // Open the camera.
    fcam->handle = -1;
    FLIPRO_CALL(FPROCam_Open,(devinfo, &fcam->handle), return TAO_ERROR);
    if (fcam->handle < 0) {
        tao_store_error(__func__, TAO_BAD_DEVICE);
        return TAO_ERROR;
    }

    // Retrieve camera capabilities.
    FPROCAP cap;
    uint32_t size = sizeof(cap);
    FLIPRO_CALL(FPROSensor_GetCapabilities,(fcam->handle, &cap, &size),
                return TAO_ERROR);

    tao_forced_store(&cam->config.sensorwidth, cap.uiMaxPixelImageWidth);
    tao_forced_store(&cam->config.sensorheight, cap.uiMaxPixelImageHeight);
    return TAO_OK;
}

static tao_status on_finalize(
    tao_camera* cam)
{
    // The camera is a FLIPRO camera.
    flipro_camera* fcam = (flipro_camera*)cam;

    // Free all acquisition buffers.
    free_buffers(fcam);

    // Close the device.
    if (fcam->handle >= 0) {
        flipro_handle handle = fcam->handle;
        fcam->handle = -1; // never close more than once
        FLIPRO_CALL(FPROCam_Close,(handle), return TAO_ERROR);
    }
    return TAO_OK;
}

static tao_status on_reset(
    tao_camera* cam)
{
    tao_store_error(__func__, TAO_NOT_YET_IMPLEMENTED);
    return TAO_ERROR;
}

static tao_status on_update_config(
    tao_camera* cam)
{
    // The camera is a FLIPRO camera.
    flipro_camera* fcam = (flipro_camera*)cam;

    if (update_binning(fcam) != TAO_OK ||
        update_image_area(fcam) != TAO_OK ||
        update_exposure(fcam) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    // Name of caller for error messages.
    const char* func = "tao_camera_check_configuration";

    // Assume success for now
    int code = TAO_SUCCESS;

    // Check region of interest.
    if (tao_camera_roi_check(&cfg->roi,
                             cam->config.sensorwidth,
                             cam->config.sensorheight) != TAO_OK) {
        code = TAO_BAD_ROI;
        goto error;
    }

    // Check exposure time and frame rate.
    if (isnan(cfg->exposuretime) || isinf(cfg->exposuretime) ||
        cfg->exposuretime < 0) {
        code = TAO_BAD_EXPOSURETIME;
        goto error;
    }
    if (isnan(cfg->framerate) || isinf(cfg->framerate) ||
        cfg->framerate <= 0) {
        code = TAO_BAD_FRAMERATE;
        goto error;
    }

    // All parameters seem to be acceptable.
    return TAO_OK;

    // Errors branch here.
error:
    tao_store_error(func, code);
    return TAO_ERROR;
}

// Apply confiConfiguration.  Configuration has already been checked.
static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    // The camera is a FLIPRO camera.
    flipro_camera* fcam = (flipro_camera*)cam;

    // Do we need to change the frame rate?
    bool reduce_fps = (cfg->framerate < cam->config.framerate);
    bool augment_fps = (cfg->framerate > cam->config.framerate);
    bool change_exposure = (cfg->exposuretime != cam->config.exposuretime);

    // Reduce frame rate if possible before changing ROI.
    if (reduce_fps) {
        if (set_exposure(fcam, cfg->exposuretime, cfg->framerate) != TAO_OK) {
            return TAO_ERROR;
        }
        change_exposure = false;
    }

    // Set binning.
    if (cfg->roi.xbin != cam->config.roi.xbin ||
        cfg->roi.ybin != cam->config.roi.ybin) {
        if (set_binning(fcam, cfg->roi.xbin, cfg->roi.ybin)  != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Set image area.
    if (cfg->roi.xoff != cam->config.roi.xoff ||
        cfg->roi.yoff != cam->config.roi.yoff ||
        cfg->roi.width != cam->config.roi.width ||
        cfg->roi.height != cam->config.roi.height) {
        if (set_image_area(fcam, cfg->roi.xoff, cfg->roi.yoff,
                           cfg->roi.width, cfg->roi.height)  != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Augment frame rate after changing image area.
    if (augment_fps || change_exposure) {
        if (set_exposure(fcam, cfg->exposuretime, cfg->framerate) != TAO_OK) {
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

static tao_status on_start(
    tao_camera* cam)
{
    // The camera is a FLIPRO camera.
    flipro_camera* fcam = (flipro_camera*)cam;

    // Compute image size in bytes.
    int size = FPROFrame_ComputeFrameSize(fcam->handle);
    if (size < 0) {
        flipro_error("FPROFrame_ComputeFrameSize", size);
        return TAO_ERROR;
    }

    // Allocates acquisition buffers.
    if (size != fcam->bufsiz) {
        // Free all acquisition buffers.
        free_buffers(fcam);
        fcam->bufsiz = size;
    }
    for (int i = 0; i < NBUFS; ++i) {
        if (fcam->bufs[i] == NULL) {
            fcam->bufs[i] = tao_malloc(size);
            if (fcam->bufs[i] == NULL) {
                return TAO_ERROR;
            }
        }
    }
    fcam->lastbuf = NBUFS - 1;

    // Start image capture.
    FLIPRO_CALL(FPROFrame_CaptureStart,(fcam->handle, 0), return TAO_ERROR);
    return TAO_OK;
}

static tao_status on_stop(
    tao_camera* cam)
{
    // The camera is a FLIPRO camera.
    flipro_camera* fcam = (flipro_camera*)cam;

    // Start image capture.
    FLIPRO_CALL(FPROFrame_CaptureStop,(fcam->handle), return TAO_ERROR);
    return TAO_OK;
}

static tao_status on_wait_buffer(
    tao_camera* cam,
    tao_acquisition_buffer* buf,
    double secs,
    int drop)
{
    // NOTE: Call FPROFrame_GetVideoFrame() or FPROFrame_GetVideoFrameEx() with
    //       a secondary thread that may call FPROFrame_CaptureAbort() to
    //       cancel the acquisition.
    tao_store_error(__func__, TAO_NOT_YET_IMPLEMENTED);
    return TAO_ERROR;
}

//-----------------------------------------------------------------------------
// UTILITIES.

// Retrieve list of connected cameras.  Caller must have locked the resource.
static tao_status update_device_list()
{
    uint32_t num = MAX_CONNECTED_CAMERAS;
    FLIPRO_CALL(FPROCam_GetCameraList, (device_list, &num), return TAO_ERROR);
    if (num >= MAX_CONNECTED_CAMERAS) {
        fprintf(stderr,
                "Warning: There may be more than %d connected cameras.  You "
                "should change\n        the value of macro "
                "`MAX_CONNECTED_CAMERAS` in file `%s`\n",
                MAX_CONNECTED_CAMERAS, __FILE__);
    }
    ndevices = num;
    return TAO_OK;
}

#define NANOSECONDS_PER_SECOND 1e9

static void store_exposure(
    flipro_camera* fcam,
    uint64_t exposuretime_ns,
    uint64_t framedelay_ns)
{
    fcam->base.config.exposuretime =
        exposuretime_ns/NANOSECONDS_PER_SECOND;
    fcam->base.config.framerate =
        NANOSECONDS_PER_SECOND/(exposuretime_ns
                                + framedelay_ns);
}

// Update exposure time and frame rate from current settings.
static tao_status update_exposure(
    flipro_camera* fcam)
{
    uint64_t exposuretime_ns, framedelay_ns;
    bool immediate;
    for (int pass = 1; pass <= 2; ++pass) {
        FLIPRO_CALL(FPROCtrl_GetExposure,(fcam->handle, &exposuretime_ns,
                                          &framedelay_ns, &immediate),
                 return TAO_ERROR);
        if (pass == 2 || !immediate) {
            break;
        }
        FLIPRO_CALL(FPROCtrl_SetExposure,(fcam->handle, exposuretime_ns,
                                          framedelay_ns, false),
                 return TAO_ERROR);
    }
    if (immediate) {
        fputs("Warning: Failed to set `immediate` to `false` in "
              "FPROCtrl_SetExposure.\n", stderr);
    }
    store_exposure(fcam, exposuretime_ns, framedelay_ns);
    return TAO_OK;
}

// Set the exposure time (in seconds) and the frame rate (in frames per
// second).
static tao_status set_exposure(
    flipro_camera* fcam,
    double exposuretime,
    double framerate)
{
    // In the SDK, the exposure time and the frame delay are in nanoseconds.
    uint64_t exposuretime_ns = lround(exposuretime*NANOSECONDS_PER_SECOND);
    uint64_t framedelay_ns = lround(NANOSECONDS_PER_SECOND/framerate)
        - exposuretime_ns;
    uint64_t actual_exposuretime_ns;
    uint64_t actual_framedelay_ns;
    FLIPRO_CALL(FPROCtrl_SetExposureEx,
                (fcam->handle, exposuretime_ns, framedelay_ns, false,
                 &actual_exposuretime_ns, &actual_framedelay_ns),
                return TAO_ERROR);
    store_exposure(fcam, actual_exposuretime_ns, actual_framedelay_ns);
    return TAO_OK;
}

// Update the pixel binning from current settings.
static tao_status update_binning(
    flipro_camera* fcam)
{
    uint32_t xbin, ybin;
    FLIPRO_CALL(FPROSensor_GetBinning,(fcam->handle, &xbin, &ybin),
                return TAO_ERROR);
    fcam->base.config.roi.xbin = xbin;
    fcam->base.config.roi.ybin = ybin;
    return TAO_OK;
}

// Set the pixel binning to given current settings.
static tao_status set_binning(
    flipro_camera* fcam,
    long xbin,
    long ybin)
{
    FLIPRO_CALL(FPROSensor_SetBinning,(fcam->handle, xbin, ybin),
                return TAO_ERROR);
    return update_binning(fcam);
}

// Update the image area from current settings.
static tao_status update_image_area(
    flipro_camera* fcam)
{
    uint32_t xoff, yoff, width, height;
    FLIPRO_CALL(FPROFrame_GetImageArea,
                (fcam->handle, &xoff, &yoff, &width, &height),
                return TAO_ERROR);
    fcam->base.config.roi.xoff = xoff;
    fcam->base.config.roi.yoff = yoff;
    fcam->base.config.roi.width = width;
    fcam->base.config.roi.height = height;
    return TAO_OK;
}

// Set the image area to given current settings.
static tao_status set_image_area(
    flipro_camera* fcam,
    long xoff,
    long yoff,
    long width,
    long height)
{
    FLIPRO_CALL(FPROFrame_SetImageArea,
                (fcam->handle, xoff, yoff, width, height), return TAO_ERROR);
    return update_image_area(fcam);
}

// Free all acquisition buffers.
static void free_buffers(
    flipro_camera* fcam)
{
    for (int i = 0; i < NBUFS; ++i) {
        void* buf = fcam->bufs[i];
        fcam->bufs[i] = NULL;
        free(buf);
    }
    fcam->bufsiz = 0;
}

//-----------------------------------------------------------------------------
// ERROR MANAGEMENT.

static void get_error_info(
    int code,
    const char** reason_ptr,
    const char** info_ptr)
{
    // For now, we do not provide any additional error information.  TAO will
    // simply reports the error code and the name of the function where the
    // error occurred.
    if (reason_ptr != NULL) {
        *reason_ptr = NULL;
    }
    if (info_ptr != NULL) {
        *info_ptr = NULL;
    }
}

static void flipro_error(
    const char* func,
    int code)
{
    tao_store_other_error(func, code, get_error_info);
}
