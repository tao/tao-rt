// tao-flipro-cameras.h -
//
// Declarations of the TAO (a Toolkit for Adaptive Optics) interface to Finger
// Lakes Instrumentation cameras via the LIBFLIPRO SDK.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).

#ifndef TAO_FLIPRO_CAMERAS_H_
#define TAO_FLIPRO_CAMERAS_H_ 1

#include <tao-cameras.h>

TAO_BEGIN_DECLS

/// @defgroup FliCameras Interface to Finger Lakes Instrumentation cameras
///                      (LIBFLIPRO).
///
/// @ingroup Cameras
///
/// @brief Interface to Finger Lakes Instrumentation cameras via the LIBFLIPRO
/// library.
///
/// @{

/// @brief Open a Fli camera.
///
/// This function connects to the Fli camera identified by `id` and returns
/// a handle to manage it.  The caller is responsible of eventually calling
/// `tao_camera_destroy` to close the camera device and release associated
/// resources.
///
/// @param num   Number of the camera.
///
/// @return The address of the new (initialized) camera; `NULL` in case of
///         failure.
///
extern tao_camera* tao_flipro_camera_open(int num);

/// @}

TAO_END_DECLS

#endif // TAO_FLIPRO_CAMERAS_H_
