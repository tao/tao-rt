// pitt-server.c -
//
// Server for the Physical Instruments Tip-Tilt in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2023, Éric Thiébaut, Clément Schotte.

#include <math.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>

#include "tao-basics.h"
#include "tao-layouts.h"
#include "tao-remote-mirrors-private.h"
#include "tao-generic.h"
#include "tao-errors.h"

static char* progname = "pitt-server";
static char error_buffer[50];

/* connect_to_device( ipaddr )
* Connects to the Physical Instruments Tip Tilt and retrieves the handle to it.
* char* ipaddr : IP address of the device
* returns int sfd : Sockels File Descriptor
* returns -1 in case of error
*/
static int connect_to_device(const char* ipaddr)
{
    // Open the driver.
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int sfd=-1; //Socket File Descriptor
    int rv;     //Return Value
    char* port = "50000";       // Physical Instruments Tip-tilt always uses
                                // port number 50000.

    /* Obtain address(es) matching host/port. */
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;          /* Any protocol */

    rv = getaddrinfo(ipaddr, port, &hints, &result);
    if (rv != 0) {
        sprintf(error_buffer, "%s: getaddrinfo", progname);
        tao_store_other_error(error_buffer, rv, NULL);
        //FIXME: Include callback to gai_strerror(rv) to get error message
        //instead of NULL.
        goto error;
    }

    /* getaddrinfo() returns a list of address structures.
       Try each address until we successfully connect(2).
       If socket(2) (or connect(2)) fails, we (close the socket
       and) try the next address. */

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype,
                    rp->ai_protocol);
        if (sfd == -1)
            continue;

        if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1)
            break;                  /* Success */

        close(sfd);
    }

    freeaddrinfo(result);           /* No longer needed */

    if (rp == NULL) {               /* No address succeeded */
        sprintf(error_buffer, "%s: connect_to_device", progname);
        tao_store_error(error_buffer, TAO_NOT_FOUND);
        goto error;
    }
    return sfd;
error:
    if (sfd != -1) {
        close(sfd);
    }
    return -1;
}

// Context for the `on_send` callback.
typedef struct context_ {
    int sfd;
    char buf[256];
} context;

/* send_command( sfd, *cmd, len )
* Sends the command cmd to the device described in sfd.
* int sfd : Socket File Descriptor of the device. Obtained with connect_to_device
* const char* cmd : Command to send. Uses the syntax defined in the device documentation. Must end with \n to send command.
* long len : length of the command to send. Automatically calculated if < 0.
* returns tao_status : either TAO_OK or TAO_ERROR
*/
static tao_status send_command(int sfd, const char* cmd, long len)
{
    if (len < 0) {
        len = tao_strlen(cmd);
    }

    if (write(sfd, cmd, len) != len) {
        tao_store_system_error("write");
        return TAO_ERROR;
    }
    return TAO_OK;
}

/* open_loop( sfd )
* Opens the loop of the Physical Instruments Tip Tilt by sending the appropriate command.
* Opening the loop means deactivating the feedback loop the devices uses internally to perfect its position.
* int sfd : Socket File Descriptor of the device. Obtained with connect_to_device
* returns tao_status : either TAO_OK or TAO_ERROR
*/
static tao_status open_loop(int sfd)
{
    // Axis 3 servomotor should never be set to 1. It is not a real axis.
    // Also keep servomotors to 0 when device is not in use.
    return send_command(sfd, "SVO 1 0 2 0 3 0\n", -1);
}

/* on_send( *rdm, data, *cmds )
* Sends the requested command.  On entry, `cmds` contains the `rdm->nacts`
* actuators commands.  On return, `cmds` contains the actuators commands
* modified to account for the deformable mirror limitations.
* returns tao_status : either TAO_OK or TAO_ERROR
*/
static tao_status on_send(
    tao_remote_mirror* rdm,
    void* data,
    double* cmds)
{
    // Context is the address of the deformable mirror driver (hardware).
    context* ctx = data;
    long len = snprintf(ctx->buf,
            sizeof(ctx->buf),
            "SVA 1 %g 2 %g\n",
            cmds[0], cmds[1]);

    if (len<=0 || len>=sizeof(ctx->buf)) {
        sprintf(error_buffer, "%s: on_send", progname);
        tao_store_other_error(error_buffer, TAO_OVERWRITTEN, NULL);
        return TAO_ERROR;
    }
    return send_command(ctx->sfd, ctx->buf, len);
}

int main(
    int argc,
    char* argv[])
{
    // Determine program name.
    const char* progname = tao_basename(argv[0]);

    // Parse arguments.  First options, then positional arguments.
    const char* usage = "Usage: %s [OPTIONS ...] [--] SERIAL NAME\n";
    const char* ipaddr = NULL; // IP address of device
    const char* name = NULL;  // server name
    bool debug = false;
    long nbufs = 10000;
    unsigned int orient = 0;
    unsigned int perms = 0077;
    int iarg = 0;
    while (++iarg < argc) {
        char dummy;
        if (argv[iarg][0] != '-') {
            // Argument is not an option.
            --iarg;
            break;
        }
        if (argv[iarg][1] == '-' && argv[iarg][2] == '\0') {
            // Argument is last option.
            break;
        }
        if (strcmp(argv[iarg], "-h") == 0
            || strcmp(argv[iarg], "-help") == 0
            || strcmp(argv[iarg], "--help") == 0) {
            printf(usage, progname);
            printf("\n");
            printf("Arguments:\n");
            printf("  IPADDR             Internet address of device.\n");
            printf("  NAME               Name of server.\n");
            printf("\n");
            printf("Options:\n");
            printf("  -nbufs NBUFS       Number of frame buffers [%ld].\n",
                   nbufs);
            printf("  -orient ORIENT     Orientation of layout [%u].\n",
                   orient);
            printf("  -perms PERMS       Bitwise mask of permissions [0%o].\n",
                   perms);
            printf("  -debug             Debug mode [%s].\n",
                   (debug ? "true" : "false"));
            printf("  -h, -help, --help  Print this help.\n");
            return EXIT_SUCCESS;
        }
        if (strcmp(argv[iarg], "-nbufs") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option `%s`.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%ld %c", &nbufs, &dummy) != 1
                || nbufs < 2) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option `%s`.\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-orient") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option `%s`.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%u %c", &orient, &dummy) != 1) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option `%s`.\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            orient &= 5;
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-perms") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option `%s`.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%u %c", &perms, &dummy) != 1) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option `%s`.\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            perms &= 0777;
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-debug") == 0) {
            debug = true;
            continue;
        }
        fprintf(stderr, "%s: Unknown option `%s`.\n", progname, argv[iarg]);
        return EXIT_FAILURE;
    }
    if (++iarg >= argc) {
        fprintf(stderr, "%s: Missing IP address.\n", progname);
        goto bad_usage;
    }
    ipaddr = argv[iarg];
    if (++iarg >= argc) {
        fprintf(stderr, "%s: Missing server name.\n", progname);
    bad_usage:
        fputc('\n', stderr);
        fprintf(stderr, usage, progname);
        fputc('\n', stderr);
        return EXIT_FAILURE;
    }
    name = argv[iarg];
    if (++iarg < argc) {
        fprintf(stderr, "%s: Too many arguments.\n", progname);
        goto bad_usage;
    }

    // Code returned on exit.
    int exitcode = EXIT_SUCCESS;

    // Device information
    long inds[] = {0, 1};
    long dim1 = 2;
    long dim2 = 1;
    double cmin = -2000;
    double cmax = 8000;

    //Connect to device
    int sfd=connect_to_device(ipaddr);
    if (sfd == -1) {
        goto error;
    }

    //Setting Servo Axis
    if (open_loop(sfd) != TAO_OK) {
        tao_report_error();
        goto error;
    }

    // Allocate the remote mirror instance.
    tao_remote_mirror* rdm = tao_remote_mirror_create(
        name, nbufs, inds, dim1, dim2, cmin, cmax, perms);
    if (rdm == NULL) {
        fprintf(stderr, "%s: Failed to create remote mirror instance\n",
                progname);
        tao_report_error();
        goto error;
    }

    // Run server loop
    context ctx = { .sfd=sfd };
    tao_remote_mirror_operations ops = {
        .on_send = on_send,
        .name = name,
        .debug = debug
    };
    tao_status status = tao_remote_mirror_run_loop(rdm, &ops, &ctx);
    if (status != TAO_OK) {
        tao_report_error();
        goto error;
    }

    // Undo everything.
done:
    if (sfd != -1) {

        //Setting Servo Axis
        if (open_loop(sfd) != TAO_OK) {
            tao_report_error();
            exitcode = EXIT_FAILURE;
        }

        //Close the socket
        if (close(sfd) != 0) {
            tao_store_system_error("close");
            tao_report_error();
            exitcode = EXIT_FAILURE;
        }
    }

    return exitcode;

    // We branch here in case of errors.
error:
    exitcode = EXIT_FAILURE;
    goto done;
}
