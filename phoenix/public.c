// public.c -
//
// Interface to ActiveSilicon Phoenix frame grabber.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2017-2024, Éric Thiébaut.
// Copyright (c) 2016, Éric Thiébaut & Jonathan Léger.

#include <math.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "tao-phoenix-private.h"
#include "tao-errors.h"
#include "tao-threads.h"

// If macro `BUFFER_FULL_IMAGE` is set, all the pixels sent by the camera are
// stored in the acquisition buffers and any cropping will be done when
// processing images.
#define BUFFER_FULL_IMAGE 1

// If macro `NULLIFY_RELEASED_BUFFERS` is true, the data address of an
// acquisition buffer is set to `NULL` when the buffer is released.
#define NULLIFY_RELEASED_BUFFERS 0

static void invalid_runlevel(
    tao_camera* cam,
    const char* func);

static void acquisition_callback(
    tHandle handle,
    uint32_t events,
    void* data);

static tao_status check_regions_of_interest(
    const tao_camera_roi* img,
    const tao_camera_roi* dev,
    long sensorwidth,
    long sensorheight);

static tao_status stop_acquisition(
    phnx_camera* pcam);

static tao_status connect_camera(
    phnx_camera* pcam);

static tao_status disconnect_camera(
    phnx_camera* pcam);

//-----------------------------------------------------------------------------
// TABLE OF KNOWN CAMERAS

static phnx_operations const * const known_cameras[] = {
    &phnx_mikrotron_mc408x,
    NULL
};

//-----------------------------------------------------------------------------
// TABLE OF OPERATIONS

// Early declarations of all virtual methods in to initialize table of
// operations.
static tao_status on_initialize(
    tao_camera* cam,
    void* ptr);

static tao_status on_finalize(
    tao_camera* cam);

static tao_status on_reset(
    tao_camera* cam);

static tao_status on_update_config(
    tao_camera* cam);

static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_start(
    tao_camera* cam);

static tao_status on_stop(
    tao_camera* cam);

static tao_status on_wait_buffer(
    tao_camera* cam,
    tao_acquisition_buffer* buf,
    double secs,
    int drop);

// Table of operations of all Phoenix cameras.  It address can be used to
// assert that a given TAO camera belongs to this family.
static tao_camera_ops ops = {
    .name          = "Phoenix",
    .initialize    = on_initialize,
    .finalize      = on_finalize,
    .reset         = on_reset,
    .update_config = on_update_config,
    .check_config  = on_check_config,
    .set_config    = on_set_config,
    .start         = on_start,
    .stop          = on_stop,
    .wait_buffer   = on_wait_buffer
};

//-----------------------------------------------------------------------------
// CREATE PHOENIX CAMERA INSTANCE

#undef USE_CONFIG_MODE

tao_camera* phnx_create_camera(
    void (*handler)(const char*,
                    int,
                    const char*),
    char* configname,
    const phnx_create_options* opts)
{
    // Check assumptions.
    assert(sizeof(ui8)  == 1);
    assert(sizeof(ui16) == 2);
    assert(sizeof(ui32) == 4);
    assert(sizeof(ui64) == 8);
    assert(sizeof(float32_t) == 4);
    assert(sizeof(float64_t) == 8);
    assert(sizeof(etParamValue) == sizeof(int));
    assert(sizeof(etStat) == sizeof(int));
    assert(sizeof(float32_t) == 4);
    assert(sizeof(float64_t) == 8);
    assert(sizeof(((phnx_camera*)0)->vendor) > CXP_DEVICE_VENDOR_NAME_LENGTH);
    assert(sizeof(((phnx_camera*)0)->model) > CXP_DEVICE_MODEL_NAME_LENGTH);

    // Default options if none specified.
    if (opts == NULL) {
        static phnx_create_options default_options =
            PHNX_CREATE_OPTIONS_DEFAULT;
        opts = &default_options;
    }

    // Check read delay.
    if (!isfinite(opts->coaxpress_read_delay) || opts->coaxpress_read_delay < 0.0) {
        fprintf(stderr, "%s: Invalid value %g seconds for CoaXPress read delay\n",
                __func__, opts->coaxpress_read_delay);
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return NULL;
    }

    // Parse open options.
    etParamValue boardnumber;
    switch (opts->boardnumber) {
    case PHNX_BOARD_NUMBER_AUTO:
        boardnumber = PHX_BOARD_NUMBER_AUTO;
        break;
    case PHNX_BOARD_NUMBER_1:
        boardnumber = PHX_BOARD_NUMBER_1;
        break;
    case PHNX_BOARD_NUMBER_2:
        boardnumber = PHX_BOARD_NUMBER_2;
        break;
    case PHNX_BOARD_NUMBER_3:
        boardnumber = PHX_BOARD_NUMBER_3;
        break;
    case PHNX_BOARD_NUMBER_4:
        boardnumber = PHX_BOARD_NUMBER_4;
        break;
    case PHNX_BOARD_NUMBER_5:
        boardnumber = PHX_BOARD_NUMBER_5;
        break;
    case PHNX_BOARD_NUMBER_6:
        boardnumber = PHX_BOARD_NUMBER_6;
        break;
    case PHNX_BOARD_NUMBER_7:
        boardnumber = PHX_BOARD_NUMBER_7;
        break;
    default:
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return NULL;
    }
    etParamValue channelnumber;
    switch (opts->channelnumber) {
    case PHNX_CHANNEL_NUMBER_AUTO:
        channelnumber = PHX_CHANNEL_NUMBER_AUTO;
        break;
    case PHNX_CHANNEL_NUMBER_1:
        channelnumber = PHX_CHANNEL_NUMBER_1;
        break;
    case PHNX_CHANNEL_NUMBER_2:
        channelnumber = PHX_CHANNEL_NUMBER_2;
        break;
    default:
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return NULL;
    }
    etParamValue configmode;
    switch (opts->configmode) {
    case PHNX_CONFIG_MODE_NORMAL:
        configmode = PHX_CONFIG_NORMAL;
        break;
    case PHNX_CONFIG_MODE_COMMS_ONLY:
        configmode = PHX_CONFIG_COMMS_ONLY;
        break;
    case PHNX_CONFIG_MODE_ACQ_ONLY:
        configmode = PHX_CONFIG_ACQ_ONLY;
        break;
    default:
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return NULL;
    }

    // Create a Phoenix handle.
    phx_error_handler *error_handler;
    if (handler != NULL) {
        assert(sizeof(etStat) == sizeof(int));
        error_handler = (phx_error_handler*)handler;
    } else {
        error_handler = phx_default_error_handler;
    }
    tHandle handle;
    etStat code = PHX_Create(&handle, error_handler);
    if (code != PHX_OK) {
        phx_error("PHX_Create", code);
        return NULL;
    }

    // Set the configuration file name.
    if (configname != NULL && configname[0] != '\0') {
        code = PHX_ParameterSet(handle, PHX_CONFIG_FILE, &configname);
        if (code != PHX_OK) {
            phx_error("PHX_ParameterSet(PHX_CONFIG_FILE)", code);
            goto error;
        }
    }

    // Set the board number.
    code = PHX_ParameterSet(handle, PHX_BOARD_NUMBER, &boardnumber);
    if (code != PHX_OK) {
        phx_error("PHX_ParameterSet(PHX_BOARD_NUMBER)", code);
        goto error;
    }

    // Set the channel number.
    code = PHX_ParameterSet(handle, PHX_CHANNEL_NUMBER, &channelnumber);
    if (code != PHX_OK) {
        phx_error("PHX_ParameterSet(PHX_CHANNEL_NUMBER)", code);
        goto error;
    }

    // Set the configuration mode.  In our case, setting this parameter was
    // blocking, so we only do that if a non-default option is chosen.
    if (configmode != PHX_CONFIG_NORMAL) {
        code = PHX_ParameterSet(handle, PHX_CONFIG_MODE, &configmode);
        if (code != PHX_OK) {
            phx_error("PHX_ParameterSet(PHX_CONFIG_MODE)", code);
            goto error;
        }
    }

    // Allocate camera instance with the configured (but not yet open) handle.
    // The remaining initialization will done by the `on_initialize` callback.
    tao_camera* cam = tao_camera_create(&ops, &handle, sizeof(phnx_camera));
    if (cam == NULL) {
        goto error;
    }
    phnx_camera* pcam = (phnx_camera*)cam;
    pcam->coaxpress.read_delay = opts->coaxpress_read_delay;
    return cam;

 error:
    // Destroy Phoenix handle.
    (void)PHX_Destroy(&handle);
    return NULL;
}

//-----------------------------------------------------------------------------
// VIRTUAL METHODS

// Allocate/free cyclic list of acquisition buffer information.
static tao_status manage_buffer_info_list(
    phnx_camera* pcam,
    long buffers)
{
    if (pcam->info == NULL) {
        pcam->buffers = 0;
    }
    if (pcam->buffers != buffers) {
        if (pcam->info != NULL) {
            tao_free(pcam->info);
        }
        pcam->buffers = 0;
        pcam->info = NULL;
        if (buffers > 0) {
            size_t size = buffers*sizeof(phnx_buffer_info);
            pcam->info = tao_malloc(size);
            if (pcam->info == NULL) {
                return TAO_ERROR;
            }
            memset(pcam->info, 0, size);
            pcam->buffers = buffers;
        }
    }
    return TAO_OK;
}

// Perform the final initialization of a Phoenix camera.  Device handle has
// been created and connected.  Remaining task is to identify the camera
// model.  This virtual method is only called once during the lifetime of the
// camera instance.
static tao_status on_initialize(
    tao_camera* cam,
    void* ptr)
{
    fprintf(stderr, "%s (in)\n", __func__);
    assert(cam->ops == &ops);
    phnx_camera* pcam = (phnx_camera*)cam;
    pcam->handle = *(tHandle*)ptr;
    pcam->is_coaxpress = false;
    pcam->coaxpress.swap = false;

    // Connect camera device.
    if (connect_camera(pcam) != TAO_OK) {
        return TAO_ERROR;
    }

    // Choose suitable pixel type for processed images.
    cam->config.pixeltype = tao_fast_pixel_type(
        cam->config.preprocessing,
        cam->config.rawencoding);
    if (cam->config.pixeltype == -1) {
        cam->config.pixeltype = TAO_FLOAT;
        tao_clear_error(NULL);
    }

    fprintf(stderr, "%s (out)\n", __func__);
    return TAO_OK;
}

// Finalize a "Phoenix" device.  Stop acquisition, close the Phoenix board and
// destroy the Phoenix handle.  No errors are reported (unless in debug mode).
static tao_status on_finalize(
    tao_camera* cam)
{
    assert(cam != NULL);
    assert(cam->ops == &ops);
    tao_status status = TAO_OK;
    phnx_camera* pcam = (phnx_camera*)cam;
    if (cam->runlevel == 2) {
        if (stop_acquisition(pcam) != TAO_OK) {
            status = TAO_ERROR;
        }
        cam->runlevel = 1;
    }
    if (cam->runlevel >= 1 && cam->runlevel < 4) {
        if (disconnect_camera(pcam) != TAO_OK) {
            status = TAO_ERROR;
        }
        cam->runlevel = 4;
    }
    manage_buffer_info_list(pcam, 0);
    assert(sizeof(pcam->handle) >= sizeof(tHandle));
    tHandle handle = pcam->handle;
    etStat code = PHX_Destroy(&handle);
    pcam->handle = handle;
    if (code != PHX_OK) {
        phx_error("PHX_Destroy", code);
        status = TAO_ERROR;
    }
    return status;
}

// Manage to revert to run-level 1.  Should only be called when run-level is 3
// (recoverable error).
static tao_status on_reset(
    tao_camera* cam)
{
    // Disconnect, then reconnect the camera.
    assert(cam->ops == &ops);
    assert(cam->runlevel == 3);
    phnx_camera* pcam = (phnx_camera*)cam;
    if (disconnect_camera(pcam) != TAO_OK ||
        connect_camera(pcam) != TAO_OK) {
        cam->runlevel = 4;
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status on_update_config(
    tao_camera* cam)
{
    // Camera has to be a "Phoenix" camera and must not be acquiring.
    assert(cam->ops == &ops);
    assert(cam->runlevel == 1);
    phnx_camera* pcam = (phnx_camera*)cam;

    // Force the updating of all updatable settings.
    if (pcam->ops->update_config == NULL) {
        tao_store_error(__func__, TAO_UNSUPPORTED);
        return TAO_ERROR;
    }
    if (pcam->ops->update_config(pcam, false) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Check the validity of a configuration.  Directly check the pixel encoding
// and the number of acquisition buffers and let the `check_config` callback
// deal with the other settings.  FIXME: The camera must be locked while
// checking the configuration.  Acquisition may however be running because
// checking does not change anything.
static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    // Camera has to be a "Phoenix" camera.
    assert(cam->ops == &ops);
    phnx_camera* pcam = (phnx_camera*)cam;

    // Check pixel processing parameters.  FIXME: Already done?
    if (tao_camera_configuration_check_preprocessing(cfg) != TAO_OK) {
        return TAO_ERROR;
    }

    // Check pixel encoding of raw acquired images.
    if (phnx_best_buffer_encoding(cfg->rawencoding) == TAO_ENCODING_UNKNOWN) {
        tao_store_error(__func__, TAO_BAD_ENCODING);
        return TAO_ERROR;
    }

    // Call virtual method with specific settings identical to the current
    // ones.
    if (pcam->ops->check_config == NULL) {
        tao_store_error(__func__, TAO_UNSUPPORTED);
        return TAO_ERROR;
    }
    if (pcam->ops->check_config(pcam, cfg) != TAO_OK) {
        return TAO_ERROR;
    }

    // Everything seems fine.
    return TAO_OK;
}

// Attempt to apply the new configuration.  Directly set the pixel encoding and
// the number of acquisition buffers and let the `set_config` callback deal
// with the other settings.  Camera must not be acquiring.  The configuration
// has already been checked.
static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    // Camera has to be a "Phoenix" camera and must not be acquiring.
    assert(cam->ops == &ops);
    assert(cam->runlevel == 1);
    phnx_camera* pcam = (phnx_camera*)cam;

    // Set number of acquisition buffers.
    if (cfg->buffers < 2) {
        tao_store_error(__func__, TAO_BAD_BUFFERS);
        return TAO_ERROR;
    }
    cam->config.buffers = cfg->buffers;

    // Call virtual method with specific settings identical to the current
    // ones.
    if (pcam->ops->set_config == NULL) {
        tao_store_error(__func__, TAO_UNSUPPORTED);
        return TAO_ERROR;
    }
    if (pcam->ops->set_config(pcam, cfg) != TAO_OK) {
        return TAO_ERROR;
    }

    // Set pixel processing parameters (they have already been checked).
    cam->config.preprocessing = cfg->preprocessing;
    cam->config.pixeltype = cfg->pixeltype;

    return TAO_OK;
}

// Virtual method to start acquisition.  This method is only called after
// initialization and if the camera is not acquiring.
static tao_status on_start(
    tao_camera* cam)
{
    // Assert that argument is a Phoenix camera at run-level 1 ("waiting") and
    // that a minimal number of acquisition buffers have been allocated.
    assert(cam->ops == &ops);
    assert(cam->runlevel == 1);
    phnx_camera* pcam = (phnx_camera*)cam;

    // Name of the caller for reporting errors.
    const char* func = "tao_camera_start_acquisition";

    // Check the compatibility of images and device regions of interest.
    const tao_camera_config* cfg = &cam->config;
    if (check_regions_of_interest(&cfg->roi,
                                  &pcam->roi,
                                  cam->config.sensorwidth,
                                  cam->config.sensorheight) != TAO_OK) {
        tao_store_error(func, TAO_BAD_ROI);
        return TAO_ERROR;
    }

    // Check pixel encoding of acquired images.
    tao_encoding rawencoding = cfg->rawencoding;
    tao_encoding bufencoding = phnx_best_buffer_encoding(rawencoding);
    if (bufencoding == TAO_ENCODING_UNKNOWN) {
        tao_store_error(func, TAO_BAD_ENCODING);
        return TAO_ERROR;
    }

    // Determine the size of pixels in acquisition buffers.
    long bitsperpixel = TAO_ENCODING_BITS_PER_PIXEL(bufencoding);
    long pixelsize = (bitsperpixel + 7) >> 3;

    // Set the parameters describing the encoding of pixels sent by the camera
    // and the encoding of pixels in the acquisition buffers.
    if (phx_define_camera_encoding(pcam, rawencoding) != TAO_OK ||
        phx_define_buffer_encoding(pcam, bufencoding) != TAO_OK) {
        return TAO_ERROR;
    }

    // Use at least 4 acquisition buffers.
    long buffers = cam->config.buffers;
    if (buffers < 4) {
        buffers = 4;
        cam->config.buffers = buffers;
    }

    // Allocate cyclic list of acquisition buffer information.
    if (manage_buffer_info_list(pcam, buffers) != TAO_OK) {
        return TAO_ERROR;
    }

    // There are no pending acquisition buffers.
    pcam->pending = 0;

    // Set the acquisition buffer parameters.  The value of parameter
    // PHX_BUF_DST_XLENGTH is the number of bytes per line of the destination
    // buffer (it must be larger of equal the width of the ROI times the number
    // of bits per pixel rounded up to a number of bytes), the value of
    // PHX_BUF_DST_YLENGTH is the number of lines in the destination buffer (it
    // must be larger or equal PHX_ROI_DST_YOFFSET plus PHX_ROI_YLENGTH.
#ifdef BUFFER_FULL_IMAGE
    etParamValue roi_src_xoffset = 0;
    etParamValue roi_src_yoffset = 0;
    etParamValue roi_xlength     = pcam->roi.width;
    etParamValue roi_ylength     = pcam->roi.height;
    etParamValue buf_dst_ylength = roi_ylength;
#else
    etParamValue roi_src_xoffset = cfg->roi.xoff - pcam->roi.xoff;
    etParamValue roi_src_yoffset = cfg->roi.yoff - pcam->roi.yoff;
    etParamValue roi_xlength     = cfg->roi.width;
    etParamValue roi_ylength     = cfg->roi.height;
    etParamValue buf_dst_ylength = roi_ylength;
#endif
    if (phx_set(pcam, PHX_ACQ_IMAGES_PER_BUFFER,            1) != TAO_OK ||
        phx_set(pcam, PHX_ACQ_BUFFER_START,                 1) != TAO_OK ||
        phx_set(pcam, PHX_ACQ_NUM_BUFFERS,            buffers) != TAO_OK ||
        phx_set(pcam, PHX_CAM_ACTIVE_XOFFSET,               0) != TAO_OK ||
        phx_set(pcam, PHX_CAM_ACTIVE_YOFFSET,               0) != TAO_OK ||
        phx_set(pcam, PHX_CAM_ACTIVE_XLENGTH,  pcam->roi.width) != TAO_OK ||
        phx_set(pcam, PHX_CAM_ACTIVE_YLENGTH, pcam->roi.height) != TAO_OK ||
        phx_set(pcam, PHX_CAM_XBINNING,                     1) != TAO_OK ||
        phx_set(pcam, PHX_CAM_YBINNING,                     1) != TAO_OK ||
        phx_set(pcam, PHX_ACQ_XSUB,                PHX_ACQ_X1) != TAO_OK ||
        phx_set(pcam, PHX_ACQ_YSUB,                PHX_ACQ_X1) != TAO_OK ||
        phx_set(pcam, PHX_ROI_SRC_XOFFSET,    roi_src_xoffset) != TAO_OK ||
        phx_set(pcam, PHX_ROI_SRC_YOFFSET,    roi_src_yoffset) != TAO_OK ||
        phx_set(pcam, PHX_ROI_XLENGTH,            roi_xlength) != TAO_OK ||
        phx_set(pcam, PHX_ROI_YLENGTH,            roi_ylength) != TAO_OK ||
        phx_set(pcam, PHX_BUF_DST_YLENGTH,    buf_dst_ylength) != TAO_OK ||
        phx_set(pcam, PHX_ROI_DST_XOFFSET,                  0) != TAO_OK ||
        phx_set(pcam, PHX_ROI_DST_YOFFSET,                  0) != TAO_OK ||
        // FIXME: PHX_BIT_SHIFT_ALIGN_LSB not defined for PHX_BIT_SHIFT.
        phx_set(pcam, PHX_BIT_SHIFT,                        0) != TAO_OK) {
        return TAO_ERROR;
    }

    // Instruct Phoenix frame grabber to use its own acquisition buffers.
    // FIXME: Following the examples in the documentation, the
    // PHX_FORCE_REWRITE flag is set but its effects are not documented and I
    // am not sure whether this is really needed.
    if (phx_set(pcam, (PHX_DST_PTR_TYPE|PHX_CACHE_FLUSH|PHX_FORCE_REWRITE),
                PHX_DST_PTR_INTERNAL) != TAO_OK) {
        return TAO_ERROR;
    }

    // Determine the stride in the acquisition buffers as given by
    // `PHX_BUF_DST_XLENGTH`.
    etParamValue buf_dst_xlength;
    if (phx_get(pcam, PHX_BUF_DST_XLENGTH, &buf_dst_xlength) != TAO_OK) {
        return TAO_ERROR;
    }
#ifdef PHNX_DEBUG
    fprintf(stderr, "Bytes per line: %ld (min. is %ld)\n",
            (long)buf_dst_xlength, cfg->roi.width*pixelsize);
#endif

    // Instantiate the acquisition buffer information.
    long stride = buf_dst_xlength;
#ifdef BUFFER_FULL_IMAGE
    long offset = ((cfg->roi.xoff - pcam->roi.xoff)*pixelsize +
                   (cfg->roi.yoff - pcam->roi.yoff)*stride);
#else
    long offset = 0;
#endif
    pcam->buf.data         = NULL;
    pcam->buf.size         = buf_dst_xlength*buf_dst_ylength;
    pcam->buf.offset       = offset;
    pcam->buf.width        = cfg->roi.width;
    pcam->buf.height       = cfg->roi.height;
    pcam->buf.stride       = stride;
    pcam->buf.encoding     = bufencoding;
    pcam->buf.serial       = 0;
    pcam->buf.frame_start  = TAO_UNKNOWN_TIME;
    pcam->buf.frame_end    = TAO_UNKNOWN_TIME;
    pcam->buf.buffer_ready = TAO_UNKNOWN_TIME;

    // Reset start and end times of next frame to detect overruns.
    pcam->frame_start = TAO_UNKNOWN_TIME;
    pcam->frame_end   = TAO_UNKNOWN_TIME;

    // Configure frame grabber for continuous acquisition and enable interrupts
    // for expected events.
    etParamValue events = (PHX_INTRPT_GLOBAL_ENABLE |
                           PHX_INTRPT_FRAME_START   |
                           PHX_INTRPT_FRAME_END     |
                           PHX_INTRPT_BUFFER_READY  |
                           PHX_INTRPT_FIFO_OVERFLOW |
                           PHX_INTRPT_FRAME_LOST    |
                           PHX_INTRPT_SYNC_LOST);
    if (phx_set(pcam, PHX_INTRPT_CLR, ~(etParamValue)0) != TAO_OK ||
        phx_set(pcam, PHX_INTRPT_SET,           events) != TAO_OK ||
        phx_set(pcam, PHX_ACQ_BLOCKING,     PHX_ENABLE) != TAO_OK ||
        phx_set(pcam, PHX_ACQ_CONTINUOUS,   PHX_ENABLE) != TAO_OK ||
        phx_set(pcam, PHX_COUNT_BUFFER_READY,        1) != TAO_OK) {
        return TAO_ERROR;
    }

    // Setup callback context.
    if (phx_set_parameter(pcam, PHX_EVENT_CONTEXT, (void*)cam) != TAO_OK) {
        return TAO_ERROR;
    }

    // Start acquisition.
    if (phx_read_stream(pcam, PHX_START, acquisition_callback) != TAO_OK) {
        return TAO_ERROR;
    }

    // Execute specific start command. (FIXME: always unlock all buffers
    // before.)
    if (pcam->ops->start != NULL && pcam->ops->start(pcam) != TAO_OK) {
        (void)PHX_StreamRead(pcam->handle, PHX_ABORT, NULL);
        (void)PHX_StreamRead(pcam->handle, PHX_UNLOCK, NULL);
        return TAO_ERROR;
    }

    // Report success.
    return TAO_OK;
}

// Virtual method to stop acquisition immediately, that is without waiting for
// the current frame.  This method is only called when the camera is acquiring.
static tao_status on_stop(
    tao_camera* cam)
{
    // Check argument and that run-level is 2 ("acquiring") then change to
    // run-level 1 ("waiting").
    assert(cam->ops == &ops);
    assert(cam->runlevel == 2);
    return stop_acquisition((phnx_camera*)cam);
}

//-----------------------------------------------------------------------------
// GET/RELEASE ACQUISITION BUFFER

// Virtual method to wait for the next frame.  This method is only called when
// the camera is acquiring.  This method assumes that the camera has been
// locked by the caller.
static tao_status on_wait_buffer(
    tao_camera* cam,
    tao_acquisition_buffer* buf,
    double secs,
    int drop)
{
    // Must be an acquiring Phoenix camera.
    assert(cam->ops == &ops);
    phnx_camera* pcam = (phnx_camera*)cam;
    assert(cam->runlevel == 2);
    assert(pcam->pending >= (pcam->buf.data == NULL ? 0 : 1));
    assert(pcam->pending <= pcam->buffers);

    // Compute absolute time-out time.
    tao_time abstime;
    bool forever;
    switch (tao_get_timeout(&abstime, secs)) {

    case TAO_TIMEOUT_PAST:
        ++cam->config.timeouts;
        return TAO_TIMEOUT;

    case TAO_TIMEOUT_NOW:
    case TAO_TIMEOUT_FUTURE:
        forever = false;
        break;

    case TAO_TIMEOUT_NEVER:
        forever = true;
        break;

    default:
        return TAO_ERROR;
    }

    // May drop pending acquisition buffers and automaticaly drop previous
    // acquisition buffer if any.
    long max_pending; // max. number of pending buffers to keep
    if (drop == 1) {
        // Drop all pending buffers but the last one if any.
        max_pending = 1;
    } else if (drop > 1) {
        // Drop all pending buffers.
        max_pending = 0;
    } else {
        // Do not drop any pending buffers.
        max_pending = pcam->pending;
    }
    while (pcam->buf.data != NULL || pcam->pending > max_pending) {
        if (pcam->buf.data == NULL) {
            // Count this as a dropped frame.
            ++pcam->base.config.droppedframes;
        } else {
            // Never automatically release more than once.
            pcam->buf.data = NULL;
        }
        etStat code = PHX_StreamRead(pcam->handle, PHX_BUFFER_RELEASE, NULL);
        if (code != PHX_OK) {
            // Not being able to release the buffer require more elaborated
            // handling than just reporting an error: we set the run-level so
            // that, at least, acquisition will be stopped.
            phx_error("PHX_StreamRead(PHX_BUFFER_RELEASE)", code);
            cam->runlevel = 3;
            return TAO_ERROR;
        }
        --pcam->pending;
    }

    // While there are no pending buffers, wait for events to be signaled or an
    // error to occur.  This is done in a `while` loop to cope with spurious
    // signaled conditions.  Index `cam->last`, number `pcam->pending`, and
    // other members are updated by the acquisition callback.
    while (pcam->pending < 1) {
        if (forever) {
            if (tao_condition_wait(&cam->cond, &cam->mutex) != TAO_OK) {
                return TAO_ERROR;
            }
        } else {
            tao_status status = tao_condition_abstimed_wait(
                &cam->cond, &cam->mutex, &abstime);
            if (status != TAO_OK) {
                if (status == TAO_TIMEOUT) {
                    ++cam->config.timeouts;
                    return TAO_TIMEOUT;
                } else {
                    return TAO_ERROR;
                }
            }
        }
    }

    // Retrieve address of first (and oldest) pending acquisition buffer.
    stImageBuff vbuf;
    etStat code = PHX_StreamRead(pcam->handle, PHX_BUFFER_GET, &vbuf);
    if (code != PHX_OK) {
        // Same remark as for PHX_BUFFER_RELEASE above.
        phx_error("PHX_StreamRead(PHX_BUFFER_GET)", code);
        cam->runlevel = 3;
        return TAO_ERROR;
    }
    pcam->buf.data = vbuf.pvAddress;

    // Instantiate acquisition buffer.
    const phnx_buffer_info* info = &pcam->info[
        (pcam->serial - pcam->pending)%pcam->buffers];
    buf->data         = pcam->buf.data;
    buf->size         = pcam->buf.size;
    buf->offset       = pcam->buf.offset;
    buf->width        = pcam->buf.width;
    buf->height       = pcam->buf.height;
    buf->stride       = pcam->buf.stride;
    buf->encoding     = pcam->buf.encoding;
    buf->serial       = info->serial;
    buf->frame_start  = info->frame_start;
    buf->frame_end    = info->frame_end;
    buf->buffer_ready = info->buffer_ready;

    return TAO_OK;
}

//-----------------------------------------------------------------------------
// ACQUISITION CALLBACK
//
// We assume continuous acquisition.  We may want to try to process all frames
// or to always process the last one and discard the frames that we had no time
// to deal with.  So we accept to loose frames but we do not want to loose the
// newest frames nor to have the frame currently processed overwritten by the
// frame-grabber.

static void acquisition_callback(
    tHandle handle,
    uint32_t events,
    void* data)
{
    // Get camera and perform minimal sanity check (no needs to lock, abort on
    // error, etc.).
    tao_camera* cam = (tao_camera*)data;
    assert(cam->ops == &ops);
    phnx_camera* pcam = (phnx_camera*)data;
    bool notify = false;

    // Get timestamp immediately (before locking because this may block for
    // some time) aborting on error.
    tao_time now;
    if (tao_get_monotonic_time(&now) != TAO_OK) {
        tao_panic();
    }

    // Lock the camera, handle events and unlock the camera.
    if (tao_camera_lock(cam) != TAO_OK) {
        tao_panic();
    }
    if ((PHX_INTRPT_FRAME_START & events) != 0) {
        // Register the time at the start of the frame using the number of
        // seconds to detect overruns.
        if (pcam->frame_start.sec > 0) {
            ++cam->config.overruns;
        }
        pcam->frame_start = now;
    }
    if ((PHX_INTRPT_FRAME_END & events) != 0) {
        // Register the time at the end of the frame using the number of
        // seconds to detect overruns.
        if (pcam->frame_end.sec > 0) {
            ++cam->config.overruns;
        }
        pcam->frame_end = now;
    }
    if ((PHX_INTRPT_BUFFER_READY & events) != 0) {
        // A new frame is available.  Increment total number of frames, serial
        // number of last acquirred buffer, and number of pending acquisition
        // buffers.
        ++cam->config.frames;
        ++pcam->serial; // FIXME: not needed
        if (pcam->pending < pcam->buffers) {
            ++pcam->pending;
        } else {
            ++cam->config.overruns;
        }
        phnx_buffer_info* info = &pcam->info[(pcam->serial - 1)%pcam->buffers];
        info->serial       = pcam->serial;
        info->frame_start  = pcam->frame_start;
        info->frame_end    = pcam->frame_end;
        info->buffer_ready = now;
        // Reset start and end times of next frame to detect overruns.
        pcam->frame_start = TAO_UNKNOWN_TIME;
        pcam->frame_end   = TAO_UNKNOWN_TIME;
        notify = true;
    }
    if ((PHX_INTRPT_FIFO_OVERFLOW & events) != 0) {
        // Fifo overflow.
        ++cam->config.overflows;
    }
    if ((PHX_INTRPT_SYNC_LOST & events) != 0) {
        // Synchronization lost.
        ++cam->config.lostsyncs;
    }
    if ((PHX_INTRPT_FRAME_LOST & events) != 0) {
        // Frame lost.
        ++cam->config.lostframes;
    }
    if (notify) {
        // Signal condition for waiting thread.
        tao_camera_broadcast(cam);
    }
    if (tao_camera_unlock(cam) != TAO_OK) {
        tao_panic();
    }
}

//-----------------------------------------------------------------------------
// STOP ACQUISITION, CONNECT/DISCONNECT CAMERA.

// Stop acquisition immediately.
static tao_status stop_acquisition(
    phnx_camera* pcam)
{
    // Since we use acquisition buffers allocated by the frame-grabber, we
    // cannot be sure that they will be available after acquisition has been
    // stopped.  We therefore pretend that there are no more pending buffers.
    pcam->pending = 0;

    // Abort acquisition, unlock acquisition buffers and call "stop" specific
    // method.  In case of errors, the members of the structure are updated
    // *before* doing anything.  The error `PHX_ERROR_NOT_IMPLEMENTED` can be
    // ignored for the `PHX_UNLOCK` command.
    tao_status status = TAO_OK;
    etStat code = PHX_StreamRead(pcam->handle, PHX_ABORT, NULL);
    if (code != PHX_OK) {
        phx_error("PHX_StreamRead(PHX_ABORT)", code);
        status = TAO_ERROR;
    }
    code = PHX_StreamRead(pcam->handle, PHX_UNLOCK, NULL);
    if (code != PHX_OK && code != PHX_ERROR_NOT_IMPLEMENTED) {
        phx_error("PHX_StreamRead(PHX_UNLOCK)", code);
        status = TAO_ERROR;
    }
    if (pcam->ops->stop != NULL && pcam->ops->stop(pcam) != TAO_OK) {
        status = TAO_ERROR;
    }
    return status;
}

// Connect or re-connect the camera.
static tao_status connect_camera(
    phnx_camera* pcam)
{
    assert(sizeof(pcam->handle) >= sizeof(tHandle));
    tHandle handle = pcam->handle;

    // Open the Phoenix board using the configured handle.  If this is the very
    // first time the camera is open, identify the model of the connected
    // camera.
    etStat code = PHX_Open(pcam->handle);
    if (code != PHX_OK) {
        phx_error("PHX_Open", code);
        return TAO_ERROR;
    }

    // If this is the very first time the camera is open, check whether we have
    // a CoaXPress camera and identify the model of the connected camera.
    if (pcam->ops == NULL) {
        if (phx_detect_coaxpress(pcam) != TAO_OK) {
            goto error;
        }
        for (int k = 0; known_cameras[k] != NULL; ++k) {
            if (known_cameras[k]->identify(pcam)) {
                pcam->ops = known_cameras[k];
                break;
            }
        }
        if (pcam->ops == NULL) {
            tao_store_error(__func__, TAO_NOT_FOUND);
            goto error;
        }
    }

    // Initialize specific camera model.
    if (pcam->ops->initialize != NULL && pcam->ops->initialize(pcam) != TAO_OK) {
        goto error;
    }

    // Execute specific "stop" command.  FIXME: Is it a good idea?
    if (pcam->ops->stop != NULL && pcam->ops->stop(pcam) != TAO_OK) {
        goto error;
    }
    return TAO_OK;

    // Close the handle in case of errors.
 error:
    (void)PHX_Close(&handle);
    pcam->handle = handle;
    return TAO_ERROR;
}

// Disconnect the camera, that is close the Phoenix board.
static tao_status disconnect_camera(
    phnx_camera* pcam)
{
    assert(sizeof(pcam->handle) >= sizeof(tHandle));
    tHandle handle = pcam->handle;
    etStat code = PHX_Close(&handle);
    pcam->handle = handle;
    if (code != PHX_OK) {
        phx_error("PHX_Close", code);
        return TAO_ERROR;
    }
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// OTHER PRIVATE ROUTINES

// FIXME: this function is used elsewhere
static void invalid_runlevel(
    tao_camera* cam,
    const char* func)
{
    int code;
    switch (cam->runlevel) {
    case 0:
        code = TAO_NOT_READY;
        break;
    case 1:
        code = TAO_NOT_ACQUIRING;
        break;
    case 2:
        code = TAO_ACQUISITION_RUNNING;
        break;
    case 3:
        code = TAO_MUST_RESET;
        break;
    case 4:
        code = TAO_UNRECOVERABLE;
        break;
    default:
        code = TAO_CORRUPTED;
    }
    tao_store_error(func, code);
}

// Check images and device regions of interest (ROI).  First check that the
// configured ROI and the hardware ROI are correct, then check whether they are
// compatible.
static tao_status check_regions_of_interest(
    const tao_camera_roi* img,
    const tao_camera_roi* dev,
    long sensorwidth,
    long sensorheight)
{
    if (tao_camera_roi_check(img, sensorwidth, sensorheight) != TAO_OK ||
        tao_camera_roi_check(dev, sensorwidth, sensorheight) != TAO_OK ||
        img->xbin != dev->xbin ||
        img->xoff < dev->xoff ||
        img->xoff + img->xbin*img->width > dev->xoff + dev->xbin*dev->width ||
        img->yoff < dev->yoff ||
        img->ybin != dev->ybin ||
        img->yoff + img->ybin*img->height > dev->yoff + dev->ybin*dev->height) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// SPECIFIC CONFIGURATION

// Attempt to load a new configuration.  The pixel encoding for the acquisition
// buffers is reset.  (FIXME: Not needed?)
tao_status phnx_load_configuration(
    tao_camera* cam,
    int id)
{
    // Camera must be a "Phoenix" camera.
    phnx_camera* pcam = phnx_get_device(cam);
    if (pcam == NULL) {
        return TAO_ERROR;
    }

    // Camera must not be acquiring.
    if (cam->runlevel != 1) {
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }

    // Call virtual method.
    if (pcam->ops->load_config == NULL) {
        tao_store_error(__func__, TAO_UNSUPPORTED);
        return TAO_ERROR;
    }
    if (pcam->ops->load_config(pcam, id) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status phnx_save_configuration(
    tao_camera* cam,
    int id)
{
    // Camera must be a "Phoenix" camera.
    phnx_camera* pcam = phnx_get_device(cam);
    if (pcam == NULL) {
        return TAO_ERROR;
    }
    // Camera must not be acquiring.
    if (cam->runlevel != 1) {
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }
    // Call virtual method.
    if (pcam->ops->save_config == NULL) {
        tao_store_error(__func__, TAO_UNSUPPORTED);
        return TAO_ERROR;
    }
    if (pcam->ops->save_config(pcam, id) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Retrieve the current camera configuration.  The camera should be locked to
// ensure that configuration does not change but it does not harm if it is not
// locked.  There are no constraints on the camera state.
void phnx_get_configuration(
    tao_camera* cam,
    tao_camera_config* cfg)
{
    tao_camera_config_copy(cfg, &cam->config);
}

tao_status phnx_set_configuration(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    // Camera must be a "Phoenix" camera.
    phnx_camera* pcam = phnx_get_device(cam);
    if (pcam == NULL) {
        return TAO_ERROR;
    }
    if (pcam->ops->set_config == NULL) {
        tao_store_error(__func__, TAO_UNSUPPORTED);
        return TAO_ERROR;
    }
    if (pcam->ops->set_config(pcam, cfg) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// UTILITIES

phnx_camera* phnx_get_device(
    tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    if (cam->ops != &ops) {
        tao_store_error(__func__, TAO_BAD_DEVICE);
        return NULL;
    }
    return (phnx_camera*)cam;
}

tao_encoding phnx_best_buffer_encoding(
    tao_encoding enc)
{
    tao_encoding col = TAO_ENCODING_COLORANT(enc);
    tao_encoding pxl = TAO_ENCODING_BITS_PER_PIXEL(enc);
    tao_encoding flg = TAO_ENCODING_FLAGS(enc);
    tao_encoding pad = flg & (TAO_ENCODING_FLAGS_MSB_PAD|
                              TAO_ENCODING_FLAGS_LSB_PAD);
    switch (col) {
    case TAO_COLORANT_MONO:
    case TAO_COLORANT_BAYER_RGGB:
    case TAO_COLORANT_BAYER_GRBG:
    case TAO_COLORANT_BAYER_GBRG:
    case TAO_COLORANT_BAYER_BGGR:
        // Each pixel is gray or has one color.
        if (flg == pad && (pxl & 0x7) == 0) {
            // Pixel size is a multiple of 8 bits and only padding flags.
            return TAO_ENCODING_2_(col, pxl);
        }
        if (flg == pad && pad == TAO_ENCODING_FLAGS_MSB_PAD) {
            // Pixel size is not a multiple of 8 bits but zero upper padded.
            // We just round the pixel size to the next multiple of 8 bits.
            return TAO_ENCODING_2_(col, TAO_ROUND_UP(pxl, 8));
        }
        break;
    case TAO_COLORANT_RGB:
    case TAO_COLORANT_BGR:
    case TAO_COLORANT_ARGB:
    case TAO_COLORANT_RGBA:
    case TAO_COLORANT_ABGR:
    case TAO_COLORANT_BGRA:
        // Each pixel has 3 or 4 components.
        if (flg == pad && (pxl & 0x7) == 0) {
            // Pixel size is a multiple of 8 bits and only padding flags.
            return TAO_ENCODING_2_(col, pxl);
        }
        break;
    case TAO_COLORANT_YUV444:
        if (enc == TAO_ENCODING_YUV444) {
            return enc;
        }
        break;
    case TAO_COLORANT_YUV422:
        if (enc == TAO_ENCODING_YUV422) {
            return enc;
        }
        break;
    case TAO_COLORANT_YUV411:
        if (enc == TAO_ENCODING_YUV411) {
            return enc;
        }
        break;
    case TAO_COLORANT_YUV420P:
        if (enc == TAO_ENCODING_YUV420P) {
            return enc;
        }
        break;
    case TAO_COLORANT_YUV420SP:
        if (enc == TAO_ENCODING_YUV420SP) {
            return enc;
        }
        break;
    }
    return TAO_ENCODING_UNKNOWN;
}

static tao_status print_board_info(
    phnx_camera* pcam,
    const char* pfx, FILE* stream);

tao_status phnx_print_camera_config(
    tao_camera* cam,
    FILE* stream)
{
    // Minimal checks and set defaults.
    phnx_camera* pcam = phnx_get_device(cam);
    if (pcam == NULL) {
        return TAO_ERROR;
    }
    if (cam->runlevel < 1 || cam->runlevel > 2) {
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }
    if (stream == NULL) {
        stream = stdout;
    }

    // Print information and settings.
    if (0 > fprintf(stream, "Camera vendor: %s\n",
                    (pcam->vendor[0] != '\0' ? pcam->vendor : "Unknown")) ||
        0 > fprintf(stream, "Camera model:  %s\n",
                    (pcam->model[0] != '\0' ? pcam->model : "Unknown")) ||
        0 > fprintf(stream, "CoaXPress camera: %s\n",
                    (pcam->is_coaxpress ? "yes" : "no")) ||
        0 > fprintf(stream, "Board information:\n")) {
        tao_store_system_error("fprintf");
        return TAO_ERROR;
    }
    if (print_board_info(pcam, "    ", stream) != TAO_OK) {
        return TAO_ERROR;
    }
    if (tao_camera_config_print(stream, &pcam->base.config) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status print_board_info(
    phnx_camera* pcam,
    const char* pfx, FILE* stream)
{
    // Retrieve information.
    etParamValue bits;
     if (phx_get(pcam, PHX_BOARD_INFO, &bits) != TAO_OK) {
        return TAO_ERROR;
    }
#define PRT(msk, txt)                                   \
    do {                                                \
        if ((bits & (msk)) == (msk)) {                  \
            fprintf(stream, "%s%s\n", pfx, txt);        \
        }                                               \
    } while (false)
#define CASE(cst, txt) case cst: fprintf(stream, "%s%s\n", pfx, txt); break
#define CASE1(cst) CASE(cst, #cst)

    PRT(PHX_BOARD_INFO_LVDS, "Board has LVDS camera interface");
    PRT(PHX_BOARD_INFO_CL, "Board has Camera Link interface");
    PRT(PHX_BOARD_INFO_CL_BASE,
        "Board is using Camera Link Base interface only");
    PRT(PHX_BOARD_INFO_CL_MEDIUM,
        "Board is using Camera Link Medium interface");
    PRT(PHX_BOARD_INFO_CL_FULL, "Board is using Camera Link Full interface");
    PRT(PHX_BOARD_INFO_PCI_EXPRESS, "Board has PCI Express interface");
    PRT(PHX_BOARD_INFO_PCI_3V, "3V PCI Interface");
    PRT(PHX_BOARD_INFO_PCI_5V, "5V PCI Interface");
    PRT(PHX_BOARD_INFO_PCI_33M, "33MHz PCI Interface");
    PRT(PHX_BOARD_INFO_PCI_66M, "66MHz PCI Interface");
    PRT(PHX_BOARD_INFO_PCI_32B, "32bit PCI Interface");
    PRT(PHX_BOARD_INFO_PCI_64B, "64bit PCI Interface");
    PRT(PHX_BOARD_INFO_BOARD_3V, "Board is 3V compatible");
    PRT(PHX_BOARD_INFO_BOARD_5V, "Board is 5V compatible");
    PRT(PHX_BOARD_INFO_BOARD_33M, "Board is 33MHz compatible");
    PRT(PHX_BOARD_INFO_BOARD_66M, "Board is 66MHz compatible");
    PRT(PHX_BOARD_INFO_BOARD_32B, "Board is 32bit compatible");
    PRT(PHX_BOARD_INFO_BOARD_64B, "Board is 64bit compatible");
    PRT(PHX_BOARD_INFO_CHAIN_MASTER, "Board has chaining jumper set to Master");
    PRT(PHX_BOARD_INFO_CHAIN_SLAVE, "Board has chaining jumper set to Slave");

    if ((bits & PHX_BOARD_INFO_PCI_EXPRESS) != PHX_BOARD_INFO_PCI_EXPRESS) {
        etParamValue val;
        if (phx_get(pcam, PHX_PCIE_INFO, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        switch ((int)(val & PHX_EMASK_PCIE_INFO_LINK_GEN)) {
            CASE(PHX_PCIE_INFO_LINK_GEN1,
                 "PCI Express link operating at Gen 1 PCI Express speed");
            CASE(PHX_PCIE_INFO_LINK_GEN2,
                 "PCI Express link operating at Gen 2 PCI Express speed");
            CASE(PHX_PCIE_INFO_LINK_GEN3,
                 "PCI Express link operating at Gen 3 PCI Express speed");
        default:
            fprintf(stream, "%sUnknown PCI Express link generation\n", pfx);
        }
        switch ((int)(val & PHX_EMASK_PCIE_INFO_LINK_X)) {
            CASE(PHX_PCIE_INFO_LINK_X1,
                 "PCI Express link operating at x1 PCI Express width");
            CASE(PHX_PCIE_INFO_LINK_X2,
                 "PCI Express link operating at x2 PCI Express width");
            CASE(PHX_PCIE_INFO_LINK_X4,
                 "PCI Express link operating at x4 PCI Express width");
            CASE(PHX_PCIE_INFO_LINK_X8,
                 "PCI Express link operating at x8 PCI Express width");
            CASE(PHX_PCIE_INFO_LINK_X12,
                 "PCI Express link operating at x12 PCI Express width");
            CASE(PHX_PCIE_INFO_LINK_X16,
                 "PCI Express link operating at x16 PCI Express width");
            CASE(PHX_PCIE_INFO_LINK_X32,
                 "PCI Express link operating at x32 PCI Express width");
        default:
            fprintf(stream, "%sUnknown PCI Express link width\n", pfx);
        }
        switch ((int)(val & PHX_EMASK_PCIE_INFO_FG_GEN)) {
            CASE(PHX_PCIE_INFO_FG_GEN1,
                 "Frame grabber only supports Gen 1 PCI Express");
            CASE(PHX_PCIE_INFO_FG_GEN2,
                 "Frame grabber supports Gen 2 PCI Express");
            CASE(PHX_PCIE_INFO_FG_GEN3,
                 "Frame grabber supports Gen 3 PCI Express");
        }
        switch ((int)(val & PHX_EMASK_PCIE_INFO_FG_X)) {
            CASE(PHX_PCIE_INFO_FG_X1,  "Frame grabber x1");
            CASE(PHX_PCIE_INFO_FG_X2,  "Frame grabber x2");
            CASE(PHX_PCIE_INFO_FG_X4,  "Frame grabber x4");
            CASE(PHX_PCIE_INFO_FG_X8,  "Frame grabber x8");
            CASE(PHX_PCIE_INFO_FG_X12, "Frame grabber x12");
            CASE(PHX_PCIE_INFO_FG_X16, "Frame grabber x16");
            CASE(PHX_PCIE_INFO_FG_X32, "Frame grabber x32");
        }
        switch ((int)(val & PHX_EMASK_PCIE_INFO_SLOT_GEN)) {
            CASE(PHX_PCIE_INFO_SLOT_GEN1, "Slot Gen1");
            CASE(PHX_PCIE_INFO_SLOT_GEN2, "Slot Gen2");
            CASE(PHX_PCIE_INFO_SLOT_GEN3, "Slot Gen3");
        }
        switch ((int)(val & PHX_EMASK_PCIE_INFO_SLOT_X)) {
            CASE(PHX_PCIE_INFO_SLOT_X1,  "Slot x1");
            CASE(PHX_PCIE_INFO_SLOT_X2,  "Slot x2");
            CASE(PHX_PCIE_INFO_SLOT_X4,  "Slot x4");
            CASE(PHX_PCIE_INFO_SLOT_X8,  "Slot x8");
            CASE(PHX_PCIE_INFO_SLOT_X12, "Slot x12");
            CASE(PHX_PCIE_INFO_SLOT_X16, "Slot x16");
            CASE(PHX_PCIE_INFO_SLOT_X32, "Slot x32");
        }
    }

    // CoaXPress information.
    if (phx_get(pcam, PHX_CXP_INFO, &bits) != TAO_OK) {
        return TAO_ERROR;
    }
    PRT(PHX_CXP_CAMERA_DISCOVERED,
        "The CoaXPress camera has completed discovery");
    PRT(PHX_CXP_CAMERA_IS_POCXP,
        "The CoaXPress camera is powered via PoCXP from the frame grabber");
    PRT(PHX_CXP_POCXP_UNAVAILABLE,
        "There is no power to the frame grabber to provide PoCXP to a camera");
    PRT(PHX_CXP_POCXP_TRIPPED,
        "The PoCXP supply to the camera has been shutdown because high "
        "current was detected");
    int links[4], nlinks = 0;
    if ((bits & PHX_CXP_LINK1_USED) != 0) {
        links[nlinks++] = 1;
    }
    if ((bits & PHX_CXP_LINK2_USED) != 0) {
        links[nlinks++] = 2;
    }
    if ((bits & PHX_CXP_LINK3_USED) != 0) {
        links[nlinks++] = 3;
    }
    if ((bits & PHX_CXP_LINK4_USED) != 0) {
        links[nlinks++] = 4;
    }
    if (nlinks == 0) {
        fprintf(stream, "%sNo CoaXPress links in use\n", pfx);
    } else if (nlinks == 1) {
        fprintf(stream, "%sCoaXPress link %d in use\n", pfx, links[0]);
    } else {
        fprintf(stream, "%sCoaXPress links ", pfx);
        for (int i = 0; i < nlinks; ++i) {
            fprintf(stream, "%d%s", links[i],
                    (i < nlinks - 2 ? ", " :
                     (i == nlinks - 2 ? " and " : " are in use\n")));
        }
    }
    PRT(PHX_CXP_LINK1_MASTER, "CoaXPress link 1 is the master link");
    PRT(PHX_CXP_LINK2_MASTER, "CoaXPress link 2 is the master link");
    PRT(PHX_CXP_LINK3_MASTER, "CoaXPress link 3 is the master link");
    PRT(PHX_CXP_LINK4_MASTER, "CoaXPress link 4 is the master link");

    if (bits != 0) {
        etParamValue val;
        if (phx_get(pcam, PHX_CXP_BITRATE, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        switch ((int)val) {
            CASE(PHX_CXP_BITRATE_UNKNOWN, "No CoaXPress camera is connected, "
                 "or a camera has not completed discovery");
            CASE(PHX_CXP_BITRATE_CXP1, "Bitrate is 1250 Mbps");
            CASE(PHX_CXP_BITRATE_CXP2, "Bitrate is 2500 Mbps");
            CASE(PHX_CXP_BITRATE_CXP3, "Bitrate is 3125 Mbps");
            CASE(PHX_CXP_BITRATE_CXP5, "Bitrate is 5000 Mbps");
            CASE(PHX_CXP_BITRATE_CXP6, "Bitrate is 6250 Mbps");
        }
        if (phx_get(pcam, PHX_CXP_DISCOVERY, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        switch ((int)val) {
            CASE(PHX_CXP_DISCOVERY_UNKNOWN, "No CoaXPress camera is connected, "
                 "or a camera has not completed discovery");
            CASE(PHX_CXP_DISCOVERY_1X,
                 "The camera is using a single CoaXPress link");
            CASE(PHX_CXP_DISCOVERY_2X,
                 "The camera is using two CoaXPress links");
            CASE(PHX_CXP_DISCOVERY_4X,
                 "The camera is using four CoaXPress links");
        }
        if (phx_get(pcam, PHX_CXP_BITRATE_MODE, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        switch ((int)val) {
            CASE1(PHX_CXP_BITRATE_MODE_AUTO);
            CASE1(PHX_CXP_BITRATE_MODE_CXP1);
            CASE1(PHX_CXP_BITRATE_MODE_CXP2);
            CASE1(PHX_CXP_BITRATE_MODE_CXP3);
            CASE1(PHX_CXP_BITRATE_MODE_CXP5);
            CASE1(PHX_CXP_BITRATE_MODE_CXP6);
        }
        if (phx_get(pcam, PHX_CXP_DISCOVERY_MODE, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        switch ((int)val) {
            CASE1(PHX_CXP_DISCOVERY_MODE_AUTO);
            CASE1(PHX_CXP_DISCOVERY_MODE_1X);
            CASE1(PHX_CXP_DISCOVERY_MODE_2X);
            CASE1(PHX_CXP_DISCOVERY_MODE_4X);
        }
        if (phx_get(pcam, PHX_CXP_POCXP_MODE, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        switch ((int)val) {
            CASE1(PHX_CXP_POCXP_MODE_AUTO);
            CASE1(PHX_CXP_POCXP_MODE_OFF);
            CASE1(PHX_CXP_POCXP_MODE_TRIP_RESET);
            CASE1(PHX_CXP_POCXP_MODE_FORCEON);
        }
    }
#undef CASE
#undef CASE1
#undef PRT
    return TAO_OK;
}
