// phoenix-config.c -
//
// Program to configure a camera connected to an ActiveSilicon Phoenix frame
// grabber.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2018-2024, Éric Thiébaut.

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <math.h>

#include "tao-errors.h"
#include "tao-phoenix.h"
#include "tao-phoenix-options.h"

int main(
    int argc,
    char* argv[])
{
    // Options (all related variables must be static).
    static tao_camera_config cfg;
    static bool quiet = false;
    static bool debug = false;
    static tao_option options[] = {
        PHNX_OPTION_LOAD,
        PHNX_OPTION_SAVE,
        PHNX_OPTION_CAMERA_ROI(cfg),
        PHNX_OPTION_RAWENCODING(cfg),
        PHNX_OPTION_FRAMERATE(cfg),
        PHNX_OPTION_EXPOSURETIME(cfg),
        // FIXME: PHNX_OPTION_BIAS(cfg),
        // FIXME: PHNX_OPTION_GAIN(cfg),
        // FIXME: PHNX_OPTION_CONNECTION(cfg),
        PHNX_OPTION_ACQUISITION_BUFFERS(cfg),
        PHNX_OPTION_QUIET(quiet),
        PHNX_OPTION_DEBUG(debug),
        PHNX_OPTION_HELP_AND_EXIT(0),
        PHNX_OPTION_LAST_ENTRY,
    };

    // Other variables.
    tao_camera* cam = NULL;
    const char* progname = tao_basename(argv[0]);
    const char* descr = "Change and/or show camera configuration";
    int status;

    // Parse the command line options and configure the camera.
    argc = phnx_start_program(&cam, &cfg, argc, argv, options, descr, "");
    if (argc < 0) {
        return EXIT_FAILURE;
    }
    if (argc != 1) {
        fprintf(stderr, "%s: too many arguments\n", progname);
        return EXIT_FAILURE;
    }
    if (debug || ! quiet) {
        // Print informations about the camera.
        if (phnx_print_camera_config(cam, stdout) != TAO_OK) {
            tao_report_error();
            tao_camera_destroy(cam);
            return EXIT_FAILURE;
        }
    }

    // Report any errors and free all resources.
    if (tao_any_errors(NULL)) {
        tao_report_error();
        status = EXIT_FAILURE;
    } else {
        status = EXIT_SUCCESS;
    }
    tao_camera_destroy(cam);
    return status;
}
