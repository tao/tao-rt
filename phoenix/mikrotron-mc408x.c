// mikrotron-mc408x.c -
//
// Routines for Mikrotron MC408x cameras.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2016, Éric Thiébaut & Jonathan Léger.
// Copyright (c) 2017-2024, Éric Thiébaut.

#include <stdio.h> // *before* <tao-errors.h>

#include "tao-errors.h"
#include "tao-generic.h"
#include "tao-phoenix-private.h"
#include "tao-coaxpress.h"
#include "tao-mikrotron-mc408x.h"

#define USE_FRAMEGRABBER_FOR_SETTING_CONNECTION 1

// Management of camera settings
// =============================
//
// Since camera imposes that its configuration be always correct, certain
// parameters have to be considered as a whole.
//
// For each single parameter or group of parameters, there are two elementary
// operations:
//
// - Updating: the parameters are read from the camera and reflected in the
//   structure describing the camera.
//
// - Setting: the parameters are set for the camera and, if the operation is
//   successful, reflected in the structure describing the camera.
//
// An additional "check" operation is provided which yields whether the
// settings are valid.
//
// Note: In case of error while changing parameters, it may be safer to
// update the parameters.
//
// When changing the configuration, we attempt to only change the settings that
// need to be changed and in an order such as to avoid clashes. The strategy
// is:
//
// 1. reduce bits per pixel if requested;
// 2. reduce frame rate if requested;
// 3. reduce exposure time if requested;
// 4. change ROI if requested;
// 5. augment exposure time if requested;
// 6. augment frame rate if requested;
// 7. augment bits per pixel if requested;

static tao_status update_exposure_time(
    phnx_camera* cam);

static tao_status check_exposure_time(
    phnx_camera* cam,
    double arg);

static tao_status set_exposure_time(
    phnx_camera* cam,
    double arg);

static tao_status update_frame_rate(
    phnx_camera* cam);

static tao_status check_frame_rate(
    phnx_camera* cam,
    double arg);

static tao_status set_frame_rate(
    phnx_camera* cam,
    double arg);

static tao_status update_region_of_interest(
    phnx_camera* cam);

static tao_status check_region_of_interest(
    phnx_camera* cam,
    const tao_camera_roi* arg);

static tao_status set_region_of_interest(
    phnx_camera* cam,
    const tao_camera_roi* arg);

static tao_status update_camera_encoding(
    phnx_camera* cam);

static tao_status check_camera_encoding(
    phnx_camera* cam,
    const tao_encoding arg);

static tao_status set_camera_encoding(
    phnx_camera* cam,
    const tao_encoding arg);

static bool is_monochrome(
    phnx_camera* cam);

static bool is_color(
    phnx_camera* cam);

static tao_status update_configuration(
    phnx_camera* cam,
    bool all);

static tao_status check_configuration(
    phnx_camera* cam,
    const tao_camera_config* cfg);

static tao_status set_configuration(
    phnx_camera* cam,
    const tao_camera_config* cfg);

static tao_status load_configuration(
    phnx_camera* cam,
    int id);

static tao_status save_configuration(
    phnx_camera* cam,
    int id);

static tao_status start(
    phnx_camera* cam);

static tao_status stop(
    phnx_camera* cam);

// Private functions and macros to query a named attribute while preserving the
// `const` property of pointers.

static inline const tao_attr* phnx_camera_unsafe_get_attr(
    const phnx_camera* cam,
    long idx)
{
    return tao_attr_table_unsafe_get_entry(&cam->base.config.attr_table, idx);
}

#define colon(x, ...) x: __VA_ARGS__

#define get_attr_(T, obj, idx)                                          \
    colon(const T*, (const tao_attr*)T##_unsafe_get_attr((const T*)(obj), idx)), \
    colon(      T*, (      tao_attr*)T##_unsafe_get_attr((const T*)(obj), idx))

#define get_attr(obj, idx)                              \
    _Generic((obj),                                     \
             get_attr_(phnx_camera,       obj, idx),    \
             get_attr_(tao_camera,        obj, idx),    \
             get_attr_(tao_camera_config, obj, idx))

#define get_key(attr)                                                   \
    _Generic((attr),                                                    \
             const tao_attr*:                  tao_attr_get_key(attr),  \
             /***/ tao_attr*: (tao_attr_key*)tao_attr_get_key(attr))

// Macro `non_const(ptr)` cast `ptr` as a non-constant pointer of the same type.
#define non_const_(T, ptr) colon(const T*, (T*)(ptr)), colon(T*, ptr)
#define non_const(ptr)                          \
    _Generic((ptr),                             \
             non_const_(tao_attr, ptr),         \
             non_const_(tao_attr_value, ptr))

// Interface to named attributes.

typedef tao_status accessor(
    phnx_camera* cam,             // camera
    int idx);                     // attribute index in configuration

typedef tao_status checker(
    const char* func,             // caller's name
    phnx_camera* cam,             // camera
    const tao_camera_config* cfg, // configuration to check
    int idx);                     // attribute index in configuration

typedef tao_status mutator(
    const char* func,             // caller's name
    phnx_camera* cam,             // camera
    const tao_camera_config* cfg, // configuration to check
    int idx);                     // attribute index in configuration

static accessor  get_bias;
static checker check_bias;
static mutator   set_bias;

static accessor  get_gain;
static checker check_gain;
static mutator   set_gain;

static accessor  get_temperature;
static accessor  get_camera_model;
static accessor  get_camera_vendor;

static accessor  get_connection;
static checker check_connection;
static mutator   set_connection;

static accessor  get_filter_mode;
static checker check_filter_mode;
static mutator   set_filter_mode;

static accessor  get_pattern_noise_reduction;
static checker check_pattern_noise_reduction;
static mutator   set_pattern_noise_reduction;

static accessor  get_pixel_pulse_reset;
static checker check_pixel_pulse_reset;
static mutator   set_pixel_pulse_reset;

static accessor  get_pulse_drain;
static checker check_pulse_drain;
static mutator   set_pulse_drain;

// Number of characters (including final null) for configuration strings.
#define CONNECTION_VALUE_SIZE    32
#define CAMERA_MODEL_VALUE_SIZE  32 // see DeviceModelName in camera XML file
#define CAMERA_VENDOR_VALUE_SIZE 32 // see DeviceVendorName in camera XML file

static struct {
    const char* name;
    uint32_t value;
} filter_mode_table[] = {
    {"raw", CXP_FILTER_MODE_RAW},
    {"mono", CXP_FILTER_MODE_MONO},
    {"color", CXP_FILTER_MODE_COLOR},
#define FILTER_MODE_VALUE_SIZE 8 /* Names in this table must not be longer than
                                  * this constant including final null. */
    {NULL, 0},
};

#define ATTR_LIST                                                       \
    ATTR_DEF("bias", true, 0,                                           \
             TAO_ATTR_INTEGER, TAO_ATTR_READABLE | TAO_ATTR_WRITABLE,   \
             get_bias, check_bias, set_bias)                            \
    ATTR_DEF("gain", true, 0,                                           \
             TAO_ATTR_INTEGER, TAO_ATTR_READABLE | TAO_ATTR_WRITABLE,   \
             get_gain, check_gain, set_gain)                            \
    ATTR_DEF("temperature", true, 0,                                    \
             TAO_ATTR_FLOAT, TAO_ATTR_READABLE | TAO_ATTR_VARIABLE,     \
             get_temperature, NULL, NULL)                               \
    ATTR_DEF("connection", true, CONNECTION_VALUE_SIZE,                 \
             TAO_ATTR_STRING, TAO_ATTR_READABLE | TAO_ATTR_WRITABLE,    \
             get_connection, check_connection, set_connection)          \
    ATTR_DEF("filterMode", true, FILTER_MODE_VALUE_SIZE,                \
             TAO_ATTR_STRING, TAO_ATTR_READABLE | TAO_ATTR_WRITABLE,    \
             get_filter_mode, check_filter_mode, set_filter_mode)       \
    ATTR_DEF("patternNoiseReduction", true, 0,                          \
             TAO_ATTR_BOOLEAN, TAO_ATTR_READABLE | TAO_ATTR_WRITABLE,   \
             get_pattern_noise_reduction,                               \
             check_pattern_noise_reduction,                             \
             set_pattern_noise_reduction)                               \
    ATTR_DEF("pixelPulseReset", true, 0,                                \
             TAO_ATTR_BOOLEAN, TAO_ATTR_READABLE | TAO_ATTR_WRITABLE,   \
             get_pixel_pulse_reset, check_pixel_pulse_reset,            \
             set_pixel_pulse_reset)                                     \
    ATTR_DEF("pulseDrain", true, 0,                                     \
             TAO_ATTR_BOOLEAN, TAO_ATTR_READABLE | TAO_ATTR_WRITABLE,   \
             get_pulse_drain, check_pulse_drain, set_pulse_drain)       \
    ATTR_DEF("cameraModel", false, CAMERA_MODEL_VALUE_SIZE,             \
             TAO_ATTR_STRING, TAO_ATTR_READABLE,                        \
             get_camera_model, NULL, NULL)                              \
    ATTR_DEF("cameraVendor", false, CAMERA_VENDOR_VALUE_SIZE,           \
             TAO_ATTR_STRING, TAO_ATTR_READABLE,                        \
             get_camera_vendor, NULL, NULL)

static tao_attr_descr attr_defs[] = {
#define ATTR_DEF(KEY,UPDATE,SIZE,TYPE,FLAGS,GET,CHECK,SET) \
    (tao_attr_descr){ .key = KEY, .type = TYPE, .flags = FLAGS, .size = SIZE },
    ATTR_LIST
#undef ATTR_DEF
};

static const int attr_count = sizeof(attr_defs)/sizeof(tao_attr_descr);

static struct {
    char*     key;
    bool      update;
    accessor* get;
    checker*  check;
    mutator*  set;
} attr_ops[] = {
#define ATTR_DEF(KEY,UPDATE,SIZE,TYPE,FLAGS,GET,CHECK,SET) { KEY, UPDATE, GET, CHECK, SET },
    ATTR_LIST
#undef ATTR_DEF
};

// Update camera configuration according to current device settings.
static tao_status update_configuration(
    phnx_camera* cam,
    bool init)
{
    // Force updating all settings if requested.
    if (update_exposure_time(     cam) != TAO_OK ||
        update_frame_rate(        cam) != TAO_OK ||
        update_region_of_interest(cam) != TAO_OK ||
        update_camera_encoding(   cam) != TAO_OK) {
        return TAO_ERROR;
    }
    for (int i = 0; i < attr_count; ++i) {
        if (init || attr_ops[i].update) {
            if (attr_ops[i].get(cam, i) != TAO_OK) {
                return TAO_ERROR;
            }
        }
    }
    return TAO_OK;
}

static tao_status check_configuration_helper(
    phnx_camera* cam,
    const tao_camera_config* cfg,
    phnx_connection* connection,
    bool adj)
{
    const char* func = "tao_camera_check_configuration";
    if (check_exposure_time(     cam,  cfg->exposuretime) != TAO_OK ||
        check_frame_rate(        cam,  cfg->framerate   ) != TAO_OK ||
        check_region_of_interest(cam, &cfg->roi         ) != TAO_OK ||
        check_camera_encoding(   cam,  cfg->rawencoding ) != TAO_OK) {
        return TAO_ERROR;
    }
    for (int i = 0; i < attr_count; ++i) {
        if (attr_ops[i].check != NULL && attr_ops[i].check(func, cam, cfg, i) != TAO_OK) {
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

static tao_status check_configuration(
    phnx_camera* cam,
    const tao_camera_config* cfg)
{
    phnx_connection connection;
    return check_configuration_helper(cam, cfg, &connection, true);
}

static tao_status set_configuration(
    phnx_camera* cam,
    const tao_camera_config* cfg)
{
    // FIXME: Caller's name for error messages.
    const char* func = __func__;

    // Apply named parameter settings.
    const unsigned check_bits = TAO_ATTR_CHECK_FLAGS|TAO_ATTR_CHECK_TYPE|TAO_ATTR_CHECK_VALUE;
    for (int i = 0; i < attr_count; ++i) {
        const tao_attr* src = get_attr(cfg, i);
        tao_attr* dst = get_attr(cam, i);
        unsigned change = tao_attr_check(src, dst, check_bits);
        if (change != 0) {
            if (change != TAO_ATTR_CHECK_VALUE) {
                // Only the value is allowed to change.
                tao_store_error(func, TAO_CORRUPTED);
                return TAO_ERROR;
            }
            if (! TAO_ATTR_IS_WRITABLE(dst)) {
                tao_store_error(func, TAO_UNWRITABLE);
                return TAO_ERROR;
            }
            if (attr_ops[i].set(func, cam, cfg, i) != TAO_OK) {
                return TAO_ERROR;
            }
        }
    }

    // Reduce bits per pixel if requested.
    tao_encoding cfg_bpp = TAO_ENCODING_BITS_PER_PIXEL(cfg->rawencoding);
    tao_encoding cam_bpp = TAO_ENCODING_BITS_PER_PIXEL(cam->base.config.rawencoding);
    if (cfg_bpp < cam_bpp && set_camera_encoding(cam, cfg->rawencoding) != TAO_OK) {
        return TAO_ERROR;
    }

    // Before reducing frame rate, reduce exposure time if requested.
    bool exposuretime_done = false;
    if (cfg->exposuretime < cam->base.config.exposuretime) {
        if (set_exposure_time(cam, cfg->exposuretime) != TAO_OK) {
            return TAO_ERROR;
        }
        exposuretime_done = true;
    }

    // Reduce frame rate if requested.
    bool framerate_done = false;
    if (cfg->framerate < cam->base.config.framerate) {
        if (set_frame_rate(cam, cfg->framerate) != TAO_OK) {
            return TAO_ERROR;
        }
        framerate_done = true;
    }

    // Change the ROI if it has to change.
    if (set_region_of_interest(cam, &cfg->roi) != TAO_OK) {
        return TAO_ERROR;
    }

    // Augment exposure time if requested.
    if (!exposuretime_done && cfg->exposuretime > cam->base.config.exposuretime) {
        if (set_exposure_time(cam, cfg->exposuretime) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Augment frame rate if requested.
    if (!framerate_done && cfg->framerate > cam->base.config.framerate) {
        if (set_frame_rate(cam, cfg->framerate) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Change encoding of camera pixels.
    if (cfg->rawencoding != cam->base.config.rawencoding) {
        if (set_camera_encoding(cam, cfg->rawencoding) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    return TAO_OK;
}

static uint32_t get_configuration_number(
    int id)
{
    switch (id) {
    case 0: return CXP_USER_SET_SELECTOR_DEFAULT;
    case 1: return CXP_USER_SET_SELECTOR_USER_SET_1;
    case 2: return CXP_USER_SET_SELECTOR_USER_SET_2;
    case 3: return CXP_USER_SET_SELECTOR_USER_SET_3;
    default: return ~(uint32_t)0;
    }
}

// Load one of the configurations memorized by the camera.
static tao_status load_configuration(
    phnx_camera* cam,
    int id)
{
    uint32_t sel = get_configuration_number(id);
    if (sel == ~(uint32_t)0) {
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return TAO_ERROR;
    }
    if (cxp_set(cam, USER_SET_DEFAULT_SELECTOR, sel) != TAO_OK ||
        cxp_exec(cam, USER_SET_LOAD) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Save current camera settings. My understanding is that the default
// configuration (factory settings) cannot be overwritten.
static tao_status save_configuration(
    phnx_camera* cam,
    int id)
{
    uint32_t sel = get_configuration_number(id);
    if (sel == ~(uint32_t)0 || sel == CXP_USER_SET_SELECTOR_DEFAULT) {
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return TAO_ERROR;
    }
    if (cxp_set(cam, USER_SET_DEFAULT_SELECTOR, sel) != TAO_OK ||
        cxp_exec(cam, USER_SET_SAVE) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Get/check/set detector bias (black level).

static tao_status get_bias(
    phnx_camera* cam,
    int idx)
{
    uint32_t val;
    if (cxp_get(cam, BLACK_LEVEL, &val)                            != TAO_OK ||
        tao_attr_set_integer_value(get_attr(cam, idx), val, false) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status check_bias(
     const char* func,
     phnx_camera* cam,
     const tao_camera_config* cfg,
     int idx)
{
    const tao_attr* attr = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(attr) == TAO_ATTR_INTEGER) {
        int64_t val;
        if (tao_attr_get_integer_value(attr, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        if (val >= cxp_min(BLACK_LEVEL) && val <= cxp_max(BLACK_LEVEL)) {
            return TAO_OK;
        }
        fprintf(stderr, "Error: Bad bias %ld, min/max: %ld/%ld\n", (long)val,
                (long)cxp_min(BLACK_LEVEL), (long)cxp_max(BLACK_LEVEL));
    }
    tao_store_error(func, TAO_BAD_BIAS);
    return TAO_ERROR;
}

static tao_status set_bias(
    const char* func,
    phnx_camera* cam,
    const tao_camera_config* cfg,
    int idx)
{
    int64_t val;
    if (check_bias(func, cam, cfg, idx)                      != TAO_OK ||
        tao_attr_get_integer_value(get_attr(cfg, idx), &val) != TAO_OK ||
        cxp_set(cam, BLACK_LEVEL, val)                       != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Get/check/set detector gain.

static tao_status get_gain(
    phnx_camera* cam,
    int idx)
{
    uint32_t val;
    if (cxp_get(cam, GAIN, &val)                                   != TAO_OK ||
        tao_attr_set_integer_value(get_attr(cam, idx), val, false) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status check_gain(
     const char* func,
     phnx_camera* cam,
     const tao_camera_config* cfg,
     int idx)
{
    const tao_attr* attr = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(attr) == TAO_ATTR_INTEGER) {
        int64_t val;
        if (tao_attr_get_integer_value(attr, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        if (val >= cxp_min(GAIN) && val <= cxp_max(GAIN)) {
            return TAO_OK;
        }
        fprintf(stderr, "Error: Bad gain %ld, min/max: %ld/%ld\n", (long)val,
                (long)cxp_min(GAIN), (long)cxp_max(GAIN));
    }
    tao_store_error(func, TAO_BAD_GAIN);
    return TAO_ERROR;
}

static tao_status set_gain(
    const char* func,
    phnx_camera* cam,
    const tao_camera_config* cfg,
    int idx)
{
    int64_t val;
    if (check_gain(func, cam, cfg, idx)                      != TAO_OK ||
        tao_attr_get_integer_value(get_attr(cfg, idx), &val) != TAO_OK ||
        cxp_set(cam, GAIN, val)                              != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Set/get exposure time (device value is in µs).

static tao_status update_exposure_time(
    phnx_camera* cam)
{
    uint32_t val;
    if (cxp_get(cam, EXPOSURE_TIME, &val) != TAO_OK) {
        return TAO_ERROR;
    }
    cam->base.config.exposuretime = 1e-6*(double)val;
    return TAO_OK;
}

static tao_status check_exposure_time(
    phnx_camera* cam,
    double arg)
{
    if (!isfinite(arg) ||
        arg <= 1e-6*((double)cxp_min(EXPOSURE_TIME) - 0.5) ||
        arg >= 1e-6*((double)cxp_max(EXPOSURE_TIME) + 0.5)) {
        fprintf(stderr, "Error: Bad exposure time %g s, min/max: %g/%g s\n", arg,
                1e-6*(double)cxp_min(EXPOSURE_TIME),
                1e-6*(double)cxp_max(EXPOSURE_TIME));
        tao_store_error(__func__, TAO_BAD_EXPOSURETIME);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status set_exposure_time(
    phnx_camera* cam,
    double arg)
{
    if (check_exposure_time(cam, arg)                != TAO_OK ||
        cxp_set(cam, EXPOSURE_TIME, lround(arg*1e6)) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}


// Get/check/set acquisition frame rate (device value is in Hz).

static tao_status update_frame_rate(
    phnx_camera* cam)
{
    uint32_t val;
    if (cxp_get(cam, ACQUISITION_FRAME_RATE, &val) != TAO_OK) {
        return TAO_ERROR;
    }
    cam->base.config.framerate = (double)val;
    return TAO_OK;
}

static tao_status check_frame_rate(
    phnx_camera* cam,
    double arg)
{
    if (!isfinite(arg) ||
        arg <= (double)cxp_min(ACQUISITION_FRAME_RATE) - 0.5 ||
        arg >= (double)cxp_max(ACQUISITION_FRAME_RATE) + 0.5) {
        fprintf(stderr, "Error: Bad frame rate %g Hz, min/max: %g/%g Hz\n", arg,
                (double)cxp_min(ACQUISITION_FRAME_RATE),
                (double)cxp_max(ACQUISITION_FRAME_RATE));
        tao_store_error(__func__, TAO_BAD_FRAMERATE);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status set_frame_rate(
    phnx_camera* cam,
    double arg)
{
    if (check_frame_rate(cam, arg)                        != TAO_OK ||
        cxp_set(cam, ACQUISITION_FRAME_RATE, lround(arg)) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}


// Get/check/set region of interest.

static tao_status update_region_of_interest(
    phnx_camera* cam)
{
    uint32_t xbin, ybin, xoff, yoff, width, height;
    if (cxp_get(cam, DECIMATION_HORIZONTAL, &xbin) != TAO_OK ||
        cxp_get(cam, DECIMATION_VERTICAL,   &ybin) != TAO_OK ||
        cxp_get(cam, OFFSET_X,              &xoff) != TAO_OK ||
        cxp_get(cam, OFFSET_Y,              &yoff) != TAO_OK ||
        cxp_get(cam, WIDTH,                &width) != TAO_OK ||
        cxp_get(cam, HEIGHT,              &height) != TAO_OK) {
        return TAO_ERROR;
    }
    tao_camera_roi_define(&cam->roi, xbin, ybin, xoff, yoff, width, height);
    cam->base.config.roi = cam->roi;
    return TAO_OK;
}

static tao_status check_region_of_interest(
    phnx_camera* cam,
    const tao_camera_roi* arg)
{
    // NOTE: Rebinning is not yet supported.
    if (arg->xbin != 1 || arg->ybin != 1 ||
        tao_camera_roi_check(arg,
                             cam->base.config.sensorwidth,
                             cam->base.config.sensorheight) != TAO_OK) {
        tao_store_error(__func__, TAO_BAD_ROI);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status set_region_of_interest(
    phnx_camera* cam,
    const tao_camera_roi* arg)
{
    // Check input ROI.
    if (check_region_of_interest(cam, arg) != TAO_OK) {
        return TAO_ERROR;
    }

    // Compute the ROI to set for the device so as to encompass the requested ROI and
    // account for hardware constraints.
    long xbin   = arg->xbin;
    long ybin   = arg->ybin;
    long xoff   = TAO_ROUND_DOWN(arg->xoff, CXP_HORIZONTAL_INCREMENT);
    long yoff   = TAO_ROUND_DOWN(arg->yoff, CXP_VERTICAL_INCREMENT);
    long width  = TAO_ROUND_UP(arg->xoff + arg->width, CXP_HORIZONTAL_INCREMENT) - xoff;
    long height = TAO_ROUND_UP(arg->yoff + arg->height, CXP_VERTICAL_INCREMENT) - yoff;

    // Set ROI parameters in such an order that the current ROI remains valid.
    if ((xbin   != cam->roi.xbin   && cxp_set(cam, DECIMATION_HORIZONTAL, xbin) != TAO_OK) ||
        (ybin   != cam->roi.ybin   && cxp_set(cam, DECIMATION_VERTICAL,   ybin) != TAO_OK) ||
        (xoff   <  cam->roi.xoff   && cxp_set(cam, OFFSET_X,              xoff) != TAO_OK) ||
        (width  != cam->roi.width  && cxp_set(cam, WIDTH,                width) != TAO_OK) ||
        (xoff   >  cam->roi.xoff   && cxp_set(cam, OFFSET_X,              xoff) != TAO_OK) ||
        (yoff   <  cam->roi.yoff   && cxp_set(cam, OFFSET_Y,              yoff) != TAO_OK) ||
        (height != cam->roi.height && cxp_set(cam, HEIGHT,              height) != TAO_OK) ||
        (yoff   >  cam->roi.yoff   && cxp_set(cam, OFFSET_Y,              yoff) != TAO_OK)) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Set/check/get of pixels sent by the camera.

static tao_status update_camera_encoding(
    phnx_camera* cam)
{
    uint32_t val;
    tao_encoding enc = TAO_ENCODING_UNKNOWN;
    if (cxp_get(cam, PIXEL_FORMAT, &val) == TAO_OK) {
        if (val == CXP_PIXEL_FORMAT_MONO8) {
            enc = TAO_ENCODING_MONO(8);
        } else if (val == CXP_PIXEL_FORMAT_MONO10) {
            enc = TAO_ENCODING_MONO(10);
        } else if (val == CXP_PIXEL_FORMAT_BAYERGR8) {
            enc = TAO_ENCODING_BAYER_GRBG(8);
        } else if (val == CXP_PIXEL_FORMAT_BAYERGR10) {
            enc = TAO_ENCODING_BAYER_GRBG(10);
        } else {
            tao_store_error(__func__, TAO_BAD_ENCODING);
        }
    }
    cam->base.config.rawencoding = enc;
    return (enc == TAO_ENCODING_UNKNOWN) ? TAO_ERROR : TAO_OK;
}

static tao_status check_camera_encoding(
    phnx_camera* cam,
    tao_encoding enc)
{
    if (enc == TAO_ENCODING_MONO(8) || enc == TAO_ENCODING_MONO(10)) {
        if (is_monochrome(cam)) {
            return TAO_OK;
        }
    } else if (enc == TAO_ENCODING_BAYER_GRBG(8) || enc == TAO_ENCODING_BAYER_GRBG(10)) {
        if (is_color(cam)) {
            return TAO_OK;
        }
    }
    tao_store_error(__func__, TAO_BAD_ENCODING);
    return TAO_ERROR;
}

static tao_status set_camera_encoding(
    phnx_camera* cam,
    tao_encoding enc)
{
    uint32_t cur, req;
    if (enc == TAO_ENCODING_MONO(8)) {
        req = CXP_PIXEL_FORMAT_MONO8;
    } else if (enc == TAO_ENCODING_MONO(10)) {
        req = CXP_PIXEL_FORMAT_MONO10;
    } else if (enc == TAO_ENCODING_BAYER_GRBG(8)) {
        req = CXP_PIXEL_FORMAT_BAYERGR8;
    } else if (enc == TAO_ENCODING_BAYER_GRBG(10)) {
        req = CXP_PIXEL_FORMAT_BAYERGR10;
    } else {
        tao_store_error(__func__, TAO_BAD_ENCODING);
        return TAO_ERROR;
    }
    if (cxp_get(cam, PIXEL_FORMAT, &cur) != TAO_OK ||
        (cur != req && cxp_set(cam, PIXEL_FORMAT, req) != TAO_OK)) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

static bool is_monochrome(
    phnx_camera* cam)
{
    tao_encoding enc = cam->base.config.rawencoding;
    return (enc == TAO_ENCODING_MONO(8) || enc == TAO_ENCODING_MONO(10));
}

static bool is_color(
    phnx_camera* cam)
{
    tao_encoding enc = cam->base.config.rawencoding;
    return (enc == TAO_ENCODING_BAYER_GRBG(8) || enc == TAO_ENCODING_BAYER_GRBG(10));
}


// Get/check/set connection parameters.

static tao_status attr_encode_connection(
    tao_attr* attr,
    const phnx_connection* conn)
{
    char buf[CONNECTION_VALUE_SIZE];
    int rv = phnx_connection_encode(buf, sizeof(buf), conn);
    if (rv < 0) {
        return TAO_ERROR;
    }
    if (rv >= sizeof(buf)) {
        // String size is too short.
        tao_store_error(__func__, TAO_BAD_SIZE);
        return TAO_ERROR;
    }
    return tao_attr_set_string_value(attr, buf, true);
}

static tao_status attr_check_connection(
    const char* func,
    phnx_connection* conn,
    const tao_attr* attr,
    bool adj)
{
    if (phnx_connection_decode(conn, attr->val.s) != TAO_OK) {
        return TAO_ERROR;
    }
    if (conn->channels < 1 || conn->channels > 4) {
        tao_store_error(func, TAO_BAD_CHANNELS);
        return TAO_ERROR;
    }
    if (conn->speed < 0) {
    bad_speed:
        tao_store_error(func, TAO_BAD_SPEED);
        return TAO_ERROR;
    }
    int64_t speed = conn->speed;
    int64_t speeds[5] = {1250, 2500, 3125, 5000, 6250};
    if (adj) {
        int imin = 0;
        int64_t dmin = -1;
        for (int i = 0; i < 5; ++i) {
            int64_t d = speed - speeds[i];
            if (d < 0) {
                d = -d;
            }
            if (i == 0 || d < dmin) {
                imin = i;
                dmin = d;
            }
        }
        conn->speed = speeds[imin];
    } else {
        bool flag = false;
        for (int i = 0; i < 5; ++i) {
            if (speed == speeds[i]) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            goto bad_speed;
        }
    }
    return TAO_OK;
}

static tao_status get_connection(
    phnx_camera* cam,
    int idx)
{
    phnx_connection conn;
    const uint32_t msk = (CXP_CONNECTION_CONFIG_CONNECTION_1 |
                          CXP_CONNECTION_CONFIG_CONNECTION_2 |
                          CXP_CONNECTION_CONFIG_CONNECTION_3 |
                          CXP_CONNECTION_CONFIG_CONNECTION_4);
    uint32_t val;
    if (cxp_get(cam, CONNECTION_CONFIG, &val) != TAO_OK) {
        return TAO_ERROR;
    }
    switch (val & msk) {
    case CXP_CONNECTION_CONFIG_CONNECTION_1:
        conn.channels = 1;
        break;
    case CXP_CONNECTION_CONFIG_CONNECTION_2:
        conn.channels = 2;
        break;
    case CXP_CONNECTION_CONFIG_CONNECTION_3:
        conn.channels = 3;
        break;
    case CXP_CONNECTION_CONFIG_CONNECTION_4:
        conn.channels = 4;
        break;
    default:
        tao_store_error(__func__, TAO_ASSERTION_FAILED);
        return TAO_ERROR;
    }
    switch (val & ~msk) {
    case CXP_CONNECTION_CONFIG_SPEED_1250:
        conn.speed = 1250;
        break;
    case CXP_CONNECTION_CONFIG_SPEED_2500:
        conn.speed = 2500;
        break;
    case CXP_CONNECTION_CONFIG_SPEED_3125:
        conn.speed = 3125;
        break;
    case CXP_CONNECTION_CONFIG_SPEED_5000:
        conn.speed = 5000;
        break;
    case CXP_CONNECTION_CONFIG_SPEED_6250:
        conn.speed = 6250;
        break;
    default:
        tao_store_error(__func__, TAO_ASSERTION_FAILED);
        return TAO_ERROR;
    }
    return attr_encode_connection(get_attr(cam, idx), &conn);
}

static tao_status check_connection(
    const char* func,
    phnx_camera* cam,
    const tao_camera_config* cfg,
    int idx)
{
    phnx_connection conn;
    if (attr_check_connection(
            func, &conn, get_attr(cfg, idx), true) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status set_connection(
    const char* func,
    phnx_camera* cam,
    const tao_camera_config* cfg,
    int idx)
{
    phnx_connection conn;
    if (attr_check_connection(
            func, &conn, get_attr(cfg, idx), true) != TAO_OK) {
        return TAO_ERROR;
    }

#if USE_FRAMEGRABBER_FOR_SETTING_CONNECTION
    if (phx_define_coaxpress_connection(cam, &conn) != TAO_OK) {
        return TAO_ERROR;
    }
    if (get_connection(cam, idx) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
#else
    uint32_t val;
    switch (conn->channels) {
    case 1:
        val = CXP_CONNECTION_CONFIG_CONNECTION_1;
        break;
    case 2:
        val = CXP_CONNECTION_CONFIG_CONNECTION_2;
        break;
    case 3:
        val = CXP_CONNECTION_CONFIG_CONNECTION_3;
        break;
    case 4:
        val = CXP_CONNECTION_CONFIG_CONNECTION_4;
        break;
    default:
        tao_store_error(__func__, TAO_BAD_CHANNELS);
        return TAO_ERROR;
    }
    switch (conn->speed) {
    case 1250:
        val |= CXP_CONNECTION_CONFIG_SPEED_1250;
        break;
    case 2500:
        val |= CXP_CONNECTION_CONFIG_SPEED_2500;
        break;
    case 3125:
        val |= CXP_CONNECTION_CONFIG_SPEED_3125;
        break;
    case 5000:
        val |= CXP_CONNECTION_CONFIG_SPEED_5000;
        break;
    case 6250:
        val |= CXP_CONNECTION_CONFIG_SPEED_6250;
        break;
        tao_store_error(__func__, TAO_BAD_SPEED);
        return TAO_ERROR;
    }
    if (cxp_set(cam, CONNECTION_CONFIG, val) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
#endif
}

// Temperature

static tao_status get_temperature(
    phnx_camera* cam,
    int idx)
{
    tao_status status;
    double temp;
    int32_t val; // must be signed
    if (cxp_set(cam, DEVICE_INFORMATION_SELECTOR,
                CXP_DEVICE_INFORMATION_SELECTOR_TEMPERATURE) != TAO_OK ||
        cxp_get(cam, DEVICE_INFORMATION, (uint32_t*)&val) != TAO_OK) {
        temp = NAN;
        status = TAO_ERROR;
    } else {
        temp = val/2.0;
        status = TAO_OK;
    }
    get_attr(cam, idx)->val.f = temp;
    return status;
}

// Filter mode.

static tao_status get_filter_mode(
    phnx_camera* cam,
    int idx)
{
    uint32_t mode;
    if (cxp_get(cam, FILTER_MODE, &mode) != TAO_OK) {
        return TAO_ERROR;
    }
    tao_attr* attr = get_attr(cam, idx);
    for (int i = 0; filter_mode_table[i].name != NULL; ++i) {
        if (filter_mode_table[i].value == mode) {
            return tao_attr_set_string_value(attr, filter_mode_table[i].name, true);
        }
    }
    tao_store_error(__func__, TAO_BAD_VALUE);
    return TAO_ERROR;
}

static tao_status check_filter_mode(
    const char* func,
    phnx_camera* cam,
    const tao_camera_config* cfg,
    int idx)
{
    const tao_attr* attr = get_attr(cfg, idx);
    const char* val;
    if (tao_attr_get_string_value(attr, &val) == TAO_OK) {
        for (int i = 0; filter_mode_table[i].name != NULL; ++i) {
            if (strcmp(val, filter_mode_table[i].name) == 0) {
                return TAO_OK;
            }
        }
    }
    tao_store_error(__func__, TAO_BAD_VALUE);
    return TAO_ERROR;
}

static tao_status set_filter_mode(
    const char* func,             // caller's name
    phnx_camera* cam,             // camera
    const tao_camera_config* cfg, // configuration to check
    int idx)                      // attribute index in configuration
{
    const tao_attr* src = get_attr(cfg, idx);
    const char* val;
    if (tao_attr_get_string_value(src, &val) == TAO_OK) {
        for (int i = 0; filter_mode_table[i].name != NULL; ++i) {
            if (strcmp(val, filter_mode_table[i].name) == 0) {
                uint32_t oldval, newval = filter_mode_table[i].value;
                if (cxp_get(cam, FILTER_MODE, &oldval) != TAO_OK ||
                    (newval != oldval && cxp_set(cam, FILTER_MODE, newval) != TAO_OK)) {
                    return TAO_ERROR;
                }
                return TAO_OK;
            }
        }
    }
    tao_store_error(func, TAO_BAD_VALUE);
    return TAO_ERROR;
}

// Boolean named attributes.

#define ENCODE(attr, id)                                                \
    static tao_status get_##attr(                                       \
        phnx_camera* cam,                                               \
        int idx)                                                        \
    {                                                                   \
        uint32_t val;                                                   \
        if (cxp_get(cam, id, &val) != TAO_OK) {                         \
            return TAO_ERROR;                                           \
        }                                                               \
        tao_attr* attr = get_attr(cam, idx);                            \
        return tao_attr_set_boolean_value(attr, val != 0, true);        \
    }                                                                   \
                                                                        \
    static tao_status check_##attr(                                     \
        const char* func,                                               \
        phnx_camera* cam,                                               \
        const tao_camera_config* cfg,                                   \
        int idx)                                                        \
    {                                                                   \
        const tao_attr* attr = get_attr(cfg, idx);                      \
        if (TAO_ATTR_TYPE(attr) != TAO_ATTR_BOOLEAN) {                  \
            tao_store_error(func, TAO_BAD_TYPE);                        \
            return TAO_ERROR;                                           \
        }                                                               \
        return TAO_OK;                                                  \
    }                                                                   \
                                                                        \
    static tao_status set_##attr(                                       \
        const char* func,                                               \
        phnx_camera* cam,                                               \
        const tao_camera_config* cfg,                                   \
        int idx)                                                        \
    {                                                                   \
        const tao_attr* src = get_attr(cfg, idx);                       \
        int newval;                                                     \
        if (tao_attr_get_boolean_value(src, &newval) != TAO_OK) {       \
            return TAO_ERROR;                                           \
        }                                                               \
        uint32_t oldval;                                                \
        if (cxp_get(cam, id, &oldval) != TAO_OK) {                      \
            return TAO_ERROR;                                           \
        }                                                               \
        if ((newval != 0) != (oldval != 0) &&                           \
            cxp_set(cam, id, (newval != 0)) != TAO_OK) {                \
            return TAO_ERROR;                                           \
        }                                                               \
        return TAO_OK;                                                  \
    }

ENCODE(pattern_noise_reduction, FIXED_PATTERN_NOISE_REDUCTION);
ENCODE(pixel_pulse_reset,       PRST_ENABLE);
ENCODE(pulse_drain,             PULSE_DRAIN_ENABLE);

#undef ENCODE

static tao_status get_camera_model(
    phnx_camera* cam,
    int idx)
{
    tao_attr* attr = get_attr(cam, idx);
    return tao_attr_set_string_value(attr, cam->model, true);
}

static tao_status get_camera_vendor(
    phnx_camera* cam,
    int idx)
{
    tao_attr* attr = get_attr(cam, idx);
    return tao_attr_set_string_value(attr, cam->vendor, true);
}

static bool identify_camera(
    phnx_camera* cam)
{
    return (cam != NULL && cam->is_coaxpress &&
            strcmp(cam->vendor, "Mikrotron GmbH") == 0 &&
            strncmp(cam->model, "MC408", 5) == 0 &&
            (cam->model[5] == '2' || cam->model[5] == '3' ||
             cam->model[5] == '6' || cam->model[5] == '7'));
}

static tao_status initialize_camera(
    phnx_camera* cam)
{
    // Check camera model.
    if (! identify_camera(cam)) {
        tao_store_error(__func__, TAO_BAD_DEVICE);
        return TAO_ERROR;
    }

    // Instantiate table of named attributes.
    tao_attr_table* attr_table = &cam->base.config.attr_table;
    if (tao_attr_table_instantiate(attr_table, attr_defs, attr_count) != TAO_OK) {
        return TAO_ERROR;
    }
    for (int i = 0; i < attr_count; ++i) {
        tao_attr* attr = get_attr(cam, i);
        if (TAO_ATTR_TYPE(attr) == TAO_ATTR_FLOAT) {
            tao_attr_set_float_value(attr, NAN, true);
        }
    }

    // Disable fixed pattern noise reduction.
    if (cxp_set(cam, FIXED_PATTERN_NOISE_REDUCTION, 0) != TAO_OK) {
        return TAO_ERROR;
    }

    // Set image filter mode to "raw".
    if (cxp_set(cam, FILTER_MODE, CXP_FILTER_MODE_RAW) != TAO_OK) {
        return TAO_ERROR;
    }

    // Disable the Pixel Pulse Reset feature (recommanded for frame rates
    // higher or equal to 100 Hz).
    if (cxp_set(cam, PRST_ENABLE, 0) != TAO_OK) {
        return TAO_ERROR;
    }

    // Get sensor size.
    uint32_t sensorwidth, sensorheight;
    if (cxp_get(cam, SENSOR_WIDTH,  &sensorwidth) != TAO_OK ||
        cxp_get(cam, SENSOR_HEIGHT, &sensorheight) != TAO_OK) {
        return TAO_ERROR;
    }
    tao_forced_store(&cam->base.config.sensorwidth, sensorwidth);
    tao_forced_store(&cam->base.config.sensorheight, sensorheight);

    // Update other device information.
    if (update_configuration(cam, true) != TAO_OK) {
        return TAO_ERROR;
    }

    // Reset sub-sampling.
    if (cam->roi.xbin != 1 || cam->roi.ybin != 1) {
        tao_camera_roi roi = cam->roi;
        tao_camera_roi_define(&roi, 1, 1, cam->roi.xoff, cam->roi.yoff,
                              cam->roi.xbin*cam->roi.width,
                              cam->roi.ybin*cam->roi.height);
        if (set_region_of_interest(cam, &roi) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // The following settings are the same as the contents of the configuration
    // file "Mikrotron_MC4080_CXP.pcf".
    if (phx_set(cam, PHX_BOARD_VARIANT, (etParamValue)PHX_DIGITAL) != TAO_OK ||
        phx_set(cam, PHX_CAM_TYPE,           PHX_CAM_AREASCAN_ROI) != TAO_OK ||
        phx_set(cam, PHX_CAM_FORMAT,       PHX_CAM_NON_INTERLACED) != TAO_OK ||
        phx_set(cam, PHX_CAM_CLOCK_POLARITY,    PHX_CAM_CLOCK_POS) != TAO_OK ||
        phx_set(cam, PHX_CAM_SRC_COL,            PHX_CAM_SRC_MONO) != TAO_OK ||
        phx_set(cam, PHX_CAM_DATA_VALID,              PHX_DISABLE) != TAO_OK ||
        phx_set(cam, PHX_CAM_HTAP_NUM,                          1) != TAO_OK ||
        phx_set(cam, PHX_CAM_HTAP_DIR,          PHX_CAM_HTAP_LEFT) != TAO_OK ||
        phx_set(cam, PHX_CAM_HTAP_TYPE,       PHX_CAM_HTAP_LINEAR) != TAO_OK ||
        phx_set(cam, PHX_CAM_HTAP_ORDER,   PHX_CAM_HTAP_ASCENDING) != TAO_OK ||
        phx_set(cam, PHX_CAM_VTAP_NUM,                          1) != TAO_OK ||
        phx_set(cam, PHX_CAM_VTAP_DIR,           PHX_CAM_VTAP_TOP) != TAO_OK ||
        phx_set(cam, PHX_CAM_VTAP_TYPE,       PHX_CAM_VTAP_LINEAR) != TAO_OK ||
        phx_set(cam, PHX_CAM_VTAP_ORDER,   PHX_CAM_VTAP_ASCENDING) != TAO_OK ||
        phx_set(cam, PHX_COMMS_DATA,             PHX_COMMS_DATA_8) != TAO_OK ||
        phx_set(cam, PHX_COMMS_STOP,             PHX_COMMS_STOP_1) != TAO_OK ||
        phx_set(cam, PHX_COMMS_PARITY,      PHX_COMMS_PARITY_NONE) != TAO_OK ||
        phx_set(cam, PHX_COMMS_SPEED,                        9600) != TAO_OK ||
        phx_set(cam, PHX_COMMS_FLOW,          PHX_COMMS_FLOW_NONE) != TAO_OK) {
        return TAO_ERROR;
    }

    // Set acquisition parameters.  FIXME: should be in "start" virtual method?
    if (phx_set(cam, PHX_DATASTREAM_VALID, PHX_DATASTREAM_ALWAYS) != TAO_OK ||
        phx_set(cam, PHX_TIMEOUT_DMA,    1000 /* milliseconds */) != TAO_OK) {
        return TAO_ERROR;
    }

    // Use native byte order for the destination buffer.  FIXME: should be in
    // "start" virtual method?
    if (TAO_IS_LITTLE_ENDIAN) {
        if (phx_set(cam, PHX_DST_ENDIAN, PHX_DST_LITTLE_ENDIAN) != TAO_OK) {
            return TAO_ERROR;
        }
    } else if (TAO_IS_BIG_ENDIAN) {
        if (phx_set(cam, PHX_DST_ENDIAN, PHX_DST_BIG_ENDIAN) != TAO_OK) {
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

static tao_status start(
    phnx_camera* cam)
{
    return cxp_exec(cam, ACQUISITION_START);
}

static tao_status stop(
    phnx_camera* cam)
{
    return cxp_exec(cam, ACQUISITION_STOP);
}

// Table of virtual methods for the Mikrotron-MC408x CoaXPress cameras.
const phnx_operations phnx_mikrotron_mc408x = {
    "Mikrotron-MC408x",
    identify_camera,
    initialize_camera,
    start,
    stop,
    update_configuration,
    check_configuration,
    set_configuration,
    save_configuration,
    load_configuration,
};
