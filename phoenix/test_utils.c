// test-utils.c -
//
// Test utility functions in TAO-Phoenix library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include <tao-phoenix.h>

#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

#define test(expr)                                              \
    do {                                                        \
        if (! (expr)) {                                         \
            fprintf(stderr, "assertion failed: %s\n", #expr);   \
            ++nerrors;                                          \
        }                                                       \
        ++ntests;                                               \
    } while (false)

int main(
    int argc,
    char* argv[])
{
    long nerrors = 0, ntests = 0;

    char buf[PHNX_CAMERA_CONFIG_CONNECTION_SIZE];
    phnx_connection conn;
    //conn = (phnx_connection){.channels = 2, .speed = 6250};
    int rv;

    rv = phnx_connection_encode(
        buf, sizeof(buf), &(phnx_connection){.channels = 2, .speed = 6250});
    test(rv >= 0 && rv < sizeof(buf));
    test(strcmp(buf, "2*6250 Mbps") == 0);

    rv = phnx_connection_encode(
        buf, sizeof(buf), &(phnx_connection){.channels = 1, .speed = 1250});
    test(rv >= 0 && rv < sizeof(buf));
    test(strcmp(buf, "1*1250 Mbps") == 0);

    test(phnx_connection_decode(&conn, "2*6.25Gbps") == TAO_OK);
    test(conn.channels == 2 && conn.speed == 6250);

    test(phnx_connection_decode(&conn, "2 * 6.25 Gbps") == TAO_OK);
    test(conn.channels == 2 && conn.speed == 6250);

    test(phnx_connection_decode(&conn, "4 * 5000 Mbps") == TAO_OK);
    test(conn.channels == 4 && conn.speed == 5000);

    test(phnx_connection_decode(&conn, "1*1250000 kbps") == TAO_OK);
    test(conn.channels == 1 && conn.speed == 1250);

    test(phnx_connection_decode(&conn, "1250 Mbps") == TAO_OK);
    test(conn.channels == 1 && conn.speed == 1250);

    test(phnx_connection_decode(&conn, "1.250 Gbps") == TAO_OK);
    test(conn.channels == 1 && conn.speed == 1250);

    test(phnx_connection_decode(&conn, "-2 * 6.25Gbps") == TAO_ERROR);
    test(phnx_connection_decode(&conn, "2*6.25Gbpsu") == TAO_ERROR);
    test(phnx_connection_decode(&conn, "2*6250mbps") == TAO_ERROR);


    // Summary.
    fprintf(stdout, "%ld test(s) passed\n", ntests - nerrors);
    fprintf(stdout, "%ld test(s) failed\n", nerrors);

    return (nerrors == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
