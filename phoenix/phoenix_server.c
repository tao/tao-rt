// phoenix-server.c -
//
// Program to run image server for a camera connected to an ActiveSilicon
// Phoenix frame grabber.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2018-2024, Éric Thiébaut.

#include <math.h>
#include <stdio.h>
#include <string.h>

#include "tao-phoenix.h"
#include "tao-errors.h"
#include "tao-camera-servers.h"
#include "tao-cameras-private.h"

static int parse_create_options(
    phnx_create_options* opts,
    const char* arg)
{
    *opts = PHNX_CREATE_OPTIONS_DEFAULT;
    if (arg == NULL) {
        return 0;
    }
    int i1, i2, i3, i4;
    char dummy;
    if (sscanf(arg, " %n%*[0-9A-Za-z]%n : %n%*[0-9A-Za-z]%n %c",
               &i1, &i2, &i3, &i4, &dummy) != 4) {
        return -1;
    }
    if (i2 - i1 == 4 && strncmp(arg + i1, "auto", 4) == 0) {
        opts->boardnumber = PHNX_BOARD_NUMBER_AUTO;
    } else if (i2 - i1 == 1) {
        switch (arg[i1]) {
        case '1': opts->boardnumber = PHNX_BOARD_NUMBER_1; break;
        case '2': opts->boardnumber = PHNX_BOARD_NUMBER_2; break;
        case '3': opts->boardnumber = PHNX_BOARD_NUMBER_3; break;
        case '4': opts->boardnumber = PHNX_BOARD_NUMBER_4; break;
        case '5': opts->boardnumber = PHNX_BOARD_NUMBER_5; break;
        case '6': opts->boardnumber = PHNX_BOARD_NUMBER_6; break;
        case '7': opts->boardnumber = PHNX_BOARD_NUMBER_7; break;
        default: return -1;
        }
    } else {
        return -1;
    }
    if (i3 - i4 == 4 && strncmp(arg + i3, "auto", 4) == 0) {
        opts->channelnumber = PHNX_CHANNEL_NUMBER_AUTO;
    } else if (i4 - i3 == 1) {
        switch (arg[i3]) {
        case '1': opts->channelnumber = PHNX_CHANNEL_NUMBER_1; break;
        case '2': opts->channelnumber = PHNX_CHANNEL_NUMBER_2; break;
        default: return -1;
        }
    } else {
        return -1;
    }
    return 0;
}

int main(
    int argc,
    char* argv[])
{
    // Determine program name.
    const char* progname = tao_basename(argv[0]);

    // Parse arguments.
    phnx_create_options opts = PHNX_CREATE_OPTIONS_DEFAULT;
    const char* name = "Phoenix";
    bool debug = false;
    bool fancy = true;
    long nbufs = 20;
    unsigned int perms = 0077;
    char dummy;
    const char* usage =
        "Usage: %s [OPTIONS ...] [--] [[BOARD:CHANNEL] NAME]\n";
    int iarg = 1;
    while (iarg < argc) {
        if (argv[iarg][0] != '-') {
            break;
        }
        if (strcmp(argv[iarg], "--") == 0) {
            iarg += 1;
            break;
        }
        if (strcmp(argv[iarg], "-h") == 0
            || strcmp(argv[iarg], "-help") == 0
            || strcmp(argv[iarg], "--help") == 0) {
            printf("\n");
            printf(usage, progname);
            printf("\n");
            printf("Launch a server for a camera connected to an "
                   "ActiveSilicon \"Phoenix\" framegrabber.\n");
            printf("\n");
            printf("Arguments:\n");
            printf("  BOARD:CHANNEL      Board and channel numbers (can be `auto`) [auto:auto].\n");
            printf("  NAME               Server name [%s].\n", name);
            printf("\n");
            printf("Options:\n");
            printf("  -nbufs NBUFS       Number of output buffers [%ld].\n", nbufs);
            printf("  -perms BITS        Bitwise mask of permissions [0%o].\n", perms);
            printf("  -coaxpress_read_delay SECS\n"
                   "                     Time to sleep for reading CoaXPress link [%gs].\n",
                   opts.coaxpress_read_delay);
            printf("  -nofancy           Do not use colors nor set window title.\n");
            printf("  -debug             Debug mode [%s].\n", (debug ? "true" : "false"));
            printf("  -h, -help, --help  Print this help.\n");
            printf("\n");
            return EXIT_SUCCESS;
        }
        if (strcmp(argv[iarg], "-nbufs") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option `%s`\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%ld %c", &nbufs, &dummy) != 1
                || nbufs < 2) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option `%s`\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            iarg += 2;
            continue;
        }
        if (strcmp(argv[iarg], "-perms") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option `%s`\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%i %c", (int*)&perms, &dummy) != 1) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option `%s`\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            perms &= 0777;
            iarg += 2;
            continue;
        }
        if (strcmp(argv[iarg], "-coaxpress_read_delay") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option `%s`\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%lf %c", &opts.coaxpress_read_delay, &dummy) != 1 ||
                !isfinite(opts.coaxpress_read_delay) || opts.coaxpress_read_delay < 0.0) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option `%s`\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            perms &= 0777;
            iarg += 2;
            continue;
        }
        if (strcmp(argv[iarg], "-nofancy") == 0) {
            fancy = false;
            iarg += 1;
            continue;
        }
        if (strcmp(argv[iarg], "-debug") == 0) {
            debug = true;
            iarg += 1;
            continue;
        }
        fprintf(stderr, "%s: Unknown option `%s`\n", progname, argv[iarg]);
        return EXIT_FAILURE;
    }
    if (argc - iarg > 2) {
        fprintf(stderr, "%s: Too many arguments\n", progname);
        fprintf(stderr, usage, progname);
        return EXIT_FAILURE;
    }
    if (argc - iarg >= 2) {
        if (parse_create_options(&opts, argv[argc - 2]) != 0) {
            fprintf(stderr, "%s: Invalid `BOARD:CHANNEL` argument\n",
                    progname);
            return EXIT_FAILURE;
        }
    }
    if (argc - iarg >= 1) {
       name = argv[argc - 1];
    }
    if (name == NULL || name[0] == 0) {
        fprintf(stderr, "%s: Invalid server name\n", progname);
        return EXIT_FAILURE;
    } else if (strlen(name) >= TAO_OWNER_SIZE) {
        fprintf(stderr, "%s: Server name too long\n", progname);
        return EXIT_FAILURE;
    }

    // Value returned on exit and created resources.
    int retval = EXIT_SUCCESS;
    tao_camera* cam = NULL;
    tao_camera_server* srv = NULL;

    // Open the camera device.  The camera state is set to "initializing" until
    // the worker thread has started.

    cam = phnx_create_camera(NULL, NULL, &opts);
    if (cam == NULL) {
        fprintf(stderr, "%s: Failed to open camera device.\n", progname);
        goto error;
    }
    if (debug) {
        fprintf(stderr, "%s: Camera device now open\n", progname);
    }

    // Retrieve initial configuration.  Make sure to synchronize the actual
    // configuration after any changes in case some parameters are not exactly
    // the requested ones.
    tao_camera_config cfg;
    if (tao_camera_get_configuration(cam, &cfg) != TAO_OK) {
        fprintf(stderr, "%s: failed to retrieve camera configuration\n", progname);
        goto error;
    }

    // Create the camera server.
    srv = tao_camera_server_create(name, cam, nbufs, perms);
    if (srv == NULL) {
        fprintf(stderr, "%s: Failed to create the camera server\n", progname);
        goto error;
    }
    if (debug) {
        fprintf(stderr, "%s: Camera server \"%s\" created with %ld buffers\n",
                progname, tao_camera_server_get_owner(srv), nbufs);
    }
    if (debug) {
        srv->loglevel = TAO_MESG_DEBUG;
    } else {
        srv->loglevel = TAO_MESG_INFO;
    }
    srv->fancy = fancy;

    // Run the server loop and destroy the server.
    if (tao_camera_server_run_loop(srv) != TAO_OK) {
        fprintf(stderr, "%s: Failed to create the camera server\n", progname);
        goto error;
    }

done:
    // Destroy the server.
    if (srv != NULL && tao_camera_server_destroy(srv) != TAO_OK) {
        fprintf(stderr, "%s: Failed to destroy the camera server\n", progname);
        retval = EXIT_FAILURE;
    }
    // Close the camera device.
    if (cam != NULL && tao_camera_destroy(cam) != TAO_OK) {
        fprintf(stderr, "%s: Failed to destroy the camera device\n", progname);
        retval = EXIT_FAILURE;
    }
    if (tao_any_errors(NULL)) {
        tao_report_error();
        retval = EXIT_FAILURE;
    }
    return retval;

error:
    retval = EXIT_FAILURE;
    goto done;
}
