// utils.c -
//
// Useful functions for TAO-Phoenix library.  These functions do not need the
// ActiveSilicon SDK to be used and can thus be tested with no connected
// cameras.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2022, Éric Thiébaut.

#include "tao-errors.h"
#include "tao-phoenix.h"

#include <math.h>
#include <string.h>
#include <stdio.h>

int phnx_connection_encode(
    char* str,
    size_t siz,
    const phnx_connection* con)
{
    // Check arguments.
    if (str == NULL || con == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }

    // Format string.
    int rv = snprintf(str, siz, "%d*%d Mbps",
                      (int)con->channels, (int)con->speed);
    if (rv < 0) {
        tao_store_system_error("snprintf");
        return -1;
    }
    return rv;
}

tao_status phnx_connection_decode(
    phnx_connection* con,
    const char* str)
{
    // Check arguments.
    if (str == NULL || con == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }

    // Parse string.
    char units, dummy;
    long num;
    double speed;
    if (sscanf(str, "%ld * %lf %cbps %c",
               &num, &speed, &units, & dummy) != 3) {
        if (sscanf(str, " %lf %cbps %c",
                   &speed, &units, &dummy) != 2) {
            goto bad_speed;
        }
        num = 1;
    }
    if (num <= 0 || num > TAO_INT32_MAX) {
        tao_store_error(__func__, TAO_BAD_CHANNELS);
        return TAO_ERROR;
    }

    // Convert speed in Mbps.
    if (units == 'G') {
        speed = 1e3*speed;
    } else if (units == 'k') {
        speed /= 1e3;
    } else if (units != 'M') {
        goto bad_speed;
    }
    speed = round(speed);
    if (!isfinite(speed) || speed < 0 || speed > TAO_INT32_MAX) {
        goto bad_speed;
    }

    // Store result.
    con->channels = num;
    con->speed = speed;
    return TAO_OK;

bad_speed:
    tao_store_error(__func__, TAO_BAD_SPEED);
    return TAO_ERROR;
}
