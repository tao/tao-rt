// tao-phoenix-options.h --
//
// Definitions for parsing of command line options for Phoenix cameras.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2021, Éric Thiébaut.

#ifndef TAO_PHOENIX_OPTIONS_H_
#define TAO_PHOENIX_OPTIONS_H_ 1

#include <tao-options.h>
#include <tao-phoenix.h>

TAO_BEGIN_DECLS

// Callbacks for command line options.

extern void phnx_show_config_id_option(
    FILE* file,
    const tao_option* opt);

extern bool phnx_parse_bias_option(
    const tao_option* opt,
    char* args[]);

extern bool phnx_parse_gain_option(
    const tao_option* opt,
    char* args[]);

extern void phnx_show_frequency_option(
    FILE* file,
    const tao_option* opt);

extern bool phnx_parse_framerate_option(
    const tao_option* opt,
    char* args[]);

extern void phnx_show_seconds_option(
    FILE* file,
    const tao_option* opt);

extern bool phnx_parse_exposuretime_option(
    const tao_option* opt,
    char* args[]);

extern void phnx_show_encoding_option(
    FILE* file,
    const tao_option* opt);

extern bool phnx_parse_encoding_option(
    const tao_option* opt,
    char* args[]);

extern void phnx_show_connection_option(
    FILE* file,
    const tao_option* opt);

extern bool phnx_parse_connection_option(
    const tao_option* opt,
    char* args[]);

extern bool phnx_parse_buffers_option(
    const tao_option* opt,
    char* args[]);


// Generic helper macros.

#define PHNX_OPTION_SOME_ENCODING(pass, name, args, descr, addr) \
    {pass, name, 1, args, descr, addr, \
     phnx_show_encoding_option, phnx_parse_encoding_option}

#define PHNX_OPTION_NONNEGATIVE_SECONDS(pass, name, args, descr, addr) \
    {pass, name, 1, args, descr, addr, \
     phnx_show_seconds_option, tao_parse_nonnegative_double_option}


// Pre-defined options.

#define PHNX_OPTION_LOAD \
    {1, "load", 1, "ID", "Load configuration", \
     NULL, phnx_show_config_id_option, tao_parse_nonnegative_int_option}

#define PHNX_OPTION_SAVE \
    {1, "save", 1 , "ID", "Save configuration", \
     NULL, phnx_show_config_id_option, tao_parse_nonnegative_int_option}

#define PHNX_OPTION_CAMERA_ROI(cfg) \
    TAO_OPTION_CAMERA_ROI(2, "roi", "XOFF,YOFF,WIDTH,HEIGHT", \
                          "Region of interest", &(cfg).roi)

#define PHNX_OPTION_RAWENCODING(cfg) \
    PHNX_OPTION_SOME_ENCODING(2, "rawencoding", "ENCODING", \
                              "Encoding of pixels in raw acquired images", \
                              &(cfg).rawencoding)

#define PHNX_OPTION_FRAMERATE(cfg) \
    {2, "framerate", 1, "FPS", "Frames per second", \
     &(cfg).framerate, phnx_show_frequency_option, \
     phnx_parse_framerate_option}

#define PHNX_OPTION_EXPOSURETIME(cfg) \
    {2, "exposuretime", 1, "SECONDS", "Exposure duration in seconds", \
     &(cfg).exposuretime, phnx_show_seconds_option, \
     phnx_parse_exposuretime_option}

#define PHNX_OPTION_BIAS(cfg) \
    {2, "bias", 1, "LEVEL", "Black level", &(cfg).bias, \
     tao_show_double_option, phnx_parse_bias_option}

#define PHNX_OPTION_GAIN(cfg) \
    {2, "gain", 1, "VALUE", "Gain", &(cfg).gain, \
     tao_show_double_option, phnx_parse_gain_option}

#define PHNX_OPTION_CONNECTION(cfg) \
    {2, "connection", 1, "N*BITRATE|auto", "CoaXPress connection settings", \
     &(cfg).connection, phnx_show_connection_option, \
     phnx_parse_connection_option}

#define PHNX_OPTION_ACQUISITION_BUFFERS(cfg) \
    {2, "buffers", 1, "NUMBER", "Number of acquisition buffers", \
     &(cfg).buffers, tao_show_int_option, phnx_parse_buffers_option}

#define PHNX_OPTION_DROP(lval) \
    TAO_OPTION_YESNO(2, "drop", \
                     "Drop frames to only process the newest ones", &(lval))

#define PHNX_OPTION_QUIET(lval) \
    TAO_OPTION_SWITCH(2, "quiet", "Quiet (non-verbose) mode", &(lval))

#define PHNX_OPTION_DEBUG(lval) \
    TAO_OPTION_SWITCH(2, "debug", "Debug mode", &(lval))

#define PHNX_OPTION_HELP_AND_EXIT(n) TAO_OPTION_HELP_AND_EXIT(2, n)

#define PHNX_OPTION_LAST_ENTRY TAO_OPTION_LAST_ENTRY

TAO_END_DECLS

#endif // TAO_PHOENIX_OPTIONS_H_
