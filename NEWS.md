# User visible changes in TAO library API

## Version 0.11.0

* New command `tao_pid` to retrieve the process identifier (PID) of a remote server.

* New command `tao_shmid` to retrieve the shared memory identifier of a remote server.

* Deformable mirror `reset` command amounts to applying actuators commands all equal to
  zero.

* Deformable mirror servers perform a `reset` command on entry and on exit.

## Version 0.10.0

* Enumeration values can be truncated (as far as a unique match is found) for
  Andor cameras.

* Camera configuration settings `bufferencoding` and `sensorencoding` have been
  merged in a single parameter `rawencoding` to choose the encoding of pixels
  in raw acquired images.  It is up to the camera driver to interpret this
  parameter.

* Support for NüVü cameras (see [doc.](nuvu/README.md)).

* Support for [Finger Lakes Instrumentation](https://www.flicamera.com/) cameras
  via the `LIBFLI` Software Development Kit (SDK).

* Camera configuration is divided in 3 parts: (1) non-configurable settings,
  (2) configurable settings common to all cameras, and (3) *named* settings
  stored as key-value pairs.  Camera information (`tao_camera_info`) structure
  has been completely replaced by camera configuration (`tao_camera_config`)
  structure.

* `struct`/`enum`/`union` have the same name as their `typedef` counterpart.


## Version 0.9.0

* Support for [Boston Micromachines](https://bostonmicromachines.com/)
  deformable mirrors.

* Partial support for [Finger Lakes Instrumentation](https://www.flicamera.com/)
  *Kepler* camera via the `LIBFLIPRO` Software Development Kit (SDK).

* The actuators commands sent to deformable mirrors are the sum of 3 terms: the
  reference commands, some given perturbation commands, and the requested
  commands.  The perturbations are only valid for the next commands sent to the
  deformable mirror.  The reference commands are valid for the subsequent
  commands sent to the mirror until a new reference commands are set.

* For deformable mirrors, the TAO library now directly takes care of dealing
  with the actuators commands being the sum of different terms and of clamping
  these values to the range `[cmin,cmax]` of acceptable values.  Bad command
  values (NaN) are automatically replaced by the mean `(cmin + cmax)/2`.  The
  *reset* command is strictly equivalent to a *send* command with all requested
  actuators values set to zero.  The `on_reset` callback is thus no longer
  needed.  As a result of these changes, the API for deformable mirrors is much
  more simple.

* The `rpath` variable of installed excutables is set so that it should not be
  necessary to set the environment variable `LD_LIBRARY_PATH` with all the
  installation directories of the various SDKs used by TAO.


## Version 0.8.2

This version was tested on Themis adaptive optics system on 2022-03-12.

* Deformable mirrors have new members `cmin` and `cmax` to indicate the minimum
  and maximum values for an actuator command.

* Shell commands renamed with an `_` (underscore) separator instead of an `-`
  (hyphen) as in Yorick and C code.


## Version 0.8.1

* Remote cameras and mirrors implement the `obj[i]` syntax to fetch the `i`-th
  output image or data-frame from `obj`.

* Add `fanspeed` and `connection` configurable camera parameters.

* Pixel pre-processing activated for Phoenix cameras.


## Version 0.8.0

* A completely new camera server has been implemented.  This server is much
  more generic and does not use XPA messaging system.  Clients communicate with
  a camera server via a *remote camera* instance.  The behavior is more similar
  to that of a deformable mirror server.  Old camera servers, XPA and *shared
  cameras* have been removed from TAO.

* Acquisition of images by a unified camera has been simplified.  In
  particular, there are no needs to release the acquisition buffer.

* New naming conventions.  Most functions are named following the template
  `tao_$TYPE_$VERB` or `tao_$TYPE_$VERB_$NOUN` with `$TYPE` the type of the
  main argument, `$VERB` a verb indication the action, and `$NOUN` a noun.
  This follows conventions in many large C libraries and helps to quickly
  figure out the methods associated with a given object type (there are almost
  450 callable functions in the main TAO library and more than 600 callable
  functions in all TAO libraries).

* Simplify and speed-up error management.  In the previous versions, error
  stack almost never had a depth greater than one which means that it would be
  sufficient to have each thread just memorize its last error.  The information
  about the last error is now stored in a per-thread static data of small size
  with qualifier `static _Thread_local` (or `static thread_local` with
  `<threads.h>`).

  Simplifications brought by the new error handling system:

  - Implementation is much more simple.

  - No needs to perform any cleanup on exit and no risks of memory leaks.

  - Bindings are much easier to write.

  Expected speed-up:

  - Only 3ns to retrieve the per-thread data instead of 13ns in former code.

  - No needs to allocate and free memory for each error.


### Andor cameras

* New utility program `andor-features`.

* New camera server `andor-server` for Andor cameras (cf. above for details).


### Phoenix cameras

* For Phoenix cameras, the buffer encoding is automatically set (after sensor
  encoding) when initializing and maybe when updating or setting the
  configuration, but no longer when starting the acquisition.  So that the
  buffer encoding should be always well defined in camera configuration.

* Specific options of Phoenix cameras are now part of the generic
  configuration.

* New camera server `phoenix-server` for Phoenix cameras (cf. above for
  details).


## Versions 0.7

* Deformable mirror client/server.

* Standardization of naming conventions (standard C imposes that names prefixed
  by `_` are reserved):

  - Public symbols and macros are prefixed by `tao_` and `TAO_`.

  - Private/helper symbols and macros are prefixed by `tao_` or `TAO_` and
    suffixed by `_`.

  - Types are no longer suffixed by `_t` (which is reserved for the C).

  - `struct`/`enum`/`union` have the same name as their `typedef` counterpart
    with a final `_` (to avoid confusion in Doxygen documentation of opaque
    structures).

  - Headers define a macro `TAO_..._H_` to avoid being included more than once.

* Provide thread-pools.

* Image server for cameras connected to a Phoenix frame-grabber.

* Provide a unified camera model.

* Implement general means to identify pixel encoding.

* Shared objects now have an *owner* name which is used to identify the
  owner/creator of the resource.  This is used to provide the XPA access point
  of the image servers.

* TAO provide servers using the [XPA Messaging
  System](https://github.com/ericmandel/xpa) and implemented on top of XPA
  servers.

* TAO is split in several parts with separate libraries (`libtao.so`,
  `libtao-xpa.so`, `libtao-fits.so`, `libtao-andor.so`, `libtao-phoenix.so`,
  ...) and headers to restrict dependencies with other libraries.

* Add some tests.  Type `make check` to run the tests, look for `*.log` files
  for the detailed results.

* Interface to r/w locks.

* Timed operations now return a `tao_status` value which may be `TAO_TIMEOUT`.
  Function `tao_get_absolute_timeout()` returns a `tao_timeout` value which
  reflects the different possible cases: errors, timeout alread expired,
  timeout is occuring now, in the future or never.  Function
  `tao_is_finite_absolute_time()` has been suppressed as the value returned by
  `tao_get_absolute_timeout()` is sufficient to figure out the kind of timeout.

* Allow for process shared mutexes and condition variables.

* Use `tao_time` for storing the time with fields of known type (unlike `struct
  timespec` whose field types depend on the implementation).

* Manage to have '-Wall -Werror -O2' the default compilation flags.

* Include `stdbool.h` and use the macros provided by this header: `bool` for
  the type of a boolean and `true` or `false` values.

* Shared images may have associated weights.

* Implement the policy for sharing images allowing no readers if there is a
  writer and at most one writer.

* C type `long` is used for array dimensions, number of elements and indices.

* Error messages are correctly printed in Julia interface.

* Anonymous semaphores are used to signal new images.
