# TAO support for Finger Lakes Instrumentation (FLI) cameras

This directory of the TAO Real-Time repository contains the sources of the TAO
library and server for Finger Lakes Instrumentation (FLI) cameras (except the
Kepler camera).


## Installation

This TAO component requires `LIBFLI` to be installed. `LIBFLI` is the **open
source** Software Development Kit (SDK) provided by [Finger Lakes
Instrumentation (FLI)](https://www.flicamera.com) for their products.

The original code source of the SDK is available at
[www.flicamera.com/software](https://www.flicamera.com/software). To facilitate
the installation of `LIBFLI` on Linux, consider the port provided at
[git-cral.univ-lyon1.fr/tao/libfli](https://git-cral.univ-lyon1.fr/tao/libfli).

To build this TAO component, call TAO `configure` script with option:

``` sh
--enable-libfli="$dir"
```

where `$dir` is the full path to the top installation directory. It is expected
that directory `$dir/include` contains the `LIBFLI` header file `libfli.h` and
that directory `$dir/lib` contains the `LIBFLI` run-time libraries `libfli.so`
and/or `libfli.sa`.


## Examples

Example output:

```
$ fli_server -list
File: "/dev/fliusb0", name: "ProLine PL16803", domain: 0x0102 (camera|usb)
File: "/dev/fliusb1", name: "MicroLine ML2020", domain: 0x0102 (camera|usb)

$ fli_server -info
File: "/dev/fliusb0", name: "ProLine PL16803", domain: 0x0102 (camera|usb)
 ├─ Device Model: "ProLine PL16803"
 ├─ Serial Number: "PL0393714"
 ├─ Firmware Revision: 292
 ├─ Hardware Revision: 256
 ├─ Library Version: "Software Development Library for Linux 1.104"
 ├─ Detector Area: 4152 × 4128 pixels, [0:4151] × [0:4127]
 ├─ Visible Area:  4096 × 4096 pixels, [36:4131] × [21:4116]
 ├─ Pixel Size: 9.000 µm ×  9.000 µm
 └─ Temperature: 23.4°C
File: "/dev/fliusb1", name: "MicroLine ML2020", domain: 0x0102 (camera|usb)
 ├─ Device Model: "MicroLine ML2020"
 ├─ Serial Number: "ML0190614"
 ├─ Firmware Revision: 292
 ├─ Hardware Revision: 256
 ├─ Library Version: "Software Development Library for Linux 1.104"
 ├─ Detector Area: 1648 × 1214 pixels, [0:1647] × [0:1213]
 ├─ Visible Area:  1600 × 1200 pixels, [24:1623] × [6:1205]
 ├─ Pixel Size: 7.400 µm ×  7.400 µm
 └─ Temperature: 19.4°C
```

## Named attributes

The following named attributes are implemented:

| Name                  | Type         | Description                       |
|:----------------------|:-------------|:----------------------------------|
| `"TargetTemperature"` | (-w) float   | Target temperature (in °C)        |
| `"NFlushes"`          | (-w) integer | Number of background flushes      |
| `"BackgroundFlush"`   | (-w) enum    | Background flushing?              |
| `"ControlShutter"`    | (-w) enum    | Control shutter mode              |
| `"FrameType"`         | (-w) enum    | Frame type                        |
| `"FanSpeed"`          | (-w) enum    | Fan speed                         |
| `"PixelXSize"`        | (r-) float   | Pixel horizontal size (in meters) |
| `"PixelYSize"`        | (r-) float   | Pixel vertical size (in meters)   |
| `"DeviceModel"`       | (r-) string  | Device model                      |
| `"SerialNumber"`      | (r-) string  | Serial number                     |
| `"FirmwareRevision"`  | (r-) integer | Firmware revision                 |
| `"HardwareRevision"`  | (r-) integer | Hardware revision                 |
| `"LibraryVersion"`    | (r-) string  | Library version                   |

Type *enum* is for named attributes whose value a one of a set of predefined
strings.  The following table summarizes these attributes.

| Name                | Value                       | Constant                                |
|:--------------------|:----------------------------|:----------------------------------------|
| `"BackgroundFlush"` | `"start"`                   | `FLI_BGFLUSH_START`                     |
|                     | `"stop"`                    | `FLI_BGFLUSH_STOP`                      |
| `"ControlShutter"`  | `"close"`                   | `FLI_SHUTTER_CLOSE`                     |
|                     | `"open"`                    | `FLI_SHUTTER_OPEN`                      |
|                     | `"externalTriggerLow"`      | `FLI_SHUTTER_EXTERNAL_TRIGGER_LOW`      |
|                     | `"externalTriggerHigh"`     | `FLI_SHUTTER_EXTERNAL_TRIGGER_HIGH`     |
|                     | `"externalExposureControl"` | `FLI_SHUTTER_EXTERNAL_EXPOSURE_CONTROL` |
| `"FanSpeed"`        | `"off"`                     | `FLI_FAN_SPEED_OFF`                     |
|                     | `"on"`                      | `FLI_FAN_SPEED_ON`                      |
| `"FrameType"`       | `"normal"`                  | `FLI_FRAME_TYPE_NORMAL`                 |
|                     | `"dark"`                    | `FLI_FRAME_TYPE_DARK`                   |
|                     | `"flood"`                   | `FLI_FRAME_TYPE_FLOOD`                  |
|                     | `"flush"`                   | `FLI_FRAME_TYPE_RBI_FLUSH`              |


## SDK functions

The coordinates of the pixels of the detector matrix are retrieved by:
```
FLIGetArrayArea(dev, &x0, &y0, &x1, &y1);
```
the resulting `x0`, `y0`, `x1`, and `y1` are constant coordinates.  The
corresponding region is `[x0:x1-1]×[y0:y1-1]` of size `(x1 x0)×(y1 - y0)`.

The coordinates of the visible pixels of the detector are retrieved by:
```
FLIGetVisibleArea(dev, &x0, &y0, &x1, &y1);
```
These coordinates are constant (i.e., not affected by setting the ROI with `FLIGetArrayArea`) and have the same logic as for `FLIGetArrayArea`.

To set the ROI, call:
```
FLISetImageArea(dev, x0, y0, x0 + width, y0 + height);
```
where `x0` and `y0` give the position of the first physical pixel of the ROI
while `width` and `height` give the size of the ROI in meta-pixels.  Meta-pixels
have a size of `xbin × ybin` physical pixels.  The binning factors can be set
with:
```
FLISetHBin(dev, xbin);
FLISetVBin(dev, ybin);
```
The ROI must be within the *visible area* thus:
```
x0 ≤ roi->xoff < x1
y0 ≤ roi->yoff < y1
roi->xoff + roi->width*roi->xbin ≤ x1
roi->yoff + roi->height*roi->ybin ≤ y1
```
where `x0`, `y0`, `x1`, and `y1` are the coordinates of the *visible area* and `roi` is a pointer to a `tao_camera_roi` structure ans is used as:
```
FLISetImageArea(dev, roi->xoff, roi->yoff,
                roi->xoff + roi->width,
                roi->yoff + roi->height);
FLISetHBin(dev, roi->xbin);
FLISetVBin(dev, roi->ybin);
```
These options may have to be set in an order which guarantees that the *image
area* is valid at any time.


## Residual Bulk Image (RBI)

See https://www.gxccd.com/art?id=418&lang=409
