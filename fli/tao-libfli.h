//
// Definitions for Finger Lakes Instrumentation cameras via the LIBFLI
// SDK.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2022-2024, Éric Thiébaut.

#ifndef TAO_LIBFLI_H_
#define TAO_LIBFLI_H_ 1

#include <libfli.h>
#include <tao-errors.h>
#include <tao-cameras.h>

TAO_BEGIN_DECLS

/**
 * Open a Finger Lakes Instrumentation camera in TAO.
 *
 * @param filename   File name of device, e.g. `"/dev/fliusb0"`.
 *
 * @return A pointer to a unified TAO camera instance, `NULL` in case of
 *         failure.
 */
extern tao_camera* tao_fli_camera_open(
    const char* filename);

//-----------------------------------------------------------------------------
// CONSTANTS

// Number of characters of a small buffer to retrieves short strings.
#define BUFLEN 64

// Windows definition of max. number of characters in file name.
#define MAX_PATH 260

//-----------------------------------------------------------------------------
// ERROR MANAGEMENT.
//
// In the LIBFLI SDK, all library functions return zero on successful
// completion, and non-zero if an error occurred.  The exact nature of an error
// can be found by treating the negative of a function’s return value as a
// system error code.
//
// Reporting an error from a LIBFLI call is therefore straightforward.
static inline void libfli_error(
    const char* func,
    int code)
{
    tao_store_error(func, -code);
}

//-----------------------------------------------------------------------------
// CALL A FUNCTION OF LIBFLI.
//
// All functions of the LIBFLI SDK return either nothing (`void`) or an
// integer code which is nonnegative on success and negative on error.  The
// following macro is a helper to report errors with the name of the function
// from the LIBFLI SDK.
//
// The `func` argument is the function of the LIBFLI SDK to call.
//
// The `args` statement is the parenthesized list of arguments for the
// function.
//
// The `on_error` argument is a simple statement like `return TAO_ERROR` or
// `goto error`, it cannot be a `break` statement as the macro statements are
// wrapped in a `do {...} while (0)` loop.
#define LIBFLI_CALL(func, args, on_error)       \
    do {                                        \
        int code_ = func args;                  \
        if (code_ < 0) {                        \
            libfli_error(#func, code_);         \
            on_error;                           \
        }                                       \
    } while (0)

#endif // TAO_LIBFLI_H_
