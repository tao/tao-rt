//
// Image server for Finger Lakes Instrumentation cameras via the LIBFLI SDK.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2022-2024, Éric Thiébaut.

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "tao-errors.h"
#include "tao-libfli.h"
#include "tao-generic.h"
#include "tao-camera-servers.h"
#include "tao-cameras-private.h"

// A property is a key-value pair.  All integer valued constants are long
// integers in LIBFLI.
typedef struct property_ {
    const char* name;
    long value;
} property;

static property domain_props[] = {
    { "none",         FLIDOMAIN_NONE },
    { "parallelport", FLIDOMAIN_PARALLEL_PORT },
    { "usb",          FLIDOMAIN_USB },
    { "serial",       FLIDOMAIN_SERIAL },
    { "inet",         FLIDOMAIN_INET },
    { "serial19200",  FLIDOMAIN_SERIAL_19200 },
    { "serial1200",   FLIDOMAIN_SERIAL_1200 },
    { NULL, -1 }
};

static property device_props[] = {
    { "none",                   FLIDEVICE_NONE },
    { "camera",                 FLIDEVICE_CAMERA },
    { "filterwheel",            FLIDEVICE_FILTERWHEEL },
    { "focuser",                FLIDEVICE_FOCUSER },
    { "hsfilterwheel",          FLIDEVICE_HS_FILTERWHEEL },
    { "raw",                    FLIDEVICE_RAW },
    { "enumeratedbyconnection", FLIDEVICE_ENUMERATE_BY_CONNECTION },
    { NULL, -1 }
};

// Yields name of property given its value.  Returns `def` if not found.
static const char* get_property_name(
    const property* props,
    long value,
    const char* def)
{
    for (int i = 0; props[i].name != NULL; ++i) {
        if (props[i].value == value) {
            return props[i].name;
        }
    }
    return def;
}

#if 0 // FIXME: unused
// Yields value of property given its name.  Returns `def` if not found.
static long get_property_value(
    const property* props,
    const char* name,
    long def)
{
    if (name != NULL) {
        int c = name[0];
        for (int i = 0; props[i].name != NULL; ++i) {
            if (props[i].name[0] == c && strcmp(props[i].name, name) == 0) {
                return props[i].value;
            }
        }
    }
    return def;
}
#endif

// The following macros respectively yield the domain and device parts of a
// `flidomain_t` property.
#define GET_DOMAIN(val) ((val) & 0x00ff)
#define GET_DEVICE(val) ((val) & 0xff00)

static tao_status walk_devices(
    flidomain_t domains,
    tao_status (*walker)(flidomain_t, char*, char*, void*),
    void* data)
{
    char file[MAX_PATH];
    char name[MAX_PATH];
    flidomain_t domain;
    tao_status status = TAO_OK;
    LIBFLI_CALL(FLICreateList,(domains), return TAO_ERROR);
    int code = FLIListFirst(&domain, file, MAX_PATH, name, MAX_PATH);
    while (code == 0) {
        if (walker(domain, file, name, data) != TAO_OK) {
            status = TAO_ERROR;
            break;
        }
        code = FLIListNext(&domain, file, MAX_PATH, name, MAX_PATH);
    }
    LIBFLI_CALL(FLIDeleteList,(), status = TAO_ERROR);
    return status;
}

static tao_status print_camera_info(
    FILE* file,
    flidev_t dev,
    const char* pfx1,
    const char* pfx2)
{
    char buf[BUFLEN];

    LIBFLI_CALL(FLIGetModel,(dev, buf, BUFLEN), return TAO_ERROR);
    fprintf(file, "%sDevice Model: \"%s\"\n", pfx1, buf);

    LIBFLI_CALL(FLIGetSerialString,(dev, buf, BUFLEN), return TAO_ERROR);
    fprintf(file, "%sSerial Number: \"%s\"\n", pfx1, buf);

    long hwrev;
    LIBFLI_CALL(FLIGetHWRevision,(dev, &hwrev), return TAO_ERROR);
    fprintf(file, "%sHardware Revision: %ld\n", pfx1, hwrev);

    long fwrev;
    LIBFLI_CALL(FLIGetFWRevision,(dev, &fwrev), return TAO_ERROR);
    fprintf(file, "%sFirmware Revision: %ld\n", pfx1, fwrev);

    LIBFLI_CALL(FLIGetLibVersion,(buf, BUFLEN), return TAO_ERROR);
    fprintf(file, "%sLibrary Version: \"%s\"\n", pfx1, buf);

    long x0, y0, x1, y1;
    LIBFLI_CALL(FLIGetArrayArea,(dev, &x0, &y0, &x1, &y1), return TAO_ERROR);
    fprintf(file, "%sDetector Area: %ld × %ld pixels, [%ld:%ld] × [%ld:%ld]\n",
            pfx1, x1 - x0, y1 - y0, x0, x1 - 1, y0, y1 - 1);
    LIBFLI_CALL(FLIGetVisibleArea,(dev, &x0, &y0, &x1, &y1), return TAO_ERROR);
    fprintf(file, "%sVisible Area:  %ld × %ld pixels, [%ld:%ld] × [%ld:%ld]\n",
            pfx1, x1 - x0, y1 - y0, x0, x1 - 1, y0, y1 - 1);

    long width, height, xbin, ybin;
    LIBFLI_CALL(FLIGetReadoutDimensions,(dev,
                                         &width,  &x0, &xbin,
                                         &height, &y0, &ybin),
                return TAO_ERROR);
    fprintf(file, "%sImage Area:  %ld × %ld pixels at offsets (%ld,%ld) "
            "and with %ld×%ld binning\n",
            pfx1, width, height, x0, y0, xbin, ybin);

    double xsize, ysize;
    LIBFLI_CALL(FLIGetPixelSize,(dev, &xsize, &ysize), return TAO_ERROR);
    fprintf(file, "%sPixel Size: %.3f µm × %.3f µm\n", pfx1,
            1e6*xsize, 1e6*ysize);

    double temp;
    LIBFLI_CALL(FLIGetTemperature,(dev, &temp), return TAO_ERROR);
    fprintf(file, "%sTemperature: %.1f°C\n", pfx2, temp);

    return TAO_OK;
}

typedef struct print_info_context_ {
    FILE* output;
    int mode;
    bool fancy;
} print_info_context;

// Walker.
static tao_status print_info_walker(
    flidomain_t domain,
    char* file,
    char* name,
    void* data)
{
    print_info_context* ctx = data;
    const char* pfx1 = (ctx->fancy ? " ├─ " : "  ");
    const char* pfx2 = (ctx->fancy ? " └─ " : "  ");
    const char* domain_name = get_property_name(
        domain_props, GET_DOMAIN(domain), "unknown");
    const char* device_name = get_property_name(
        device_props, GET_DEVICE(domain), "unknown");

    fprintf(ctx->output,
            "File: \"%s\", name: \"%s\", domain: 0x%04x (%s|%s)\n",
            file, name, (unsigned)domain, device_name, domain_name);
    if (ctx->mode == 0) {
        return TAO_OK;
    }
    flidev_t dev = FLI_INVALID_DEVICE;
    LIBFLI_CALL(FLIOpen,(&dev, file, domain), return TAO_ERROR);
    tao_status status = TAO_OK;
    if (GET_DEVICE(domain) == FLIDEVICE_CAMERA) {
        if (print_camera_info(ctx->output, dev, pfx1, pfx2) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    LIBFLI_CALL(FLIClose,(dev), status = TAO_ERROR);
    return status;
}

int main(
    int argc,
    char* argv[])
{
    // Determine program name.
    const char* progname = tao_basename(argv[0]);

    // Parse arguments.
    const char* usage = "Usage: %s [OPTIONS ...] [--] [DEVICE NAME]\n";
    const char* device = NULL;
    const char* name = NULL;
    int list = 0;
    bool debug = false;
    bool fancy = true;
    long nbufs = 5;
    unsigned int perms = 0077;
    int iarg = 0;
    while (++iarg < argc) {
        char dummy;
        if (argv[iarg][0] != '-') {
            // Argument is not an option.
            --iarg;
            break;
        }
        if (argv[iarg][1] == '-' && argv[iarg][2] == '\0') {
            // Argument is last option.
            break;
        }
        if (strcmp(argv[iarg], "-h") == 0
            || strcmp(argv[iarg], "-help") == 0
            || strcmp(argv[iarg], "--help") == 0) {
            printf("\n");
            printf(usage, progname);
            printf("\n");
            printf("Launch a server for a Finger Lakes Instrumentation (FLI) "
                   "camera managed by the LIBFLI SDK.\n");
            printf("\n");
            printf("Arguments:\n");
            printf("  DEVICE             Camera device, e.g. \"%s\".\n",
                   "/dev/fliusb0");
            printf("  NAME               Server name.\n");
            printf("\n");
            printf("Options:\n");
            printf("  -info              List information about available "
                   "devices.\n");
            printf("  -list              List names of available devices.\n");
            printf("  -nbufs NBUFS       Number of output buffers [%ld].\n",
                   nbufs);
            printf("  -nofancy           Do not use colors nor set window "
                   "title.\n");
            printf("  -perms PERMS       Bitwise mask of permissions [0%o].\n",
                   perms);
            printf("  -debug             Debug mode [%s].\n",
                   (debug ? "true" : "false"));
            printf("  -h, -help, --help  Print this help.\n");
            printf("\n");
            return EXIT_SUCCESS;
        }
        if (strcmp(argv[iarg], "-info") == 0) {
            list = 2;
            continue;
        }
        if (strcmp(argv[iarg], "-list") == 0) {
            list = (list == 0 ? 1 : list);
            continue;
        }
        if (strcmp(argv[iarg], "-nbufs") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option %s.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%ld %c", &nbufs, &dummy) != 1
                || nbufs < 2) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option %s.\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-perms") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option %s.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%i %c", (int*)&perms, &dummy) != 1) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option %s.\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            perms &= 0777;
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-nofancy") == 0) {
            fancy = false;
            continue;
        }
        if (strcmp(argv[iarg], "-debug") == 0) {
            debug = true;
            continue;
        }
        fprintf(stderr, "%s: Unknown option %s.\n", progname, argv[iarg]);
        return EXIT_FAILURE;
    }
    if (list == 0) {
        if (++iarg >= argc) {
            fprintf(stderr, "%s: Missing device name.\n", progname);
            goto bad_usage;
        }
        device = argv[iarg];
        if (++iarg >= argc) {
            fprintf(stderr, "%s: Missing server name.\n", progname);
            goto bad_usage;
        }
        name = argv[iarg];
    }
    if (++iarg < argc) {
        fprintf(stderr, "%s: Too many arguments.\n", progname);
    bad_usage:
        fputc('\n', stderr);
        fprintf(stderr, usage, progname);
        fputc('\n', stderr);
        return EXIT_FAILURE;
    }

    // Code returned on exit.
    int exitcode = EXIT_SUCCESS;

    // Just list available devices if requested to do so and return.
    if (list != 0) {
        print_info_context ctx = {
            .output = stdout,
            .mode = (list > 1),
            .fancy = true
        };
        flidomain_t domains = (FLIDOMAIN_USB|FLIDEVICE_CAMERA);
        if (walk_devices(domains, print_info_walker, &ctx) != TAO_OK) {
            tao_report_error();
            exitcode = EXIT_FAILURE;
        }
        return exitcode;
    }

    // Pre-set addresses of server for cleanup.
    tao_camera_server* srv = NULL;

    // Open camera.
    tao_camera* cam = tao_fli_camera_open(device);
    if (cam == NULL) {
        // FIXME: report TAO error?
        fprintf(stderr, "%s: Failed to open FLI camera device \"%s\".\n",
                progname, device);
        goto error;
    }

    // Create camera server and run it.
    srv = tao_camera_server_create(name, cam, nbufs, perms);
    if (srv == NULL) {
        // FIXME: report TAO error?
        fprintf(stderr, "%s: Failed to create the camera server.\n",
                progname);
        goto error;
    }
    if (debug) {
        srv->loglevel = TAO_MESG_DEBUG;
    } else {
        srv->loglevel = TAO_MESG_INFO;
    }
    srv->fancy = fancy;
    if (tao_camera_server_run_loop(srv) != TAO_OK) {
        fprintf(stderr, "%s: Failed to run the camera server.\n",
                progname);
        goto error;
    }

done:
    // Destroy the server.
    if (srv != NULL && tao_camera_server_destroy(srv) != TAO_OK) {
        fprintf(stderr, "%s: Failed to destroy the camera server.\n",
                progname);
        exitcode = EXIT_FAILURE;
    }
    // Close the camera device.
    if (cam != NULL && tao_camera_destroy(cam) != TAO_OK) {
        fprintf(stderr, "%s: Failed to destroy the camera device.\n",
                progname);
        exitcode = EXIT_FAILURE;
    }
    if (tao_any_errors(NULL)) {
        tao_report_error();
        exitcode = EXIT_FAILURE;
    }
    return exitcode;

error:
    exitcode = EXIT_FAILURE;
    goto done;
}
