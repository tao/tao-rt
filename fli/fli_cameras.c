//
// Implementation of TAO camera API for Finger Lakes Instrumentation cameras via the
// LIBFLI SDK.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2022-2024, Éric Thiébaut.

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "tao-threads.h" // for mutexes
#include "tao-errors.h" // for error management
#include "tao-cameras-private.h" // for the definition of tao_camera
#include "tao-generic.h" // for tao_clamp, etc.
#include "tao-macros.h" // for TAO_STATIC_ARRAY_LENGTH, etc.
#include "tao-libfli.h"

// Initial settings.
#define INITIAL_TARGET_TEMPERATURE  (20.0) // °C
#define INITIAL_NFLUSHES            (0)
#define INITIAL_BACKGROUND_FLUSH    (false)
#define INITIAL_SHUTTER             FLI_SHUTTER_CLOSE
#define INITIAL_EXPOSURE_TIME_MS    (1) // milliseconds
#define INITIAL_FPS                 (10) // frames per second
#define INITIAL_FRAME_TYPE          FLI_FRAME_TYPE_NORMAL
#define INITIAL_FAN_SPEED           FLI_FAN_SPEED_OFF

// Temperature is in the range [-55°C, 45°C].
#define TEMPERATURE_MIN (-55.0)
#define TEMPERATURE_MAX ( 45.0)
#define TEMPERATURE_TOL (  0.1) // tolerance

// Assume exposure time is in the range [0, 60] seconds.
#define EXPOSURETIME_MIN ( 0.0)  // in seconds
#define EXPOSURETIME_MAX (60.0)  // in seconds
#define EXPOSURETIME_TOL (0.001) // tolerance in seconds

// Number of flushes [0, 16] seconds.
#define NFLUSHES_MIN  (0)
#define NFLUSHES_MAX (16)

// Structure describing a camera.
typedef struct fli_camera_ {
    tao_camera            base;///< A FLI camera is based on a unified TAO camera.
    flidev_t               dev;///< Handle to camera device.
    const double         xsize;///< Physical pixel size along X.
    const double         ysize;///< Physical pixel size along Y.
    const long              x0;///< Origin of abscissae on the detector.
    const long              y0;///< Origin of ordinates on the detector.
    long              nflushes;///< Number of flushes.
    long               shutter;///< Shutter control parameter.
    long               bgflush;///< Background flushing?
    tao_acquisition_buffer buf;///< Acquisition buffer.
    void               *tmpbuf;///< Temporary buffer for a single row of 16-bit pixels.
    size_t              tmpsiz;///< Size of temporary buffer.
    flitdirate_t      tdi_rate;///< Rate for FLISetTDI.
    flitdiflags_t    tdi_flags;///< Flags for FLISetTDI.
    fliframe_t      frame_type;///< Argument for FLISetFrameType.
    bool            continuous;///< Image acquisition in video mode.
    bool              exposing;///< Camera is exposing.
} fli_camera;

// Declaration of private methods and of the table grouping these methods for the unified
// camera API.

static tao_status on_initialize(
    tao_camera* cam,
    void* ptr);

static tao_status on_finalize(
    tao_camera* cam);

static tao_status on_reset(
    tao_camera* cam);

static tao_status on_update_config(
    tao_camera* cam);

static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_start(
    tao_camera* cam);

static tao_status on_stop(
    tao_camera* cam);

static tao_status on_wait_buffer(
    tao_camera* cam,
    tao_acquisition_buffer* buf,
    double secs,
    int drop);

static tao_camera_ops ops = {
    .name          = "FLICamera",
    .initialize    = on_initialize,
    .finalize      = on_finalize,
    .reset         = on_reset,
    .update_config = on_update_config,
    .check_config  = on_check_config,
    .set_config    = on_set_config,
    .start         = on_start,
    .stop          = on_stop,
    .wait_buffer   = on_wait_buffer
};

//-----------------------------------------------------------------------------
// Private functions and macros to query a named attribute while preserving the
// `const` property of pointers.

static inline const tao_attr* fli_camera_unsafe_get_attr(
    const fli_camera* cam,
    long idx)
{
    return tao_attr_table_unsafe_get_entry(&cam->base.config.attr_table, idx);
}

#define colon(x, ...) x: __VA_ARGS__

#define get_attr_(T, obj, idx)                                          \
    colon(const T*, (const tao_attr*)T##_unsafe_get_attr((const T*)(obj), idx)), \
    colon(      T*, (      tao_attr*)T##_unsafe_get_attr((const T*)(obj), idx))

// `get_attr(obj, idx)` yields the named attribute at index `idx` for object
// `obj`. Arguments are not checked.
#define get_attr(obj, idx)                              \
    _Generic((obj),                                     \
             get_attr_(fli_camera,        obj, idx),    \
             get_attr_(tao_camera,        obj, idx),    \
             get_attr_(tao_camera_config, obj, idx))

//-----------------------------------------------------------------------------
// Public function to connect to a Finger Lakes Instrumentation camera with the
// LIBFLI library.
tao_camera* tao_fli_camera_open(
    const char* filename)
{
    // Argument to FLIOpen is not an immutable string, so we store the file
    // name in a local buffer.
    size_t len = TAO_STRLEN(filename);
    char buf[MAX_PATH];
    if (len < 1 || len >= MAX_PATH) {
        tao_store_error(__func__, TAO_BAD_FILENAME);
        return NULL;
    }
    memcpy(buf, filename, (len+1)*sizeof(char));

    // Open a FLI device assuming and USB connected camera.
    flidomain_t domain = (FLIDOMAIN_USB|FLIDEVICE_CAMERA);
    flidev_t dev = FLI_INVALID_DEVICE;
    LIBFLI_CALL(FLIOpen,(&dev, buf, domain), return NULL);

    // Allocate camera instance with the FLI device provided as contextual
    // data. The remaining initialization will done by the `on_initialize`
    // virtual method.
    return tao_camera_create(&ops, &dev, sizeof(fli_camera));
}

//-----------------------------------------------------------------------------
// MANAGEMENT OF ATTRIBUTES

// Yield whether two named attributes have the same value (and type).
static bool same_attr_val(
    const tao_attr* A,
    const tao_attr* B)
{
    int type = TAO_ATTR_TYPE(A);
    if (TAO_ATTR_TYPE(B) == type) {
        switch (type) {
        case TAO_ATTR_BOOLEAN:
            return (A->val.b != false) == (B->val.b != false);
        case TAO_ATTR_INTEGER:
            return A->val.i == B->val.i;
        case TAO_ATTR_FLOAT:
            return A->val.f == B->val.f;
        case TAO_ATTR_STRING:
            return strcmp(A->val.s, B->val.s) == 0;
        }
    }
    return false;
}

typedef struct property_ {
    const char* name;
    long value;
} property;

static tao_status set_property(
    fli_camera* fcam,
    int i,
    long value,
    tao_status (*mutator)(fli_camera*, long),
    const property* props)
{
    tao_attr* attr = get_attr(fcam, i);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_STRING) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    const char* str = NULL;
    for (int i = 0; props[i].name != NULL; ++i) {
        if (props[i].value == value) {
            str = props[i].name;
            break;
        }
    }
    if (str == NULL) {
        tao_store_error(__func__, TAO_BAD_VALUE);
        return TAO_ERROR;
    }
    if (mutator(fcam, value) != TAO_OK) {
        return TAO_ERROR;
    }
    return tao_attr_set_string_value(attr, str, true);
}

static tao_status get_property(
    const char* func,
    long* value,
    fli_camera* fcam, // FIXME: unused???
    const tao_camera_config* cfg,
    int i,
    const property* props)
{
    const char* val;
    if (tao_attr_get_string_value(get_attr(cfg, i), &val) != TAO_OK) {
        return TAO_ERROR;
    }
    int idx = -1;
    for (int j = 0; props[j].name != NULL; ++j) {
        if (strcmp(props[j].name, val) == 0) {
            idx = j;
            break;
        }
    }
    if (idx < 0) {
        tao_store_error(func, TAO_BAD_VALUE);
        return TAO_ERROR;
    }
    if (value != NULL) {
        *value = props[idx].value;
    }
    return TAO_OK;
}

static struct {
    flibitdepth_t bitdepth;
    tao_encoding encoding;
} bitdepth_props[] = {
    { FLI_MODE_8BIT,  TAO_ENCODING_MONO( 8) },
    { FLI_MODE_16BIT, TAO_ENCODING_MONO(16) },
};

// Verify target temperature.
static tao_status verify_target_temperature(
    const char* func,
    double temperature)
{
    if (isnan(temperature)
        || temperature <= (TEMPERATURE_MIN - TEMPERATURE_TOL)
        || temperature >= (TEMPERATURE_MAX + TEMPERATURE_TOL)) {
        tao_store_error(func, TAO_BAD_TEMPERATURE);
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Verify number of flushes.
static tao_status verify_nflushes(
    const char* func,
    long nflushes)
{
    if (nflushes < NFLUSHES_MIN || nflushes > NFLUSHES_MAX) {
        tao_store_error(func, TAO_BAD_VALUE);
        return TAO_ERROR;
    }
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// CURRENT TEMPERATURE

static tao_status update_current_temperature(
    fli_camera* fcam,
    int i)
{
    double temp;
    tao_attr* attr = get_attr(fcam, i);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_FLOAT) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    LIBFLI_CALL(FLIGetTemperature,(fcam->dev, &temp), return TAO_ERROR);
    return tao_attr_set_float_value(attr, temp, true);
}

//-----------------------------------------------------------------------------
// TARGET TEMPERATURE

static tao_status set_target_temperature(
    fli_camera* fcam,
    int i,
    double temp)
{
    tao_attr* attr = get_attr(fcam, i);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_FLOAT) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    if (verify_target_temperature(__func__, temp) != TAO_OK) {
        return TAO_ERROR;
    }
    temp = tao_clamp(temp, TEMPERATURE_MIN, TEMPERATURE_MAX);
    LIBFLI_CALL(FLISetTemperature,(fcam->dev, temp), return TAO_ERROR);
    return tao_attr_set_float_value(attr, temp, true);
}

static tao_status init_target_temperature(
    fli_camera* fcam,
    int i)
{
    return set_target_temperature(fcam, i, INITIAL_TARGET_TEMPERATURE);
}

static tao_status check_target_temperature(
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    double temp;
    if (tao_attr_get_float_value(get_attr(fcam, i), &temp) != TAO_OK) {
        return TAO_ERROR;
    }
    return verify_target_temperature(__func__, temp);
}

static tao_status config_target_temperature(
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    double temp;
    if (tao_attr_get_float_value(get_attr(fcam, i), &temp) != TAO_OK) {
        return TAO_ERROR;
    }
    return set_target_temperature(fcam, i, temp);
}

//-----------------------------------------------------------------------------
// NUMBER OF BACKGROUND FLUSHES

static tao_status set_nflushes(
    fli_camera* fcam,
    int i,
    long nflushes)
{
    tao_attr* attr = get_attr(fcam, i);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    if (verify_nflushes(__func__, nflushes) != TAO_OK) {
        return TAO_ERROR;
    }
    LIBFLI_CALL(FLISetNFlushes,(fcam->dev, nflushes), return TAO_ERROR);
    tao_status status = tao_attr_set_integer_value(attr, nflushes, true);
    // We make this parameter available with a more immediate access because
    // calling FLIExposeFrame() or FLIControlShutter() stop background
    // flushing.
    fcam->nflushes = nflushes;
    return status;
}

static tao_status init_nflushes(
    fli_camera* fcam,
    int i)
{
    return set_nflushes(fcam, i, INITIAL_NFLUSHES);
}

static tao_status check_nflushes(
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    int64_t val;
    if (tao_attr_get_integer_value(get_attr(fcam, i), &val) != TAO_OK) {
        return TAO_ERROR;
    }
    return verify_nflushes(__func__, val);
}

static tao_status config_nflushes(
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    int64_t val;
    if (tao_attr_get_integer_value(get_attr(fcam, i), &val) != TAO_OK) {
        return TAO_ERROR;
    }
    return set_nflushes(fcam, i, val);
}

//-----------------------------------------------------------------------------
// BACKGROUND FLUSHING

static property background_flush_props[] = {
#define BACKGROUND_FLUSH_SIZE 8 /* Names in this table must not be longer than
                                 * this constant including the final null. */
    { "stop",  FLI_BGFLUSH_STOP },
    { "start", FLI_BGFLUSH_START },
    { NULL, 0},
};

static tao_status background_flush_mutator(
    fli_camera* fcam,
    long value)
{
    LIBFLI_CALL(FLIControlBackgroundFlush,(fcam->dev, value),
                return TAO_ERROR);
    // We must store this parameter with a more immediate access because
    // calling FLIExposeFrame() or FLIControlShutter() stop background
    // flushing.
    fcam->bgflush = value;
    return TAO_OK;
}

static tao_status set_background_flush(
    fli_camera* fcam,
    int i,
    long value)
{
    return set_property(
        fcam, i, value, background_flush_mutator, background_flush_props);
}

static tao_status init_background_flush(
    fli_camera* fcam,
    int i)
{
    return set_background_flush(fcam, i, INITIAL_SHUTTER);
}

static tao_status get_background_flush(
    const char* func,
    long* value,
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    return get_property(func, value, fcam, cfg, i, background_flush_props);
}

static tao_status check_background_flush(
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    return get_background_flush(__func__, NULL, fcam, cfg, i);
}

static tao_status config_background_flush(
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    flishutter_t shutter;
    if (get_background_flush(__func__, &shutter, fcam, cfg, i)!= TAO_OK) {
        return TAO_ERROR;
    }
    return set_background_flush(fcam, i, shutter);
}

//-----------------------------------------------------------------------------
// CONTROL SHUTTER

static property control_shutter_props[] = {
#define CONTROL_SHUTTER_SIZE 24 /* Names in this table must not be longer than
                                 * this constant including the final null. */
    { "close",                   FLI_SHUTTER_CLOSE },
    { "open",                    FLI_SHUTTER_OPEN },
    { "externalTriggerLow",      FLI_SHUTTER_EXTERNAL_TRIGGER_LOW },
    { "externalTriggerHigh",     FLI_SHUTTER_EXTERNAL_TRIGGER_HIGH },
    { "externalExposureControl", FLI_SHUTTER_EXTERNAL_EXPOSURE_CONTROL },
    { NULL, 0},
};

static tao_status control_shutter_mutator(
    fli_camera* fcam,
    long value)
{
    LIBFLI_CALL(FLIControlShutter,(fcam->dev, value), return TAO_ERROR);
    // We must store this parameter with a more immediate access because
    // calling FLIExposeFrame() or FLIControlShutter() stop background
    // flushing.
    fcam->shutter = value;
    return TAO_OK;
}

static tao_status set_control_shutter(
    fli_camera* fcam,
    int i,
    long value)
{
    return set_property(
        fcam, i, value, control_shutter_mutator, control_shutter_props);
}

static tao_status init_control_shutter(
    fli_camera* fcam,
    int i)
{
    return set_control_shutter(fcam, i, INITIAL_SHUTTER);
}

static tao_status get_control_shutter(
    const char* func,
    long* value,
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    return get_property(func, value, fcam, cfg, i, control_shutter_props);
}

static tao_status check_control_shutter(
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    return get_control_shutter(__func__, NULL, fcam, cfg, i);
}

static tao_status config_control_shutter(
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    flishutter_t shutter;
    if (get_control_shutter(__func__, &shutter, fcam, cfg, i) != TAO_OK) {
        return TAO_ERROR;
    }
    return set_control_shutter(fcam, i, shutter);
}

//-----------------------------------------------------------------------------
// FRAME TYPE

static property frame_type_props[] = {
#define FRAME_TYPE_SIZE 8 /* Names in this table must not be longer than
                           * this constant including the final null. */
    { "normal", FLI_FRAME_TYPE_NORMAL },
    { "dark",   FLI_FRAME_TYPE_DARK },
    { "flood",  FLI_FRAME_TYPE_FLOOD },
    { "flush",  FLI_FRAME_TYPE_RBI_FLUSH },
    { NULL, 0},
};

static tao_status frame_type_mutator(
    fli_camera* fcam,
    long value)
{
    LIBFLI_CALL(FLISetFrameType,(fcam->dev, value), return TAO_ERROR);
    fcam->frame_type = value;
    return TAO_OK;
}

static tao_status set_frame_type(
    fli_camera* fcam,
    int i,
    long value)
{
    return set_property(fcam, i, value, frame_type_mutator, frame_type_props);
}

static tao_status init_frame_type(
    fli_camera* fcam,
    int i)
{
    return set_frame_type(fcam, i, INITIAL_FRAME_TYPE);
}

static tao_status get_frame_type(
    const char* func,
    long* value,
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    return get_property(func, value, fcam, cfg, i, frame_type_props);
}

static tao_status check_frame_type(
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    return get_frame_type(__func__, NULL, fcam, cfg, i);
}

static tao_status config_frame_type(
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    fliframe_t frame;
    if (get_frame_type(__func__, &frame, fcam, cfg, i)!= TAO_OK) {
        return TAO_ERROR;
    }
    return set_frame_type(fcam, i, frame);
}

//-----------------------------------------------------------------------------
// FAN SPEED

static property fan_speed_props[] = {
#define FAN_SPEED_SIZE 4 /* Names in this table must not be longer than
                          * this constant including the final null. */
    { "off", FLI_FAN_SPEED_OFF },
    { "on",  FLI_FAN_SPEED_ON },
    { NULL, 0},
};

static tao_status fan_speed_mutator(
    fli_camera* fcam,
    long value)
{
    LIBFLI_CALL(FLISetFanSpeed,(fcam->dev, value), return TAO_ERROR);
    return TAO_OK;
}

static tao_status set_fan_speed(
    fli_camera* fcam,
    int i,
    long value)
{
    return set_property(fcam, i, value, fan_speed_mutator, fan_speed_props);
}

static tao_status init_fan_speed(
    fli_camera* fcam,
    int i)
{
    return set_fan_speed(fcam, i, INITIAL_FAN_SPEED);
}

static tao_status get_fan_speed(
    const char* func,
    long* value,
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    return get_property(func, value, fcam, cfg, i, fan_speed_props);
}

static tao_status check_fan_speed(
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    return get_fan_speed(__func__, NULL, fcam, cfg, i);
}

static tao_status config_fan_speed(
    fli_camera* fcam,
    const tao_camera_config* cfg,
    int i)
{
    long fan_speed;
    if (get_fan_speed(__func__, &fan_speed, fcam, cfg, i)!= TAO_OK) {
        return TAO_ERROR;
    }
    return set_fan_speed(fcam, i, fan_speed);
}

//-----------------------------------------------------------------------------
// PIXEL SIZE

static tao_status init_pixel_xsize(
    fli_camera* fcam,
    int i)
{
    tao_attr* attr = get_attr(fcam, i);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_FLOAT) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    return tao_attr_set_float_value(attr, fcam->xsize, true);
}

static tao_status init_pixel_ysize(
    fli_camera* fcam,
    int i)
{
    tao_attr* attr = get_attr(fcam, i);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_FLOAT) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    return tao_attr_set_float_value(attr, fcam->ysize, true);
}

//-----------------------------------------------------------------------------
// DEVICE MODEL, SERIAL NUMBER, FIRMWARE REVISION, HARDWARE REVISION, ...

static tao_status init_device_model(
    fli_camera* fcam,
    int i)
{
    char buf[BUFLEN];
    tao_attr* attr = get_attr(fcam, i);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_STRING) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    LIBFLI_CALL(FLIGetModel,(fcam->dev, buf, BUFLEN), return TAO_ERROR);
    buf[BUFLEN - 1] = 0;
    return tao_attr_set_string_value(attr, buf, true);
}

static tao_status init_serial_number(
    fli_camera* fcam,
    int i)
{
    char buf[BUFLEN];
    tao_attr* attr = get_attr(fcam, i);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_STRING) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    LIBFLI_CALL(FLIGetSerialString,(fcam->dev, buf, BUFLEN), return TAO_ERROR);
    buf[BUFLEN - 1] = 0;
    return tao_attr_set_string_value(attr, buf, true);
}

static tao_status init_firmware_revision(
    fli_camera* fcam,
    int i)
{
    long fwrev;
    tao_attr* attr = get_attr(fcam, i);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    LIBFLI_CALL(FLIGetFWRevision,(fcam->dev, &fwrev), return TAO_ERROR);
    return tao_attr_set_integer_value(attr, fwrev, true);
}

static tao_status init_hardware_revision(
    fli_camera* fcam,
    int i)
{
    long hwrev;
    tao_attr* attr = get_attr(fcam, i);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    LIBFLI_CALL(FLIGetHWRevision,(fcam->dev, &hwrev), return TAO_ERROR);
    return tao_attr_set_integer_value(attr, hwrev, true);
}

static tao_status init_library_version(
    fli_camera* fcam,
    int i)
{
    char buf[BUFLEN];
    tao_attr* attr = get_attr(fcam, i);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_STRING) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    LIBFLI_CALL(FLIGetLibVersion,(buf, BUFLEN), return TAO_ERROR);
    buf[BUFLEN - 1] = 0;
    return tao_attr_set_string_value(attr, buf, true);
}

//-----------------------------------------------------------------------------
// NAMED ATTRIBUTES

#define DEVICE_MODEL_SIZE      32
#define SERIAL_NUMBER_SIZE     32
#define LIBRARY_VERSION_SIZE   64

#define ATTR_LIST                                                       \
    ATTR_DEF("Temperature", 0,                                          \
             TAO_ATTR_FLOAT,                                            \
             update_current_temperature,                                \
             update_current_temperature,                                \
             NULL,                                                      \
             NULL)                                                      \
    ATTR_DEF("TargetTemperature", 0,                                    \
             TAO_ATTR_FLOAT,                                            \
             init_target_temperature,                                   \
             NULL,                                                      \
             check_target_temperature,                                  \
             config_target_temperature)                                 \
    ATTR_DEF("NFlushes", 0,                                             \
             TAO_ATTR_INTEGER,                                          \
             init_nflushes,                                             \
             NULL,                                                      \
             check_nflushes,                                            \
             config_nflushes)                                           \
    ATTR_DEF("BackgroundFlush", BACKGROUND_FLUSH_SIZE,                  \
             TAO_ATTR_STRING,                                           \
             init_background_flush,                                     \
             NULL,                                                      \
             check_background_flush,                                    \
             config_background_flush)                                   \
    ATTR_DEF("ControlShutter", CONTROL_SHUTTER_SIZE,                    \
             TAO_ATTR_STRING,                                           \
             init_control_shutter,                                      \
             NULL,                                                      \
             check_control_shutter,                                     \
             config_control_shutter)                                    \
    ATTR_DEF("FrameType", FRAME_TYPE_SIZE,                              \
             TAO_ATTR_STRING,                                           \
             init_frame_type,                                           \
             NULL,                                                      \
             check_frame_type,                                          \
             config_frame_type)                                         \
    ATTR_DEF("FanSpeed", FAN_SPEED_SIZE,                                \
             TAO_ATTR_STRING,                                           \
             init_fan_speed,                                            \
             NULL,                                                      \
             check_fan_speed,                                           \
             config_fan_speed)                                          \
    ATTR_DEF("PixelXSize", 0,                                           \
             TAO_ATTR_FLOAT,                                            \
             init_pixel_xsize,                                          \
             NULL,                                                      \
             NULL,                                                      \
             NULL)                                                      \
    ATTR_DEF("PixelYSize", 0,                                           \
             TAO_ATTR_FLOAT,                                            \
             init_pixel_ysize,                                          \
             NULL,                                                      \
             NULL,                                                      \
             NULL)                                                      \
    ATTR_DEF("DeviceModel", DEVICE_MODEL_SIZE,                          \
             TAO_ATTR_STRING,                                           \
             init_device_model,                                         \
             NULL,                                                      \
             NULL,                                                      \
             NULL)                                                      \
    ATTR_DEF("SerialNumber", SERIAL_NUMBER_SIZE,                        \
             TAO_ATTR_STRING,                                           \
             init_serial_number,                                        \
             NULL,                                                      \
             NULL,                                                      \
             NULL)                                                      \
    ATTR_DEF("FirmwareRevision", 0,                                     \
             TAO_ATTR_INTEGER,                                          \
             init_firmware_revision,                                    \
             NULL,                                                      \
             NULL,                                                      \
             NULL)                                                      \
    ATTR_DEF("HardwareRevision", 0,                                     \
             TAO_ATTR_INTEGER,                                          \
             init_hardware_revision,                                    \
             NULL,                                                      \
             NULL,                                                      \
             NULL)                                                      \
    ATTR_DEF("LibraryVersion", LIBRARY_VERSION_SIZE,                    \
             TAO_ATTR_STRING,                                           \
             init_library_version,                                      \
             NULL,                                                      \
             NULL,                                                      \
             NULL)

// Bit TAO_ATTR_READABLE is set if the attribute value can be retrieved. Bit
// TAO_ATTR_VARIABLE is set if attribute can spontaneously change. Bit
// TAO_ATTR_WRITABLE is set if the attribute value can be modified with a
// function of the LIBFLI SDK.
#define ATTR_FLAGS(INIT,UPDATE,CHECK,CONFIG)                            \
    (((INIT) != NULL || (UPDATE) != NULL ? TAO_ATTR_READABLE : 0) |     \
     ((UPDATE) != NULL ? TAO_ATTR_VARIABLE : 0) |                       \
     ((CONFIG) != NULL ? TAO_ATTR_WRITABLE : 0))

static tao_attr_descr attr_defs[] = {
#define ATTR_DEF(KEY,SIZE,TYPE,INIT,UPDATE,CHECK,CONFIG)                \
    (tao_attr_descr){ .key = KEY, .type = TYPE, .size = SIZE,           \
                      .flags = ATTR_FLAGS(INIT,UPDATE,CHECK,CONFIG) },
    ATTR_LIST
#undef ATTR_DEF
};

static const int attr_count = sizeof(attr_defs)/sizeof(tao_attr_descr);

static struct {
    tao_status (*init  )(fli_camera*, int);
    tao_status (*update)(fli_camera*, int);
    tao_status (*check )(fli_camera*, const tao_camera_config*, int);
    tao_status (*config)(fli_camera*, const tao_camera_config*, int);
} attr_ops[] = {
#define ATTR_DEF(KEY,SIZE,TYPE,INIT,UPDATE,CHECK,CONFIG)                \
    { .init = INIT, .update = UPDATE, .check = CHECK, .config = CONFIG },
    ATTR_LIST
#undef ATTR_DEF
};

// Update image area. Camera must have been (partialy) initialized (offsets x0
// and y0 are part of the computations).
static tao_status update_image_roi(
    fli_camera* fcam)
{
    // Get the current image area.
    long width,  xoff, xbin;
    long height, yoff, ybin;
    LIBFLI_CALL(FLIGetReadoutDimensions,(fcam->dev,
                                         &width,  &xoff, &xbin,
                                         &height, &yoff, &ybin),
                return TAO_ERROR);

    // Reflect this in the camera configuration.
    tao_camera_roi* roi = &fcam->base.config.roi;
    roi->xbin = xbin;
    roi->ybin = ybin;
    roi->xoff = xoff - fcam->x0;
    roi->yoff = yoff - fcam->y0;
    roi->width = width;
    roi->height = height;
    return TAO_OK;
}

// Set region of interest. It is assumed that the ROI parameters have been
// checked by. This automatically updates the `fcam` structure.
static tao_status set_image_roi(
    fli_camera* fcam,
    const tao_camera_roi* roi)
{
    bool update = false;
    tao_camera_roi* cam_roi = &fcam->base.config.roi;

    // Change binning factors if different.
    if (cam_roi->xbin != roi->xbin) {
        LIBFLI_CALL(FLISetHBin,(fcam->dev, roi->xbin), return TAO_ERROR);
        cam_roi->xbin = roi->xbin;
        update = true;
    }
    if (cam_roi->ybin != roi->ybin) {
        LIBFLI_CALL(FLISetVBin,(fcam->dev, roi->ybin), return TAO_ERROR);
        cam_roi->ybin = roi->ybin;
        update = true;
    }

    // Change the image area if different.
    if (cam_roi->xoff != roi->xoff || cam_roi->width  != roi->width ||
        cam_roi->yoff != roi->yoff || cam_roi->height != roi->height) {
        long x0 = fcam->x0 + roi->xoff;
        long y0 = fcam->y0 + roi->yoff;
        long x1 = x0 + roi->width;
        long y1 = y0 + roi->height;
        LIBFLI_CALL(FLISetImageArea,(fcam->dev, x0, y0, x1, y1),
                    return TAO_ERROR);
        cam_roi->xoff   = roi->xoff;
        cam_roi->yoff   = roi->yoff;
        cam_roi->width  = roi->width;
        cam_roi->height = roi->height;
        update = true;
    }

    // If any changes, update the settings (even though this should not
    // be necessary).
    if (update && update_image_roi(fcam) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Allocate/resize/free buffer.
static tao_status manage_buffer(
    void** bufptr,
    size_t* sizptr,
    size_t size)
{
    if (*bufptr == NULL || *sizptr != size) {
        *sizptr = 0;
        if (*bufptr != NULL) {
            void* oldbuf = *bufptr;
            *bufptr = NULL;
            free(oldbuf);
        }
        *bufptr = tao_malloc(size);
        if (*bufptr == NULL) {
            return TAO_ERROR;
        }
        *sizptr = size;
    }
    return TAO_OK;
}

static void attr_print_flags(FILE* file, unsigned flags)
{
#define PRT(p,x) do { if (((flags)|(x)) == (x)) fputs(p #x, file); } while (0)
    PRT("",  TAO_ATTR_READABLE);
    PRT("|", TAO_ATTR_WRITABLE);
    PRT("|", TAO_ATTR_VARIABLE);
#undef PRT
}

static tao_status on_initialize(
    tao_camera* cam,
    void* ptr)
{
    // The camera is a FLI camera.
    fli_camera* fcam = (fli_camera*)cam;

    // Get a pointer to the camera configuration and clear configuration.
    tao_camera_config* cfg = &cam->config;
    memset(cfg, 0, sizeof(*cfg));

    // The contextual data is the device.
    flidev_t dev = *(flidev_t*)ptr;

    // No acquisition buffers yet.
    fcam->buf.data = NULL;
    fcam->tmpbuf = NULL;

    // Default number of acquisition buffers.
    cfg->buffers = 4;

    // Retrieve detector dimensions and offsets (the "visible area").
    long x0, y0, x1, y1;
    LIBFLI_CALL(FLIGetVisibleArea,(dev, &x0, &y0, &x1, &y1), goto error);
    tao_forced_store(&fcam->x0, x0);
    tao_forced_store(&fcam->y0, y0);
    tao_forced_store(&cfg->sensorwidth, x1 - x0);
    tao_forced_store(&cfg->sensorheight, y1 - y0);

    // Retrieve initial ROI.
    if (update_image_roi(fcam) != TAO_OK) {
        goto error;
    }

    // Get physical size of pixels.
    LIBFLI_CALL(FLIGetPixelSize,
                (dev, (double*)&fcam->xsize, (double*)&fcam->ysize),
                goto error);

    // Camera pixel depth.  There is no function for retrieving the current
    // pixel depth, so we try the different possibilities until success.
    // We assume a fixed pixel depth of 16 bits by default.
    cfg->rawencoding = TAO_ENCODING_MONO(16);
    for (int i = 0; i < TAO_STATIC_ARRAY_LENGTH(bitdepth_props); ++i) {
        int err = FLISetBitDepth(dev, bitdepth_props[i].bitdepth);
        if (err == 0) {
            cfg->rawencoding = bitdepth_props[i].encoding;
            break;
        }
    }
    cfg->preprocessing = TAO_PREPROCESSING_NONE;
    if (cfg->rawencoding == TAO_ENCODING_MONO(8)) {
        cfg->pixeltype = TAO_UINT8;
    } else if (cfg->rawencoding == TAO_ENCODING_MONO(16)) {
        cfg->pixeltype = TAO_UINT16;
    } else {
        // This should never happens...
        tao_store_error(__func__, TAO_BAD_ENCODING);
        goto error;
    }

    // Set initial exposure time and frame rate.
    long exposuretime = INITIAL_EXPOSURE_TIME_MS;
    LIBFLI_CALL(FLISetExposureTime,(dev, exposuretime), goto error);
    cfg->exposuretime = 1e-3*exposuretime; // seconds
    cfg->framerate = INITIAL_FPS; // images per second

    // Other settings.  FIXME: Origin of time.
    cfg->preprocessing = TAO_PREPROCESSING_NONE;

    // Instantiate table of named attributes.
    tao_attr_table* attr_table = &fcam->base.config.attr_table;
    if (tao_attr_table_instantiate(attr_table, attr_defs, attr_count) != TAO_OK) {
        return TAO_ERROR;
    }

    // Initialize attribute values and check consistency.
    for (int i = 0; i < attr_count; ++i) {
        // Get attribute from camera configuration.
        tao_attr* attr = get_attr(fcam, i);

        // Check bitwise flags.
        if (TAO_ATTR_FLAGS(attr) != attr_defs[i].flags) {
            fprintf(stderr, "(BUG) Camera attribute \"%s\", has flags `",
                    attr_defs[i].key);
            attr_print_flags(stderr, tao_attr_get_flags(attr));
            fputs("`, but should be `", stderr);
            attr_print_flags(stderr, attr_defs[i].flags);
            fputs("`\n", stderr);
            fflush(stderr);
        }

        // Floating-point attributes initially set to NaN.
        if (TAO_ATTR_TYPE(attr) == TAO_ATTR_FLOAT) {
            tao_attr_set_float_value(attr, NAN, true);
        }

        // Call initializer if any.
        if (attr_ops[i].init != NULL && attr_ops[i].init(fcam, i) != TAO_OK) {
            goto error;
        }
    }

    // So far, so good...
    return TAO_OK;

    // Close the device on error.
error:
    (void)FLIClose(dev);
    return TAO_ERROR;
}

static tao_status on_finalize(
    tao_camera* cam)
{
    // The camera is a FLI camera.
    fli_camera* fcam = (fli_camera*)cam;

    // Variable to store returned result.
    tao_status status = TAO_OK;

    // Cancel exposure and close device.
    flidev_t dev = fcam->dev;
    fcam->dev = FLI_INVALID_DEVICE; // never close more than once
    if (dev != FLI_INVALID_DEVICE) {
        LIBFLI_CALL(FLICancelExposure,(dev), status = TAO_ERROR);
        LIBFLI_CALL(FLIControlShutter,(dev, FLI_SHUTTER_CLOSE),
                    status = TAO_ERROR);
        LIBFLI_CALL(FLIClose,(dev), status = TAO_ERROR);
    }

    // Free acquisition buffers.
    if (manage_buffer(&fcam->tmpbuf, &fcam->tmpsiz, 0) != TAO_OK) {
        status = TAO_ERROR;
    }
    if (manage_buffer(&fcam->buf.data, &fcam->buf.size, 0) != TAO_OK) {
        status = TAO_ERROR;
    }
    return status;
}

static tao_status on_reset(
    tao_camera* cam)
{
    // This is the same as stopping acquisition.
    return on_stop(cam);
}

static tao_status on_update_config(
    tao_camera* cam)
{
    // The camera is a FLI camera.
    fli_camera* fcam = (fli_camera*)cam;

    // Update (some) named attributes.
    for (int i = 0; i < attr_count; ++i) {
        if (attr_ops[i].update != NULL && attr_ops[i].update(fcam, i) != TAO_OK) {
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

// Check sensor and buffer encodings.
static tao_status check_encodings(
    const char* func,
    fli_camera* fcam,
    const tao_camera_config* cfg)
{
    for (int i = 0; i < TAO_STATIC_ARRAY_LENGTH(bitdepth_props); ++i) {
        if (cfg->rawencoding == bitdepth_props[i].encoding) {
            return TAO_OK;
        }
    }
    tao_store_error(__func__, TAO_BAD_ENCODING);
    return TAO_ERROR;
}

static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    // The camera is a FLI camera.
    fli_camera* fcam = (fli_camera*)cam;

    // Name of caller for error messages.
    const char* func = "tao_camera_check_configuration";

    // Error code in case of failure.
    int code = TAO_SUCCESS;

    // Check region of interest.
    if (tao_camera_roi_check(&cfg->roi,
                             cam->config.sensorwidth,
                             cam->config.sensorheight) != TAO_OK) {
        code = TAO_BAD_ROI;
        goto error;
    }

    // Check exposure time and frame rate.
    if (isnan(cfg->exposuretime)
        || cfg->exposuretime <= (EXPOSURETIME_MIN - EXPOSURETIME_TOL)
        || cfg->exposuretime >= (EXPOSURETIME_MAX + EXPOSURETIME_TOL)) {
        code = TAO_BAD_EXPOSURETIME;
        goto error;
    }
    if (isnan(cfg->framerate)
        || cfg->framerate*EXPOSURETIME_MAX < 1
        || cfg->framerate*cfg->exposuretime >= 1) {
        code = TAO_BAD_FRAMERATE;
        goto error;
    }

    // Check pre-processing parameters.  NOTE: In principle, pre-processing
    // parameters have already been checked by
    // `tao_camera_check_configuration`.
    if (tao_camera_configuration_check_preprocessing(cfg) != TAO_OK) {
        return TAO_ERROR;
    }

    // Check encoding parameters.
    if (check_encodings(func, fcam, cfg) != TAO_OK) {
        return TAO_ERROR;
    }

    // Check value of configurable named attributes.
    for (int i = 0; i < attr_count; ++i) {
        if (! same_attr_val(get_attr(fcam, i), get_attr(cfg, i))) {
            if (attr_ops[i].config == NULL) {
                code = TAO_UNWRITABLE;
                goto error;
            }
            if (attr_ops[i].check != NULL && attr_ops[i].check(fcam, cfg, i) != TAO_OK) {
                return TAO_ERROR;
            }
        }
    }

    // Check number of number of acquisition buffers.
    if (cfg->buffers < 2) {
        code = TAO_BAD_BUFFERS;
        goto error;
    }

    // All parameters seem to be acceptable.
    return TAO_OK;

    // Errors branch here with a given error code.
error:
    tao_store_error(func, code);
    return TAO_ERROR;
}

// In principle, `on_check_config` has been already called.
static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    // The camera is a FLI camera.
    fli_camera* fcam = (fli_camera*)cam;

    // Shortcut to camera configuration.
    tao_camera_config* cam_cfg = &cam->config;

    // Name of caller for error messages.
    const char* func = "tao_camera_set_configuration";

    // Error code in case of failure.
    int code = TAO_SUCCESS;

    // Set region of interest.  In principle, the ROI parameters have been
    // checked by `tao_camera_roi_check`.
    if (set_image_roi(fcam, &cfg->roi) != TAO_OK) {
        return TAO_ERROR;
    }

    // Set encodings.
    if (cam_cfg->rawencoding != cfg->rawencoding) {
        for (int i = 0; i < TAO_STATIC_ARRAY_LENGTH(bitdepth_props); ++i) {
            if (cfg->rawencoding == bitdepth_props[i].encoding) {
                LIBFLI_CALL(FLISetBitDepth,
                            (fcam->dev, bitdepth_props[i].bitdepth),
                            return TAO_ERROR);
                cam_cfg->rawencoding = cfg->rawencoding;
                break;
            }
        }
        if (cam_cfg->rawencoding != cfg->rawencoding) {
            code = TAO_BAD_ENCODING;
            goto error;
        }
    }

    // Set pre-processing parameters.
    cam_cfg->preprocessing = cfg->preprocessing;
    cam_cfg->pixeltype = cfg->pixeltype;
    cam_cfg->buffers = cfg->buffers;

    // Set exposure time (in milliseconds for the device).
    long milliseconds = lround(cfg->exposuretime*1e3);
    LIBFLI_CALL(FLISetExposureTime,(fcam->dev, milliseconds),
                return TAO_ERROR);
    cam_cfg->exposuretime = milliseconds/1e3;
    cam_cfg->framerate = cfg->framerate;

    // Configure named attributes.
    for (int i = 0; i < attr_count; ++i) {
        if (! same_attr_val(get_attr(fcam, i), get_attr(cfg, i))) {
            if (attr_ops[i].config == NULL) {
                code = TAO_UNWRITABLE;
                goto error;
            }
            if (attr_ops[i].config(fcam, cfg, i) != TAO_OK) {
                return TAO_ERROR;
            }
        }
    }

    // Everything went fine...
    return TAO_OK;

    // Errors branch here with a given error code.
error:
    tao_store_error(func, code);
    return TAO_ERROR;
}

static tao_status start_exposure(
    fli_camera* fcam)
{
    // FIXME: Not all of this is required?  Use already existing functions?
    LIBFLI_CALL(FLISetFrameType,(fcam->dev, fcam->frame_type),
                return TAO_ERROR);
    long exposure_time = lround(1e3*fcam->base.config.exposuretime);
    LIBFLI_CALL(FLISetExposureTime,(fcam->dev, exposure_time),
                return TAO_ERROR);
    LIBFLI_CALL(FLISetTDI,(fcam->dev, fcam->tdi_rate, fcam->tdi_flags),
                return TAO_ERROR);
    LIBFLI_CALL(FLIExposeFrame,(fcam->dev),
                return TAO_ERROR);
    return TAO_OK;
}

static tao_status on_start(
    tao_camera* cam)
{
    // The camera is a FLI camera.
    fli_camera* fcam = (fli_camera*)cam;

    // Allocate buffers.
    long width = cam->config.roi.width;
    long height = cam->config.roi.height;
    tao_encoding encoding = cam->config.rawencoding;
    long stride;
    size_t size;
    if (encoding == TAO_ENCODING_MONO(8)) {
        stride = width*sizeof(uint8_t);
        size = width*sizeof(uint16_t);
    } else if (encoding == TAO_ENCODING_MONO(16)) {
        stride = width*sizeof(uint16_t);
        size = 0;
    } else {
        tao_store_error(__func__, TAO_BAD_ENCODING);
        return TAO_ERROR;
    }
    if (manage_buffer(&fcam->tmpbuf, &fcam->tmpsiz, size) != TAO_OK) {
        return TAO_ERROR;
    }
    tao_acquisition_buffer* buf = &fcam->buf;
    size = height*stride;
    if (manage_buffer(&buf->data, &buf->size, size) != TAO_OK) {
        return TAO_ERROR;
    }
    buf->offset = 0;
    buf->width = width;
    buf->height = height;
    buf->stride = stride;
    buf->encoding = encoding;

    // Try to start in video mode, otherwise start a first exposure and manage
    // to trigger exposures one by one later.
    if (FLIStartVideoMode(fcam->dev) == 0) {
        fcam->continuous = true;
    } else {
        fcam->continuous = false;
        if (start_exposure(fcam) != TAO_OK) {
            fcam->exposing = false;
            return TAO_ERROR;
        }
    }
    fcam->exposing = true;

    return TAO_OK;
}

static tao_status on_stop(
    tao_camera* cam)
{
    fli_camera* fcam = (fli_camera*)cam;
    tao_status status = TAO_OK;
    if (fcam->continuous) {
        LIBFLI_CALL(FLIStopVideoMode,(fcam->dev), status = TAO_ERROR);
        fcam->continuous = false;
        fcam->exposing = false;
    } else if (fcam->exposing) {
        LIBFLI_CALL(FLICancelExposure,(fcam->dev), status = TAO_ERROR);
        fcam->exposing = false;
    }
    return status;
}

static tao_status get_frame_status(
    flidev_t dev,
    bool* ready,
    double* secs)
{
    long status, timeleft;
    LIBFLI_CALL(FLIGetDeviceStatus,(dev, &status), return TAO_ERROR);
    LIBFLI_CALL(FLIGetExposureStatus,(dev, &timeleft), return TAO_ERROR);
    if (status == FLI_CAMERA_STATUS_UNKNOWN) {
        *ready = (timeleft == 0);
    } else {
        *ready = (status & FLI_CAMERA_DATA_READY) != 0;
    }
    *secs = timeleft*1e-3;
    return TAO_OK;
}

static tao_status on_wait_buffer(
    tao_camera* cam,
    tao_acquisition_buffer* buf,
    double secs,
    int drop) // FIXME: ignored
{
    // Check time limit.
    if (secs < 0) {
        return TAO_TIMEOUT;
    }
    if (isnan(secs)) {
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return TAO_ERROR;
    }

    // The camera is a FLI camera.
    fli_camera* fcam = (fli_camera*)cam;

    // Start exposure if not exposing.
    if (! fcam->exposing) {
        if (start_exposure(fcam) != TAO_OK) {
            return TAO_ERROR;
        }
        fcam->exposing = true;
    }

    // Wait for an image to be available.
    bool ready;
    double timeleft;
    while (true) {
        // Check whether an image is available.
        if (get_frame_status(fcam->dev, &ready, &timeleft) != TAO_OK) {
            return TAO_ERROR;
        }
        if (ready) {
            break;
        }
        if (secs <= 0) {
            return TAO_TIMEOUT;
        }
        // Simply wait for the current exposure to finish but no longer than
        // the time limit and by chunks of 0.1 second to keep some
        // responsiveness.
        double sleeptime = tao_min(0.1, tao_min(timeleft, secs));
        if (tao_sleep(sleeptime) != TAO_OK) {
            return TAO_ERROR;
        }
        secs -= sleeptime;
    }

    // Grab image.
    tao_acquisition_buffer* cambuf = &fcam->buf;
    long width  = cambuf->width;
    long height = cambuf->height;
    long stride = cambuf->stride;
    uint8_t* data = ((uint8_t*)cambuf->data) + cambuf->offset;
    if (fcam->continuous) {
        // Grab image as a video frame.
        LIBFLI_CALL(FLIGrabVideoFrame,(fcam->dev, data, height*stride),
                    return TAO_ERROR);
    } else {
        // Clear exposing flag (to trigger a new exposure later) and
        // Grab image row-by-row.
        fcam->exposing = false;
        if (cambuf->encoding == TAO_ENCODING_MONO(8)) {
            // Grab image row-by-row using internal row buffer.
            void* tmpbuf = fcam->tmpbuf;
            for (long j = 0; j < height; ++j) {
                LIBFLI_CALL(FLIGrabRow,(fcam->dev, tmpbuf, width),
                            return TAO_ERROR);
                memcpy(data + j*stride, tmpbuf, width*sizeof(uint8_t));
            }
        } else {
            // Directly grab image row-by-row in output buffer.
            for (long j = 0; j < height; ++j) {
                LIBFLI_CALL(FLIGrabRow,
                            (fcam->dev, data + j*stride, width),
                            return TAO_ERROR);
            }
        }
    }
    cambuf->serial += 1;
    cam->config.frames += 1;
    *buf = *cambuf;
    return TAO_OK;
}
