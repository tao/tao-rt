# Remote cameras in TAO

A camera server runs at least two threads:
- the *server thread* is in charge of receiving commands from the clients and
  relaying them to the worker thread;
- the *worker thread* effectively manages the camera device and process
  acquired images.

A camera server is made of several different parts:
- a *server monitor* which stores all the resources of the camera server and
  has a mutex and a condition variable to synchronize the server thread and the
  worker thread;
- a *remote camera* instance created by the server and used to communicate with
  clients;
- a *camera device* to operate on the camera.

One important information are the run-levels of the camera device and of the
worker thread which should be as set according to the following table:

| Worker | Device | Remote       | Worker state                              |
|:-------|:-------|:-------------|:------------------------------------------|
| 0      | 1      | initializing | Worker has not yet started                |
| 1      | 1      | waiting      | Worker is idle                            |
| 2      | 2      | working      | Worker is acquiring and processing images |
| 3      | x -> 1 | unreachable  | Worker has exited (i.e., joinable)        |
| 4      | x -> 1 | unreachable  | Joining a terminated worker has failed    |

Columns "Worker" and "Device" give the run-levels of the worker thread and of
the camera device ("x -> 1" means that camera acquisition should be stopped);
column "Remote" give the state of the remote camera.

Most of the time, the remote camera state is set according to the run-level of
the worker thread.  However, when a command is accepted, the remote camera
state is set to a "transitory state" indicating which command is being
precessed.  When the command is completed, the remote camera state is again set
according to the run-level of the worker thread.

When the server loop is started, the server thread ensures that the camera
device is at run-level 1 (waiting), set the worker run-level to 0, and creates
the worker thread which shall set its run-level to 1 once its initialization
has completed.

When the run-level of the worker is 1 and acquisition is started, the worker
thread calls `tao_camera_start_acquisition` so that the camera device and the
worker thread are at run-level 2.

When the run-level of the worker is 2 and acquisition is stopped, the worker
thread calls `tao_camera_stop_acquisition` so that the camera device and the
worker thread are at run-level 1.

The following rules always apply if a worker is running:

- only the worker thread can change its run-level;
- only the worker thread operates the camera device;
- the worker thread is in-charge of updating the remote camera state and
  information;

If the server thread wants to terminate the worker thread, it has to force a
"quit" command:

```.c
server_lock(srv);
if (srv->task != TAO_COMMAND_NONE) {
    remote_camera_lock(srv);
    remote_camera_serialcommand(srv) += 1;
    remote_camera_notify(srv);
    remote_camera_unlock(srv);
}
srv->task = TAO_COMMAND_QUIT;
server_notify(srv);
server_unlock(srv);
```

## Server thread implementation

Running the server thread (`tao_camera_server_run_loop`) amounts to executing
the following code:

```
╭ lock remote camera
│ ╭ lock server monitor
│ │ start worker thread
│ │ wait on server monitor for worker to be initialized
│ ╰ unlock server monitor
│ write shmid of remote camera to global configuration
│ run server loop
│ ╭ lock server monitor
│ │ kill worker thread
│ ╰ unlock server monitor
╰ unlock remote camera
```

Hence the caller of `tao_camera_server_run_loop` must not own any locks
on the server monitor or remote camera.

The sentences like "wait on `obj`" mean to wait on the condition variable
associated with object `obj` for some conditions to hold.  These are always
implemented as follows:

```
while (!expr) {
    condition_wait(&obj->cond, &obj->mutex);
}
```

where `expr` is an expression that is true when the conditions are satisfied.
During the call to `condition_wait` (eventually resulting in a call to
`pthread_cond_wait`), the mutex is unlocked and the calling thread suspended.

The server loop is equivalent to repeatedly executing:

```
# Receive a command from a client:
╭ lock remote camera (virtual)
│ wait on remote camera for a new command from client
│ memorize the command and any associated data from the remote camera
│ write transitory state to remote camera
│ erase remote camera command
│ notify remote camera to wake up clients
╰ unlock remote camera

# Relay the command to the worker:
╭ lock server monitor
│ wait on server monitor for worker to be ready for command
│ if server monitor run-level ok
│ │ write command and any associated data to server monitor for worker
│ │ notify server monitor to wake up worker
│ │ wait on server monitor for worker to acknowledge command
│ ╰
╰ unlock server monitor
```

To clarify which resources are locked at any time, the initial *locking* and
final *unlocking* of the remote camera are indicated as *virtual*.  In the
actual code, only the very first lock (on entry of the loop) and very last
unlock (on exit of the loop) are effectively done (at the above level), the
other pairs of virtual unlocking-locking of the remote camera are fused in a
no-operation.

Data associated with a command can be for instance the configuration settings
for a "config" command.


## Client implementation

Looking at the server implementation, a client shall implement the sending of a
command as follows:

```
# Send a command to the server:
╭ lock remote camera
│ wait on remote camera for the server to be ready to accept a command
│ n = read command counter
│ write the command and any associated data to the remote camera
│ notify remote camera to wake up the server thread
│ wait on remote camera for the server to acknowledge receiving the command
╰ unlock remote camera
return n + 1
```

The returned value is the serial number of the command.  The last wait is
optional.  To wait for the `n`-th command to be actually done, it is sufficient
to:

```
# Wait for n-th command to be done:
╭ lock remote camera
│ wait on remote camera until command counter is ≥ n
│ read camera state
╰ unlock remote camera
return state
```

where we have supposed that the caller is interested in knowing the camera
state after the `n`-th command.  However note that there are no guarantees that
no other commands have been executed since then.

A simplified implementation (no checking of errors) in C of sending a command
by a client is:

```.c
// The handle to the remote camera.
tao_remote_camera* rcam = ...;

// Lock the remote camera for exclusive access.
tao_remote_camera_lock(rcam);

// Wait for the server to be ready for executing a new command.
while (rcam->state < TAO_STATE_UNREACHABLE &&
       rcam->command != TAO_COMMAND_NONE) {
    tao_remote_camera_wait_condition(rcam);
}

tao_serial serial = -1;
if (rcam->state < TAO_STATE_UNREACHABLE &&
    rcam->command == TAO_COMMAND_NONE) {
    // Server can accept a new command.
    // Copy new configuration if needed.
    if (command == TAO_COMMAND_CONFIG) {
        memcpy(&rcam->arg.config, config, sizeof(tao_camera_config));
    }
    // Memorize what will be the serial number of the command.
    num = rcam->ncmds + 1;
    // Set next command to execute and notify the server.
    rcam->command = command;
    tao_remote_camera_broadcast_condition(rcam);
    // Wait for the server to acknowledge the execution of the command.
    while (rcam->state < TAO_STATE_UNREACHABLE &&
           rcam->command != TAO_COMMAND_NONE) {
        tao_remote_camera_wait_condition(rcam);
    }
}
tao_remote_camera_unlock(rcam);

return num;
```

Note that it is possible to set a command while the remote camera is
initializing (i.e. when its state is `TAO_STATE_INITIALIZING`) providing there
are no pending commands (i.e., its pending command must be `TAO_COMMAND_NONE`).

A simplified implementation (no checking of errors) in C of the client waiting
for a command to be executed:

```.c
// The handle to the remote camera and the command counter to wait for.
tao_remote_camera* rcam = ...;
tao_serial serial = ...;

// Lock the remote camera for exclusive access.
tao_remote_camera_lock(rcam);

// Wait for the server to be ready for executing a new command.
while (rcam->state < TAO_STATE_UNREACHABLE &&
       rcam->ncmds < serial) {
    tao_remote_camera_wait_condition(rcam);
}
tao_state state = tao_remote_camera_get_state(rcam);
tao_remote_camera_unlock(rcam);

return state;
```


## Worker thread implementation

Once started, the worker thread executes the following code:

```
╭ lock server monitor
│ set run-level of server monitor to 1
│ notify server monitor to wake up server thread
│ run worker loop
╰ unlock server monitor
```

Setting the run-level to 1 and notifying the server monitor is to let the
server thread know that the worker thread has started.  Running the worker loop
amounts to repeatedly do (*virtual* has the same meaning as for the server
thread pseudo-code):

```
╭ lock server monitor (virtual)
│ wait on server monitor for a command from the server or for run-level not idle
│ if worker thread run-level is 3 (or more)
│ │ exit loop
│ ╰
│ if any command
│ │ ╭ lock camera device
│ │ │ synchronize runlevels
│ │ │ execute the command on the camera device
│ │ │ synchronize runlevels
│ │ │ ╭ lock remote camera
│ │ │ │ increment remote camera command counter
│ │ │ │ update remote camera state
│ │ │ │ reflect camera device settings into remote camera
│ │ │ ╰ unlock camera device
│ │ ╰ unlock camera device
│ │ erase server monitor command
│ │ notify server monitor to wake up server
│ ╰
│ if worker thread run-level is 3 (or more)
│ │ exit loop
│ ╰
│ if worker thread run-level is 2
│ │ ╭ lock camera device
│ │ │ synchronize runlevels
│ │ │ ╭ unlock server monitor
│ │ │ │ wait on camera device for next acquisition buffer
│ │ │ ╰ lock server monitor
│ │ │ synchronize runlevels
│ │ ╰ unlock camera device
│ │ if acquisition not cancelled and no timeout
│ │ │ process acquisition buffer
│ │ │ unlock shared array with current image and lock next one
│ │ │ ╭ lock remote camera
│ │ │ │ increment remote camera image counter
│ │ │ │ notify remote camera to wake up clients
│ │ │ ╰ unlock remote camera
│ │ ╰
│ ╰
╰ unlock server monitor (virtual)
```

The sentence "synchronize runlevels" means that the run-level of the server
thread is synchronized to be consistent with that of the camera device when it
may have changed (because the camera device was previously unlocked or because
a function operating on the camera device was called).  This synchronization is
done by the function `update_worker_runlevel` in the `camera-servers.c` code.

While waiting for the next acquisition buffer, the server monitor is unlocked.
Hence the server thread can lock the server monitor (to send commands) during
the topmost wait and while the worker is waiting for an image.  This is the
reason of testing whether acquisition was cancelled after waiting for an image
(run-level > 2, or "abort" or "quit" command).

Executing a command by the worker thread is done while the worker owns the
locks on the server monitor and on the camera device.  In the meantime, the
server thread waits on the server monitor for the command to be erased (so that
it knows that the command has been executed) and then attempts to lock the
camera device to get its, possibly, new state and report it to the clients.


## Overriding commands

Commands may be allowed to override a pending command.  For instance, any number
of consecutive *start* commands amount to just one or it is more important to
apply the last actuators commands on a deformable mirror.

### Version with two command counters

The following pseudo-code implements overriding of commands with 2 counters and
a queue of a single pending command:

```{.c}
// Client part to send a command `cmd` to the remote server via `ctrl`.
lock(ctrl)
if (! override(ctrl, cmd)) {
    while (ctrl->command != NONE) {
        wait(ctrl)
    }
}
ctrl->command = cmd
ctrl->queued += 1
num = ctrl->queued
notify(ctrl)
unlock(ctrl)
return num

// Server part to wait for commands and process them.
lock(ctrl)
while (ctrl->command == none) {
    wait(ctrl)
}
process(ctrl->command)
ctrl->command = NONE
ctrl->processed = ctrl->queued
notify(ctrl)
unlock(ctrl)
```

Here `ctrl` is some structure equipped with a mutex and a condition variable
that is shared between the clients and the server.  The member `ctrl->command`
is the current command (or `NONE` if there is no pending command), members
`ctrl->queued` and `ctrl->processed` are the number of queued and processed
commands respectively.  The function `override(ctrl, cmd)` yields whether the
command `cmd` overrides the current pending command in `ctrl`. The function
`lock(ctrl)` locks the mutex of `ctrl` while `unlock(ctrl)` unlocks it.  The
function `wait(ctrl)` waits on the condition variable of `ctrl` and
`notify(ctrl)` signals or broadcasts to others that a change in `ctrl` may have
occurred.

Actual code is more complicated because, there may be errors, timeouts (to avoid
waiting for ever), and the server may have been killed (it is not sufficient to
just check the value of pending command while waiting).

A variant may be to unlock the resources while processing the command but this
is only of interest if something in the `ctrl` structure has changed at that
time.


### Version with one command counter

If it is preferable that overriden commands count for nothing, then a single
member `ctrl->ncmds` is required to count the number of processed commands.  The
code then becomes:

```{.c}
// Client part to send a command `cmd` to the remote server via `ctrl`.
lock(ctrl)
if (override(ctrl, cmd)) {
    num = ctrl->ncmds
} else {}
    while (ctrl->command != NONE) {
        wait(ctrl)
    }
    num = ctrl->ncmds + 1
}
ctrl->command = cmd
notify(ctrl)
unlock(ctrl)
return num

// Server part to wait for commands and process them.
lock(ctrl)
while (ctrl->command == none) {
    wait(ctrl)
}
process(ctrl->command)
ctrl->command = NONE
ctrl->ncmds += 1
notify(ctrl)
unlock(ctrl)
```

## Benchmarking

Using the Andor simulated camera (with 2560×2160 pixels), at a frame rate of
20Hz, and on an AMD Ryzen Threadripper 2950X 16-Core Processor at 3.5GHz, the
following timings have been measured for the worker thread of the camera
server:

| Pre-processing | Pixel Type | % CPU | GFlops |
|:---------------|:-----------|:-----:|-------:|
| none           | `uint16`   | 10%   |    5.5 |
| none           | `uint32`   | 13%   |    4.2 |
| none           | `float`    | 13%   |    4.2 |
| none           | `double`   | 17%   |    3.3 |
| affine         | `float`    | 15%   |    7.4 |
| affine         | `double`   | 21%   |    5.3 |
| full           | `float`    | 22%   |   12.6 |
| full           | `double`   | 35%   |    7.9 |

The CPU power in GFlops has been estimated assuming that the number of
operations per pixel is 1 when pre-preprocessing is *none*, 2 when
pre-preprocessing is *affine*, and 5 when pre-preprocessing is *full*.

Hence full pre-processing of a 400×400 image in single precision could be done
in about 60µs.
