# Interfacing a new camera type in TAO

Implementing an interface to a new type of cameras, say the infamous `Foo`
cameras, in TAO requires at least 2 files:

- a public header file;

- a C code file.


## Public header file

The minimal public header file, say `tao-foo-cameras.h`, is something like:

```{.c}
#ifndef TAO_FOO_CAMERAS_H_
#define TAO_FOO_CAMERAS_H_ 1

#include <tao-cameras.h>

TAO_BEGIN_DECLS

/// @defgroup FooCameras  Interface to Foo cameras.
///
/// @ingroup Cameras
///
/// @brief Interface to Foo cameras.
///
/// Some description of this module.
///
/// @{

/// @brief Open a Foo camera.
///
/// This function connects to the Foo camera identified by `id` and returns
/// a handle to manage it.  The caller is responsible of eventually calling
/// `tao_camera_destroy` to close the camera device and release associated
/// resources.
///
/// @param id   Identifier of the camera.
///
/// @return The address of the new (initialized) camera; `NULL` in case of
///         failure.
///
extern tao_camera* tao_foo_camera_open(int id);

/// @}

TAO_END_DECLS

#endif // TAO_FOO_CAMERAS_H_
```

Preprocessor directives with the `TAO_FOO_CAMERAS_H_` macro are to allow for
multiple includes of the file without breaking the compilation.

Most comments are to provide some documentation that can be automatically built
by [Doxygen](https://www.doxygen.org/index.html).

The surrounding by the macros `TAO_BEGIN_DECLS` and `TAO_END_DECLS` is to be
able to link with C++ in spite of TAO libraries being written in C for maximum
portability.  These macros are provided by `<tao-basics.h>` which is
automatically included by `<tao-cameras.h>`.

The only mandatory public declaration is the prototype of the function that
can be called to connect to a given `Foo` camera.  Argument list may be
anything, the return value must however be a pointer to a `tao_camera`
structure which is created by calling `tao_camera_create()`.


# Template C code

Below is a template of the C code that you have to fill to implement the
unified camera interface.  This interface consists in 9 functions.  In this
template, all these functions return an error indicating that they are not yet
implemented (of course since supporting these family of cameras is new in TAO).

```{.c}
#include "tao-foo-cameras.h" // needed for the prototype of tao_foo_camera_open
#include "tao-errors.h" // for error management
#include "tao-cameras-private.h" // for the definition of tao_camera structure

//-----------------------------------------------------------------------------
// Declaration of private methods and of the table grouping these methods for
// the unified camera API.

static tao_status on_initialize(
    tao_camera* cam);

static tao_status on_finalize(
    tao_camera* cam);

static tao_status on_reset(
    tao_camera* cam);

static tao_status on_update_config(
    tao_camera* cam);

static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_start(
    tao_camera* cam);

static tao_status on_stop(
    tao_camera* cam);

static tao_status on_wait_buffer(
    tao_camera* cam,
    tao_acquisition_buffer* buf,
    double secs,
    int drop);

static tao_camera_ops ops = {
    .name          = "FooCamera", // FIXME: Change the name!
    .initialize    = on_initialize,
    .finalize      = on_finalize,
    .reset         = on_reset,
    .update_config = on_update_config,
    .check_config  = on_check_config,
    .set_config    = on_set_config,
    .start         = on_start,
    .stop          = on_stop,
    .wait_buffer   = on_wait_buffer
};

//-----------------------------------------------------------------------------
// Definition of the public function to connect to a Foo camera.

tao_camera* tao_foo_camera_open(
    int id)
{
    tao_store_error(__func__, TAO_NOT_YET_IMPLEMENTED);
    return NULL;
}

//-----------------------------------------------------------------------------
// Definition of the methods to manage Foo cameras.

static tao_status on_initialize(
    tao_camera* cam)
{
    tao_store_error(__func__, TAO_NOT_YET_IMPLEMENTED);
    return TAO_ERROR;
}

static tao_status on_finalize(
    tao_camera* cam)
{
    tao_store_error(__func__, TAO_NOT_YET_IMPLEMENTED);
    return TAO_ERROR;
}

static tao_status on_reset(
    tao_camera* cam)
{
    tao_store_error(__func__, TAO_NOT_YET_IMPLEMENTED);
    return TAO_ERROR;
}

static tao_status on_update_config(
    tao_camera* cam)
{
    tao_store_error(__func__, TAO_NOT_YET_IMPLEMENTED);
    return TAO_ERROR;
}

static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    tao_store_error(__func__, TAO_NOT_YET_IMPLEMENTED);
    return TAO_ERROR;
}

static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    tao_store_error(__func__, TAO_NOT_YET_IMPLEMENTED);
    return TAO_ERROR;
}

static tao_status on_start(
    tao_camera* cam)
{
    tao_store_error(__func__, TAO_NOT_YET_IMPLEMENTED);
    return TAO_ERROR;
}
static tao_status on_stop(
    tao_camera* cam)
{
    tao_store_error(__func__, TAO_NOT_YET_IMPLEMENTED);
    return TAO_ERROR;
}

static tao_status on_wait_buffer(
    tao_camera* cam,
    tao_acquisition_buffer* buf,
    double secs,
    int drop)
{
    tao_store_error(__func__, TAO_NOT_YET_IMPLEMENTED);
    return TAO_ERROR;
}
```
