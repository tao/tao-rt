//
// Image server for Andor cameras.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2019-2024, Éric Thiébaut.

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "tao-andor.h"
#include "tao-errors.h"
#include "tao-camera-servers.h"
#include "tao-cameras-private.h"

static void print_name(
    FILE* output,
    const char* name,
    long pad,
    int c)
{
    long len = TAO_STRLEN(name);
    fprintf(output, "  %s", (len > 0 ? name : ""));
    if (c == ':') {
        fputc(c, output);
        while (++len <= pad) {
            fputc(' ', output);
        }
    } else if (c == '>') {
        fputc(' ', output);
        while (++len <= pad) {
            fputc('-', output);
        }
        fputc(c, output);
        fputc(' ', output);
    } else if (c == '.') {
        fputc(' ', output);
        while (++len <= pad) {
            fputc('.', output);
        }
        fputc(' ', output);
    } else {
        while (++len <= pad) {
            fputc(' ', output);
        }
        fputc(' ', output);
    }
}

static tao_status print_feature(
    FILE* output,
    AT_H handle,
    int mode,
    const andor_known_feature* known_feature)
{
    // Small buffer for enumerated values.
    const int buflen = 128;
    wchar_t buf[buflen];

    // Retrieve as much as possible information about the feature.
    andor_feature feature;
    if (andor_instantiate_feature(&feature, handle, known_feature,
                                  ANDOR_FEATURE_UNKNOWN) != TAO_OK) {
        return TAO_ERROR;
    }
    if (!feature.implemented) {
        // Skip feature not implemented for this camera.
        return TAO_OK;
    }
    print_name(output, feature.key, 30, '.');
    fputs((feature.readonly ? "r-" : "rw"), output);
    switch (feature.type) {
    case ANDOR_FEATURE_BOOLEAN:
        fprintf(output, " [boolean] = %s\n",
                (feature.val.b ? "true" : "false"));
        break;
    case ANDOR_FEATURE_INTEGER:
        fprintf(output, " [integer] = " TAO_INT64_FORMAT(,d),
                feature.val.i);
        if (feature.readonly) {
            fputc('\n', output);
        } else {
            fprintf(output, " (min = " TAO_INT64_FORMAT(,d)
                    ", max = " TAO_INT64_FORMAT(,d) ")\n",
                    feature.min.i, feature.max.i);
        }
        break;
    case ANDOR_FEATURE_FLOAT:
        fprintf(output, " [float] = %g", feature.val.f);
        if (feature.readonly) {
            fputc('\n', output);
        } else {
            fprintf(output, " (min = %g max = %g)\n",
                feature.min.f, feature.max.f);
        }
        break;
    case ANDOR_FEATURE_STRING:
        fprintf(output, " [string] = \"%ls\"\n",
                feature.val.s);
        break;
    case ANDOR_FEATURE_ENUM:
        fprintf(output, " [enum] = %d => \"%ls\" (",
                feature.index, feature.val.s);
        int count = feature.max.i - feature.min.i + 1;
        for (int i = 0; i < count; ++i) {
            int code = AT_GetEnumStringByIndex(handle, feature.wkey, i,
                                               buf, buflen);
            if (code != AT_SUCCESS) {
                andor_error("AT_GetEnumStringByIndex", code);
                fputc('\n', output);
                return TAO_ERROR;
            }
            fprintf(output, "%s%d => \"%ls\"",
                    (i > 0 ? ", " : ""), i, buf);
        }
        fputs(")\n", output);
        break;
    case ANDOR_FEATURE_COMMAND:
        fputs(" [command]\n", output);
        break;
    default:
        fputs(" [unknown]\n", output);
        break;
    }
    return TAO_OK;
}

static const char* system_features[] = {
    "DeviceCount", "SoftwareVersion", "LogLevel", NULL
};

static const char* camera_features[] = {
    "CameraModel", "CameraName",
    "SensorWidth", "SensorHeight",
    "PixelEncoding", NULL
};

static tao_status print_device_info(
    FILE* output,
    const char* progname,
    int devnum,
    int mode)
{
    tao_status status = TAO_OK;
    AT_H handle = AT_HANDLE_SYSTEM;

    // Open device, if not system.
    if (devnum >= 0 && andor_open(devnum, &handle) != TAO_OK) {
        fprintf(stderr, "%s: Failed to open Andor camera device %d.\n",
                progname, devnum);
        return TAO_OK;
    }
    fprintf(output, "Andor device %d:\n", devnum);
    if (mode < 2) {
        // Just print a summary of a few features.
        andor_feature feature;
        const char** features = (handle == AT_HANDLE_SYSTEM ?
                                 system_features : camera_features);
        for (int k = 0; features[k] != NULL; ++k) {
            int i = andor_known_feature_try_find(features[k]);
            if (i < 0) {
                // Not found or error.
                if (i == -1) {
                    fprintf(stderr, "(WARNING) Andor camera feature "
                            "\"%s\" not found in list of known features.\n",
                            features[k]);
                }
                continue;
            }
            if (andor_instantiate_feature(&feature, handle,
                                          &andor_known_features[i],
                                          ANDOR_FEATURE_UNKNOWN) != TAO_OK) {
                return TAO_ERROR;
            }
            if (feature.implemented) {
                print_name(output, feature.key, 30, '.');
                switch (feature.type) {
                case ANDOR_FEATURE_BOOLEAN:
                    fprintf(output, "%s\n",
                            (feature.val.b ? "true" : "false"));
                    break;
                case ANDOR_FEATURE_INTEGER:
                    fprintf(output, TAO_INT64_FORMAT(,d) "\n", feature.val.i);
                    break;
                case ANDOR_FEATURE_FLOAT:
                    fprintf(output, "%g\n", feature.val.f);
                    break;
                case ANDOR_FEATURE_STRING:
                case ANDOR_FEATURE_ENUM:
                    fprintf(output, "\"%ls\"\n", feature.val.s);
                    break;
                case ANDOR_FEATURE_COMMAND:
                    fputs("[command]\n", output);
                    break;
                default:
                    fputs("[unknown]\n", output);
                    break;
                }
            }
        }
    } else {
        // Print all known features.
        for (int i = 0; i < andor_known_features_number; ++i) {
            if (print_feature(output, handle, mode,
                              &andor_known_features[i]) != TAO_OK) {
                status = TAO_ERROR;
            }
        }
    }

    // Close device, if not system.
    if (devnum >= 0 && andor_close(handle) != TAO_OK) {
        fprintf(stderr, "%s: Failed to close Andor camera device %d.\n",
                progname, devnum);
        status = TAO_ERROR;
    }
    return status;
}

static int format_owner(
    char* dst,
    long dst_size,
    const char* str,
    int devnum)
{
    long nstr = TAO_STRLEN(str);
    long k = 0; // number of consecutive '%'
    char buf[32];
    sprintf(buf, "%d", devnum);
    long nbuf = TAO_STRLEN(buf);
    long j = 0;
    long j1 = dst_size - 1;
    long j2 = dst_size - nbuf;
    for (long i = 0; i < nstr; ++i) {
        int c = str[i];
        if (c == '%') {
            if (++k == 2) {
                if (j >= j1) {
                    return -2;
                }
                dst[j++] = '%';
                k = 0;
            }
        } else if (k == 1) {
            if (c != 'd') {
                return -1;
            }
            if (j >= j2) {
                return -2;
            }
            for (long l = 0; l < nbuf; ++l) {
                dst[j+l] = buf[l];
            }
            j += nbuf;
            k = 0;
        } else {
            if (j >= j1) {
                return -2;
            }
            dst[j++] = c;
        }
    }
    if (k != 0 || j < 1) {
        return -1;
    }
    dst[j] = '\0';
    return 0;
}

int main(
    int argc,
    char* argv[])
{
    // Determine program name.
    const char* progname = tao_basename(argv[0]);

    // Initialize value returned on exit and address of allocated resources for
    // cleanup.
    int exitcode = EXIT_SUCCESS;
    tao_camera_server* srv = NULL;
    tao_camera* cam = NULL;

    // The number of devices is initialized after the parsing of arguments.
    long ndevices = 0;

    // Parse arguments.
    const char* usage = "Usage: %s [OPTIONS ...] [--] [DEV [NAME]]\n";
    const char* name = "Andor%d";
    int list = 0;
    bool debug = false;
    bool fancy = true;
    long nbufs = 5;
    unsigned int perms = 0077;
    int iarg = 0;
    while (++iarg < argc) {
        char dummy;
        if (argv[iarg][0] != '-') {
            // Argument is not an option.
            --iarg;
            break;
        }
        if (argv[iarg][1] == '-' && argv[iarg][2] == '\0') {
            // Argument is last option.
            break;
        }
        if (strcmp(argv[iarg], "-h") == 0
            || strcmp(argv[iarg], "-help") == 0
            || strcmp(argv[iarg], "--help") == 0) {
            printf("\n");
            printf(usage, progname);
            printf("\n");
            printf("Launch a server for an Andor camera.\n");
            printf("\n");
            printf("Arguments:\n");
            printf("  DEV                Camera device number (-1 for "
                   "system, ≥ 0 for a camera).\n");
            printf("  NAME               Server name (may have a %%d to "
                   "insert device number) [%s].\n", name);
            printf("\n");
            printf("Options:\n");
            printf("  -info              Long information about device(s).\n");
            printf("  -list              Brief information about device(s).\n");
            printf("  -nbufs NBUFS       Number of output buffers [%ld].\n",
                   nbufs);
            printf("  -perms BITS        Bitwise mask of permissions [0%o].\n",
                   perms);
            printf("  -nofancy           Do not use colors nor set window "
                   "title.\n");
            printf("  -debug             Debug mode [%s].\n",
                   (debug ? "true" : "false"));
            printf("  -h, -help, --help  Print this help.\n");
            printf("\n");
            return EXIT_SUCCESS;
        }
        if (strcmp(argv[iarg], "-info") == 0) {
            list = 2;
            continue;
        }
        if (strcmp(argv[iarg], "-list") == 0) {
            list = (list == 0 ? 1 : list);
            continue;
        }
        if (strcmp(argv[iarg], "-nbufs") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option %s.\n",
                        progname, argv[iarg]);
                goto error;
            }
            if (sscanf(argv[iarg+1], "%ld %c", &nbufs, &dummy) != 1
                || nbufs < 2) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option %s.\n",
                        progname, argv[iarg+1], argv[iarg]);
                goto error;
            }
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-perms") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option %s.\n",
                        progname, argv[iarg]);
                goto error;
            }
            if (sscanf(argv[iarg+1], "%i %c", (int*)&perms, &dummy) != 1) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option %s.\n",
                        progname, argv[iarg+1], argv[iarg]);
                goto error;
            }
            perms &= 0777;
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-nofancy") == 0) {
            fancy = false;
            continue;
        }
        if (strcmp(argv[iarg], "-debug") == 0) {
            debug = true;
            continue;
        }
        fprintf(stderr, "%s: Unknown option %s.\n", progname, argv[iarg]);
        goto error;
    }

    // Get the number of Andor devices. This also initializes the Andor SDK.
    ndevices = andor_get_ndevices();
    if (ndevices < 1) {
        if (ndevices == 0) {
            fprintf(stderr, "%s: No devices found.\n",progname);
        }
        goto error;
    }

    // Get device number (optional in list mode).
    long devnum = -2; // -2 = list all devices
    if (++iarg >= argc) {
        if (list == 0) {
            fprintf(stderr, "%s: Missing device number.\n", progname);
            goto bad_usage;
        }
    } else if (tao_parse_long(argv[iarg], &devnum, 0) != TAO_OK ||
               devnum < (list == 0 ? 0 : -1) || devnum >= ndevices) {
        fprintf(stderr, "%s: Invalid device number %s.\n",
                progname, argv[iarg]);
        goto error;
    }

    // Get server name (not in list mode).
    char owner[TAO_OWNER_SIZE];
    if (list == 0) {
        if (++iarg < argc) {
            name = argv[iarg];
        }
        int rv = format_owner(owner, TAO_OWNER_SIZE, name, devnum);
        if (rv != 0) {
            if (rv == -2) {
                fprintf(stderr, "%s: Owner name too long.\n", progname);
            } else {
                fprintf(stderr, "%s: Invalid owner name.\n", progname);
            }
            goto error;
        }
        name = owner;
    }
    if (++iarg < argc) {
        fprintf(stderr, "%s: Too many arguments.\n", progname);
    bad_usage:
        fputc('\n', stderr);
        fprintf(stderr, usage, progname);
        fputc('\n', stderr);
        goto error;
    }

    // Just list available devices if requested to do so and return.
    if (list != 0) {
        FILE* output = stdout;
        int firstdev = (devnum < -1 ? -1 : devnum);
        int lastdev = (devnum < -1 ? ndevices - 1 : devnum);
        for (int dev = firstdev; dev <= lastdev; ++dev) {
            if (dev > firstdev) {
                fputc('\n', output);
            }
            if (print_device_info(output, progname, dev, list) != TAO_OK) {
                exitcode = EXIT_FAILURE;
                break;
            }
        }
        goto done;
    }

    // Open the camera device.  The camera state is set to "initializing" until
    // the worker thread has started.
    cam = andor_open_camera(devnum);
    if (cam == NULL) {
        fprintf(stderr, "%s: Failed to open Andor camera device %ld.\n",
                progname, devnum);
        goto error;
    }
    if (debug) {
        fprintf(stderr, "%s: Camera device %ld now open.\n", progname, devnum);
    }

    // Retrieve initial configuration.  Make sure to synchronize the actual
    // configuration after any changes in case some parameters are not exactly
    // the requested ones.
    tao_camera_config cfg;
    if (tao_camera_get_configuration(cam, &cfg) != TAO_OK) {
        fprintf(stderr, "%s: Failed to retrieve camera configuration.\n",
                progname);
        goto error;
    }
    if (debug) {
        // Apply the current configuration, this should not change anything
        // (this is just for debugging purposes).
        fprintf(stdout, "%s: ", progname);
        andor_print_supported_encodings(stdout, cam);
        fprintf(stdout, "\n");
        fprintf(stdout, "%s: Initial camera configuration:\n", progname);
        tao_camera_config_print(stdout, &cam->config);
        fprintf(stdout, "\n");
        if (tao_camera_set_configuration(cam, &cfg) != TAO_OK) {
            fprintf(stderr, "%s: Failed to restore camera configuration.\n",
                    progname);
            goto error;
        }
        if (tao_camera_update_configuration(cam) != TAO_OK) {
            fprintf(stderr, "%s: Failed to update camera configuration.\n",
                    progname);
            goto error;
        }
        if (tao_camera_get_configuration(cam, &cfg) != TAO_OK) {
            fprintf(stderr, "%s: Failed to retrieve camera configuration.\n",
                    progname);
            goto error;
        }
        fprintf(stdout, "%s: Camera configuration has been retrieved.\n",
                progname);
        fprintf(stdout, "%s: Restored camera configuration:\n", progname);
        tao_camera_config_print(stdout, &cam->config);
        fprintf(stdout, "\n");
    }

    // Create the camera server.
    srv = tao_camera_server_create(name, cam, nbufs, perms);
    if (srv == NULL) {
        fprintf(stderr, "%s: Failed to create the camera server.\n",
                progname);
        goto error;
    }
    if (debug) {
        fprintf(stderr,
                "%s: Camera server \"%s\" created with %ld buffers.\n",
                progname, tao_camera_server_get_owner(srv), nbufs);
    }
    if (debug) {
        srv->loglevel = TAO_MESG_DEBUG;
    } else {
        srv->loglevel = TAO_MESG_INFO;
    }
    srv->fancy = fancy;

    // Run the server loop.
    if (tao_camera_server_run_loop(srv) != TAO_OK) {
        fprintf(stderr, "%s: Failed to run the camera server.\n",
                progname);
        goto error;
    }

done:
    // Destroy the server.
    if (srv != NULL && tao_camera_server_destroy(srv) != TAO_OK) {
        fprintf(stderr, "%s: Failed to destroy the camera server.\n",
                progname);
        exitcode = EXIT_FAILURE;
    }

    // Close the camera device.
    if (cam != NULL && tao_camera_destroy(cam) != TAO_OK) {
        fprintf(stderr, "%s: Failed to destroy the camera device.\n",
                progname);
        exitcode = EXIT_FAILURE;
    }

    // Finalize Andor SDK.
    if (ndevices >= 0 && andor_finalize_library() != TAO_OK) {
        fprintf(stderr, "%s: Failed to finalize Andor SDK.\n",
                progname);
        exitcode = EXIT_FAILURE;
    }

    // Report any pending error.
    if (tao_any_errors(NULL)) {
        tao_report_error();
        exitcode = EXIT_FAILURE;
    }
    return exitcode;

error:
    exitcode = EXIT_FAILURE;
    goto done;
}
