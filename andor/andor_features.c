//
// Database of known Andor features.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2024, Éric Thiébaut.

#include "tao-andor.h"

const andor_known_feature andor_known_features[] = {
#define L(a,b,c) {#a, L ## #a, ANDOR_FEATURE_##b, c}
    // clang-format off
    L(AccumulateCount            , INTEGER, true ),
    L(AcquisitionStart           , COMMAND, false),
    L(AcquisitionStop            , COMMAND, false),
    L(AlternatingReadoutDirection, BOOLEAN, true ),
    L(AOIBinning                 , ENUM   , false),
    L(AOIHBin                    , INTEGER, false),
    L(AOIHeight                  , INTEGER, false),
    L(AOILayout                  , ENUM   , true ),
    L(AOILeft                    , INTEGER, false),
    L(AOIStride                  , INTEGER, false),
    L(AOITop                     , INTEGER, false),
    L(AOIVBin                    , INTEGER, false),
    L(AOIWidth                   , INTEGER, false),
    L(AuxiliaryOutSource         , ENUM   , true ),
    L(AuxOutSourceTwo            , ENUM   , true ),
    L(Baseline                   , INTEGER, true ),
    L(BitDepth                   , ENUM   , true ),
    L(BufferOverflowEvent        , INTEGER, true ),
    L(BytesPerPixel              , FLOAT  , true ),
    L(CameraAcquiring            , BOOLEAN, false),
    L(CameraModel                , STRING , true ),
    L(CameraName                 , STRING , true ),
    L(CameraPresent              , BOOLEAN, true ),
    L(ControllerID               , STRING , true ),
    L(CoolerPower                , FLOAT  , true ),
    L(CycleMode                  , ENUM   , false),
    L(DeviceCount                , INTEGER, true ),
    L(DeviceVideoIndex           , INTEGER, true ),
    L(ElectronicShutteringMode   , ENUM   , true ),
    L(EventEnable                , BOOLEAN, true ),
    L(EventSelector              , ENUM   , true ),
    L(EventsMissedEvent          , INTEGER, true ),
    L(ExposedPixelHeight         , INTEGER, true ),
    L(ExposureEndEvent           , INTEGER, true ),
    L(ExposureStartEvent         , INTEGER, true ),
    L(ExposureTime               , FLOAT  , false),
    L(ExternalTriggerDelay       , FLOAT  , true ),
    L(FanSpeed                   , ENUM   , true ),
    L(FastAOIFrameRateEnable     , BOOLEAN, true ),
    L(FirmwareVersion            , STRING , true ),
    L(FrameCount                 , INTEGER, true ),
    L(FrameRate                  , FLOAT  , false),
    L(FullAOIControl             , BOOLEAN, true ),
    L(ImageSizeBytes             , INTEGER, false),
    L(InterfaceType              , STRING , true ),
    L(IOInvert                   , BOOLEAN, true ),
    L(IOSelector                 , ENUM   , true ),
    L(LineScanSpeed              , FLOAT  , true ),
    L(LogLevel                   , ENUM   , true ),
    L(LongExposureTransition     , FLOAT  , true ),
    L(LUTIndex                   , INTEGER, true ),
    L(LUTValue                   , INTEGER, true ),
    L(MaxInterfaceTransferRate   , FLOAT  , true ),
    L(MetadataEnable             , BOOLEAN, true ),
    L(MetadataFrame              , BOOLEAN, true ),
    L(MetadataTimestamp          , BOOLEAN, true ),
    L(MicrocodeVersion           , STRING , true ),
    L(MultitrackBinned           , BOOLEAN, true ),
    L(MultitrackCount            , INTEGER, true ),
    L(MultitrackEnd              , INTEGER, true ),
    L(MultitrackSelector         , INTEGER, true ),
    L(MultitrackStart            , INTEGER, true ),
    L(Overlap                    , BOOLEAN, true ),
    L(PixelCorrection            , ENUM   , true ), // FIXME: Boolean on Neo
    L(PixelEncoding              , ENUM   , false),
    L(PixelHeight                , FLOAT  , true ),
    L(PixelReadoutRate           , ENUM   , true ),
    L(PixelWidth                 , FLOAT  , true ),
    L(PreAmpGain                 , ENUM   , true ),
    L(PreAmpGainChannel          , ENUM   , true ),
    L(PreAmpGainControl          , ENUM   , true ),
    L(PreAmpGainSelector         , ENUM   , true ),
    L(ReadoutTime                , FLOAT  , true ),
    L(RollingShutterGlobalClear  , BOOLEAN, true ),
    L(RowNExposureEndEvent       , INTEGER, true ),
    L(RowNExposureStartEvent     , INTEGER, true ),
    L(RowReadTime                , FLOAT  , true ),
    L(ScanSpeedControlEnable     , BOOLEAN, true ),
    L(SensorCooling              , BOOLEAN, true ),
    L(SensorHeight               , INTEGER, false),
    L(SensorReadoutMode          , ENUM   , true ),
    L(SensorTemperature          , FLOAT  , true ),
    L(SensorWidth                , INTEGER, false),
    L(SerialNumber               , STRING , true ),
    L(ShutterMode                , ENUM   , true ),
    L(ShutterOutputMode          , ENUM   , true ),
    L(ShutterTransferTime        , FLOAT  , true ),
    L(SimplePreAmpGainControl    , ENUM   , true ),
    L(SoftwareTrigger            , COMMAND, false),
    L(SoftwareVersion            , STRING , true ),
    L(SpuriousNoiseFilter        , BOOLEAN, true ),
    L(StaticBlemishCorrection    , BOOLEAN, true ),
    L(SynchronousTriggering      , BOOLEAN, true ),
    L(TargetSensorTemperature    , FLOAT  , true ),
    L(TemperatureControl         , ENUM   , true ),
    L(TemperatureStatus          , ENUM   , true ),
    L(TimestampClock             , INTEGER, true ),
    L(TimestampClockFrequency    , INTEGER, true ),
    L(TimestampClockReset        , COMMAND, false),
    L(TriggerMode                , ENUM   , false),
    L(VerticallyCentreAOI        , BOOLEAN, false),
    // clang-format on
#undef L
};

const int andor_known_features_number =
    sizeof(andor_known_features)/sizeof(andor_known_features[0]);

tao_status andor_foreach_known_feature(
    tao_status (*callback)(const andor_known_feature*, void*),
    void *data) // Client data.
{
    for (int i = 0; i < andor_known_features_number; ++i) {
        if (callback(&andor_known_features[i], data) != TAO_OK) {
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}
