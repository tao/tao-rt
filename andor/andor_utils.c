//
// Useful functions for the TAO interface to Andor cameras.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2019-2024, Éric Thiébaut.


#include <math.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>

#include "tao-andor.h"
#include "tao-errors.h"
#include "tao-generic.h"

//-----------------------------------------------------------------------------
// INITIALIZATION

// FIXME: shall we protect this variable by a mutex?
static bool andor_initialized = false;

#define ANDOR_XML_FILE_PATH "/tmp/andor_pat_temp_CIS2051RO_AF.xml"
#define ANDOR_XML_FILE_MODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH)

tao_status andor_initialize_library(void)
{
    if (! andor_initialized) {
        // Before initializing Andor SDK, we set read and write permissions for
        // all users to the XML file created by the SDK in "/tmp" otherwise
        // initialization will crash for others.
        int fd = open(ANDOR_XML_FILE_PATH, O_RDWR | O_CREAT,
                      ANDOR_XML_FILE_MODE);
        if (fd == -1) {
            fprintf(stderr,
                    "Error: XML file \"%s\" cannot be open/created for r/w access.\n"
                    "       You may either remove this file or change its access permissions with:\n\n"
                    "           sudo chmod a+rw \"%s\"\n",
                    ANDOR_XML_FILE_PATH, ANDOR_XML_FILE_PATH);
            tao_store_system_error("open");
            return TAO_ERROR;
        }
        (void)close(fd);

        // Initialize Andor SDK.
        ANDOR_CALL(AT_InitialiseLibrary,(), return TAO_ERROR);
        (void)chmod(ANDOR_XML_FILE_PATH, ANDOR_XML_FILE_MODE);
        andor_initialized = true;
    }
    return TAO_OK;
}

// Finalize the library (if it has been initialized) and try to change
// access permissions for the XML file to avoid crash for others.
tao_status andor_finalize_library(void)
{
    if (andor_initialized) {
        ANDOR_CALL(AT_FinaliseLibrary,(), return TAO_ERROR);
        (void)unlink(ANDOR_XML_FILE_PATH);
        andor_initialized = false;
    }
    return TAO_OK;
}

int andor_get_ndevices(void)
{
    if (!andor_initialized && andor_initialize_library() != TAO_OK) {
        return -1;
    }
    AT_64 ival;
    ANDOR_CALL(AT_GetInt,(AT_HANDLE_SYSTEM, L"DeviceCount", &ival), return -1);
    if (ival < 0 || ival > INT_MAX) {
        tao_store_error(__func__, TAO_BAD_VALUE);
        return -1;
    }
    return ival;
}

#define SDK_VERSION_MAXLEN 32
static char sdk_version[SDK_VERSION_MAXLEN];

const char* andor_get_software_version(
    void)
{
    if (!andor_initialized && andor_initialize_library() != TAO_OK) {
        tao_clear_error(NULL);
    error:
        tao_report_error();
        strcat(sdk_version, "0.0.0");
        return sdk_version;
    }

    if (sdk_version[0] == 0) {
        AT_WC version[SDK_VERSION_MAXLEN];
        ANDOR_CALL(AT_GetString,(AT_HANDLE_SYSTEM, L"SoftwareVersion",
                                 version, SDK_VERSION_MAXLEN), goto error);
        for (int i = 0; i < SDK_VERSION_MAXLEN - 1; ++i) {
            sdk_version[i] = (char)version[i];
            if (sdk_version[i] == 0) {
                break;
            }
        }
        sdk_version[SDK_VERSION_MAXLEN - 1] = 0;
    }
    return sdk_version;
}

//-----------------------------------------------------------------------------
// OPEN/CLOSE DEVICE

tao_status andor_open(
    int devnum,
    AT_H* handle)
{
    if (handle == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    ANDOR_CALL(AT_Open,(devnum, handle), return TAO_ERROR);
    return TAO_OK;
}

tao_status andor_close(
    AT_H handle)
{
    ANDOR_CALL(AT_Close,(handle), return TAO_ERROR);
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// FEATURES

// This function yields whether a feature is implemented for the handle.
tao_status andor_is_implemented(
    AT_H handle,
    const AT_WC *key,
    bool *ptr)
{
    int code;
    if (key == NULL) {
        code = AT_ERR_NULL_FEATURE;
    } else if (ptr == NULL) {
        code = AT_ERR_NULL_VALUE;
    } else {
        AT_BOOL val;
        code = AT_IsImplemented(handle, key, &val);
        if (code == AT_SUCCESS) {
            *ptr = (val != AT_FALSE);
            return TAO_OK;
        }
    }
    andor_error("AT_IsImplemented", code);
    return TAO_ERROR;
}

// A feature is read-only if it can never be modified (whatever the value of
// other features).  It is an error to call this function on a feature that is
// not implemented.
tao_status andor_is_readonly(
    AT_H handle,
    const AT_WC *key,
    bool *ptr)
{
    int code;
    if (key == NULL) {
        code = AT_ERR_NULL_FEATURE;
    } else if (ptr == NULL) {
        code = AT_ERR_NULL_VALUE;
    } else {
        AT_BOOL val;
        code = AT_IsReadOnly(handle, key, &val);
        if (code == AT_SUCCESS) {
            *ptr = (val != AT_FALSE);
            return TAO_OK;
        }
    }
    andor_error("AT_IsReadOnly", code);
    return TAO_ERROR;
}

// A feature may be temporarily not readable because of the value of other
// features.  The result is false if the feature is not implemented.
tao_status andor_is_readable(
    AT_H handle,
    const AT_WC *key,
    bool *ptr)
{
    int code;
    if (key == NULL) {
        code = AT_ERR_NULL_FEATURE;
    } else if (ptr == NULL) {
        code = AT_ERR_NULL_VALUE;
    } else {
        AT_BOOL val;
        code = AT_IsReadable(handle, key, &val);
        if (code == AT_SUCCESS) {
            *ptr = (val != AT_FALSE);
            return TAO_OK;
        }
    }
    if (code == AT_ERR_NOTIMPLEMENTED) {
        *ptr = false;
        return TAO_OK;
    }
    andor_error("AT_IsReadable", code);
    return TAO_ERROR;
}

// A feature may be temporarily not writable because of the value of other
// features.  The result is false if the feature is not implemented.
tao_status andor_is_writable(
    AT_H handle,
    const AT_WC *key,
    bool *ptr)
{
    int code;
    if (key == NULL) {
        code = AT_ERR_NULL_FEATURE;
    } else if (ptr == NULL) {
        code = AT_ERR_NULL_VALUE;
    } else {
        AT_BOOL val;
        code = AT_IsWritable(handle, key, &val);
        if (code == AT_SUCCESS) {
            *ptr = (val != AT_FALSE);
            return TAO_OK;
        }
    }
    if (code == AT_ERR_NOTIMPLEMENTED) {
        *ptr = false;
        return TAO_OK;
    }
    andor_error("AT_IsWritable", code);
    return TAO_ERROR;
}

static inline void wstr_copy(
    wchar_t* dst,
    const wchar_t* src,
    int len,
    int maxlen)
{
    for (int i = 0; i < len; ++i) {
        dst[i] = src[i];
    }
    for (int i = len; i < maxlen; ++i) {
        dst[i] = '\0';
    }
}

int andor_known_feature_try_find(
    const char* key)
{
    if (key == NULL){
        return -2;
    }
    char c = key[0];
    if (c != 0) {
        for (int idx = 0; idx < andor_known_features_number; ++idx) {
            if (andor_known_features[idx].key[0] == c &&
                strcmp(andor_known_features[idx].key, key) == 0) {
                return idx;
            }
        }
    }
    return -1;
}

// Attempt to instantiate a camera feature.  At least members `implemented` and
// `readonly` are instantiated.  If feature is currently readable, its type,
// current value, and limits (for integer, float, and enumerated features) are
// retrieved; otherwise its type is set to `ANDOR_FEATURE_UNKNOWN`.
tao_status andor_instantiate_feature(
    andor_feature*             rec, // Feature record.
    AT_H                    handle, // Camera/system handle.
    const andor_known_feature* def, // Index of feature in table of known features.
    int                       type) // Assumed feature type or `ANDOR_FEATURE_UNKNOWN`.
{
    // Minimal check.
    if (rec == NULL || def == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }

    // Small buffer for a wide string.
    wchar_t buf[ANDOR_STRING_MAXLEN];

    // Clear feature record and set minimal information.
    memset(rec, 0, sizeof(*rec));
    rec->key = def->key;
    rec->wkey = def->wkey;
    rec->type = ANDOR_FEATURE_UNKNOWN;
    rec->implemented = false;
    rec->readonly = false;
    rec->named = false;
    rec->index = -1;

    //  Check whether feature is implemented.
    if (andor_is_implemented(handle, rec->wkey, &rec->implemented) != TAO_OK) {
        return TAO_ERROR;
    }
    if (!rec->implemented) {
        // Skip the remaining if not implemented.
        return TAO_OK;
    }

    //  Check whether feature may be modified.
    if (andor_is_readonly(handle, rec->wkey, &rec->readonly) != TAO_OK) {
        return TAO_ERROR;
    }

    //  Check whether feature is currently readable.
    bool readable;
    if (andor_is_readable(handle, rec->wkey, &readable) != TAO_OK) {
        return TAO_ERROR;
    }
    if (!readable) {
        // Return if not yet readble.  Type can be determined later.
        return TAO_OK;
    }

    // Feature may or must have a boolean value?
    if (type == ANDOR_FEATURE_UNKNOWN || type == ANDOR_FEATURE_BOOLEAN) {
        AT_BOOL val;
        int code = AT_GetBool(handle, rec->wkey, &val);
        if (code == AT_SUCCESS) {
            rec->type = ANDOR_FEATURE_BOOLEAN;
            rec->val.b = (val != AT_FALSE);
            return TAO_OK;
        } else if (type != ANDOR_FEATURE_UNKNOWN
                   || code != AT_ERR_NOTIMPLEMENTED) {
            andor_error("AT_GetBool", code);
            return TAO_ERROR;
        }
    }

    // Feature may or must have an integer value?
    if (type == ANDOR_FEATURE_UNKNOWN || type == ANDOR_FEATURE_INTEGER) {
        AT_64 val;
        int code = AT_GetInt(handle, rec->wkey, &val);
        if (code == AT_SUCCESS) {
            rec->type = ANDOR_FEATURE_INTEGER;
            rec->val.i = val;
            ANDOR_CALL(AT_GetIntMin, (handle, rec->wkey, &val), return TAO_ERROR);
            rec->min.i = val;
            ANDOR_CALL(AT_GetIntMax, (handle, rec->wkey, &val), return TAO_ERROR);
            rec->max.i = val;
            return TAO_OK;
        } else if (type != ANDOR_FEATURE_UNKNOWN
                   || code != AT_ERR_NOTIMPLEMENTED) {
            andor_error("AT_GetInt", code);
            return TAO_ERROR;
        }
    }

    // Feature may or must have a floating-point value?
    if (type == ANDOR_FEATURE_UNKNOWN || type == ANDOR_FEATURE_FLOAT) {
        double val;
        int code = AT_GetFloat(handle, rec->wkey, &val);
        if (code == AT_SUCCESS) {
            rec->type = ANDOR_FEATURE_FLOAT;
            rec->val.f = val;
            ANDOR_CALL(AT_GetFloatMin, (handle, rec->wkey, &val), return TAO_ERROR);
            rec->min.f = val;
            ANDOR_CALL(AT_GetFloatMax, (handle, rec->wkey, &val), return TAO_ERROR);
            rec->max.f = val;
            return TAO_OK;
        } else if (type != ANDOR_FEATURE_UNKNOWN
                   || code != AT_ERR_NOTIMPLEMENTED) {
            andor_error("AT_GetFloat", code);
            return TAO_ERROR;
        }
    }

    // Feature may or must have a string value?
    if (type == ANDOR_FEATURE_UNKNOWN || type == ANDOR_FEATURE_STRING) {
        int code = AT_GetString(
            handle, rec->wkey, buf, TAO_STATIC_ARRAY_LENGTH(buf));
        if (code == AT_SUCCESS) {
            rec->type = ANDOR_FEATURE_STRING;
            long len = wcslen(buf);
            long maxlen = ANDOR_FEATURE_VALUE_MAXLEN;
            if (len >= maxlen) {
                fprintf(stderr, "Warning: ANDOR_FEATURE_VALUE_MAXLEN = %ld is "
                        "too small, should be at least %ld (%s, line %d)\n",
                        maxlen, len + 1, __FILE__, __LINE__);
                len = maxlen - 1;
            }
            wstr_copy(rec->val.s, buf, len, maxlen);
            return TAO_OK;
        } else if (type != ANDOR_FEATURE_UNKNOWN
                   || code != AT_ERR_NOTIMPLEMENTED) {
            andor_error("AT_GetString", code);
            return TAO_ERROR;
        }
    }

    // Feature may or must have an enumeration value?
    if (type == ANDOR_FEATURE_UNKNOWN || type == ANDOR_FEATURE_ENUM) {
        int idx, cnt;
        int code = AT_GetEnumIndex(handle, rec->wkey, &idx);
        if (code == AT_SUCCESS) {
            rec->type = ANDOR_FEATURE_ENUM;
            rec->index = idx;
            ANDOR_CALL(AT_GetEnumStringByIndex,
                       (handle, rec->wkey, idx, buf, TAO_STATIC_ARRAY_LENGTH(buf)),
                       return TAO_ERROR);
            long len = wcslen(buf);
            long maxlen = ANDOR_FEATURE_VALUE_MAXLEN;
            if (len >= maxlen) {
                fprintf(stderr, "Warning: ANDOR_FEATURE_VALUE_MAXLEN = %ld is "
                        "too small, should be at least %ld (%s, line %d)\n",
                        maxlen, len + 1, __FILE__, __LINE__);
                len = maxlen - 1;
            }
            wstr_copy(rec->val.s, buf, len, maxlen);
            ANDOR_CALL(AT_GetEnumCount, (handle, rec->wkey, &cnt), return TAO_ERROR);
            rec->min.i = 0;
            rec->max.i = cnt - 1;
            return TAO_OK;
        } else if (type != ANDOR_FEATURE_UNKNOWN
                   || code != AT_ERR_NOTIMPLEMENTED) {
            andor_error("AT_GetEnumIndex", code);
            return TAO_ERROR;
        }
    }

    // Feature may or must be a command?
    if (type == ANDOR_FEATURE_UNKNOWN || type == ANDOR_FEATURE_COMMAND) {
        rec->type = ANDOR_FEATURE_COMMAND;
        return TAO_OK;
    }

    // Assumed type must be wrong.
    tao_store_error(__func__, TAO_BAD_TYPE);
    return TAO_ERROR;
}

static struct {
    const char*    name;
    const wchar_t* wname;
    tao_encoding value;
} encoding_definitions[] = {
#define ENCDEF(val, str) { str, L##str, val }
    ENCDEF(ANDOR_ENCODING_MONO8, "Mono8"),
    ENCDEF(ANDOR_ENCODING_MONO12, "Mono12"),
    ENCDEF(ANDOR_ENCODING_MONO12CODED, "Mono12Coded"),
    ENCDEF(ANDOR_ENCODING_MONO12CODEDPACKED, "Mono12CodedPacked"),
    ENCDEF(ANDOR_ENCODING_MONO12PACKED, "Mono12Packed"),
    ENCDEF(ANDOR_ENCODING_MONO16, "Mono16"),
    ENCDEF(ANDOR_ENCODING_MONO22PACKEDPARALLEL, "Mono22PackedParallel"),
    ENCDEF(ANDOR_ENCODING_MONO22PARALLEL, "Mono22Parallel"),
    ENCDEF(ANDOR_ENCODING_MONO32, "Mono32"),
    ENCDEF(ANDOR_ENCODING_RGB8PACKED, "RGB8Packed"),
#undef ENCDEF
    {NULL, NULL, ANDOR_ENCODING_UNKNOWN}
};

const char * andor_name_of_encoding(
    tao_encoding enc)
{
    for (int i = 0; encoding_definitions[i].name != NULL; ++i) {
        if (encoding_definitions[i].value == enc) {
            return encoding_definitions[i].name;
        }
    }
    return "Unknown";
}

const wchar_t * andor_wide_name_of_encoding(
    tao_encoding enc)
{
    for (int i = 0; encoding_definitions[i].wname != NULL; ++i) {
        if (encoding_definitions[i].value == enc) {
            return encoding_definitions[i].wname;
        }
    }
    return L"Unknown";
}

tao_encoding andor_name_to_encoding(
    const char* name)
{
    for (int i = 0; encoding_definitions[i].name != NULL; ++i) {
        if (strcasecmp(encoding_definitions[i].name, name) == 0) {
            return encoding_definitions[i].value;
        }
    }
    return ANDOR_ENCODING_UNKNOWN;
}

tao_encoding andor_wide_name_to_encoding(
    const wchar_t* name)
{
    for (int i = 0; encoding_definitions[i].wname != NULL; ++i) {
        if (wcscasecmp(encoding_definitions[i].wname, name) == 0) {
            return encoding_definitions[i].value;
        }
    }
    return ANDOR_ENCODING_UNKNOWN;
}
