// tao-andor.h --
//
// Definitions for Andor cameras in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2021, Éric Thiébaut.

#ifndef TAO_ANDOR_H_
#define TAO_ANDOR_H_ 1

#include <wchar.h>
#include <limits.h>

#include <atcore.h>

#include <tao-cameras.h>

TAO_BEGIN_DECLS

/**
 * @defgroup Andor   API for using Andor SDK in TAO
 *
 * @brief API for using Andor SDK in TAO.
 *
 * @{
 */

//-----------------------------------------------------------------------------
// ERROR MANAGEMENT

/**
 * Store Andor SDK error.
 *
 * This functions stores Andor error information as the last TAO error of the
 * calling thread.
 *
 * @param func   The name of the function in Andor SDK that returned an error.
 * @param code   The code returned by the function.
 */
extern void andor_error(
    const char* func,
    int code);

/**
 * Retrieve message associated Andor SDK error code.
 *
 * @param code   The code returned by a function of the Andor SDK function.
 */
extern const char* andor_error_reason(
    int code);

/**
 * Retrieve the human readable name of Andor SDK error code.
 *
 * @param code   The code returned by a function of the Andor SDK function.
 */
extern const char* andor_error_name(
    int code);

// Macro to report errors for simple calls to a function of the Andor SDK.
#define ANDOR_CALL(func,args,on_error)          \
    do {                                        \
        int code_ = func args;                  \
        if (code_ != AT_SUCCESS) {              \
            andor_error(#func, code_);          \
            on_error;                           \
        }                                       \
    } while (0)

//-----------------------------------------------------------------------------
// INITIALIZE/FINALIZE LIBRARY

/**
 * Initialize Andor SDK.
 *
 * This function initializes the Andor SDK.  Calling this function more than
 * once is harmless.  The caller should eventually call
 * andor_finalize_library().
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status andor_initialize_library(
    void);

/**
 * Finalize Andor SDK.
 *
 * This function finalizes the Andor SDK.  Calling this function only does
 * something if andor_initialize_library() has been called before.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status andor_finalize_library(
    void);

/**
 * Get the number of available Andor devices.
 *
 * This function yields the number of available Andor devices.  If the Andor
 * SDK has not been initialized, andor_initialize_library() is
 * automatically called.
 *
 * @return The number of available devices, `-1` on error.
 */
extern int andor_get_ndevices(
    void);

/**
 * Get the version of the Andor SDK.
 *
 * This function never fails, in case of errors, the returned version number is
 * `"0.0.0"` and error messages are printed (nothing is left as the last TAO
 * error of the calling thread).
 *
 * @return A string.
 */
const char* andor_get_software_version(
    void);

/**
 * Open an Andor camera.
 *
 * @param devnum   Andor device number.
 *
 * @param handle   Pointer to camera handle.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status andor_open(
    int devnum,
    AT_H* handle);

/**
 * Close an Andor camera.
 *
 * @param handle   Camera handle.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status andor_close(
    AT_H handle);

//-----------------------------------------------------------------------------
// FEATURES

/**
 * Possible types for a feature of an Andor camera.
 */
typedef enum andor_feature_type_ {
    ANDOR_FEATURE_UNKNOWN = 0,
    ANDOR_FEATURE_BOOLEAN,
    ANDOR_FEATURE_INTEGER,
    ANDOR_FEATURE_FLOAT,
    ANDOR_FEATURE_ENUM,
    ANDOR_FEATURE_STRING,
    ANDOR_FEATURE_COMMAND
} andor_feature_type;

/**
 * Description of a known Andor feature.
 */
typedef struct andor_known_feature_ {
    const char*     key;///< Feature name in Andor SDK.
    const wchar_t* wkey;///< Idem with wide characters.
    int            type;///< Type of feature.
    bool          named;///< Named parameter in TAO?
} andor_known_feature;


/**
 * Array of known Andor features.
 */
extern const andor_known_feature andor_known_features[];

/**
 * Number of known Andor features.
 */
extern const int andor_known_features_number;


/**
 * Retrrieve the index of a feature in the table of known Andor features.
 *
 * @param key       Feature name to find.
 *
 * @return A nonnegative index on success, `-1` if feature is not found, and
 *         `-2` if arguments are wrong. This function leaves the last thread
 *         error unchanged.
 */
extern int andor_known_feature_try_find(
    const char* key);

/**
 * Execute a function for each known Andor feature.
 *
 * This function executes a callback for each known Andor feature.  The
 * callback is repeatedly called with a structure describing a given known
 * feature until the callback returns another status than @ref TAO_OK or until
 * the list of known features is exhausted.
 *
 * @param callback   Function to be called for each known Andor feature.
 *
 * @param data       Any needed data for the callback.
 *
 * @return @ref TAO_OK if `callback` returns @ref TAO_OK for all features;
 *         @ref TAO_ERROR otherwise.
 */
extern tao_status andor_foreach_known_feature(
    tao_status (*callback)(const andor_known_feature*, void *),
    void *data);

/**
 * Maximum length of a feature string/enumeration value.
 *
 * This macro gives the maximum number of characters (counting the final null)
 * of the value of an Andor string or enumeration feature.
 */
#define ANDOR_FEATURE_VALUE_MAXLEN 64

/**
 * Maximum length of a small buffer to store a feature name or value.
 *
 * This macro gives the maximum number of characters (counting the final null)
 * of a small buffer to retrieve an Andor string.
 */
#define ANDOR_STRING_MAXLEN 80

/**
 * Andor feature record.
 *
 * This structure is used to record all available information about an Andor
 * feature.
 */
typedef struct andor_feature_ {
    const char* key;
    const wchar_t* wkey;
    int type;
    bool implemented;
    bool readonly;
    bool named;
    int index; // ≥ 0 for enumerated features, -1 for others
    union {
        bool b;
        int64_t i;
        double f;
        wchar_t s[ANDOR_FEATURE_VALUE_MAXLEN];
    } val;
    union {
        int64_t i;
        double f;
    } min;
    union {
        int64_t i;
        double f;
    } max;
} andor_feature;

/**
 * Retrieve all possible information about an Andor feature.
 *
 * This function retrieves all possible information about a given Andor
 * feature. At least members `rec->key`, `rec->implemented`, and
 * `rec->readonly` are instantiated. If the feature is currently readable, its
 * type, current value, and limits (for integer, float, and enumerated
 * features) are retrieved; otherwise its type is set to
 * `ANDOR_FEATURE_UNKNOWN`.
 *
 * @param rec     Address where to store the feature information.
 *
 * @param handle  Handle to device owning the feature.
 *
 * @param def     Known feature definition.
 *
 * @param type    Assumed type of the feature. Can be `ANDOR_FEATURE_UNKNOWN`
 *                to attempt to automatically guess the type; otherwise, if the
 *                feature is readable, its type must be the assumed one.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status andor_instantiate_feature(
    andor_feature *rec,
    AT_H handle,
    const andor_known_feature* def,
    int type);

extern tao_status andor_is_implemented(
    AT_H handle,
    const AT_WC *key,
    bool *ptr);

extern tao_status andor_is_readonly(
    AT_H handle,
    const AT_WC *key,
    bool *ptr);

extern tao_status andor_is_readable(
    AT_H handle,
    const AT_WC *key,
    bool *ptr);

extern tao_status andor_is_writable(
    AT_H handle,
    const AT_WC *key,
    bool *ptr);

//-----------------------------------------------------------------------------

/**
 * Open an Andor camera.
 *
 * This function opens an Andor camera and returns a high level TAO camera
 * instance.
 *
 * @param dev    Device number to which the camera is connected.
 *
 * @return A valid address of a TAO camera on success, `NULL` on error.
 */
extern tao_camera* andor_open_camera(
    int dev);

/**
 * Print the list of supported encodings by an Andor camera.
 *
 * @param out   The output file stream where to print.
 * @param cam   The address of an Andor camera.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status andor_print_supported_encodings(
    FILE* out,
    const tao_camera* cam);

/**
 * The various pixel encodings.
 */
#define ANDOR_ENCODING_UNKNOWN              TAO_ENCODING_UNKNOWN
#define ANDOR_ENCODING_MONO8                TAO_ENCODING_ANDOR_MONO8
#define ANDOR_ENCODING_MONO12               TAO_ENCODING_ANDOR_MONO12
#define ANDOR_ENCODING_MONO12CODED          TAO_ENCODING_ANDOR_MONO12CODED
#define ANDOR_ENCODING_MONO12CODEDPACKED    TAO_ENCODING_ANDOR_MONO12CODEDPACKED
#define ANDOR_ENCODING_MONO12PACKED         TAO_ENCODING_ANDOR_MONO12PACKED
#define ANDOR_ENCODING_MONO16               TAO_ENCODING_ANDOR_MONO16
#define ANDOR_ENCODING_MONO22PACKEDPARALLEL TAO_ENCODING_ANDOR_MONO22PACKEDPARALLEL
#define ANDOR_ENCODING_MONO22PARALLEL       TAO_ENCODING_ANDOR_MONO22PARALLEL
#define ANDOR_ENCODING_MONO32               TAO_ENCODING_ANDOR_MONO32
#define ANDOR_ENCODING_RGB8PACKED           TAO_ENCODING_ANDOR_RGB8PACKED
#define ANDOR_ENCODING_FLOAT                TAO_ENCODING_FLOAT(CHAR_BIT*sizeof(float))
#define ANDOR_ENCODING_DOUBLE               TAO_ENCODING_FLOAT(CHAR_BIT*sizeof(double))

/**
 * The maximum number of pixel encodings.
 */
#define ANDOR_MAX_ENCODINGS 16 // should be sufficient...

/**
 * Get the name of a given pixel encoding.
 *
 * @param encoding      The pixel encoding.
 *
 * @return The name of the encoding, `"Unknown"` if unknown.
 */
extern const char* andor_name_of_encoding(
    tao_encoding enc);

/**
 * Get the name of a given pixel encoding.
 *
 * @param encoding      The pixel encoding.
 *
 * @return The name of the encoding as a wide string (so that it can be
 * directly feed in Andor SDK functions), `L"Unknown"` if unknown.
 */
extern const wchar_t* andor_wide_name_of_encoding(
    tao_encoding enc);

/**
 * Get the identifier of a given pixel encoding name.
 *
 * @param name      The name of the pixel encoding.
 *
 * @return A pixel encoding identifier, `ANDOR_ENCODING_UNKNOWN` if unknown.
 */
extern tao_encoding andor_name_to_encoding(
    const char* name);

/**
 * Get the identifier of a given pixel encoding name.
 *
 * @param name      The name of the pixel encoding.
 *
 * @return A pixel encoding identifier, `ANDOR_ENCODING_UNKNOWN` if unknown.
 */
extern tao_encoding andor_wide_name_to_encoding(
    const wchar_t* name);

/**
 * @brief Andor camera model identifier.
 */
typedef enum andor_camera_model_ {
    ANDOR_MODEL_UNKNOWN,
    ANDOR_MODEL_APOGEE,
    ANDOR_MODEL_BALOR,
    ANDOR_MODEL_ISTAR,
    ANDOR_MODEL_MARANA,
    ANDOR_MODEL_NEO,
    ANDOR_MODEL_SIMCAM,
    ANDOR_MODEL_SONA,
    ANDOR_MODEL_SYSTEM,
    ANDOR_MODEL_ZYLA
} andor_camera_model;

/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_ANDOR_H_
