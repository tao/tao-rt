//
// Implement TAO interface to Andor cameras.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2024, Éric Thiébaut.

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "tao-andor.h"
#include "tao-errors.h"
#include "tao-generic.h"
#include "tao-cameras-private.h"

#if 0
#  define DEBUG(...) fprintf(stderr, __VA_ARGS__)
#else
#  define DEBUG(...)
#endif

// Image buffers must be aligned on a 8-byte boundary.
#define ALIGNED_BUFFER_SIZE(size) ((size) + 7)
#define ALIGNED_BUFFER_ADDRESS(ptr) \
    ((AT_U8*)(((size_t)(ptr) + (size_t)7) & ~(size_t)7))

/**
 * Structure to store acquisition buffer for an Andor camera.
 */
typedef struct andor_buffer_ {
    void*  addr; // address as returned by malloc, not aligned
    int    size; // size of aligned part
} andor_buffer;

/**
 * Andor camera.
 *
 * The Andor camera class extends the TAO camera class and the `base` member
 * must come first. The other members are specific to all Andor cameras.
 * Getting the type definition of the handle member is the only reason to
 * include `<atcore.h>` here. The array of acquisition buffers is to track
 * malloc'ed buffers.
 */
typedef struct andor_camera_ {
    tao_camera            base;///< Unified TAO camera.
    AT_H                handle;///< Camera handle.
    tao_encoding encs[ANDOR_MAX_ENCODINGS];///< Supported pixel encodings.
    int                  nencs;///< Number of supported pixel encodings.
    int                  nbufs;///< Number of acquisition buffers.
    andor_buffer         *bufs;///< Array of acquisition buffers.
    tao_acquisition_buffer buf;///< Current acquisition buffer. Except base
                               ///  address, all acquisition buffers have the
                               ///  same settings which are instantiated when
                               ///  acquisition starts.
    long                nattrs;///< Number of implemented features. This is the
                               ///  same as the number of named attributes.
    andor_feature  features[0];///< Implemented features, flexible array.
} andor_camera;

//-----------------------------------------------------------------------------
// TABLE OF CAMERA OPERATIONS

// Early declarations of all virtual methods to initialize table of operations.

static tao_status on_initialize(
    tao_camera* cam,
    void* ptr);

static tao_status on_finalize(
    tao_camera* cam);

static tao_status on_reset(
    tao_camera* cam);

static tao_status on_update_config(
    tao_camera* cam);

static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_start(
    tao_camera* cam);

static tao_status on_stop(
    tao_camera* cam);

static tao_status on_wait_buffer(
    tao_camera* cam,
    tao_acquisition_buffer* buf,
    double secs,
    int drop);

// Table of operations of all Andor cameras.  Its address can be used to
// assert that a given TAO camera belongs to this family.
static const tao_camera_ops ops = {
    .name           = "Andor",
    .initialize     = on_initialize,
    .finalize       = on_finalize,
    .reset          = on_reset,
    .update_config  = on_update_config,
    .check_config   = on_check_config,
    .set_config     = on_set_config,
    .start          = on_start,
    .stop           = on_stop,
    .wait_buffer    = on_wait_buffer,
};

tao_camera* andor_open_camera(
    int dev)
{
    // Check device number after initializing the library if needed.
    int ndevices = andor_get_ndevices();
    if (ndevices < 0) {
        return NULL;
    }
    if (dev < 0 || dev >= ndevices) {
        tao_store_error(__func__, TAO_BAD_DEVICE);
        return NULL;
    }

    // Allocate camera instance with the device number provided as contextual
    // data. The remaining initialization will done by the `on_initialize`
    // virtual method.
    size_t size = offsetof(andor_camera, features)
        + andor_known_features_number*sizeof(andor_feature);
    return tao_camera_create(&ops, &dev, size);
}

tao_status andor_print_supported_encodings(
    FILE* out,
    const tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (cam->ops != &ops) {
        tao_store_error(__func__, TAO_BAD_DEVICE);
        return TAO_ERROR;
    }
    andor_camera* acam = (andor_camera*)cam;
    if (fprintf(out, "Supported pixel encodings: [") < 0) {
        goto fprintf_error;
    }
    for (int k = 0; k < acam->nencs; ++k) {
        if (k > 0) {
            if (fputs(", ", out) < 0) {
                goto fputs_error;
            }
        }
        if (fprintf(out, "%s",
                    andor_name_of_encoding(acam->encs[k])) < 0) {
            goto fprintf_error;
        }
    }
    if (fputs("]\n", out) < 0) {
        goto fputs_error;
    }
    return TAO_OK;
fprintf_error:
    tao_store_system_error("fprintf");
    return TAO_ERROR;
fputs_error:
    tao_store_system_error("fputs");
    return TAO_ERROR;
}

//-----------------------------------------------------------------------------
// Private functions and macros to query a named attribute while preserving the
// `const` property of pointers.

static inline const tao_attr* andor_camera_unsafe_get_attr(
    const andor_camera* acam,
    long idx)
{
    return tao_attr_table_unsafe_get_entry(&acam->base.config.attr_table, idx);
}

#define colon(x, ...) x: __VA_ARGS__

#define get_attr_(T, obj, idx)                                          \
    colon(const T*, (const tao_attr*)T##_unsafe_get_attr((const T*)(obj), idx)), \
    colon(      T*, (      tao_attr*)T##_unsafe_get_attr((const T*)(obj), idx))

// `get_attr(obj, idx)` yields the named attribute at index `idx` for object
// `obj`. Arguments are not checked.
#define get_attr(obj, idx)                              \
    _Generic((obj),                                     \
             get_attr_(andor_camera,      obj, idx),    \
             get_attr_(tao_camera,        obj, idx),    \
             get_attr_(tao_camera_config, obj, idx))

// `get_key(attr)` yields the key of the named attribute `attr` with the
// same const or non-const property as `attr`.
#define get_key(attr)                                                   \
    _Generic((attr),                                                    \
             const tao_attr*:                tao_attr_get_key(attr),    \
             /***/ tao_attr*: (tao_attr_key*)tao_attr_get_key(attr))

//-----------------------------------------------------------------------------
// MANAGE ANDOR DEVICE INSTANCE

static tao_status disable_vertical_centering(
    andor_camera* acam);

static tao_status get_width(
    andor_camera* acam,
    long* valptr);

static tao_status get_height(
    andor_camera* acam,
    long* valptr);

static tao_status get_pixelencoding(
    andor_camera* acam,
    tao_encoding* valptr);

static tao_status set_pixelencoding(
    andor_camera* acam,
    tao_encoding val);

static tao_status alloc_buffers(
    andor_camera* acam,
    int nbufs,
    int size);

static void free_buffers(
    andor_camera* acam);

static tao_status update_pixel_encodings(
    andor_camera* acam);

static char* encoding_sprintf(
    char* str,
    tao_encoding enc);

static tao_status initialize_named_attributes(
    andor_camera* acam)
{
    // First, count the number of implemented named attributes and retrieve
    // their characteristics and their current values.
    acam->nattrs = 0; // Number of named attributes for this camera.
    for (int i = 0; i < andor_known_features_number; ++i) {
        // Get i-th known feature.
        const andor_known_feature* known_feature = &andor_known_features[i];

        // Skip feature if it is not a named attribute or a command.
        if (! known_feature->named || known_feature->type == ANDOR_FEATURE_COMMAND) {
            continue;
        }

        // Retrieve as much as possible information about the feature. We know
        // that the `acam->features` array is large enough for all known
        // features.
        andor_feature* feat = &acam->features[acam->nattrs];
        if (andor_instantiate_feature(feat, acam->handle, known_feature,
                                      ANDOR_FEATURE_UNKNOWN) != TAO_OK) {
            fprintf(stderr, "(WARNING) Failed to instantiate Andor camera feature \"%s\"\n",
                    known_feature->key);
            feat->implemented = false;
        }
        if (feat->implemented) {
            // One more feature is implemented for this camera.
            ++acam->nattrs;
        } else {
            // Skip feature not implemented for this camera.
            memset(feat, 0, sizeof(andor_feature));
        }
    }
    if (acam->nattrs < 1) {
        // No named attributes for this camera.
        return TAO_OK;
    }

    // Second, build a table of named attribute definitions to instantiate the
    // camera table of named attributes.
    size_t size = acam->nattrs*sizeof(tao_attr_descr);
    tao_attr_descr* defs = malloc(size);
    if (defs == NULL) {
        tao_store_system_error("malloc");
        return TAO_ERROR;
    }
    memset(defs, 0, size);
    tao_status status = TAO_OK;
    for (int i = 0; i < acam->nattrs; ++i) {
        // Determine attribute bits and value size.
        unsigned flags = 0;
        tao_attr_type type;
        size = 0;
        switch (acam->features[i].type) {
        case ANDOR_FEATURE_BOOLEAN:
            type = TAO_ATTR_BOOLEAN;
            break;
        case ANDOR_FEATURE_INTEGER:
            type = TAO_ATTR_INTEGER;
            break;
        case ANDOR_FEATURE_FLOAT:
            type = TAO_ATTR_FLOAT;
            break;
        case ANDOR_FEATURE_ENUM:
        case ANDOR_FEATURE_STRING:
            type = TAO_ATTR_STRING;
            size = ANDOR_FEATURE_VALUE_MAXLEN;
            break;
        default:
            tao_store_error(__func__, TAO_BAD_TYPE);
            status = TAO_ERROR;
        }
        if (status != TAO_OK) {
            break;
        }
        if (acam->features[i].readonly) {
            flags |= TAO_ATTR_READABLE;
        } else {
            flags |= TAO_ATTR_READABLE | TAO_ATTR_WRITABLE;
        }

        // Instantiate attribute description.
        defs[i].key = acam->features[i].key;
        defs[i].type = type;
        defs[i].flags = flags;
        defs[i].size = size;
    }
    if (status == TAO_OK) {
        status = tao_attr_table_instantiate(
            &acam->base.config.attr_table, defs, acam->nattrs);
    }
    free(defs);
    if (status != TAO_OK) {
        return status;
    }

    // Third. Instantiate the values of the named attributes into the generic
    // camera configuration structure.
    char buf[ANDOR_FEATURE_VALUE_MAXLEN];
    for (int i = 0; i < acam->nattrs && status == TAO_OK; ++i) {
        tao_attr* attr = get_attr(acam, i);
        switch (acam->features[i].type) {
        case ANDOR_FEATURE_BOOLEAN:
            status = tao_attr_set_boolean_value(attr, acam->features[i].val.b, true);
            break;
        case ANDOR_FEATURE_INTEGER:
            status = tao_attr_set_integer_value(attr, acam->features[i].val.i, true);
            break;
        case ANDOR_FEATURE_FLOAT:
            status = tao_attr_set_float_value(attr, acam->features[i].val.f, true);
            break;
        case ANDOR_FEATURE_ENUM:
        case ANDOR_FEATURE_STRING:
            status = tao_transcode_unicode_string_to_utf8(acam->features[i].val.s, buf, sizeof(buf));
            if (status == TAO_OK) {
                status = tao_attr_set_string_value(attr, buf, true);
            }
            break;
        default:
            tao_store_error(__func__, TAO_BAD_TYPE); // FIXME: TAO_BAD_VALUE_TYPE
            status = TAO_ERROR;
        }
    }
    return status;
}

// In case of errors during initialization, we simply call the `on_finalize`
// virtual method.
static tao_status on_initialize(
    tao_camera* cam,
    void* ptr)
{
    // Fetch Andor device structure and device number.
    assert(cam->ops == &ops);
    andor_camera* acam = (andor_camera*)cam;
    long devnum = *(int*)ptr;
    int code;

    // Open camera handle.
    if (andor_open(devnum, &acam->handle) != TAO_OK) {
        acam->handle = AT_HANDLE_SYSTEM; // to not close
        return TAO_ERROR;
    }

    // Instantiate sensor size.
    AT_64 sensorwidth;
    code = AT_GetInt(acam->handle, L"SensorWidth", &sensorwidth);
    if (code != AT_SUCCESS) {
        andor_error("AT_GetInt(SensorWidth)", code);
        goto error;
    }
    tao_forced_store(&cam->config.sensorwidth, sensorwidth);
    AT_64 sensorheight;
    code = AT_GetInt(acam->handle, L"SensorHeight", &sensorheight);
    if (code != AT_SUCCESS) {
        andor_error("AT_GetInt(SensorHeight)", code);
        goto error;
    }
    tao_forced_store(&cam->config.sensorheight, sensorheight);

    // Get list of supported pixel encodings. (Must be done before updating the
    // configuration.)
    if (update_pixel_encodings(acam) != TAO_OK) {
        goto error;
    }

    // Disable centering of region of interest.
    if (disable_vertical_centering(acam) != TAO_OK) {
        goto error;
    }

    // Create list of named attributes for this camera.
    if (initialize_named_attributes(acam) != TAO_OK) {
        goto error;
    }

    // Update configuration using the callback.
    if (on_update_config(cam) != TAO_OK) {
        goto error;
    }

    // Everything went fine...
    return TAO_OK;

    // We branch here in case of errors.
error:
    on_finalize(cam);
    return TAO_ERROR;
}

// Finalize internal resources.  Manage to mark destroyed data as such to avoid
// freeing more than once.  This is to allow calling this method several times
// with no damages.  Acquisitions buffers are destroyed by the caller after
// calling this virtual method.
static tao_status on_finalize(
    tao_camera* cam)
{
    tao_status status = TAO_OK;
    assert(cam != NULL);
    assert(cam->ops == &ops);
    andor_camera* acam = (andor_camera*)cam;
    if (acam->handle != AT_HANDLE_SYSTEM) {
        free_buffers(acam);
        if (andor_close(acam->handle) != TAO_OK) {
            status = TAO_ERROR;
        }
        acam->handle = AT_HANDLE_SYSTEM;
    }
    return status;
}

static tao_status on_reset(
    tao_camera* cam)
{
    assert(cam != NULL);
    assert(cam->ops == &ops);
    assert(cam->runlevel == 3);
    // We shall revert to run-level 1 (sleeping) if possible. FIXME: This is
    // not yet implemented, we could reset the USB device as done in Julia
    // code.
    tao_store_error("tao_camera_reset", TAO_UNSUPPORTED);
    return TAO_ERROR;
}

static tao_status update_binning(
    tao_camera* cam)
{
    andor_camera* acam = (andor_camera*)cam;
    int code = AT_SUCCESS;

    // First try to get AOIHBin and AOIVBin features directly
    AT_64 val;
    if (code == AT_SUCCESS) {
        code = AT_GetInt(acam->handle, L"AOIHBin", &val);
        if (code == AT_SUCCESS) {
            cam->config.roi.xbin = val;
        } else if (code != AT_ERR_NOTIMPLEMENTED) {
            andor_error("AT_GetInt(AOIHBin)", code);
            return TAO_ERROR;
        }
    }
    if (code == AT_SUCCESS) {
        code = AT_GetInt(acam->handle, L"AOIVBin", &val);
        if (code == AT_SUCCESS) {
            cam->config.roi.ybin = val;
        } else if (code != AT_ERR_NOTIMPLEMENTED) {
            andor_error("AT_GetInt(AOIVBin)", code);
            return TAO_ERROR;
        }
    }
    if (code == AT_SUCCESS) {
        return TAO_OK;
    }

    // Second, try to figure out the binning factors from the maximum possible size of the
    // ROI and the sensor size.
    code = AT_GetIntMax(acam->handle, L"AOIWidth", &val);
    if (code != AT_SUCCESS) {
        andor_error("AT_GetIntMax(AOIWidth)", code);
        return TAO_ERROR;
    }
    cam->config.roi.xbin = cam->config.sensorwidth/val;
    code = AT_GetIntMax(acam->handle, L"AOIHeight", &val);
    if (code != AT_SUCCESS) {
        andor_error("AT_GetIntMax(AOIHeight)", code);
        return TAO_ERROR;
    }
    cam->config.roi.ybin = cam->config.sensorheight/val;
    return TAO_OK;
}

static tao_status on_update_config(
    tao_camera* cam)
{
    assert(cam != NULL);
    assert(cam->ops == &ops);
    andor_camera* acam = (andor_camera*)cam;
    int code;

    // Update ROI settings.
    tao_status status = update_binning(cam);
    if (status != TAO_OK) {
        return status;
    }
    AT_64 left;
    code = AT_GetInt(acam->handle, L"AOILeft", &left);
    if (code == AT_ERR_NOTIMPLEMENTED) {
        left = 1;
    } else if (code != AT_SUCCESS) {
        andor_error("AT_GetInt(AOILeft)", code);
        return TAO_ERROR;
    }
    cam->config.roi.xoff = left - 1;
    AT_64 top;
    code = AT_GetInt(acam->handle, L"AOITop", &top);
    if (code == AT_ERR_NOTIMPLEMENTED) {
        top = 1;
    } else if (code != AT_SUCCESS) {
        andor_error("AT_GetInt(AOITop)", code);
        return TAO_ERROR;
    }
    cam->config.roi.yoff = top - 1;
    AT_64 width;
    code = AT_GetInt(acam->handle, L"AOIWidth", &width);
    if (code == AT_ERR_NOTIMPLEMENTED) {
        width = cam->config.sensorwidth;
    } else if (code != AT_SUCCESS) {
        andor_error("AT_GetInt(AOIWidth)", code);
        return TAO_ERROR;
    }
    cam->config.roi.width = width;
    AT_64 height;
    code = AT_GetInt(acam->handle, L"AOIHeight", &height);
    if (code == AT_ERR_NOTIMPLEMENTED) {
        height = cam->config.sensorheight;
    } else if (code != AT_SUCCESS) {
        andor_error("AT_GetInt(AOIHeight)", code);
        return TAO_ERROR;
    }
    cam->config.roi.height = height;

    // ExposureTime
    double exposuretime;
    code = AT_GetFloat(acam->handle, L"ExposureTime", &exposuretime);
    if (code != AT_SUCCESS) {
        andor_error("AT_GetFloat(ExposureTime)", code);
        return TAO_ERROR;
    }
    cam->config.exposuretime = exposuretime;

    // FrameRate
    double framerate;
    code = AT_GetFloat(acam->handle, L"FrameRate", &framerate);
    if (code != AT_SUCCESS) {
        andor_error("AT_GetFloat(FrameRate)", code);
        return TAO_ERROR;
    }
    cam->config.framerate = framerate;

    // Get pixel encoding.
    if (get_pixelencoding(acam, &cam->config.rawencoding) != TAO_OK) {
        return TAO_ERROR;
    }

    // Update the values of all named attributes. The attribute value in the
    // generic camera configuration and the feature value are both updated.
    const int buflen = ANDOR_FEATURE_VALUE_MAXLEN;
    wchar_t buf[buflen];
    for (int i = 0; i < acam->nattrs; ++i) {
        tao_attr* attr = get_attr(acam, i);
        andor_feature* feat = &acam->features[i];
        if (feat->type == ANDOR_FEATURE_BOOLEAN) {
            AT_BOOL val;
            ANDOR_CALL(AT_GetBool,(acam->handle, feat->wkey, &val), return TAO_ERROR);
            attr->val.b = (val != AT_FALSE);
            feat->val.b = (val != AT_FALSE);
            DEBUG("update boolean: %ls -> %s\n", feat->wkey, feat->val.b ? "true" : "false");
        } else if (feat->type == ANDOR_FEATURE_INTEGER) {
            AT_64 val;
            ANDOR_CALL(AT_GetInt,(acam->handle, feat->wkey, &val), return TAO_ERROR);
            attr->val.i = val;
            feat->val.i = val;
            DEBUG("update integer: %ls -> %ld\n", feat->wkey, feat->val.i);
        } else if (feat->type == ANDOR_FEATURE_FLOAT) {
            double val;
            ANDOR_CALL(AT_GetFloat,(acam->handle, feat->wkey, &val), return TAO_ERROR);
            attr->val.f = val;
            feat->val.f = val;
            DEBUG("update float:   %ls -> %g\n", feat->wkey, feat->val.f);
        } else if (feat->type == ANDOR_FEATURE_STRING || feat->type == ANDOR_FEATURE_ENUM) {
            int idx = -1;
            if (feat->type == ANDOR_FEATURE_STRING) {
                ANDOR_CALL(AT_GetString,(acam->handle, feat->wkey, buf, buflen),
                           return TAO_ERROR);
            } else {
                ANDOR_CALL(AT_GetEnumIndex,(acam->handle, feat->wkey, &idx),
                           return TAO_ERROR);
                ANDOR_CALL(AT_GetEnumStringByIndex,(acam->handle, feat->wkey, idx, buf, buflen),
                           return TAO_ERROR);
            }
            buf[buflen - 1] = 0;
            long len = wcslen(buf);
            if (len >= ANDOR_FEATURE_VALUE_MAXLEN || len >= TAO_ATTR_VAL_SIZE(attr)) {
                tao_store_error(__func__, TAO_BAD_SIZE);
                return TAO_ERROR;
            }
            if (tao_transcode_unicode_string_to_utf8(buf, attr->val.s, len + 1) != TAO_OK) {
                return TAO_ERROR;
            }
            if (len > 0) {
                memcpy(feat->val.s, buf, len*sizeof(wchar_t));
            }
            feat->val.s[len] = 0;
            feat->index = idx;
            DEBUG("update %s %ls -> \"%ls\"\n",
                  feat->type == ANDOR_FEATURE_STRING ? "string: " : "enum:   ",
                  feat->wkey, feat->val.s);
        } else {
            DEBUG("error: unknown type for feature %ls\n", feat->wkey);
            tao_store_error(__func__, TAO_BAD_TYPE);
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

static tao_status check_or_set_attribute(
    const char* func,
    andor_camera* acam,
    const tao_camera_config* cfg,
    int i,
    bool set);

static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    // The camera is an Andor one.
    andor_camera* acam = (andor_camera*)cam;

    // Name of caller for error messages.
    const char* func = "tao_camera_check_configuration";

    // Error code in case of failure.
    int code = TAO_SUCCESS;

    // Check region of interest.
    if (tao_camera_roi_check(&cfg->roi,
                             cam->config.sensorwidth,
                             cam->config.sensorheight) != TAO_OK) {
        code = TAO_BAD_ROI;
        goto error;
    }

    // Check exposure time and frame rate.
    if (isnan(cfg->exposuretime) || isinf(cfg->exposuretime) ||
        cfg->exposuretime < 0) {
        code = TAO_BAD_EXPOSURETIME;
        goto error;
    }
    if (isnan(cfg->framerate) || isinf(cfg->framerate) ||
        cfg->framerate <= 0) {
        code = TAO_BAD_FRAMERATE;
        goto error;
    }

    // Check encoding of acquired images.
    int idx = -1;
    for (int k = 0; k < acam->nencs; ++k) {
        if (acam->encs[k] == cfg->rawencoding) {
            idx = k;
            break;
        }
    }
    if (idx == -1) {
        code = TAO_BAD_ENCODING;
        goto error;
    }

    // Check pre-processing level and pixel type.  NOTE: In principle,
    // pre-processing parameters have already been checked by
    // `tao_camera_check_configuration`.
    if (TAO_ENCODING_COLORANT(cfg->rawencoding) != TAO_COLORANT_MONO) {
        char str[TAO_ENCODING_STRING_SIZE];
        fprintf(stderr, "Only monochrome encoding is supported by TAO "
                "(rawencoding = 0x%08x / %s)\n", cfg->rawencoding,
                encoding_sprintf(str, cfg->rawencoding));
        code = TAO_BAD_ENCODING;
        goto error;
    }
    if (tao_camera_configuration_check_preprocessing(cfg) != TAO_OK) {
        return TAO_ERROR;
    }

    // Check number of number of acquisition buffers.
    if (cfg->buffers < 2) {
        code = TAO_BAD_BUFFERS;
        goto error;
    }

    // Check named attributes.
    for (int i = 0; i < acam->nattrs; ++i) {
        if (check_or_set_attribute(__func__, acam, cfg, i, false) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // All parameters seem to be acceptable.
    return TAO_OK;

    // Errors branch here.
error:
    tao_store_error(func, code);
    return TAO_ERROR;
}

static tao_status set_binning(
    tao_camera* cam,
    long xbin,
    long ybin)
{
    andor_camera* acam = (andor_camera*)cam;
    int code = AT_SUCCESS;

    // First try to set AOIHBin and AOIVBin features directly
    if (code == AT_SUCCESS && xbin != cam->config.roi.xbin) {
        code = AT_SetInt(acam->handle, L"AOIHBin", xbin);
        fprintf(stderr, "xbin: %ld -> %ld (code = %d)\n", cam->config.roi.xbin, xbin, code);
        if (code != AT_SUCCESS && code != AT_ERR_NOTIMPLEMENTED) {
            andor_error("AT_SetInt(AOIHBin)", code);
            return TAO_ERROR;
        }
    }
    if (code == AT_SUCCESS && ybin != cam->config.roi.ybin) {
        code = AT_SetInt(acam->handle, L"AOIVBin", ybin);
        fprintf(stderr, "ybin: %ld -> %ld (code = %d)\n", cam->config.roi.ybin, ybin, code);
        if (code != AT_SUCCESS && code != AT_ERR_NOTIMPLEMENTED) {
            andor_error("AT_SetInt(AOIVBin)", code);
            return TAO_ERROR;
        }
    }
    if (code == AT_SUCCESS) {
        return TAO_OK;
    }

    // Second, try to use AOIBinning feature. We retireve the current index, then try all
    // possible options.
    int old_index;
    code = AT_GetEnumIndex(acam->handle, L"AOIBinning", &old_index);
    if (code != AT_SUCCESS) {
        andor_error("AT_GetEnumIndex(AOIBinning)", code);
        return TAO_ERROR;
    }
    int count;
    code = AT_GetEnumCount(acam->handle, L"AOIBinning", &count);
    if (code != AT_SUCCESS) {
        andor_error("AT_GetEnumCount(AOIBinning)", code);
        return TAO_ERROR;
    }
    if (count > 1) {
        // Try all other possible options than the current one.
        for (int new_index = 0; new_index < count; ++new_index) {
            if (new_index == old_index) {
                continue;
            }
            code = AT_SetEnumIndex(acam->handle, L"AOIBinning", new_index);
            fprintf(stderr, "binning: %d -> %d (code = %d)\n", old_index, new_index, code);
            if (code != AT_SUCCESS) {
                andor_error("AT_SetEnumIndex(AOIBinning)", code);
                return TAO_ERROR;
            }
            AT_64 max_width;
            code = AT_GetIntMax(acam->handle, L"AOIWidth", &max_width);
            if (code != AT_SUCCESS) {
                andor_error("AT_GetIntMax(AOIWidth)", code);
                return TAO_ERROR;
            }
            fprintf(stderr, "xbin -> %ld\n", (long)(cam->config.sensorwidth/max_width));
            if (cam->config.sensorwidth != max_width*xbin) {
                continue;
            }
            AT_64 max_height;
            code = AT_GetIntMax(acam->handle, L"AOIHeight", &max_height);
            if (code != AT_SUCCESS) {
                andor_error("AT_GetIntMax(AOIHeight)", code);
                return TAO_ERROR;
            }
            fprintf(stderr, "ybin -> %ld\n", (long)(cam->config.sensorheight/max_height));
            if (cam->config.sensorheight != max_height*ybin) {
                continue;
            }
            // We have found a correct setting.
            return TAO_OK;
        }
        // No errors but no corresponding setting found. Restore old setting.
        code = AT_SetEnumIndex(acam->handle, L"AOIBinning", old_index);
        if (code != AT_SUCCESS) {
            andor_error("AT_SetEnumIndex(AOIBinning)", code);
            return TAO_ERROR;
        }
    }

    // No correct setting found.
    tao_store_error(__func__, TAO_BAD_ROI);
    return TAO_ERROR;
}

// Configuration has already been checked.
static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    // Get Andor device and current configuration.
    assert(cam != NULL);
    assert(cam->ops == &ops);
    andor_camera* acam = (andor_camera*)cam;
    int code;

    // If any changes in the ROI are requested, try to apply these changes. First the
    // binning, then the other parameters in the order recommended in the Andor SDK doc
    // (Section 4.5, "Area of Interest").
    if (cfg->roi.xbin   != cam->config.roi.xbin  ||
        cfg->roi.ybin   != cam->config.roi.ybin  ||
        cfg->roi.xoff   != cam->config.roi.xoff  ||
        cfg->roi.yoff   != cam->config.roi.yoff  ||
        cfg->roi.width  != cam->config.roi.width ||
        cfg->roi.height != cam->config.roi.height) {
        // 1., 2. Change binning settings if necessary.
        tao_status status =  set_binning(cam, cfg->roi.xbin, cfg->roi.ybin);
        if (status != TAO_OK) {
            return status;
        }
        // 3. AOIWidth
        code = AT_SetInt(acam->handle, L"AOIWidth", cfg->roi.width);
        if (code != AT_SUCCESS) {
            andor_error("AT_SetInt(AOIWidth)", code);
            return TAO_ERROR;
        }
        // 4. AOILeft
        code = AT_SetInt(acam->handle, L"AOILeft", cfg->roi.xoff + 1);
        if (code != AT_SUCCESS) {
            andor_error("AT_SetInt(AOILeft)", code);
            return TAO_ERROR;
        }
        // 5. AOIHeight
        code = AT_SetInt(acam->handle, L"AOIHeight", cfg->roi.height);
        if (code != AT_SUCCESS) {
            andor_error("AT_SetInt(AOIHeight)", code);
            return TAO_ERROR;
        }
        // 6. VerticallyCentreAOI
        if (disable_vertical_centering(acam) != TAO_OK) {
            return TAO_ERROR;
        }
        // 7. AOITop
        code = AT_SetInt(acam->handle, L"AOITop", cfg->roi.yoff + 1);
        if (code != AT_SUCCESS) {
            andor_error("AT_SetInt(AOITop)", code);
            return TAO_ERROR;
        }
    }

    // Change encoding if requested to.
    if (cfg->rawencoding != cam->config.rawencoding) {
        if (set_pixelencoding(acam, cfg->rawencoding) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Directly copy settings (pixel type, preprocessing method, and number of acquisition
    // buffers) that require no other check than those performed by
    // `tao_camera_check_configuration`.
    cam->config.pixeltype     = cfg->pixeltype;
    cam->config.preprocessing = cfg->preprocessing;
    cam->config.buffers       = cfg->buffers;

    // Change frame rate and exposure time. First reduce the frame rate if
    // requested, then change the exposure time if requested, finally augment
    // the frame rate if requested.
    for (int pass = 1; pass <= 2; ++pass) {
        if ((pass == 1 && cfg->framerate < cam->config.framerate) ||
            (pass == 2 && cfg->framerate > cam->config.framerate)) {
            // Change frame-rate.
            code = AT_SetFloat(acam->handle, L"FrameRate", cfg->framerate);
            if (code != AT_SUCCESS) {
                andor_error("AT_SetFloat(FrameRate)", code);
                return TAO_ERROR;
            }
        }
        if (pass == 1 && cfg->exposuretime != cam->config.exposuretime) {
            // Change exposure-time.
            code = AT_SetFloat(acam->handle, L"ExposureTime", cfg->exposuretime);
            if (code != AT_SUCCESS) {
                andor_error("AT_SetFloat(ExposureTime)", code);
                return TAO_ERROR;
            }
        }
    }

    // Set named attributes.
    for (int i = 0; i < acam->nattrs; ++i) {
        if (check_or_set_attribute(__func__, acam, cfg, i, true) != TAO_OK) {
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

static tao_status check_or_set_attribute(
    const char* func,
    andor_camera* acam,
    const tao_camera_config* cfg,
    int i,
    bool set)
{
    tao_error_code code = TAO_SUCCESS;
    andor_feature* feat = &acam->features[i];
    /***/ tao_attr* cam_attr = get_attr(acam, i);
    const tao_attr* cfg_attr = get_attr(cfg, i);

    // For strings and enumerations, a temporary buffer is needed to convert
    // strings into wide-strings. This buffer must have the same number of
    // characters as `feat->val.s`.
    const long buflen = ANDOR_FEATURE_VALUE_MAXLEN;
    wchar_t buf[ANDOR_FEATURE_VALUE_MAXLEN];

    // Check validity of attribute type.
    int attr_type;
    switch (feat->type) {
    case ANDOR_FEATURE_BOOLEAN:
        attr_type = TAO_ATTR_BOOLEAN;
        break;
    case ANDOR_FEATURE_INTEGER:
        attr_type = TAO_ATTR_INTEGER;
        break;
    case ANDOR_FEATURE_FLOAT:
        attr_type = TAO_ATTR_FLOAT;
        break;
    case ANDOR_FEATURE_ENUM:
        attr_type = TAO_ATTR_STRING;
        break;
    case ANDOR_FEATURE_STRING:
        attr_type = TAO_ATTR_STRING;
        break;
    default:
        code = TAO_BAD_TYPE;
        goto error;
    }
    if (TAO_ATTR_TYPE(cam_attr) != attr_type) {
        code = TAO_CORRUPTED;
        goto error;
    }

    // Check whether something has changed.
    unsigned chgs = tao_attr_check(cam_attr, cfg_attr,
                                   TAO_ATTR_CHECK_NAME|TAO_ATTR_CHECK_FLAGS|
                                   TAO_ATTR_CHECK_TYPE|TAO_ATTR_CHECK_VALUE);
    if (chgs == 0) {
        // No changes.
        return TAO_OK;
    }
    if (chgs != TAO_ATTR_CHECK_VALUE) {
        // Only value is allowed to change,
        code = TAO_FORBIDDEN_CHANGE;
        goto error;
    }

    // Value has changed.
    if (set) {
        // Check whether feature is currently writable.
        AT_BOOL bval;
        ANDOR_CALL(AT_IsWritable,(acam->handle, feat->wkey, &bval), return TAO_ERROR);
        if (bval == AT_FALSE) {
            code = TAO_UNWRITABLE;
            goto error;
        }
    } else {
        // Make sure that feature is not permanently read-only.
        AT_BOOL bval;
        ANDOR_CALL(AT_IsReadOnly,(acam->handle, feat->wkey, &bval), return TAO_ERROR);
        if (bval == AT_TRUE) {
            code = TAO_UNWRITABLE;
            goto error;
        }
     }

    // Apply changes if requested to do so.
    if (feat->type == ANDOR_FEATURE_BOOLEAN) {
        if (set) {
            AT_BOOL val = cfg_attr->val.b ? AT_TRUE : AT_FALSE;
            ANDOR_CALL(AT_SetBool,(acam->handle, feat->wkey,  val), return TAO_ERROR);
        }
    } else if (feat->type == ANDOR_FEATURE_INTEGER) {
        if (set) {
            AT_64 val = cfg_attr->val.i;
            ANDOR_CALL(AT_SetInt,(acam->handle, feat->wkey,  val), return TAO_ERROR);
        }
    } else if (feat->type == ANDOR_FEATURE_FLOAT) {
        if (set) {
            double val = cfg_attr->val.f;
            ANDOR_CALL(AT_SetFloat,(acam->handle, feat->wkey,  val), return TAO_ERROR);
        }
    } else if (feat->type == ANDOR_FEATURE_STRING) {
        long len = strnlen(cfg_attr->val.s, TAO_ATTR_VAL_SIZE(cfg_attr));
        if (len >= TAO_ATTR_VAL_SIZE(cfg_attr) || len >= TAO_ATTR_VAL_SIZE(cam_attr)) {
            // Source string is not null-terminated or cannot be stored in
            // destination.
            code = TAO_BAD_SIZE;
            goto error;
        }
        if (set) {
            // Attempt to apply changes.
            if (len >= buflen) {
                // String cannot be stored into buffer for conversion.
                code = TAO_BAD_SIZE;
                goto error;
            }
            if (tao_transcode_utf8_string_to_unicode(cfg_attr->val.s, buf, buflen) != TAO_OK) {
                return TAO_ERROR;
            }
            buf[len] = 0;
            ANDOR_CALL(AT_SetString,(acam->handle, feat->wkey, buf), return TAO_ERROR);
        }
    } else if (feat->type == ANDOR_FEATURE_ENUM) {
        // Find a unique match among the enumeration strings as if glob-style
        // "${val}*" was specified.
        int enum_count; // number of possible enumeration values
        ANDOR_CALL(AT_GetEnumCount,(acam->handle, feat->wkey, &enum_count), return TAO_ERROR);
        int match_idx = -1; // index of matching value
        int match_len = -1; // length of matching value
        int match_cnt =  0; // number of matching values
        int val_size = TAO_ATTR_VAL_SIZE(cfg_attr);
        for (int j = 0; j < enum_count; ++j) {
            ANDOR_CALL(AT_GetEnumStringByIndex,(acam->handle, feat->wkey, j, buf, buflen),
                       return TAO_ERROR);
            int len = -1; // length of value if match found
            for (int i = 0; i < val_size; ++i) {
                if (cfg_attr->val.s[i] == 0) {
                    // Possibly truncated match found.
                    len = i;
                    break;
                }
                if (cfg_attr->val.s[i] != buf[i]) {
                    // Strings do not match.
                    break;
                }
            }
            if (len > 0) {
                if (len > match_len) {
                    // First match of this length.
                    match_idx = j;
                    match_len = len;
                    match_cnt = 1;
                } else if (len == match_len) {
                    // One more match of this length.
                    ++match_cnt;
                }
            }
        }
        if (match_cnt != 1) {
            // No matches or more than one matches found.
            code = TAO_BAD_VALUE;
            goto error;
        }
        if (set) {
            // Attempt to apply changes.
            ANDOR_CALL(AT_SetEnumIndex,(acam->handle, feat->wkey, match_idx), return TAO_ERROR);
        }
    } else {
        code = TAO_BAD_TYPE;
        goto error;
    }
    return TAO_OK;

 error:
    tao_store_error(func, code);
    return TAO_ERROR; // Error.
}

// Make sure the camera does not use any old acquisition buffers and zero-fill
// acquisition buffer settings.  Some settings (encoding, stride, width,
// height, etc.) are instantiated by this function, others will be updated by
// the on_wait_buffer virtual method.
static tao_status flush_buffers(
    andor_camera* acam)
{
    memset(&acam->buf, 0, sizeof(acam->buf));
    ANDOR_CALL(AT_Flush, (acam->handle), return TAO_ERROR);
    return TAO_OK;
}

static inline tao_status queue_buffer(
    andor_camera* acam,
    AT_U8* ptr,
    int siz)
{
    ANDOR_CALL(AT_QueueBuffer,(acam->handle, ptr, siz), return TAO_ERROR);
    return TAO_OK;
}

static tao_status on_start(
    tao_camera* cam)
{
    // Get Andor device.
    assert(cam != NULL);
    assert(cam->ops == &ops);
    assert(cam->runlevel == 1);
    andor_camera* acam = (andor_camera*)cam;

    // Discard acquisition buffers.
    if (flush_buffers(acam) != TAO_OK) {
        return TAO_ERROR;
    }

    // Get the current pixel encoding.
    if (get_pixelencoding(acam, &acam->buf.encoding) != TAO_OK) {
        return TAO_ERROR;
    }

    // Get size of acquisition buffers in bytes.
    AT_64 size;
    int code = AT_GetInt(acam->handle, L"ImageSizeBytes", &size);
    if (code != AT_SUCCESS) {
        andor_error("AT_GetInt(ImageSizeBytes)", code);
        return TAO_ERROR;
    }

    // Get image size (even though it is written in the device configuration,
    // we read it again).
    long width, height;
    if (get_width(acam, &width) != TAO_OK || get_height(acam, &height) != TAO_OK) {
        return TAO_ERROR;
    }

    // Get size of one row in the image in bytes.
    AT_64 stride;
    code = AT_GetInt(acam->handle, L"AOIStride", &stride);
    if (code != AT_SUCCESS) {
        andor_error("AT_GetInt(AOIStride)", code);
        return TAO_ERROR;
    }

    // Instantiate acquisition buffer parameters.
    acam->buf.data   = NULL;
    acam->buf.size   = size;
    acam->buf.offset = 0;
    acam->buf.width  = width;
    acam->buf.height = height;
    acam->buf.stride = stride;

    // Use at least 2 acquisition buffers.
    if (cam->config.buffers < 2) {
        cam->config.buffers = 2;
    }
    int nbufs = cam->config.buffers;

    // Allocate new device acquisition buffers as needed and queue them.
    if (alloc_buffers(acam, nbufs, ALIGNED_BUFFER_SIZE(size)) != TAO_OK) {
        return TAO_ERROR;
    }
    for (int i = 0; i < acam->nbufs; ++i) {
        AT_U8* ptr = ALIGNED_BUFFER_ADDRESS(acam->bufs[i].addr);
        if (queue_buffer(acam, ptr, size) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Set the camera to continuously acquires frames.
    AT_BOOL bval;
    code = AT_IsImplemented(acam->handle, L"CycleMode", &bval);
    if (code != AT_SUCCESS) {
        andor_error("AT_IsImplemented(CycleMode)", code);
        return TAO_ERROR;
    }
    if (bval == AT_TRUE) {
        code = AT_SetEnumString(acam->handle, L"CycleMode", L"Continuous");
        if (code != AT_SUCCESS) {
            andor_error("AT_SetEnumString(CycleMode,Continuous)", code);
            return TAO_ERROR;
        }
    }
    code = AT_IsImplemented(acam->handle, L"TriggerMode", &bval);
    if (code != AT_SUCCESS) {
        andor_error("AT_IsImplemented(TriggerMode)", code);
        return TAO_ERROR;
    }
    if (bval == AT_TRUE) {
        code = AT_SetEnumString(acam->handle, L"TriggerMode", L"Internal");
        if (code != AT_SUCCESS) {
            andor_error("AT_SetEnumString(TriggerMode,Internal)", code);
            return TAO_ERROR;
        }
    }

    // Start the acquisition.
    code = AT_Command(acam->handle, L"AcquisitionStart");
    if (code != AT_SUCCESS) {
        andor_error("AT_Command(AcquisitionStart)", code);
        return TAO_ERROR;
    }

    // Check that acquisition is running if `CameraAcquiring` feature is
    // implemented. FIXME: This is disabled because it does not work. Perhaps
    // because of some latency before start becomes effective. We could
    // implement a wait here.
#if 0
    code = AT_IsImplemented(acam->handle, L"CameraAcquiring", &bval);
    if (code != AT_SUCCESS) {
        andor_error("AT_IsImplemented(CameraAcquiring)", code);
        return TAO_ERROR;
    }
    if (bval == AT_TRUE) {
        code = AT_GetBool(acam->handle, L"CameraAcquiring", &bval);
        if (code != AT_SUCCESS) {
            andor_error("AT_GetBool(CameraAcquiring)", code);
            return TAO_ERROR;
        }
        if (bval != AT_TRUE) {
            tao_store_error(__func__, TAO_NOT_ACQUIRING);
            return TAO_ERROR;
        }
    }
#endif

    // Report success.
    return TAO_OK;
}

static tao_status on_stop(
    tao_camera* cam)
{
    // Get Andor device.
    assert(cam->ops == &ops);
    assert(cam->runlevel == 2);
    andor_camera* acam = (andor_camera*)cam;

    // Stop the acquisition.
    int code = AT_Command(acam->handle, L"AcquisitionStop");
    if (code != AT_SUCCESS) {
        andor_error("AT_Command(AcquisitionStop)", code);
        return TAO_ERROR;
    }

#if 0
    // Check that acquisition is no running if `CameraAcquiring` feature is
    // implemented. FIXME: Does not work (see abobe in `on_start`).
    AT_BOOL bval;
    code = AT_IsImplemented(acam->handle, L"CameraAcquiring", &bval);
    if (code != AT_SUCCESS) {
        andor_error("AT_IsImplemented(CameraAcquiring)", code);
        return TAO_ERROR;
    }
    if (bval == AT_TRUE) {
        code = AT_GetBool(acam->handle, L"CameraAcquiring", &bval);
        if (code != AT_SUCCESS) {
            andor_error("AT_GetBool(CameraAcquiring)", code);
            return TAO_ERROR;
        }
        if (bval == AT_TRUE) {
            tao_store_error(__func__, TAO_ACQUISITION_RUNNING);
            return TAO_ERROR;
        }
    }
#endif

    // Discard acquisition buffers.
    if (flush_buffers(acam) != TAO_OK) {
        return TAO_ERROR;
    }

    // Report success.
    return TAO_OK;
}

static tao_status on_wait_buffer(
    tao_camera* cam,
    tao_acquisition_buffer* buf,
    double secs,
    int drop)
{
    // Get Andor device and check assertions.
    andor_camera* acam = (andor_camera*)cam;
    assert(cam->ops == &ops);
    assert(cam->runlevel == 2);

    // Convert timeout value to milliseconds.
    unsigned int tm = AT_INFINITE;
    double msecs = round(TAO_MILLISECONDS_PER_SECOND*secs);
    if (msecs < tm) {
        tm = msecs;
    }

    // Re-queue previous acquisition buffer if any.
    if (acam->buf.data != NULL) {
        AT_U8* ptr = acam->buf.data;
        int    siz = acam->buf.size;
        acam->buf.data = NULL; // never re-queue more than once
        if (queue_buffer(acam, ptr, siz) != TAO_OK) {
            // Not being able to re-queue the buffer requires more elaborated
            // handling than just reporting an error: we set the run-level so
            // that, at least, acquisition will be stopped.
            cam->runlevel = 3;
            return TAO_ERROR;
        }
    }

    // Wait for a new image to be available.
    int siz;
    AT_U8* ptr;
    int code = AT_WaitBuffer(acam->handle, &ptr, &siz, tm);
    if (code == AT_SUCCESS) {
        // Register address and size of acquisition buffer for further re-queue
        // and update counters.
        acam->buf.data = ptr;
        acam->buf.size = siz;
        ++cam->config.frames;
    } else if (code == AT_ERR_TIMEDOUT) {
        ++cam->config.timeouts;
        return TAO_TIMEOUT;
    } else {
        andor_error("AT_WaitBuffer", code);
        return TAO_ERROR;
    }

    // Set acquisition buffer information.  FIXME: Use Andor meta-data
    // information.
    tao_time now;
    if (tao_get_monotonic_time(&now) != TAO_OK) {
        tao_panic();
    }

    // Update last acquisition buffer.
    buf->data         = acam->buf.data;
    buf->size         = acam->buf.size;
    buf->offset       = acam->buf.offset;
    buf->width        = acam->buf.width;
    buf->height       = acam->buf.height;
    buf->stride       = acam->buf.stride;
    buf->encoding     = acam->buf.encoding;
    buf->serial       = cam->config.frames;
    buf->frame_start  = now;
    buf->frame_end    = now;
    buf->buffer_ready = now;
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// ACCESSORS, MUTATORS, AND UTILITIES

// Disable centering of region of interest.
static tao_status disable_vertical_centering(
    andor_camera* acam)
{
    int code = AT_SetBool(acam->handle, L"VerticallyCentreAOI", AT_FALSE);
    if (code != AT_SUCCESS && code != AT_ERR_NOTIMPLEMENTED) {
        andor_error("AT_SetInt(VerticallyCentreAOI)", code);
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Must be called after setting sensorwidth.
static tao_status get_width(
    andor_camera* acam,
    long* valptr)
{
    AT_64 val;
    int code = AT_GetInt(acam->handle, L"AOIWidth", &val);
    if (code == AT_ERR_NOTIMPLEMENTED) {
        val = acam->base.config.sensorwidth;
    } else if (code != AT_SUCCESS) {
        andor_error("AT_GetInt(AOIWidth)", code);
        return TAO_ERROR;
    }
    *valptr = val;
    return TAO_OK;
}

// Must be called after setting sensorheight.
static tao_status get_height(
    andor_camera* acam,
    long* valptr)
{
    AT_64 val;
    int code = AT_GetInt(acam->handle, L"AOIHeight", &val);
    if (code == AT_ERR_NOTIMPLEMENTED) {
        val = acam->base.config.sensorheight;
    } else if (code != AT_SUCCESS) {
        andor_error("AT_GetInt(AOIHeight)", code);
        return TAO_ERROR;
    }
    *valptr = val;
    return TAO_OK;
}

static tao_status get_pixelencoding(
    andor_camera* acam,
    tao_encoding* valptr)
{
    int idx;
    int code = AT_GetEnumIndex(acam->handle, L"PixelEncoding", &idx);
    if (code != AT_SUCCESS) {
        andor_error("AT_GetInt(PixelEncoding)", code);
        return TAO_ERROR;
    }
    if (idx < 0 || idx >= acam->nencs) {
        tao_store_error(__func__, TAO_OUT_OF_RANGE);
        return TAO_ERROR;
    }
    *valptr = acam->encs[idx];
    return TAO_OK;
}

static tao_status set_pixelencoding(
    andor_camera* acam,
    tao_encoding val)
{
    int idx = -1;
    for (int k = 0; k < acam->nencs; ++k) {
        if (acam->encs[k] == val) {
            idx = k;
            break;
        }
    }
    if (idx == -1) {
        tao_store_error("tao_camera_set_configuration", TAO_BAD_ENCODING);
        return TAO_ERROR;
    }
    int code = AT_SetEnumIndex(acam->handle, L"PixelEncoding", idx);
    if (code != AT_SUCCESS) {
        andor_error("AT_SetInt(PixelEncoding)", code);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status alloc_buffers(
    andor_camera* acam,
    int nbufs,
    int size)
{
    // Check arguments.
    if (size < 0 || nbufs < 0 || (nbufs > 0 && size <= 0)) {
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return TAO_ERROR;
    }

    // Reuse buffers if possible.
    if (acam->nbufs == nbufs) {
        bool keepbufs = true;
        for (int i = 0; i < nbufs; ++i) {
            if (acam->bufs[i].addr == NULL || acam->bufs[i].size != size) {
                keepbufs = false;
                break;
            }
        }
        if (keepbufs) {
            return TAO_OK;
        }
    }

    // Explicitely free the buffers (even though there may be none).
    free_buffers(acam);

    // (Re)allocate buffers.
    acam->bufs = (andor_buffer*)tao_calloc(nbufs, sizeof(andor_buffer));
    if (acam->bufs == NULL) {
        acam->nbufs = 0;
        return TAO_ERROR;
    }
    acam->nbufs = nbufs;
    tao_status status = TAO_OK;
    for (int i = 0; i < nbufs; ++i) {
        if (status == TAO_OK) {
            acam->bufs[i].addr = tao_malloc(size);
            if (acam->bufs[i].addr == NULL) {
                acam->bufs[i].size = 0;
                status = TAO_ERROR;
            } else {
                acam->bufs[i].size = size;
            }
        } else {
            acam->bufs[i].addr = NULL;
            acam->bufs[i].size = 0;
        }
    }
    if (status != TAO_OK) {
        free_buffers(acam);
    }
    return status;
}

static void free_buffers(
    andor_camera* acam)
{
    // Make sure the frame-grabber no longer use any buffers before effectively
    // free the buffers.  The previous acquirred buffer, if any, is gone.
    (void)AT_Flush(acam->handle);
    andor_buffer* bufs = acam->bufs;
    int nbufs = acam->nbufs;
    acam->bufs = NULL;
    acam->nbufs = 0;
    if (bufs != NULL) {
        for (int i = 0; i < nbufs; ++i) {
            if (bufs[i].addr != NULL) {
                free(bufs[i].addr);
            }
        }
        free(bufs);
    }
}

static tao_status update_pixel_encodings(
    andor_camera* acam)
{
    AT_WC buf[ANDOR_STRING_MAXLEN];
    int cnt, status;

    // Get the number of supported current pixel encoding.
    status = AT_GetEnumCount(acam->handle, L"PixelEncoding", &cnt);
    if (status != AT_SUCCESS) {
        andor_error("AT_GetEnumCount(PixelEncoding)", status);
        return TAO_ERROR;
    }
    if (cnt > ANDOR_MAX_ENCODINGS) {
        // FIXME: Use the logging system...
        fprintf(stderr, "Warning: Fix ANDOR_MAX_ENCODINGS to be %d\n", cnt);
        cnt = ANDOR_MAX_ENCODINGS;
    }
    for (int idx = 0; idx < cnt; ++idx) {
        status = AT_GetEnumStringByIndex(acam->handle, L"PixelEncoding", idx,
                                         buf, TAO_STATIC_ARRAY_LENGTH(buf));
        if (status != AT_SUCCESS) {
            andor_error("AT_GetEnumStringByIndex(PixelEncoding)",
                              status);
            return TAO_ERROR;
        }
        acam->encs[idx] = andor_wide_name_to_encoding(buf);
    }
    for (int idx = cnt; idx < ANDOR_MAX_ENCODINGS; ++idx) {
        acam->encs[idx] = TAO_ENCODING_UNKNOWN;
    }
    acam->nencs = cnt;
    return TAO_OK;
}

static char* encoding_sprintf(
    char* str,
    tao_encoding enc)
{
    if (tao_format_encoding(str, enc) != TAO_OK) {
        strcat(str, "invalid");
    }
    return str;
}
