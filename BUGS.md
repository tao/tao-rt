# Known issues in TAO

- Assumptions made by some functions (like `tao_clamp` and `tao_safe_clamp`) are
  broken by compilation option `-ffast-math`.  Do not use this option until a
  workaround is found.
