;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((indent-tabs-mode . nil)
         (tab-width . 8)
         (fill-column . 90)
         (ispell-local-dictionary . "american")))
 (c-mode . ((flycheck-clang-include-path . ("." "../base"))
            (indent-tabs-mode . nil)
            (c-file-style . "bsd")
            (c-basic-offset . 4)
            (fill-column . 90)))
 (java-mode . ((indent-tabs-mode . nil)
               (c-basic-offset . 4)))
 (julia-mode . ((indent-tabs-mode . nil)))
 (makefile-automake-mode . ((indent-tabs-mode . t)))
 (makefile-gmake-mode . ((indent-tabs-mode . t)))
 (sh-mode . ((indent-tabs-mode . nil)
             (sh-basic-offset . 4)))
 (tcl-mode . ((indent-tabs-mode . nil)
              (tcl-default-application . "wish")
              (tcl-indent-level . 4)))
 (yorick-mode . ((indent-tabs-mode . nil)
                 (c-basic-offset . 4)
                 (fill-column . 90))))
