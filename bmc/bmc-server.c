// bmc-server.c -
//
// Server for the Boston Micromachines deformable mirrors in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2022-2024, Éric Thiébaut.

#include <math.h>
#include <stdio.h>
#include <string.h>

#include <BMCApi.h>

#include "tao-basics.h"
#include "tao-layouts.h"
#include "tao-remote-mirrors-private.h"
#include "tao-generic.h"
#include "tao-errors.h"

// Get error details from the Boston Micromachines SDK.
static void bmc_error_getter(
    int code,
    const char** reason,
    const char** info)
{
    if (reason != NULL) {
        *reason = BMCErrorString(code);
    }
    if (info != NULL) {
        *info = NULL; // textual value of error code will be used
    }
}

// Set TAO last error with an error from the Boston Micromachines SDK.
static void bmc_error(
    const char* func,
    int code)
{
    tao_store_other_error(func, code, bmc_error_getter);
}

// Context for the `on_send` callback.
typedef struct context_ {
    DM* hdm;
    uint32_t* lut;
} context;

// Send the requested command.  On entry, `cmds` contains the `rdm->nacts`
// actuators commands.  On return, `cmds` contains the actuators commands
// modified to account for the deformable mirror limitations.
static tao_status on_send(
    tao_remote_mirror* rdm,
    void* data,
    double* cmds)
{
    // Context is the address of the deformable mirror driver (hardware).
    context* ctx = data;
    BMCRC rv = BMCSetArray(ctx->hdm, cmds, ctx->lut);
    if (rv != NO_ERR) {
        bmc_error("tao_remote_mirror_send_commands", rv);
        return TAO_ERROR;
    }
    return TAO_OK;
}

int main(
    int argc,
    char* argv[])
{
    // Determine program name.
    const char* progname = tao_basename(argv[0]);

    // Parse arguments.  First options, then positional arguments.
    const char* usage = "Usage: %s [OPTIONS ...] [--] SERIAL NAME\n";
    const char* serial = NULL; // serial number of device
    const char* name = NULL;  // server name
    const char* mappath = NULL; // map path for the driver
    bool debug = false;
    long nbufs = 10000;
    unsigned int orient = 0;
    unsigned int perms = 0077;
    int iarg = 0;
    while (++iarg < argc) {
        char dummy;
        if (argv[iarg][0] != '-') {
            // Argument is not an option.
            --iarg;
            break;
        }
        if (argv[iarg][1] == '-' && argv[iarg][2] == '\0') {
            // Argument is last option.
            break;
        }
        if (strcmp(argv[iarg], "-h") == 0
            || strcmp(argv[iarg], "-help") == 0
            || strcmp(argv[iarg], "--help") == 0) {
            printf(usage, progname);
            printf("\n");
            printf("Arguments:\n");
            printf("  SERIAL             Serial number of device.\n");
            printf("  NAME               Name of server.\n");
            printf("\n");
            printf("Options:\n");
            printf("  -mappath PATH      Map path for the driver.\n");
            printf("  -nbufs NBUFS       Number of frame buffers [%ld].\n",
                   nbufs);
            printf("  -orient ORIENT     Orientation of layout [%u].\n",
                   orient);
            printf("  -perms PERMS       Bitwise mask of permissions [0%o].\n",
                   perms);
            printf("  -debug             Debug mode [%s].\n",
                   (debug ? "true" : "false"));
            printf("  -h, -help, --help  Print this help.\n");
            return EXIT_SUCCESS;
        }
        if (strcmp(argv[iarg], "-mappath") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option `%s`.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            mappath = argv[iarg+1];
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-nbufs") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option `%s`.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%ld %c", &nbufs, &dummy) != 1
                || nbufs < 2) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option `%s`.\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-orient") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option `%s`.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%u %c", &orient, &dummy) != 1) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option `%s`.\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            orient &= 5;
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-perms") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option `%s`.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%u %c", &perms, &dummy) != 1) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option `%s`.\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            perms &= 0777;
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-debug") == 0) {
            debug = true;
            continue;
        }
        fprintf(stderr, "%s: Unknown option `%s`.\n", progname, argv[iarg]);
        return EXIT_FAILURE;
    }
    if (++iarg >= argc) {
        fprintf(stderr, "%s: Missing serial number.\n", progname);
        goto bad_usage;
    }
    serial = argv[iarg];
    if (++iarg >= argc) {
        fprintf(stderr, "%s: Missing server name.\n", progname);
    bad_usage:
        fputc('\n', stderr);
        fprintf(stderr, usage, progname);
        fputc('\n', stderr);
        return EXIT_FAILURE;
    }
    name = argv[iarg];
    if (++iarg < argc) {
        fprintf(stderr, "%s: Too many arguments.\n", progname);
        goto bad_usage;
    }

    // Code returned on exit.
    int exitcode = EXIT_SUCCESS;

    // Deformable mirror driver and its initialization level (0 if driver not
    // open, 1 if driver open but LUT map not yet load, 2 if driver open and
    // LUT map loaded).  The initialization level is used on exit to undo
    // things.
    DM hdm = {};
    int initlevel = 0;

    // Pre-set all resources to NULL to make it easier to track which ones have
    // been allocated and cleanup on normat or early (i.e. error) return.
    uint32_t* lut = NULL;          // Lookup table.
    long* inds = NULL;             // Grid of indices.
    uint8_t* msk = NULL;           // Mask of actuators.
    tao_remote_mirror* rdm = NULL; // Remote deformable mirror.

    // Open the driver.
    BMCRC rv = BMCOpen(&hdm, serial);
    if (rv != NO_ERR) {
        fprintf(stderr,
                "%s: Failed to open the deformable mirror driver (%s).\n",
                progname, BMCErrorString(rv));
        goto error;
    }
    initlevel = 1;

    // Load the actuators lookup table.
    long nacts = hdm.ActCount;
    long dim = hdm.ActCountWidth;
    lut = calloc(nacts, sizeof(*lut));
    if (lut == NULL) {
        fprintf(stderr,
                "%s: Failed to allocate the actuators lookup table.\n",
                progname);
        goto error;
    }
    rv = BMCLoadMap(&hdm, mappath, lut);
    if (rv != NO_ERR) {
        fprintf(stderr,
                "%s: Failed to load the actuators lookup table (%s).\n",
                progname, BMCErrorString(rv));
        goto error;
    }
    initlevel = 2;

    // Set all actuators to 0.
    rv = BMCClearArray(&hdm);
    if (rv != NO_ERR) {
        fprintf(stderr,
                "%s: Failed to set all actuators to zero on entry (%s).\n",
                progname, BMCErrorString(rv));
        goto error;
    }

    // Build mirror mask and layout indices.
    msk = calloc(dim*dim, sizeof(*msk));
    if (msk == NULL) {
        fprintf(stderr, "%s: Cannot allocate mirror mask of size %ld×%ld "
                "for %ld actuators\n", progname, dim, dim, nacts);
        goto error;
    }
    inds = calloc(dim*dim, sizeof(*inds));
    if (inds == NULL) {
        fprintf(stderr, "%s: Cannot allocate array for layout indices\n",
                progname);
        goto error;
    }
    if (tao_layout_mask_instantiate(msk, dim, dim, nacts, inds) == NULL) {
        fprintf(stderr, "%s: Failed to instantiate mirror mask\n",
                progname);
        goto error;
    }
    nacts = tao_indexed_layout_build(inds, msk, dim, dim, orient);
    if (nacts < 1) {
        fprintf(stderr, "%s: Failed to build layout indices\n", progname);
        tao_report_error();
        goto error;
    }

    // Allocate the remote mirror instance.  BMC actuators levels are in the
    // range [0,1].
    rdm = tao_remote_mirror_create(
        name, nbufs, inds, dim, dim, 0.0, +1.0, perms);
    if (rdm == NULL) {
        fprintf(stderr, "%s: Failed to create remote mirror instance\n",
                progname);
        tao_report_error();
        goto error;
    }

    // Run loop (on entry of the loop we own the lock on the remote mirror
    // instance).
    context ctx = { .hdm = &hdm, .lut = lut };
    tao_remote_mirror_operations ops = {
        .on_send = on_send,
        .name = name,
        .debug = debug
    };
    tao_status status = tao_remote_mirror_run_loop(rdm, &ops, &ctx);
    if (status != TAO_OK) {
        tao_report_error();
        goto error;
    }

    // Undo everything.
done:
    if (initlevel >= 2) {
        // Set all actuators to 0.
        rv = BMCClearArray(&hdm);
        if (rv != NO_ERR) {
            fprintf(stderr,
                    "%s: Failed to set all actuators to zero on exit (%s).\n",
                    progname, BMCErrorString(rv));
            exitcode = EXIT_FAILURE;
        }
        initlevel = 1;
    }
    if (initlevel >= 1) {
        // Close the driver.
        rv = BMCClose(&hdm);
        if (rv != NO_ERR) {
            fprintf(stderr,
                    "%s: Failed to close the deformable mirror driver (%s).\n",
                    progname, BMCErrorString(rv));
            exitcode = EXIT_FAILURE;
        }
        initlevel = 0;
    }
    if (rdm != NULL) {
        if (rdm->base.state != TAO_STATE_UNREACHABLE) {
            // FIXME: should lock
            rdm->base.state = TAO_STATE_UNREACHABLE;
        }
        if (rdm->base.command != TAO_COMMAND_NONE) {
            // FIXME: should lock
            rdm->base.command = TAO_COMMAND_NONE;
        }
        tao_remote_mirror_detach(rdm);
    }
    if (lut != NULL) {
        free(lut);
    }
    if (msk != NULL) {
        free(msk);
    }
    if (inds != NULL) {
        free(inds);
    }
    return exitcode;

    // We branch here in case of errors.
error:
    exitcode = EXIT_FAILURE;
    goto done;
}
