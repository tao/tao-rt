// tao-nuvu.h -
//
// Public API for NüVü cameras in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2022, Éric Thiébaut & Anthony Berdeu.

#ifndef TAO_NUVU_H_
#define TAO_NUVU_H_ 1

#include <tao-errors.h>
#include <tao-cameras.h>

TAO_BEGIN_DECLS

/**
 * Open a NüVü camera in TAO.
 *
 * @param dev      Interface to which camera is connected.  The format is
 *                 `"$board.$n.$c"` where `$board` is the name of the board or
 *                 interface, `$c` is the board number (0 for the first one),
 *                 and `$c` is the channel number or `"auto"`.  Known
 *                 board/interface are "edt" for EDT camera link, "gige" for
 *                 GigE interface, "mil" for MIL camera link, "sapera" for
 *                 Sapera camera link, "virtual", or "auto" (this takes no
 *                 board number).
 *
 * @param nbufs    Number of acquisition buffers.
 *
 * @return A pointer to a unified TAO camera instance, `NULL` in case of
 *         failure.
 */
extern tao_camera* tao_nuvu_open_camera(
    const char* dev,
    int nbufs);

/**
 * List connected NüVü cameras.
 *
 * @param output   Output stream.
 *
 * @return A pointer to a unified TAO camera instance, `NULL` in case of
 *         failure.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_nuvu_list_cameras(
    FILE* output);

TAO_END_DECLS

#endif // TAO_NUVU_H_
