// test_readout_mode.c -
//
// Small program to test the readout mode syntax.
//
// Compile with:
//
//     gcc -O -o test_readout_mode test_readout_mode.c -lm
//
// Usage:
//
//     ./test_readout_mode MODE1 MODE2 ...
//
#include <limits.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <ctype.h>

enum Ampli { NOTHING = 0, EM, CONV };

typedef struct {
    enum Ampli ampli;
    int vfreq;
    int hfreq;
} readout_mode;

// Maximum frequency.  This limit is needed to avoid string buffer overflow
// when formatting the readout-mode and to avoid integer overflow when decoding
// the readout-mode.
#define FREQ_MAX INT_MAX

// This macro yields the absolute difference of its arguments (which are
// evaluated twice).
#define ABS_DIFF(a, b) ((a) >= (b) ? (a) - (b) : (b) - (a))

// This function formats the readout-mode in a string that has at most 32
// characters including the final null. The string has the form
// "$AMPLI/$VFREQ/$HFREQ" where "$AMPLI" is the name of the amplifier ("EM" or
// "CONV") while "$VFREQ" and "$HFREQ" are the vertical and horizontal
// frequencies (with units). The returned value is -1 on error, 0 on success.
static int readout_mode_format(
    char* buf,
    const readout_mode* mode)
{
    const char* ampli_name;
    switch (mode->ampli) {
#define CASE(x) case x: ampli_name = #x; break
        CASE(EM);
        CASE(CONV);
#undef CASE
    default:
        return -1;
    }
    int off = sprintf(buf, "%s", ampli_name);
    for (int pass = 1; pass <= 2; ++pass) {
        double freq = (pass == 1 ? mode->vfreq : mode->hfreq);
        if (freq < 0 || freq > FREQ_MAX) {
            return -1;
        }
        if (freq < 1e3) {
            off += sprintf(buf + off, "/%.0f Hz", freq);
        } else if (freq < 1e6) {
            off += sprintf(buf + off, "/%.3f kHz", freq/1e3);
        } else if (freq < 1e9) {
            off += sprintf(buf + off, "/%.3f MHz", freq/1e6);
        } else {
            off += sprintf(buf + off, "/%.3f GHz", freq/1e9);
        }
    }
    return 0;
}

// This function decodes the readout-mode from a string that has the form
// "$AMPLI/$VFREQ/$HFREQ" where "$AMPLI" is the name of the amplifier ("EM" or
// "CONV") while "$VFREQ" and "$HFREQ" are the vertical and horizontal
// frequencies (with units).  The returned value is -1 on error, 0 on success.
static int readout_mode_decode(
    readout_mode* mode,
    const char* str)
{
    int off;
    if (strncmp(str, "EM", 2) == 0) {
        mode->ampli = EM;
        off = 2;
    } else if (strncmp(str, "CONV", 4) == 0) {
        mode->ampli = CONV;
        off = 4;
    } else {
        return -1;
    }
    for (int pass = 1; pass <= 2; ++pass) {
        while (isspace(str[off])) {
            ++off;
        }
        if (str[off] == '/') {
            ++off;
        } else {
            return -1;
        }
        int nbr;
        double val;
        if (sscanf(str + off, "%lf%n", &val, &nbr) != 1) {
            return -1;
        }
        off += nbr;
        while (isspace(str[off])) {
            ++off;
        }
        switch (str[off]) {
        case 'k': val *= 1e3; ++off; break;
        case 'M': val *= 1e6; ++off; break;
        case 'G': val *= 1e9; ++off; break;
        }
        if (str[off] == 'H' && str[off+1] == 'z') {
            off += 2;
        } else {
            return -1;
        }
        if (val < 0 || val > FREQ_MAX) {
            return -1;
        }
        int freq = lround(val);
        if (pass == 1) {
            mode->vfreq = freq;
        } else {
            mode->hfreq = freq;
        }
    }
    while (isspace(str[off])) {
        ++off;
    }
    if (str[off] != '\0') {
        return -1;
    }
    return 0;
}

int
main(int argc, char* argv[])
{
    char buf[64];
    readout_mode mode;
    for (int i = 1; i < argc; ++i) {
        const char* str = argv[i];
        if (readout_mode_decode(&mode, str) == -1) {
            fprintf(stderr, "decode error: \"%s\"\n", str);
            continue;
        }
        fprintf(stdout, "\"%s\" -> ampli = %d, vfreq = %d Hz, hfreq = %d Hz\n",
                str, mode.ampli, mode.vfreq, mode.hfreq);
        if (readout_mode_format(buf, &mode) == -1) {
            fprintf(stderr, "format error: \"%s\"\n", str);
            continue;
        }
        fprintf(stdout, "\"%s\" -> \"%s\"\n\n", str, buf);
    }
    return 0;
}
