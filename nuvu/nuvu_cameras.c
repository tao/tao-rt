// nuvu_cameras.c -
//
// Support for NüVü cameras in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2022-2024, Éric Thiébaut & Anthony Berdeu.

#include <limits.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

#include <nc_driver.h>

#include "tao-errors.h"
#include "tao-nuvu.h"
#include "tao-cameras-private.h"
#include "tao-generic.h"

#define PRT_INFO(...) fprintf(stderr, "(INFO) " __VA_ARGS__)

#ifdef NUVU_DEBUG
#  define PRT_DEBUG(...) fprintf(stderr, "(DEBUG) " __VA_ARGS__)
#else
#  define PRT_DEBUG(...)
#endif

//-----------------------------------------------------------------------------
// Special parameter values.

// The value of a given parameter when not available may be chosen so as to
// indicate that the parameter is not available or as a suitable default value.

// Availability of the following parameters depends on the readout mode.
#define UNAVAILABLE_ANALOG_GAIN        0
#define UNAVAILABLE_ANALOG_OFFSET     -1
#define UNAVAILABLE_BINNING_X          1
#define UNAVAILABLE_BINNING_Y          1
#define UNAVAILABLE_CALIBRATED_EM_GAIN 0
#define UNAVAILABLE_RAW_EM_GAIN        0

// Availability of the following parameters does not depend on the readout
// mode.
#define UNAVAILABLE_EXPOSURE_TIME 0.0
#define UNAVAILABLE_WAITING_TIME  0.0
#define UNAVAILABLE_TEMPERATURE   NAN

//-----------------------------------------------------------------------------
// Management of errors.

#define NUVU_CALL(func, args, on_error)         \
    do {                                        \
        int code_ = func args;                  \
        if (code_ != 0) {                       \
            nuvu_error(#func, code_);           \
            on_error;                           \
        }                                       \
    } while (0)

static void get_error_details(
    int code,
    const char** reason,
    const char** info)
{
}

static void nuvu_error(
    const char* func,
    int code)
{
    tao_store_other_error(func, code, get_error_details);
}

//-----------------------------------------------------------------------------

// Structure describing a NüVü camera.
typedef struct nuvu_camera_ {
    tao_camera base;
    NcCam handle;
    bool open_shutter;///< Open shutter when acquiring?
    bool ctrl_timestamp_available;

    // Indices of named attributes for updating.
    int readout_time_index;
    int waiting_time_index;
    int frame_transfer_duration_index;
} nuvu_camera;

// Private functions and macros to query a named attribute while preserving the
// `const` property of pointers.

// The macro `ENCODE(F,X,...)` encodes read-only and read-write versions of a
// static inline function `F` with arguments `...` and returning expression
// `X`. Argument `F` is of the form `R* pfx` with return type `R` and function
// name prefix `pfx`. The suffixes `_ro` and `rw` are appended to `pfx` to
// generate the names of the inline functions.
#define ENCODE(F,X,...)                                         \
    static inline const F##_ro(const __VA_ARGS__) { return X; } \
    static inline /***/ F##_rw(/***/ __VA_ARGS__) { return X; }

ENCODE(tao_attr_table* nuvu_get_attr_table,
       &cam->base.config.attr_table,
       nuvu_camera* cam);

ENCODE(tao_attr_table* camera_get_attr_table,
       &cam->config.attr_table,
       tao_camera* cam);

ENCODE(tao_attr_table* config_get_attr_table,
       &cfg->attr_table,
       tao_camera_config* cfg);

ENCODE(tao_attr* attr_table_get_entry,
       (void*)tao_attr_table_unsafe_get_entry(tbl, idx),
       tao_attr_table* tbl, long idx);

#undef ENCODE

// `get_attr_table(obj)` yields the table of named attribute for object `obj`
// with the same const or non-const property as `obj`.
#define get_attr_table(obj)                                             \
    _Generic((obj),                                                     \
             const nuvu_camera*:         nuvu_get_attr_table_ro,        \
             /***/ nuvu_camera*:         nuvu_get_attr_table_rw,        \
             const tao_camera*:        camera_get_attr_table_ro,        \
             /***/ tao_camera*:        camera_get_attr_table_rw,        \
             const tao_camera_config*: config_get_attr_table_ro,        \
             /***/ tao_camera_config*: config_get_attr_table_rw)(obj)

#define attr_table_get_entry(tbl, idx)                                  \
    _Generic((tbl),                                                     \
             const tao_attr_table*: attr_table_get_entry_ro,            \
             /***/ tao_attr_table*: attr_table_get_entry_rw)(tbl, idx)

// `get_attr(obj, idx)` yields the named attribute at index `idx` for object
// `obj`. Arguments are not checked.
#define get_attr(obj, idx) \
    attr_table_get_entry(get_attr_table(obj), idx)

// Declaration of private methods and of the table grouping these methods for
// the unified camera API.

static tao_status on_initialize(
    tao_camera* cam,
    void* ptr);

static tao_status on_finalize(
    tao_camera* cam);

static tao_status on_reset(
    tao_camera* cam);

static tao_status on_update_config(
    tao_camera* cam);

static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_start(
    tao_camera* cam);

static tao_status on_stop(
    tao_camera* cam);

static tao_status on_wait_buffer(
    tao_camera* cam,
    tao_acquisition_buffer* buf,
    double secs,
    int drop);

static tao_camera_ops nuvu_ops = {
    .name          = "NüVü Camera",
    .initialize    = on_initialize,
    .finalize      = on_finalize,
    .reset         = on_reset,
    .update_config = on_update_config,
    .check_config  = on_check_config,
    .set_config    = on_set_config,
    .start         = on_start,
    .stop          = on_stop,
    .wait_buffer   = on_wait_buffer
};

// Private function to parse device name.
static int parse_device_name(
    const char* dev,
    int* unit,
    int* channel);

// Private function to format device name.
static const char* format_device_name(
    char* buf,
    int unit,
    int channel);

// Private functions to list cameras.
static tao_status list_cameras(
    FILE* output,
    NcCtrlList list);

// Public function to connect to a NüVü camera.
tao_camera* tao_nuvu_open_camera(
    const char* dev,
    int nbufs)
{
    int unit, channel;
    if (dev == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    if (parse_device_name(dev, &unit, &channel) != 0) {
        tao_store_error(__func__, TAO_BAD_DEVICE);
        return NULL;
    }
    NcCam ncam;
    NUVU_CALL(ncCamOpen,(unit, channel, nbufs, &ncam), return NULL);
    return tao_camera_create(&nuvu_ops, &ncam, sizeof(nuvu_camera));
}

// Public function to list connected NüVü camera.
tao_status tao_nuvu_list_cameras(
    FILE* output)
{
    NcCtrlList list;

    // Get a list of available controllers
    NUVU_CALL(ncControllerListOpen,(&list), return TAO_ERROR);

    // List all the detected cameras.
    tao_status status = list_cameras(output, list);

    // Close the controller list.
    NUVU_CALL(ncControllerListFree,(list),status = TAO_ERROR);
    return status;
}

static int parse_number(const char* str, int* off, int* num)
{
    int rv = -1; // bad until first digit
    int n = 0;
    int i = *off - 1;
    while (true) {
        ++i;
        int d = str[i];
        if ('0' <= d && d <= '9') {
            n = 10*n + (d - '0');
            rv = 0; // ok at least one digit
        } else {
            *off = i;
            *num = n;
            return rv;
        }
    }
}

static int parse_device_name(
    const char* dev,
    int* unit,
    int* channel)
{
    if (dev == NULL) {
        return -1;
    }

    // Parse connection/board type.
    int n, off;
    if (strncmp(dev, "auto", 4) == 0) {
        off = 4;
        *unit = NC_AUTO_DETECT;
        if (dev[off] == 0 || strcmp(dev + off, ".auto") == 0) {
            *channel = NC_AUTO_CHANNEL;
            return 0;
        }
    } else {
        if (strncmp(dev, "edt", 3) == 0) {
            off = 3;
            *unit = EDT_CL;
        } else if (strncmp(dev, "gige", 4) == 0) {
            off = 4;
            *unit = PT_GIGE;
        } else if (strncmp(dev, "mil", 3) == 0) {
            off = 3;
            *unit = MIL_CL;
        } else if (strncmp(dev, "sapera", 6) == 0) {
            off = 6;
            *unit = SAPERA_CL;
        } else if (strncmp(dev, "virtual", 7) == 0) {
            off = 7;
            *unit = VIRTUAL;
        } else {
            return -1;
        }

        // Parse board number.
        if (dev[off] != '.') {
            return -1;
        }
        ++off;
        if (parse_number(dev, &off, &n) != 0 || n < 0 || n >= 0xffff) {
            return -1;
        }
        *unit |= n;
    }

    // Parse channel number.
    if (dev[off] != '.') {
        return -1;
    }
    ++off;
    if (strcmp(dev + off, "auto") == 0) {
        *channel = NC_AUTO_CHANNEL;
    } else {
        if (parse_number(dev, &off, &n) != 0 || n < 0 || n >= 0xffff) {
            return -1;
        }
        *channel = n;
    }
    return 0;
}

static const char* format_device_name(
    char* buf,
    int unit,
    int channel)
{
    if (unit == NC_AUTO_DETECT) {
        if (channel == NC_AUTO_CHANNEL) {
            strcat(buf, "auto");
        } else {
            sprintf(buf, "auto.%d", channel);
        }
    } else {
        const char* name = "unknown";
        int number = (unit & 0x0000ffff);
        switch ((unit & 0xffff0000)) {
        case AUTO_COMM:
            name = "auto";
            break;
        case VIRTUAL:
            name = "virtual";
            break;
        case EDT_CL:
            name = "edt";
            break;
        case PT_GIGE:
            name = "gige";
            break;
        case MIL_CL:
            name = "mil";
            break;
        case SAPERA_CL:
            name = "sapera";
            break;
        }
        if (channel == NC_AUTO_CHANNEL) {
            sprintf(buf, "%s.%d.auto", name, number);
        } else {
            sprintf(buf, "%s.%d.%d", name, number, channel);
        }
    }
    return buf;
}

static tao_status list_cameras(
    FILE* output,
    NcCtrlList list)
{
    int ncams, channel, unit, width, height;
    char model[64];
    char serial[64];
    char type[64];
    char interface[128];
    char device[64];

    // Get number of connected devices.
    NUVU_CALL(ncControllerListGetSize,(list, &ncams), return TAO_ERROR);
    fprintf(output, "Number of connected NüVü cameras: %d\n", ncams);

    // List all connected devices.
    for (int i = 0; i < ncams; ++i) {
        NUVU_CALL(ncControllerListGetPortUnit,
                  (list, i, &unit),
                  return TAO_ERROR);
        NUVU_CALL(ncControllerListGetPortChannel,
                  (list, i, &channel),
                  return TAO_ERROR);
        NUVU_CALL(ncControllerListGetModel,
                  (list, i, model, sizeof(model)),
                  return TAO_ERROR);
        NUVU_CALL(ncControllerListGetSerial,
                  (list, i, serial, sizeof(serial)),
                  return TAO_ERROR);
        NUVU_CALL(ncControllerListGetPortInterface,
                  (list, i, interface, sizeof(interface)),
                  return TAO_ERROR);
        NUVU_CALL(ncControllerListGetDetectorType,
                  (list, i, type, sizeof(type)),
                  return TAO_ERROR);
        NUVU_CALL(ncControllerListGetDetectorSize,
                  (list, i, &width, &height),
                  return TAO_ERROR);

        fprintf(output, "Camera #%d [%s]\n", i,
                format_device_name(device, unit, channel));
        fprintf(output, " ├─ Camera model:   %s\n", model);
        fprintf(output, " ├─ Serial number:  %s\n", serial);
        fprintf(output, " ├─ Port unit:      0x%x\n", (unsigned)unit);
        fprintf(output, " ├─ Port channel:   %d\n", channel);
        fprintf(output, " ├─ Interface name: %s\n", interface);
        fprintf(output, " ├─ Detector type:  %s\n", type);
        fprintf(output, " └─ Detector size:  %d × %d\n", width, height);
    }
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// Declaration of private functions.

static tao_status check_camera_roi(
    nuvu_camera* ncam,
    const tao_camera_roi* roi);

static tao_status do_camera_roi(
    nuvu_camera* ncam,
    const tao_camera_roi* roi);

static tao_status do_exposure(
    nuvu_camera* ncam,
    const tao_camera_config* cfg);

// Yields whether integer type `T` is signed.
#define IS_SIGNED(T)  (((T)(-1)) < ((T)(0)))

// Yields whether a parameter is available.  The `value` argument is only
// relevant if the parameter value is an enumeration or a binning factor.
static inline bool is_available(
    const nuvu_camera* ncam,
    enum Features param,
    int value)
{
    return ncCamParamAvailable(ncam->handle, param, value) == NC_SUCCESS;
}

// Image buffers have pixels of type `NcImage`.  This function yields the
// corresponding encoding.
static inline tao_encoding get_buffer_encoding(void)
{
    return IS_SIGNED(NcImage) ?
        TAO_ENCODING_SIGNED(8*sizeof(NcImage)) :
        TAO_ENCODING_UNSIGNED(8*sizeof(NcImage));
}

// Image buffers have pixels of type `NcImage`.  This function yields the pixel
// type that best corresponds to an image buffer.
static inline tao_eltype get_raw_pixel_type(void)
{
    if (sizeof(NcImage) <= 1) {
        return IS_SIGNED(NcImage) ? TAO_INT8 : TAO_UINT8;
    } else if (sizeof(NcImage) <= 2) {
        return IS_SIGNED(NcImage) ? TAO_INT16 : TAO_UINT16;
    } else if (sizeof(NcImage) <= 4) {
        return IS_SIGNED(NcImage) ? TAO_INT32 : TAO_UINT32;
    } else if (sizeof(NcImage) <= 8) {
        return IS_SIGNED(NcImage) ? TAO_INT64 : TAO_UINT64;
    } else {
        return -1;
    }
}

// Get timestamp of acquired image.
static inline void get_image_timestamp(
    tao_time* t,
    nuvu_camera* ncam,
    NcImage* img)
{
    if (ncam->ctrl_timestamp_available) {
        struct tm date;
        double frac;
        int status;
        if (ncCamGetCtrlTimestamp(
                ncam->handle, img, &date, &frac, &status) != NC_SUCCESS) {
            t->sec = 0;
            t->nsec = 0;
            return;
        }
        t->sec = mktime(&date);
        t->nsec = lround(frac*1e9);
        TAO_TIME_NORMALIZE(tao_time_member, t->sec, t->nsec);
    } else {
        t->sec = 0;
        t->nsec = 0;
    }
}

// Yields whether 2 floating-point values are identical, that they have the
// same value or are both NaN.
static bool inline identical_d(double a, double b)
{
    return isnan(a) ? isnan(b) : a == b;
}

//-----------------------------------------------------------------------------
// Configuration.

typedef struct {
    const char* key; // value name
    int        type; // value type
    unsigned   size; // value size
    tao_status (*init)(nuvu_camera*, int);
    tao_status (*update)(nuvu_camera*, int);
    tao_status (*check)(const nuvu_camera*, const tao_camera_config*, int);
    tao_status (*config)(nuvu_camera*, const tao_camera_config*, int);
} named_attribute;

// Change the shutter mode.  Do nothing if setting shutter mode is not
// available.
static tao_status set_shutter_mode(
    nuvu_camera* ncam,
    enum ShutterMode mode)
{
    if (is_available(ncam, SHUTTER_MODE, (int)mode)) {
        NUVU_CALL(ncCamSetShutterMode,(ncam->handle, mode),
                  return TAO_ERROR);
    }
    return TAO_OK;
}

static tao_status update_shutter_mode(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_BOOLEAN) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    return tao_attr_set_boolean_value(attr, (ncam->open_shutter ? 1 : 0), true);
}

static tao_status check_shutter_mode(
    const nuvu_camera* ncam,
    const tao_camera_config* cfg,
    int idx)
{
    const tao_attr* attr = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_BOOLEAN) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status config_shutter_mode(
    nuvu_camera* ncam,
    const tao_camera_config* cfg,
    int idx)
{
    const tao_attr* src = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(src) != TAO_ATTR_BOOLEAN) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }
    tao_attr* dst = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(dst) != TAO_ATTR_BOOLEAN) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    int mode;
    if (tao_attr_get_boolean_value(src, &mode) != TAO_OK ||
        tao_attr_set_boolean_value(dst, mode, true) != TAO_OK) {
        return TAO_ERROR;
    }
    ncam->open_shutter = mode;
    return TAO_OK;
}

// Analog gain.

static tao_status update_analog_gain(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    int val;
    if (is_available(ncam, ANALOG_GAIN, 0)) {
        int req = 1;
        NUVU_CALL(ncCamGetAnalogGain,(ncam->handle, req, &val),
                  return TAO_ERROR);
    } else {
        val = UNAVAILABLE_ANALOG_GAIN;
    }
    return tao_attr_set_integer_value(attr, val, true);
}

static tao_status check_analog_gain(
    const nuvu_camera* ncam,
    const tao_camera_config* cfg,
    int idx)
{
    const tao_attr* attr = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status config_analog_gain(
    nuvu_camera* ncam,
    const tao_camera_config* cfg,
    int idx)
{
    const tao_attr* src = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(src) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }
    tao_attr* dst = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(dst) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    if (is_available(ncam, ANALOG_GAIN, 0)) {
        int64_t val;
        if (tao_attr_get_integer_value(src, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        int val_min, val_max;
        NUVU_CALL(ncCamGetAnalogGainRange,
                  (ncam->handle, &val_min, &val_max),
                   return TAO_ERROR);
        if (val < val_min) {
            val = val_min;
        }
        if (val > val_max) {
            val = val_max;
        }
        NUVU_CALL(ncCamSetAnalogGain,(ncam->handle, val),
                  return TAO_ERROR);
        if (update_analog_gain(ncam, idx) != TAO_OK) {
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

// Analog offset.

static tao_status update_analog_offset(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    int val;
    if (is_available(ncam, ANALOG_OFFSET, 0)) {
        int req = 1;
        NUVU_CALL(ncCamGetAnalogOffset,(ncam->handle, req, &val),
                  return TAO_ERROR);
    } else {
        val = UNAVAILABLE_ANALOG_OFFSET;
    }
    return tao_attr_set_integer_value(attr, val, true);
}

static tao_status check_analog_offset(
    const nuvu_camera* ncam,
    const tao_camera_config* cfg,
    int idx)
{
    const tao_attr* attr = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status config_analog_offset(
    nuvu_camera* ncam,
    const tao_camera_config* cfg,
    int idx)
{
    const tao_attr* src = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(src) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }
    tao_attr* dst = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(dst) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    if (is_available(ncam, ANALOG_OFFSET, 0)) {
        int64_t val;
        if (tao_attr_get_integer_value(src, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        int val_min, val_max;
        NUVU_CALL(ncCamGetAnalogOffsetRange,
                  (ncam->handle, &val_min, &val_max),
                   return TAO_ERROR);
        if (val < val_min) {
            val = val_min;
        }
        if (val > val_max) {
            val = val_max;
        }
        NUVU_CALL(ncCamSetAnalogOffset,(ncam->handle, val),
                  return TAO_ERROR);
        if (update_analog_offset(ncam, idx) != TAO_OK) {
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

// Raw EM gain.

static tao_status update_raw_em_gain(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    int val;
    if (is_available(ncam, RAW_EM_GAIN, 0)) {
        int req = 1;
        NUVU_CALL(ncCamGetRawEmGain,(ncam->handle, req, &val),
                  return TAO_ERROR);
    } else {
        val = UNAVAILABLE_RAW_EM_GAIN;
    }
    return tao_attr_set_integer_value(attr, val, true);
}

static tao_status check_raw_em_gain(
    const nuvu_camera* ncam,
    const tao_camera_config* cfg,
    int idx)
{
    const tao_attr* attr = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status config_raw_em_gain(
    nuvu_camera* ncam,
    const tao_camera_config* cfg,
    int idx)
{
    const tao_attr* src = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(src) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }
    tao_attr* dst = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(dst) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    if (is_available(ncam, RAW_EM_GAIN, 0)) {
        int64_t val;
        if (tao_attr_get_integer_value(src, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        int val_min, val_max;
        NUVU_CALL(ncCamGetRawEmGainRange,
                  (ncam->handle, &val_min, &val_max),
                   return TAO_ERROR);
        if (val < val_min) {
            val = val_min;
        }
        if (val > val_max) {
            val = val_max;
        }
        NUVU_CALL(ncCamSetRawEmGain,(ncam->handle, val),
                  return TAO_ERROR);
        if (update_raw_em_gain(ncam, idx) != TAO_OK) {
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

// Calibrated EM gain.

static tao_status update_calibrated_em_gain(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    int val;
    if (is_available(ncam, CALIBRATED_EM_GAIN, 0)) {
        int req = 1;
        NUVU_CALL(ncCamGetCalibratedEmGain,(ncam->handle, req, &val),
                  return TAO_ERROR);
    } else {
        val = UNAVAILABLE_CALIBRATED_EM_GAIN;
    }
    return tao_attr_set_integer_value(attr, val, true);
}

static tao_status check_calibrated_em_gain(
    const nuvu_camera* ncam,
    const tao_camera_config* cfg,
    int idx)
{
    const tao_attr* attr = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status config_calibrated_em_gain(
    nuvu_camera* ncam,
    const tao_camera_config* cfg,
    int idx)
{
    const tao_attr* src = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(src) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }
    tao_attr* dst = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(dst) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    if (is_available(ncam, CALIBRATED_EM_GAIN, 0)) {
        int64_t val;
        if (tao_attr_get_integer_value(src, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        int val_min, val_max;
        NUVU_CALL(ncCamGetCalibratedEmGainRange,
                  (ncam->handle, &val_min, &val_max),
                   return TAO_ERROR);
        if (val < val_min) {
            val = val_min;
        }
        if (val > val_max) {
            val = val_max;
        }
        NUVU_CALL(ncCamSetCalibratedEmGain,(ncam->handle, val),
                  return TAO_ERROR);
        if (update_calibrated_em_gain(ncam, idx) != TAO_OK) {
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

// Target detector temperature.

static tao_status update_target_detector_temperature(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_FLOAT) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    double val;
    if (is_available(ncam, TARGET_DETECTOR_TEMP, 0)) {
        int req = 1;
        NUVU_CALL(ncCamGetTargetDetectorTemp,(ncam->handle, req, &val),
                  return TAO_ERROR);
    } else {
        val = UNAVAILABLE_TEMPERATURE;
    }
    PRT_DEBUG("%s\n", __func__);
    return tao_attr_set_float_value(attr, val, true);
}

static tao_status check_target_detector_temperature(
    const nuvu_camera* ncam,
    const tao_camera_config* cfg,
    int idx)
{
    const tao_attr* attr = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_FLOAT) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }
    double val;
    if (tao_attr_get_float_value(attr, &val) != TAO_OK) {
        return TAO_ERROR;
    }
    bool ok;
    if (is_available(ncam, TARGET_DETECTOR_TEMP, 0)) {
        ok = !isnan(val);
    } else {
        ok = isnan(val);
    }
    if (! ok) {
        tao_store_error(__func__, TAO_BAD_TEMPERATURE);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status config_target_detector_temperature(
    nuvu_camera* ncam,
    const tao_camera_config* cfg,
    int idx)
{
    const tao_attr* src = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(src) != TAO_ATTR_FLOAT) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }
    tao_attr* dst = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(dst) != TAO_ATTR_FLOAT) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    double val;
    if (tao_attr_get_float_value(src, &val) != TAO_OK) {
        return TAO_ERROR;
    }
    bool ok;
    if (is_available(ncam, TARGET_DETECTOR_TEMP, 0)) {
        double val_min, val_max;
        NUVU_CALL(ncCamGetTargetDetectorTempRange,
                  (ncam->handle, &val_min, &val_max),
                   return TAO_ERROR);
        ok = !isnan(val);
        if (ok) {
            if (val < val_min) {
                val = val_min;
            }
            if (val > val_max) {
                val = val_max;
            }
            NUVU_CALL(ncCamSetTargetDetectorTemp,(ncam->handle, val),
                      return TAO_ERROR);
            PRT_DEBUG("%s\n", __func__);
            if (tao_attr_set_float_value(dst, val, true) != TAO_OK) {
                return TAO_ERROR;
            }
        }
    } else {
        ok = isnan(val);
    }
    if (! ok) {
        tao_store_error(__func__, TAO_BAD_TEMPERATURE);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status update_detector_temperature(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_FLOAT) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    double val;
    if (is_available(ncam, DETECTOR_TEMP, 0)) {
        NUVU_CALL(ncCamGetDetectorTemp,(ncam->handle, &val),
                  return TAO_ERROR);
    } else {
        val = UNAVAILABLE_TEMPERATURE;
    }
    PRT_DEBUG("%s\n", __func__);
    return tao_attr_set_float_value(attr, val, true);
}

#define ENCODE(func, id)                                                \
    static tao_status update_##func(                                    \
        nuvu_camera* ncam,                                              \
        int idx)                                                        \
    {                                                                   \
        tao_attr* attr = get_attr(ncam, idx);                           \
        if (TAO_ATTR_TYPE(attr) != TAO_ATTR_FLOAT) {                    \
            tao_store_error(__func__, TAO_CORRUPTED);                   \
            return TAO_ERROR;                                           \
        }                                                               \
        double val;                                                     \
        int code = ncCamGetComponentTemp(ncam->handle, id, &val);       \
        if (code != NC_SUCCESS) {                                       \
            if (code != NC_ERROR_CAM_NO_FEATURE) {                      \
                nuvu_error("ncCamGetComponentTemp", code);              \
                return TAO_ERROR;                                       \
            }                                                           \
            val = UNAVAILABLE_TEMPERATURE;                              \
        }                                                               \
        PRT_DEBUG("%s\n", __func__);                                    \
        return tao_attr_set_float_value(attr, val, true);               \
    }

ENCODE(         ccd_temperature, NC_TEMP_CCD);
ENCODE(  controller_temperature, NC_TEMP_CONTROLLER);
ENCODE(power_supply_temperature, NC_TEMP_POWER_SUPPLY);
ENCODE(        fpga_temperature, NC_TEMP_FPGA);
ENCODE(    heatsink_temperature, NC_TEMP_HEATINK);
#undef ENCODE

static tao_status update_overscan_lines(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    int val;
    NUVU_CALL(ncCamGetOverscanLines,(ncam->handle, &val),
              return TAO_ERROR);
    return tao_attr_set_integer_value(attr, val, true);
}

// Updating of the waiting time is automatically done by `do_exposure`.
static tao_status init_waiting_time(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_FLOAT) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    int slow = 0;
    double val;
    NUVU_CALL(ncCamGetWaitingTime,(ncam->handle, slow, &val),
              return TAO_ERROR);
    PRT_DEBUG("%s\n", __func__);
    if (tao_attr_set_float_value(attr, val/1e3 /* ms -> s */, true) != TAO_OK) {
        return TAO_ERROR;
    }
    ncam->waiting_time_index = idx;
    return TAO_OK;
}

// Updating of the readout time is automatically done by `do_exposure`.
static tao_status init_readout_time(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_FLOAT) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    double val;
    NUVU_CALL(ncCamGetReadoutTime,(ncam->handle, &val),
              return TAO_ERROR);
    PRT_DEBUG("%s\n", __func__);
    if (tao_attr_set_float_value(attr, val/1e3 /* ms -> s */, true) != TAO_OK) {
        return TAO_ERROR;
    }
    PRT_DEBUG("%s: set ncam->readout_time_index to %d\n", __func__, idx);
    ncam->readout_time_index = idx;
    return TAO_OK;
}

static tao_status update_frame_latency(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_INTEGER) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    int val;
    NUVU_CALL(ncCamGetFrameLatency,(ncam->handle, &val),
              return TAO_ERROR);
    return tao_attr_set_integer_value(attr, val, true);
}

static tao_status update_frame_transfer_duration(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_FLOAT) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    double val;
    NUVU_CALL(ncCamGetFrameTransferDuration,(ncam->handle, &val),
              return TAO_ERROR);
    PRT_DEBUG("%s\n", __func__);
    if (tao_attr_set_float_value(attr, val/1e3 /* ms -> s */, true) != TAO_OK) {
        return TAO_ERROR;
    }
    ncam->frame_transfer_duration_index = idx;
    return TAO_OK;
}

static tao_status init_detector_type(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_STRING) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    enum DetectorType val;
    NUVU_CALL(ncCamGetDetectorType,(ncam->handle, &val), return TAO_ERROR);
    const char* name;
    switch (val) {
#define CASE(x) case x: name = #x; break
        CASE(CCD60);
	CASE(CCD97);
        CASE(CCD201_20);
	CASE(CCD207_00);
	CASE(CCD207_10);
	CASE(CCD207_40);
        CASE(CCD220);
#undef CASE
    default: name = "UNKNOWN_CCD";
    }
    return tao_attr_set_string_value(attr, name, true);
}

static tao_status init_serial_number(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_STRING) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    char val[32];
    NUVU_CALL(ncCamGetSerialNumber,(ncam->handle, val), return TAO_ERROR);
    return tao_attr_set_string_value(attr, val, true);
}

// Maximum frequency.  This limit is needed to avoid string buffer overflow
// when formatting the readout-mode and to avoid integer overflow when decoding
// the readout-mode.
#define FREQ_MAX INT_MAX

// This macro yields the absolute difference of its arguments (which are
// evaluated twice).
#define ABS_DIFF(a, b) ((a) >= (b) ? (a) - (b) : (b) - (a))

typedef struct {
    enum Ampli ampli;
    int vfreq;
    int hfreq;
} readout_mode;

// This function formats the readout-mode in a string that has at most 32
// characters including the final null. The string has the form
// "$AMPLI/$VFREQ/$HFREQ" where "$AMPLI" is the name of the amplifier ("EM" or
// "CONV") while "$VFREQ" and "$HFREQ" are the vertical and horizontal
// frequencies (with units). The returned value is -1 on error, 0 on success.
static int readout_mode_format(
    char* buf,
    const readout_mode* mode)
{
    const char* ampli_name;
    switch (mode->ampli) {
#define CASE(x) case x: ampli_name = #x; break
        CASE(EM);
        CASE(CONV);
#undef CASE
    default:
        return -1;
    }
    int off = sprintf(buf, "%s", ampli_name);
    for (int pass = 1; pass <= 2; ++pass) {
        double freq = (pass == 1 ? mode->vfreq : mode->hfreq);
        if (freq < 0 || freq > FREQ_MAX) {
            return -1;
        }
        if (freq < 1e3) {
            off += sprintf(buf + off, "/%.0f Hz", freq);
        } else if (freq < 1e6) {
            off += sprintf(buf + off, "/%.3f kHz", freq/1e3);
        } else if (freq < 1e9) {
            off += sprintf(buf + off, "/%.3f MHz", freq/1e6);
        } else {
            off += sprintf(buf + off, "/%.3f GHz", freq/1e9);
        }
    }
    return 0;
}

// This function decodes the readout-mode from a string that has the form
// "$AMPLI/$VFREQ/$HFREQ" where "$AMPLI" is the name of the amplifier ("EM" or
// "CONV") while "$VFREQ" and "$HFREQ" are the vertical and horizontal
// frequencies (with units).  The returned value is -1 on error, 0 on success.
static int readout_mode_decode(
    readout_mode* mode,
    const char* str)
{
    int off;
    if (strncmp(str, "EM", 2) == 0) {
        mode->ampli = EM;
        off = 2;
    } else if (strncmp(str, "CONV", 4) == 0) {
        mode->ampli = CONV;
        off = 4;
    } else {
        return -1;
    }
    for (int pass = 1; pass <= 2; ++pass) {
        while (isspace(str[off])) {
            ++off;
        }
        if (str[off] == '/') {
            ++off;
        } else {
            return -1;
        }
        int nbr;
        double val;
        if (sscanf(str + off, "%lf%n", &val, &nbr) != 1) {
            return -1;
        }
        off += nbr;
        while (isspace(str[off])) {
            ++off;
        }
        switch (str[off]) {
        case 'k': val *= 1e3; ++off; break;
        case 'M': val *= 1e6; ++off; break;
        case 'G': val *= 1e9; ++off; break;
        }
        if (str[off] == 'H' && str[off+1] == 'z') {
            off += 2;
        } else {
            return -1;
        }
        if (val < 0 || val > FREQ_MAX) {
            return -1;
        }
        int freq = lround(val);
        if (pass == 1) {
            mode->vfreq = freq;
        } else {
            mode->hfreq = freq;
        }
    }
    while (isspace(str[off])) {
        ++off;
    }
    if (str[off] != '\0') {
        return -1;
    }
    return 0;
}

static tao_status update_readout_mode(
    nuvu_camera* ncam,
    int idx)
{
    tao_attr* attr = get_attr(ncam, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_STRING) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    readout_mode mode;
    char ampli_name[8];
    int nth;
    char buf[32];
    NUVU_CALL(ncCamGetCurrentReadoutMode,
              (ncam->handle, &nth, &mode.ampli, ampli_name,
               &mode.vfreq, &mode.hfreq), return TAO_ERROR);
    if (readout_mode_format(buf, &mode) != 0) {
        tao_store_error(__func__, TAO_BAD_VALUE);
        return TAO_ERROR;
    }
    return tao_attr_set_string_value(attr, buf, true);
}

static tao_status check_readout_mode(
    const nuvu_camera* ncam,
    const tao_camera_config* cfg,
    int idx)
{
    const tao_attr* attr = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(attr) != TAO_ATTR_STRING) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }
    readout_mode mode;
    if (readout_mode_decode(&mode, attr->val.s) != 0) {
        tao_store_error(__func__, TAO_BAD_VALUE);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status config_readout_mode(
    nuvu_camera* ncam,
    const tao_camera_config* cfg,
    int idx)
{
    // Decode new readout-mode settings.
    const tao_attr* src = get_attr(cfg, idx);
    if (TAO_ATTR_TYPE(src) != TAO_ATTR_STRING) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return TAO_ERROR;
    }
    readout_mode mode;
    if (readout_mode_decode(&mode, src->val.s) != 0) {
    bad_value:
        tao_store_error(__func__, TAO_BAD_VALUE);
        return TAO_ERROR;
    }

    // Find the nearest approximation in the available modes.  The horizontal
    // frequency has priority over the vertical one.
    enum Ampli ampli_type;
    char ampli_name[8];
    int vfreq, hfreq;
    int best_vfreq_dist = INT_MAX;
    int best_hfreq_dist = INT_MAX;
    int best_mode = -1;
    int nbr;
    NUVU_CALL(ncCamGetNbrReadoutModes,(ncam->handle, &nbr), return TAO_ERROR);
    for (int i = 1; i <= nbr; ++i) {
        NUVU_CALL(ncCamGetReadoutMode,(ncam->handle, i, &ampli_type,
                                       ampli_name, &vfreq, &hfreq),
                  return TAO_ERROR);
        if (ampli_type == mode.ampli) {
            int vfreq_dist = ABS_DIFF(vfreq, mode.vfreq);
            int hfreq_dist = ABS_DIFF(hfreq, mode.hfreq);
            if ((hfreq_dist < best_hfreq_dist) ||
                (hfreq_dist == best_hfreq_dist &&
                 vfreq_dist < best_vfreq_dist)) {
                best_hfreq_dist = hfreq_dist;
                best_vfreq_dist = vfreq_dist;
                best_mode = i;
            }
        }
    }
    if (best_mode == -1) {
        goto bad_value;
    }

    // Set the readout-mode.
    NUVU_CALL(ncCamSetReadoutMode,(ncam->handle, best_mode),
              return TAO_ERROR);

    // Changing the readout-mode may change many other parameters.
    if (on_update_config(&ncam->base) != TAO_OK) {
        return TAO_ERROR;
    }

    return TAO_OK;
}

static named_attribute named_attributes[] = {
    {
        "ShutterMode",
        TAO_ATTR_BOOLEAN,
        sizeof(int),
        NULL,
        update_shutter_mode,
        check_shutter_mode,
        config_shutter_mode,
    },
    {
        "AnalogGain",
        TAO_ATTR_INTEGER,
        sizeof(int64_t),
        NULL,
        update_analog_gain,
        check_analog_gain,
        config_analog_gain,
    },
    {
        "AnalogOffset",
        TAO_ATTR_INTEGER,
        sizeof(int64_t),
        NULL,
        update_analog_offset,
        check_analog_offset,
        config_analog_offset,
    },
    {
        "RawEMGain",
        TAO_ATTR_INTEGER,
        sizeof(int64_t),
        NULL,
        update_raw_em_gain,
        check_raw_em_gain,
        config_raw_em_gain,
    },
    {
        "CalibratedEMGain",
        TAO_ATTR_INTEGER,
        sizeof(int64_t),
        NULL,
        update_calibrated_em_gain,
        check_calibrated_em_gain,
        config_calibrated_em_gain,
    },
    {
        "TargetDetectorTemperature",
        TAO_ATTR_FLOAT,
        sizeof(double),
        NULL,
        update_target_detector_temperature,
        check_target_detector_temperature,
        config_target_detector_temperature,
    },
    {
        "DetectorTemperature",
        TAO_ATTR_FLOAT,
        sizeof(double),
        NULL,
        update_detector_temperature,
        NULL,
        NULL,
    },
    {
        "CCDTemperature",
        TAO_ATTR_FLOAT,
        sizeof(double),
        NULL,
        update_ccd_temperature,
        NULL,
        NULL,
    },
    {
        "ControllerTemperature",
        TAO_ATTR_FLOAT,
        sizeof(double),
        NULL,
        update_controller_temperature,
        NULL,
        NULL,
    },
    {
        "PowerSupplyTemperature",
        TAO_ATTR_FLOAT,
        sizeof(double),
        NULL,
        update_power_supply_temperature,
        NULL,
        NULL,
    },
    {
        "FPGATemperature",
        TAO_ATTR_FLOAT,
        sizeof(double),
        NULL,
        update_fpga_temperature,
        NULL,
        NULL,
    },
    {
        "HeatsinkTemperature",
        TAO_ATTR_FLOAT,
        sizeof(double),
        NULL,
        update_heatsink_temperature,
        NULL,
        NULL,
    },
    {
        "OverscanLines",
        TAO_ATTR_INTEGER,
        sizeof(int64_t),
        NULL,
        update_overscan_lines,
        NULL,
        NULL,
    },
    {
        "DetectorType",
        TAO_ATTR_STRING,
        32,
        init_detector_type,
        NULL,
        NULL,
        NULL,
    },
    {
        "WaitingTime",
        TAO_ATTR_FLOAT,
        sizeof(double),
        init_waiting_time,
        NULL,
        NULL,
        NULL,
    },
    {
        "ReadoutTime",
        TAO_ATTR_FLOAT,
        sizeof(double),
        init_readout_time,
        NULL,
        NULL,
        NULL,
    },
    {
        "FrameLatency",
        TAO_ATTR_INTEGER,
        sizeof(int64_t),
        NULL,
        update_frame_latency,
        NULL,
        NULL,
    },
    {
        "FrameTransferDuration",
        TAO_ATTR_FLOAT,
        sizeof(double),
        NULL,
        update_frame_transfer_duration,
        NULL,
        NULL,
    },
    {
        "DetectorType",
        TAO_ATTR_STRING,
        32,
        init_detector_type,
        NULL,
        NULL,
        NULL,
    },
    {
        "SerialNumber",
        TAO_ATTR_STRING,
        32,
        init_serial_number,
        NULL,
        NULL,
        NULL,
    },
    {
        "ReadoutMode",
        TAO_ATTR_STRING,
        32,
        NULL,
        update_readout_mode,
        check_readout_mode,
        config_readout_mode,
    },
    {NULL, 0, 0, NULL, NULL, NULL, NULL}
};

//-----------------------------------------------------------------------------
// Implementation of generic camera API.

static tao_status on_initialize(
    tao_camera* cam,
    void* ptr)
{
    PRT_DEBUG("%s (\"%s\" %d)\n", __func__, __FILE__, __LINE__);
    // The camera is a NüVü camera.
    nuvu_camera* ncam = (nuvu_camera*)cam;
    ncam->handle = *(NcCam*)ptr;
    ncam->open_shutter = true;

    // Instantiate table of named attributes.
    const long len = sizeof(named_attributes)/sizeof(named_attribute);
    tao_attr_descr defs[len];
    long ndefs = 0;
    for (long k = 0; k < len && named_attributes[k].key != NULL; ++k) {
        PRT_DEBUG("%s: instantiate attribute \"%s\"\n", __func__, named_attributes[k].key);
        defs[k].key = named_attributes[k].key;
        defs[k].type = named_attributes[k].type;
        defs[k].flags = TAO_ATTR_READABLE;
        if (named_attributes[k].update != NULL) {
            defs[k].flags |= TAO_ATTR_VARIABLE;
        }
        if (named_attributes[k].config != NULL) {
            defs[k].flags |= TAO_ATTR_WRITABLE;
        }
        defs[k].size = named_attributes[k].size;
        ++ndefs;
    }
    tao_attr_table* attr_table = get_attr_table(cam);
    if (attr_table == NULL) {
        goto error;
    }
    if (tao_attr_table_instantiate(attr_table, defs, ndefs) != TAO_OK) {
        goto error;
    }

    // Call initializers of named attributes if any.
    for (long k = 0; k < ndefs; ++k) {
        if (named_attributes[k].init != NULL) {
            if (named_attributes[k].init(ncam, k) != TAO_OK) {
                goto error;
            }
        } else if (named_attributes[k].update != NULL) {
            if (named_attributes[k].update(ncam, k) != TAO_OK) {
                goto error;
            }
        }
    }
    PRT_DEBUG("%s: ncam->readout_time_index = %d\n", __func__,
              ncam->readout_time_index);
    PRT_DEBUG("%s: ncam->waiting_time_index = %d\n", __func__,
              ncam->waiting_time_index);
    PRT_DEBUG("%s: ncam->frame_transfer_duration_index = %d\n", __func__,
              ncam->frame_transfer_duration_index);

    // Set Readout mode to the prefered and always valid one.
    NUVU_CALL(ncCamSetReadoutMode,(ncam->handle, 1), goto error);

    // Get detector size.
    int width, height;
    NUVU_CALL(ncCamGetMaxSize,(ncam->handle, &width, &height),
              goto error);
    tao_forced_store(&cam->config.sensorwidth, width);
    tao_forced_store(&cam->config.sensorheight, height);
    PRT_INFO("sensor size: %d × %d\n", width, height);

    // Set initial ROI and retrieve it.
    tao_camera_roi roi = {
        .width = width,
        .height = height,
        .xoff = 0,
        .yoff = 0,
        .xbin = 1,
        .ybin = 1 };
    if (do_camera_roi(ncam, &roi) != TAO_OK) {
        goto error;
    }
    PRT_INFO("roi: %ld × %ld (xoff=%ld, yoff=%ld, xbin=%ld, ybin=%ld)\n",
             cam->config.roi.width, cam->config.roi.width,
             cam->config.roi.xoff, cam->config.roi.yoff,
             cam->config.roi.xbin, cam->config.roi.ybin);

    // Retrieve exposure time and (approximative) frame rate.
    if (do_exposure(ncam, NULL) != TAO_OK) {
        goto error;
    }
    PRT_INFO("exposure time: %.3g µs\n",cam->config.exposuretime * 1e6);
    PRT_INFO("frame rate: %f fps\n", cam->config.framerate);

    // Default number of acquisition buffers.
    cam->config.buffers = 4;

    // Default encodings and pre-processing.
    tao_encoding encoding = get_buffer_encoding();
    cam->config.rawencoding = encoding;
    cam->config.preprocessing = TAO_PREPROCESSING_NONE;
    cam->config.pixeltype = get_raw_pixel_type();

    // Set time-stamp mode.
    ncam->ctrl_timestamp_available = is_available(ncam, CTRL_TIMESTAMP, 0);
    if (ncam->ctrl_timestamp_available) {
        NUVU_CALL(ncCamSetTimestampMode,(ncam->handle, INTERNAL_TIMESTAMP),
                  return TAO_ERROR);
    }

    // Initialization was successful.
    return TAO_OK;

    // Close the device on error.
error:
    (void)ncCamClose(ncam->handle);
    return TAO_ERROR;
}

static tao_status on_finalize(
    tao_camera* cam)
{
    // The camera is a NüVü camera.
    nuvu_camera* ncam = (nuvu_camera*)cam;
    tao_status status = TAO_OK;

    // Abort acquisition.
    NUVU_CALL(ncCamAbort,(ncam->handle),
              status = TAO_ERROR);

    // Close shutter.
    if (set_shutter_mode(ncam, CLOSE) != TAO_OK) {
        status = TAO_ERROR;
    }

    // Wait for camera to be at a safe temperature.
    NUVU_CALL(ncCamPrepareShutdown,(ncam->handle, 1, NULL),
              status = TAO_ERROR);

    // Close camera handle.
    NUVU_CALL(ncCamClose,(ncam->handle),
              status = TAO_ERROR);

    return status;
}

static tao_status on_reset(
    tao_camera* cam)
{
    tao_store_error(__func__, TAO_NOT_YET_IMPLEMENTED);
    return TAO_ERROR;
}

static tao_status on_update_config(
    tao_camera* cam)
{
    // The camera is a NüVü camera.
    nuvu_camera* ncam = (nuvu_camera*)cam;

    // Update ROI.
    if (do_camera_roi(ncam, NULL) != TAO_OK) {
        return TAO_ERROR;
    }

    // Update exposure settings.
    if (do_exposure(ncam, NULL) != TAO_OK) {
        return TAO_ERROR;
    }

    // Update named attributes.
    for (int k = 0; named_attributes[k].key != NULL; ++k) {
        if (named_attributes[k].update != NULL) {
            if (named_attributes[k].update(ncam, k) != TAO_OK) {
                return TAO_ERROR;
            }
        }
    }
    return TAO_OK;
}

static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    // The camera is a NüVü camera.
    nuvu_camera* ncam = (nuvu_camera*)cam;

    // Check ROI.
    if (check_camera_roi(ncam, &cfg->roi) != TAO_OK) {
        return TAO_ERROR;
    }

    // Check raw encoding.
    if (cfg->rawencoding != get_buffer_encoding()) {
        tao_store_error(__func__, TAO_BAD_ENCODING);
        return TAO_ERROR;
    }

    // Check named attributes.
    for (int k = 0; named_attributes[k].key != NULL; ++k) {
        if (named_attributes[k].check == NULL) {
            continue;
        }
        unsigned change = tao_attr_check(
             get_attr(cfg, k), get_attr(ncam, k),
             TAO_ATTR_CHECK_FLAGS|TAO_ATTR_CHECK_TYPE|TAO_ATTR_CHECK_VALUE);
        if (change != 0) {
            if ((change & TAO_ATTR_CHECK_FLAGS) != 0) {
                // Flags are not allowed to change.
                tao_store_error(__func__, TAO_CORRUPTED);
                return TAO_ERROR;
            }
            if (named_attributes[k].check(ncam, cfg, k) != TAO_OK) {
                return TAO_ERROR;
            }
        }
    }

    return TAO_OK;
}

static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    // The camera is a NüVü camera.
    nuvu_camera* ncam = (nuvu_camera*)cam;
    tao_camera_config* cam_cfg = &ncam->base.config;

    // Configure image ROI.
    bool update_exposure = false;
    if (tao_camera_roi_compare(&cam->config.roi, &cfg->roi) != 0) {
        if (do_camera_roi(ncam, &cfg->roi) != TAO_OK) {
            return TAO_ERROR;
        }
        update_exposure = true;
    }

    // Configure exposure time and frame rate.
    if (!identical_d(cam->config.exposuretime, cfg->exposuretime) ||
        !identical_d(cam->config.framerate, cfg->framerate)) {
        if (do_exposure(ncam, cfg) != TAO_OK) {
            return TAO_ERROR;
        }
        update_exposure = false;
    }

    // Update exposure settings which may have changed due to changes in the
    // ROI.
    if (update_exposure) {
        if (do_exposure(ncam, NULL) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Configure pixel type and pre-processing (already checked).
    cam_cfg->pixeltype = cfg->pixeltype;
    cam_cfg->preprocessing = cfg->preprocessing;

    // Configure named attributes.
    for (int k = 0; named_attributes[k].key != NULL; ++k) {
        if (named_attributes[k].config == NULL) {
            continue;
        }
        unsigned change = tao_attr_check(
            get_attr(cfg, k), get_attr(ncam, k),
            TAO_ATTR_CHECK_FLAGS|TAO_ATTR_CHECK_TYPE|TAO_ATTR_CHECK_VALUE);
        if (change != 0 && named_attributes[k].config(ncam, cfg, k) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // So far so good...
    return TAO_OK;
}

// Start image acquisition in continuous mode.
static tao_status on_start(
    tao_camera* cam)
{
    // The camera is a NüVü camera.
    nuvu_camera* ncam = (nuvu_camera*)cam;

    // Open shutter.
    enum ShutterMode mode = (ncam->open_shutter ? OPEN : CLOSE);
    if (set_shutter_mode(ncam, mode) != TAO_OK) {
        return TAO_ERROR;
    }

    // Start the acquisition.
    int nimgs = 0; // number of expected images, 0 for continuous mode
    NUVU_CALL(ncCamStart,(ncam->handle, nimgs), return TAO_ERROR);

    // Flush pending images.
    NUVU_CALL(ncCamFlushReadQueues,(ncam->handle), return TAO_ERROR);

    return TAO_OK;
}

static tao_status on_stop(
    tao_camera* cam)
{
    // The camera is a NüVü camera.
    nuvu_camera* ncam = (nuvu_camera*)cam;

    // Abort acquisition.
    NUVU_CALL(ncCamAbort,(ncam->handle), return TAO_ERROR);

    // Close shutter.
    if (set_shutter_mode(ncam, CLOSE) != TAO_OK) {
        return TAO_ERROR;
    }

    // Flush pending images.
    NUVU_CALL(ncCamFlushReadQueues,(ncam->handle), return TAO_ERROR);

    return TAO_OK;
}

static tao_status on_wait_buffer(
    tao_camera* cam,
    tao_acquisition_buffer* buf,
    double secs,
    int drop)
{
    // The camera is a NüVü camera.
    nuvu_camera* ncam = (nuvu_camera*)cam;

    // Deal with pending image buffers.  If `drop < 1`, the first (and oldest)
    // pending buffer is delivered; if `drop = 1`, the last (and newest)
    // pending buffer is delivered and all other pending buffers are released;
    // if `drop > 1`, all pending buffers are released and only a freshly
    // acquired buffer can be returned.
    NcImage* img = NULL;
    while (true) {
        // When repeatedly calling `ncCamReadChronologicalNonBlocking`, the
        // last image successfully read is the last one.
        NcImage* oldest;
        int code = ncCamReadChronologicalNonBlocking(
            ncam->handle, &oldest, NULL);
        if (code == NC_SUCCESS) {
            img = oldest;
            if (drop < 1) {
                // The oldest pending image is delivered.
                break;
            }
        } else if (code == NC_ERROR_GRAB_NO_IMAGE) {
            // No more pending images.
            if (drop > 1) {
                // Any pending image is discarded.
                img = NULL;
            }
            break;
        } else {
            // Some unexpected error occured.
            nuvu_error("ncCamReadChronologicalNonBlocking", code);
            return TAO_ERROR;
        }
    }

    // If there are no pending acquisition buffers, the function waits for
    // a new buffer to be acquired (but no longer than the given timeout).
    if (img == NULL) {
        // Set the timeout.
        if (secs <= 0) {
            return TAO_TIMEOUT;
        }
        int ms = (secs*1000 < INT_MAX + 0.5
                  ? lround(secs*1000) // s -> ms
                  : -1);              // wait forever
        NUVU_CALL(ncCamSetTimeout, (ncam->handle, ms), return TAO_ERROR);

        // Wait for next image to be available.
        int code = ncCamReadChronological(ncam->handle, &img, NULL);
        if (code == NC_ERROR_GRAB_TIMEOUT) {
            return TAO_TIMEOUT;
        } else if (code != NC_SUCCESS) {
            // Some unexpected error occured.
            nuvu_error("ncCamReadChronological", code);
            return TAO_ERROR;
        }
    }

    // Instantiate the acquisition buffer.
    int width, height;
    NUVU_CALL(ncCamGetSize,(ncam->handle, &width, &height),return TAO_ERROR);
    buf->data = img;
    buf->offset = 0;
    buf->width = width;
    buf->height = height;
    buf->stride = width*sizeof(NcImage);
    buf->size = height*buf->stride;
    buf->encoding = get_buffer_encoding();
    buf->serial = ++cam->config.frames;
    buf->frame_start = TAO_UNKNOWN_TIME;
    get_image_timestamp(&buf->frame_end, ncam, img);
    buf->buffer_ready = TAO_UNKNOWN_TIME;
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// Management of the camera ROI.

static tao_status check_camera_roi(
    nuvu_camera* ncam,
    const tao_camera_roi* roi)
{
    // Validity of ROI for sensor size has already been asserted, but setting
    // the ROI may not be allowed.
    long sensorwidth = ncam->base.config.sensorwidth;
    long sensorheight = ncam->base.config.sensorheight;
    if (is_available(ncam, ROI, 0)
        ? (roi->xoff < 0 || roi->yoff < 0 ||
           roi->xbin < 1 || roi->ybin < 1 ||
           roi->width < 1 || roi->height < 1 ||
           roi->xoff + roi->xbin*roi->width > sensorwidth ||
           roi->yoff + roi->ybin*roi->height > sensorheight)
        : (roi->xoff != 0 || roi->yoff != 0 ||
           roi->xbin != 1 || roi->ybin != 1 ||
           roi->width != sensorwidth || roi->height != sensorheight)) {
        tao_store_error(__func__, TAO_BAD_ROI);
        return TAO_ERROR;
    }

    // Check the validity of the binning factors.
    if ((roi->xbin != 1 && !is_available(ncam, BINNING_X, roi->xbin)) ||
        (roi->ybin != 1 && !is_available(ncam, BINNING_Y, roi->ybin))) {
        tao_store_error(__func__, TAO_BAD_VALUE);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status do_camera_roi(
    nuvu_camera* ncam,
    const tao_camera_roi* roi)
{
    tao_status status = TAO_OK;
    bool available = is_available(ncam, ROI, 0);
    if (roi != NULL) {
        // Configure camera ROI.
        if (check_camera_roi(ncam, roi) != TAO_OK) {
            return TAO_ERROR;
        }
        if (available) {
            int xoff = roi->xoff;
            int yoff = roi->yoff;
            int width = roi->width*roi->xbin;
            int height = roi->height*roi->ybin;;
            int xbin = roi->xbin;
            int ybin = roi->ybin;
            NUVU_CALL(ncCamSetMRoiPosition,(ncam->handle, 0, xoff, yoff),
                      status = TAO_ERROR);
            NUVU_CALL(ncCamSetMRoiSize,(ncam->handle, 0, width, height),
                      status = TAO_ERROR);
            NUVU_CALL(ncCamMRoiApply,(ncam->handle),
                      status = TAO_ERROR);
            NUVU_CALL(ncCamSetBinningMode,(ncam->handle, xbin, ybin),
                      status = TAO_ERROR);
        }
    }
    // Retrieve actual ROI.
    tao_camera_roi* cam_roi = &ncam->base.config.roi;
    if (available) {
        int xoff, yoff;
        NUVU_CALL(ncCamGetMRoiPosition,(ncam->handle, 0, &xoff, &yoff),
                  status = TAO_ERROR);
        int width, height;
        NUVU_CALL(ncCamGetMRoiSize,(ncam->handle, 0, &width, &height),
                  status = TAO_ERROR);
        int xbin, ybin;
        NUVU_CALL(ncCamGetBinningMode,(ncam->handle, &xbin, &ybin),
                  status = TAO_ERROR);
        cam_roi->xoff = xoff;
        cam_roi->yoff = yoff;
        cam_roi->xbin = xbin;
        cam_roi->ybin = ybin;
        cam_roi->width  = width/xbin;
        cam_roi->height = height/ybin;
    } else {
        cam_roi->xoff = 0;
        cam_roi->yoff = 0;
        cam_roi->xbin = 1;
        cam_roi->ybin = 1;
        cam_roi->width  = ncam->base.config.sensorwidth;
        cam_roi->height = ncam->base.config.sensorheight;
    }
    return status;
}

//-----------------------------------------------------------------------------
// Management of the camera exposure time and frame rate.

static tao_status get_exposure_time(
    nuvu_camera* ncam,
    int req,
    double* ptr)
{
    NUVU_CALL(ncCamGetExposureTime,(ncam->handle, req, ptr),
              return TAO_ERROR);
    *ptr /= 1000; // ms -> s
    return TAO_OK;
}

static tao_status set_exposure_time(
    nuvu_camera* ncam,
    double val)
{
    val *= 1000; // s -> ms
    NUVU_CALL(ncCamSetExposureTime,(ncam->handle, val),
              return TAO_ERROR);
    return TAO_OK;
}

static tao_status get_waiting_time(
    nuvu_camera* ncam,
    int req,
    double* ptr)
{
    NUVU_CALL(ncCamGetWaitingTime,(ncam->handle, req, ptr),
              return TAO_ERROR);
    *ptr /= 1000; // ms -> s
    return TAO_OK;
}

static tao_status set_waiting_time(
    nuvu_camera* ncam,
    double val)
{
    val *= 1000; // s -> ms
    NUVU_CALL(ncCamSetWaitingTime,(ncam->handle, val),
              return TAO_ERROR);
    return TAO_OK;
}

static tao_status get_readout_time(
    nuvu_camera* ncam,
    double* ptr)
{
    NUVU_CALL(ncCamGetReadoutTime,(ncam->handle, ptr),
              return TAO_ERROR);
    *ptr /= 1000; // ms -> s
    return TAO_OK;
}

// Depending on the second argument, this function configure and/or update
// the exposure settings.
static tao_status do_exposure(
    nuvu_camera* ncam,
    const tao_camera_config* cfg)
{
    tao_camera_config* cam_cfg = &ncam->base.config;
    int req = 1;

    // Deal with exposure time.
    double exposure_time;
    if (is_available(ncam, EXPOSURE, 0)) {
        if (cfg != NULL) {
            // Configure exposure time.
            if (set_exposure_time(ncam, cfg->exposuretime) != TAO_OK) {
                return TAO_ERROR;
            }
        }
        // Retrieve actual exposure time.
        if (get_exposure_time(ncam, req, &exposure_time) != TAO_OK) {
            return TAO_ERROR;
        }
    } else {
        exposure_time = UNAVAILABLE_EXPOSURE_TIME;
    }
    cam_cfg->exposuretime = exposure_time;

    // Retrieve readout time.
    double readout_time;
    PRT_DEBUG("%s: ncam->readout_time_index = %ld\n", __func__,
              (long)ncam->readout_time_index);
    if (get_readout_time(ncam, &readout_time) != TAO_OK ||
        tao_attr_set_float_value(get_attr(ncam, ncam->readout_time_index),
                                 readout_time, false) != TAO_OK) {
        return TAO_ERROR;
    }

    // Deal with waiting time.
    double waiting_time, frame_rate;
    if (is_available(ncam, WAITING_TIME, 0)) {
        if (cfg != NULL) {
            // Clamp frame rate to maximum possible value and configure
            // waiting time to achieve this frame rate.
            frame_rate = tao_min(cfg->framerate,
                                 1.0/tao_max(exposure_time, readout_time));
            waiting_time = tao_max(0.0, 1.0/frame_rate - exposure_time);
            if (set_waiting_time(ncam, waiting_time) != TAO_OK) {
                return TAO_ERROR;
            }
        }
        // Retrieve actual waiting time.
        if (get_waiting_time(ncam, req, &waiting_time) != TAO_OK) {
            return TAO_ERROR;
        }
    } else {
        waiting_time = UNAVAILABLE_WAITING_TIME;
    }
    PRT_DEBUG("%s\n", __func__);
    if (tao_attr_set_float_value(get_attr(ncam, ncam->waiting_time_index),
                                 waiting_time, false) != TAO_OK) {
        return TAO_ERROR;
    }

    // Update frame rate.
    frame_rate = 1.0/tao_max(exposure_time + waiting_time, readout_time);
    cam_cfg->framerate = frame_rate;
    return TAO_OK;
}
