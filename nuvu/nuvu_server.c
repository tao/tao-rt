// nuvu_server.c -
//
// Image server for NüVü cameras.
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (c) 2022, Éric Thiébaut & Anthony Berdeu.
// Copyright (c) 2024, Éric Thiébaut.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tao-errors.h"
#include "tao-nuvu.h"
#include "tao-camera-servers.h"
#include "tao-cameras.h"

int main(
    int argc,
    char* argv[])
{
    // Determine program name.
    const char* progname = tao_basename(argv[0]);

    // Parse arguments.
    const char* usage = "Usage: %s [OPTIONS ...] [--] [DEVICE NAME]\n";
    const char* device = NULL;
    const char* name = NULL;
    bool list = false;
    bool debug = false;
    bool fancy = true;
    long nbufs = 5;
    unsigned int perms = 0077;
    int iarg = 0;
    while (++iarg < argc) {
        char dummy;
        if (argv[iarg][0] != '-') {
            // Argument is not an option.
            --iarg;
            break;
        }
        if (argv[iarg][1] == '-' && argv[iarg][2] == '\0') {
            // Argument is last option.
            break;
        }
        if (strcmp(argv[iarg], "-h") == 0
            || strcmp(argv[iarg], "-help") == 0
            || strcmp(argv[iarg], "--help") == 0) {
            printf("\n");
            printf(usage, progname);
            printf("\n");
            printf("Launch a server for a NüVü camera.\n");
            printf("\n");
            printf("Arguments:\n");
            printf("  DEVICE             Device name, e.g. \"edt.0.0\".\n");
            printf("  NAME               Server name.\n");
            printf("\n");
            printf("Options:\n");
            printf("  -list              List available cameras.\n");
            printf("  -nbufs NBUFS       Number of output buffers [%ld].\n",
                   nbufs);
            printf("  -nofancy           Do not use colors nor set window "
                   "title.\n");
            printf("  -perms PERMS       Bitwise mask of permissions [0%o].\n",
                   perms);
            printf("  -debug             Debug mode [%s].\n",
                   (debug ? "true" : "false"));
            printf("  -h, -help, --help  Print this help.\n");
            printf("\n");
            return EXIT_SUCCESS;
        }
        if (strcmp(argv[iarg], "-list") == 0) {
            list = true;
            continue;
        }
        if (strcmp(argv[iarg], "-nbufs") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option %s.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%ld %c", &nbufs, &dummy) != 1
                || nbufs < 2) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option %s.\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-perms") == 0) {
            if (iarg + 1 >= argc) {
                fprintf(stderr, "%s: Missing argument for option %s.\n",
                        progname, argv[iarg]);
                return EXIT_FAILURE;
            }
            if (sscanf(argv[iarg+1], "%i %c", (int*)&perms, &dummy) != 1) {
                fprintf(stderr, "%s: Invalid value \"%s\" for option %s.\n",
                        progname, argv[iarg+1], argv[iarg]);
                return EXIT_FAILURE;
            }
            perms &= 0777;
            ++iarg;
            continue;
        }
        if (strcmp(argv[iarg], "-nofancy") == 0) {
            fancy = false;
            continue;
        }
        if (strcmp(argv[iarg], "-debug") == 0) {
            debug = true;
            continue;
        }
        fprintf(stderr, "%s: Unknown option %s.\n", progname, argv[iarg]);
        return EXIT_FAILURE;
    }
    if (!list) {
        // Get device name.
        if (++iarg >= argc) {
            fprintf(stderr, "%s: Missing device name.\n", progname);
            goto bad_usage;
        }
        device = argv[iarg];
        // Get server name.
        if (++iarg >= argc) {
            fprintf(stderr, "%s: Missing server name.\n", progname);
            goto bad_usage;
        }
        name = argv[iarg];
    }
    if (++iarg < argc) {
        fprintf(stderr, "%s: Too many arguments.\n", progname);
    bad_usage:
        fputc('\n', stderr);
        fprintf(stderr, usage, progname);
        fputc('\n', stderr);
        return EXIT_FAILURE;
    }

    // Code returned on exit.
    int exitcode = EXIT_SUCCESS;

    // Just list available devices if requested to do so and return.
    if (list) {
        if (tao_nuvu_list_cameras(stdout) != TAO_OK) {
            tao_report_error();
            exitcode = EXIT_FAILURE;
        }
        return exitcode;
    }

    // Pre-set addresses of server for cleanup.
    tao_camera_server* srv = NULL;

    // Open camera.
    tao_camera* cam = tao_nuvu_open_camera(device, 4);
    if (cam == NULL) {
        fprintf(stderr, "%s: Failed to open NüVü camera.\n", progname);
        goto error;
    }

    // Create camera server and run it.
    srv = tao_camera_server_create(name, cam, nbufs, perms);
    if (srv == NULL) {
        fprintf(stderr, "%s: Failed to create camera server.\n", progname);
        goto error;
    }
    if (debug) {
        srv->loglevel = TAO_MESG_DEBUG;
    } else {
        srv->loglevel = TAO_MESG_INFO;
    }
    srv->fancy = fancy;
    if (tao_camera_server_run_loop(srv) != TAO_OK) {
        fprintf(stderr, "%s: Failed to run the camera server.\n",
                progname);
        goto error;
    }

done:
    // Destroy the server.
    if (srv != NULL && tao_camera_server_destroy(srv) != TAO_OK) {
        fprintf(stderr, "%s: Failed to destroy camera server.\n", progname);
        exitcode = EXIT_FAILURE;
    }
    // Close the camera device.
    if (cam != NULL && tao_camera_destroy(cam) != TAO_OK) {
        fprintf(stderr, "%s: Failed to close camera.\n", progname);
        exitcode = EXIT_FAILURE;
    }
    if (tao_any_errors(NULL)) {
        tao_report_error();
        exitcode = EXIT_FAILURE;
    }
    return exitcode;

error:
    exitcode = EXIT_FAILURE;
    goto done;
}
