# NüVü Cameras in TAO

The TAO library provides support for NüVü cameras in the form of a camera
server.


## Starting the server

The TAO/NüVü camera server is launched by:

```.sh
nuvu_server [$OPTIONS ...] [--] [$DEVICE $NAME]
```

where `$DEVICE` is the interface to which camera is connected and `$NAME` is
the server name.  The interface is specified as `"$board.$n.$c"` where `$board`
is the name of the board or interface, `$c` is the board number (0 for the
first one), and `$c` is the channel number or `auto`.  Known board/interface
are `edt` for EDT camera link, `gige` for GigE interface, `mil` for MIL camera
link, `sapera` for Sapera camera link, `virtual`, or `auto` (this takes no
board number).  For example: `edt.0.0` for the first camera connected on the
first EDT camera link board.

Options `$OPTIONS` are:

- `-list` to list available cameras (no other arguments shall be specified).

- `-nbufs $NBUFS` to specify the number of output buffers.

- `-nofancy` to not use colors nor set window title.

- `-perms $PERMS` to specify permissions for group and other users.

- `-debug` to set debug mode.

- `  -h`, `-help`, or `--help` to print a short help.


## Named Attributes

In addition to generic TAO camera parameters, the TAO/NüVü camera server
provides the following named attributes for the clients:

- `AnalogGain` and `AnalogOffset` are the analog gain and offset.  When not
  available, the analog gain is set to 0 and the analog offset is set to -1.
  This depends on the chosen readout mode.

- `RawEMGain` and `CalibratedEMGain` are the raw and calibrated gains of the
  electron multiplier.  Their values is 0 if not available, this depends on the
  chosen readout mode.  If `CalibratedEMGain` is available, it is to be
  preferred.

- `DetectorTemperature`, `CCDTemperature`, `ControllerTemperature`,
  `FPGATemperature`, `HeatsinkTemperature`, and `PowerSupplyTemperature` are
  read-only parameters giving the temperatures (in °C) of various parts of the
  camera.  When not available, `NaN` value is returned.

- `DetectorType` yields the name of the detector type, e.g. `"CCD60"`.

- `FrameLatency` yields the the number of frames that must be returned before
  valid frames enter the processing or read queues.

- `FrameTransferDuration` yields the frame transfer duration (in seconds).

- `OverscanLines` yields the number of overscan lines.

- `ReadoutTime` yields the readout time (in seconds).

- `SerialNumber` yields the serial number of the camera.

- `ShutterMode` is a boolean indicating whether to open the shutter when
  acquiring images.

- `TargetDetectorTemperature ` is the target temperature of the detector (in
 °C, `NaN` if not available).

- `WaitingTime` yields the waiting time (in seconds, 0.0 if not available).

- `ReadoutMode` is a string of the form `"$AMPLI/$VFREQ/$HFREQ"` where `$AMPLI`
  is the name of the amplifier (`"EM"` or `"CONV"`) while `$VFREQ` and `$HFREQ`
  are the vertical and horizontal frequencies (with units: `Hz`, `kHz`, `MHz`,
  or `GHz`).  For example: `"CONV/10MHz/20MHz"` or `"EM / 5.0 MHz / 10.O MHz"`.
  Spaces are not significant but the string shall be as short as possible (no
  more than 32 characters including the final null).  When choosing a readout
  mode, the criterion is to choose the mode with the closest horizontal
  frequency for a given amplifier type and, in case of tie, the mode with the
  closest vertical frequency is chosen.


## Implementation notes

These notes are to clarify our understanding of the NüVü SDK.

### Reading an image

The functions `ncCamRead`, `ncCamReadChronological`, etc. yields a pointer to
the image buffer.  Image buffers have pixels of type `NcImage`:

```.c
typedef unsigned short NcImage;
```

This fixes the buffer encoding of a NüVü camera to be
`TAO_ENCODING_UNSIGNED(16)`.

The function `ncCamGetSize` yields the size of the image buffers.
The function `ncCamGetOverscanLines` yields the number of overscan lines
appended to the buffer.

When repeatedly calling `ncCamReadChronologicalNonBlocking`, the last image
successfully read is the last one.  For example:

```.c
NcImage* last_image = NULL;
while (true) {
    NcImage* oldest_image;
    int code = ncCamReadChronologicalNonBlocking(cam, &oldest_image, NULL);
    if (code == NC_SUCCESS) {
        last_image = oldest_image;
    } else if (code == NC_ERROR_GRAB_NO_IMAGE) {
        break;
    } else {
        // Some unexpected error occured.
        ...
    }
}
```

### Availability of parameters

The availability of the following features does not depend on the readout mode:
`EXPOSURE`, `WAITING_TIME`, `TRIGGER_MODE`, `SHUTTER_MODE`, `SHUTTER_POLARITY`,
`EXTERNAL_SHUTTER`, `EXTERNAL_SHUTTER_MODE`, `EXTERNAL_SHUTTER_DELAY`,
`FIRE_POLARITY`, `MINIMUM_PULSE_WIDTH`, `ARM_POLARITY`, `TARGET_DETECTOR_TEMP`,
`DETECTOR_TEMP`, `CTRL_TIMESTAMP`, `GPS_PRESENT`, and `GPS_LOCKED`.

The availability of the following features depends on the readout mode:
`CALIBRATED_EM_GAIN`, `RAW_EM_GAIN`, `ANALOG_GAIN`, `ANALOG_OFFSET`,
`BINNING_X`, `BINNING_Y`, `ROI`, `CROP_MODE_X`, and `CROP_MODE_Y`.


## Starting acquisition

```.c
// Open shutter.
int mode = (ncam->open_shutter ? OPEN : CLOSE);
NUVU_CALL(ncCamSetShutterMode,(ncam->handle, mode), return TAO_ERROR);

// Start acquisition.
#if 1 //FIXME: which one is correct?
NUVU_CALL(ncCamStart,(ncam->handle, nimgs), return TAO_ERROR);
#else
NUVU_CALL(ncCamPrepareAcquisition,(ncam->handle, nimgs), return TAO_ERROR);
NUVU_CALL(ncCamBeginAcquisition,(ncam->handle), return TAO_ERROR);
#endif

// Flush pending images.
NUVU_CALL(ncCamFlushReadQueues,(ncam->handle), return TAO_ERROR);
```

## Readout modes

Example of available readout modes:

| Mode | Ampli  | Vert. Freq. | Horiz. Freq. |
|:-----|:-------|------------:|-------------:|
| 1    | 1 (EM) |      10 MHz |       10 MHz |
| 2    | 1 (EM) |       5 MHz |       10 MHz |
| 3    | 1 (EM) |       2 MHz |       10 MHz |
| 4    | 1 (EM) |      10 MHz |       20 MHz |
| 5    | 1 (EM) |       5 MHz |       20 MHz |
| 6    | 1 (EM) |       2 MHz |       20 MHz |
| 7    | 1 (EM) |      10 MHz |        5 MHz |
| 8    | 1 (EM) |       5 MHz |        5 MHz |
| 9    | 1 (EM) |       2 MHz |        5 MHz |
| 10   | 1 (EM) |      10 MHz |        1 MHz |
| 11   | 1 (EM) |       5 MHz |        1 MHz |
| 12   | 1 (EM) |       2 MHz |        1 MHz |
